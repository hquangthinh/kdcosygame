﻿using System;
using Hangfire;
using KinhDo.CosyPhotoGame.JobServer.HangFire;
using log4net;

namespace KinhDo.CosyPhotoGame.JobServer
{
    public class JobServerConsoleHostedApp
    {
        private static readonly ILog Logger = LogManager.GetLogger("JobServerConsoleHostedApp");
        private BackgroundJobServer _backgroundJobServer;

        public void Run()
        {
            Logger.Debug($"Service starting at {DateTime.Now}");

            HangFireJobConfiguration.ConfigureHangfireJob();
            _backgroundJobServer = HangFireJobConfiguration.StartBackgroundJobServer();

            Logger.Debug($"Service started successfully at {DateTime.Now}");
        }

        public void Stop()
        {
            _backgroundJobServer?.Dispose();
        }
    }
}
