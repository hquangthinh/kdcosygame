﻿using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KinhDo.CosyPhotoGame.JobServer
{
    class Program
    {
        static void Main(string[] args)
        {
            // Self-installation
            if (args.Length > 0)
            {
                switch (args[0])
                {
                    case "-install":
                    case "-i":
                        {
                            SafeInstallService();
                            break;
                        }
                    case "-uninstall":
                    case "-u":
                        {
                            SafeUninstallService();
                            break;
                        }
                    case "-console":
                    case "-c":
                        {
                            MainConsole(args);
                            break;
                        }
                }
            }
            else
            {
                ServiceBase.Run(new ServiceBase[]
                {
                    new JobServerWinServiceHost() 
                });
            }
        }

        static void SafeInstallService()
        {
            var pbsService =
                ServiceController.GetServices()
                    .FirstOrDefault(
                        s =>
                            "jobserverwinservicehost".Equals(s.ServiceName.ToLower()) ||
                            "jobserverwinservicehost".Equals(s.DisplayName.ToLower()));
            if (pbsService != null)
            {
                var message =
                    $"Cannot install service jobserverwinservicehost. There's an existing service in system with the same [Name] - {pbsService.ServiceName} and [DisplayName] - {pbsService.DisplayName}. Please remove the existing service and retry installation process.";
                MessageBox.Show(message, @"Service Installation Error", MessageBoxButtons.OK);
                throw new Exception(message);
            }

            try
            {
                ManagedInstallerClass.InstallHelper(new[] { "/LogToConsole=true", System.Reflection.Assembly.GetExecutingAssembly().Location });
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurs when install service jobserverwinservicehost with reason {ex.Message}",
                    @"Service Installation Error", MessageBoxButtons.OK);
                throw;
            }
        }

        static void SafeUninstallService()
        {
            try
            {
                ManagedInstallerClass.InstallHelper(new[] { "/u", "/LogToConsole=true", System.Reflection.Assembly.GetExecutingAssembly().Location });
            }
            catch (Exception ex)
            {
                MessageBox.Show($"An error occurs when uninstall service jobserverwinservicehost with reason {ex.Message}",
                    @"Service Installation Error", MessageBoxButtons.OK);
                throw;
            }

        }

        static void MainConsole(string[] args)
        {
            var consoleHostedApp = new JobServerConsoleHostedApp();
            consoleHostedApp.Run();
            Console.WriteLine(@"Press any key to stop service");
            Console.ReadLine();
            consoleHostedApp.Stop();
        }
    }
}