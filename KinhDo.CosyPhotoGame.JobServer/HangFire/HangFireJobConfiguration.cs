﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hangfire;

namespace KinhDo.CosyPhotoGame.JobServer.HangFire
{
    public static class HangFireJobConfiguration
    {
        public static void ConfigureHangfireJob()
        {
            GlobalConfiguration.Configuration
                .UseSqlServerStorage("Default")
                .UseFilter(new LogFailureAttribute());
        }

        public static BackgroundJobServer StartBackgroundJobServer()
        {
            return new BackgroundJobServer();
        }
    }
}
