﻿using System.Web.Mvc;
using Abp.Web.Mvc.Authorization;

namespace KinhDo.CosyAppAdmin.Web.Controllers
{
    [AbpMvcAuthorize]
    public class HomeController : CosyAppAdminControllerBase
    {
        public ActionResult Index()
        {
            return View("~/App/Main/views/layout/layout.cshtml"); //Layout of the angular application.
        }
	}
}