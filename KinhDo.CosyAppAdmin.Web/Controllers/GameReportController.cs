﻿using System;
using System.ComponentModel;
using System.Text;
using System.Web.Mvc;
using KinhDo.CosyAppAdmin.Players;

namespace KinhDo.CosyAppAdmin.Web.Controllers
{
    public class GameReportController : Controller
    {
        // GET: GameReport
        public ActionResult Index()
        {
            var playerService = new PlayerAppService();
            var gameReport = playerService.ExportAllGameResults();
            var stringBuilder = new StringBuilder();
            // Header
            stringBuilder.AppendFormat("First Name, Last Name, Email, Date, Percent Complete (%), Complete Game");
            stringBuilder.AppendLine();
            // Line Items
            foreach (var reportDto in gameReport)
            {
                stringBuilder.Append(
                    $"{reportDto.FirstName},{reportDto.LastName},{reportDto.Email},{reportDto.ResultDate},{reportDto.PercentComplete},{reportDto.CompleteGame}");
                stringBuilder.AppendLine();
            }
            var fileContentBytes = Encoding.UTF8.GetBytes(stringBuilder.ToString());
            //http://stackoverflow.com/questions/155097/microsoft-excel-mangles-diacritics-in-csv-files
            var resultBytes = new byte[fileContentBytes.LongLength + 3];
            resultBytes[0] = 0xEF;
            resultBytes[1] = 0xBB;
            resultBytes[2] = 0xBF;
            Array.Copy(fileContentBytes, 0, resultBytes, 3, fileContentBytes.LongLength);
            return File(resultBytes, "application/vnd.ms-excel", "Report.csv");
        }
    }
}