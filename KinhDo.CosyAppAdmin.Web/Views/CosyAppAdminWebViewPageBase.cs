﻿using Abp.Web.Mvc.Views;

namespace KinhDo.CosyAppAdmin.Web.Views
{
    public abstract class CosyAppAdminWebViewPageBase : CosyAppAdminWebViewPageBase<dynamic>
    {

    }

    public abstract class CosyAppAdminWebViewPageBase<TModel> : AbpWebViewPage<TModel>
    {
        protected CosyAppAdminWebViewPageBase()
        {
            LocalizationSourceName = CosyAppAdminConsts.LocalizationSourceName;
        }
    }
}