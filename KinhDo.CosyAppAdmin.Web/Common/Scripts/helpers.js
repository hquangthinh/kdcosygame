﻿var App = App || {};
(function () {

    var appLocalizationSource = abp.localization.getSource('CosyAppAdmin');
    App.localize = function () {
        return appLocalizationSource.apply(this, arguments);
    };

})(App);