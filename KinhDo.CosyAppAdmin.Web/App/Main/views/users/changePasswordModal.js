﻿(function(abp, angular) {
    angular.module('app')
        .controller('app.views.users.changePasswordModal',
        [
            '$scope', '$modalInstance', 'accountService', 'userModel',
            function ($scope, $modalInstance, accountService, userModel) {

                var vm = this;

                vm.user = {
                    userName: userModel.userName,
                    password: '',
                    passwordConfirm: ''
                };

                vm.save = function() {
                    accountService.updateUserPassword(vm.user)
                        .success(function() {
                            abp.notify.info(App.localize('SavedSuccessfully'));
                            $modalInstance.close();
                        });
                };

                vm.cancel = function() {
                    $modalInstance.dismiss();
                };
            }
        ]);
})((abp || (abp = {})), (angular || undefined));