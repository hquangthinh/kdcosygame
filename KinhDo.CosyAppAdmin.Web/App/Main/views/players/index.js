﻿(function() {
    angular.module('app').controller('app.views.players.index', [
        '$scope', '$state', 'playerService',
        function ($scope, $state, playerService) {

            var vm = this;
            vm.isSearchPanelCollapsed = false;
            vm.searchParams = {
                pageNumber: 1,
                pageSize: 10
            };
            vm.model = {};

            function getPlayers(searchPlayerCommand) {
                playerService.searchPlayers(searchPlayerCommand || {})
                    .then(function (result) {
                        vm.model = result.data;
                        vm.searchParams.pageNumber = vm.model.pageNumber;
                    });
            }

            vm.toggleSearchPanel = function toggleSearchPanel() {
                vm.isSearchPanelCollapsed = !vm.isSearchPanelCollapsed;
            };

            vm.search = function() {
                getPlayers(vm.searchParams);
            }

            vm.reset = function () {
                vm.searchParams = {
                    pageNumber: 1,
                    pageSize: 10
                };
                getPlayers(vm.searchParams);
            };

            vm.viewDetail = function(playerModel) {
                $state.go('player-detail', { playerId: playerModel.id });
            };

            vm.pageChanged = function () {
                getPlayers(vm.searchParams);
            };

            getPlayers(vm.searchParams);

        }
    ]);
})();