﻿(function () {
    angular.module('app').controller('PlayerDetailDetailCtrl', [
        '$scope', '$state', 'playerService',
        function ($scope, $state, playerService) {

            var ctrl = this;
            ctrl.model = {};

            function getPlayerDetail() {
                playerService.getPlayerDetail({
                        Id: $state.params.playerId
                    })
                    .then(function (okResponse) {
                            ctrl.model = okResponse.data;
                        },
                        function(errorResponse) {
                            console.log(errorResponse);
                        });
            };

            ctrl.cancel = function() {
                $state.go('players');
            };

            ctrl.viewDetail = function () {
                if (ctrl.model && ctrl.model.latestVideoUrl) {
                    window.open(ctrl.model.latestVideoUrl, '_blank');
                }
            }

            getPlayerDetail();

        }
    ]);
})();