﻿(function(abp, angular) {
    angular.module('app')
        .service('accountService',
        [
            '$http', function ($http) {

                var service = this;

                service.updateUserPassword = updateUserPassword;

                function updateUserPassword(updatePasswordModel) {
                    return $http({
                        url: abp.appPath + 'api/Account/UpdatePassword',
                        method: 'POST',
                        data: JSON.stringify(updatePasswordModel)
                    });
                }
            }
        ]);
})((abp || (abp = {})), (angular || undefined));