﻿(function (abp, angular) {
    angular.module('app')
        .service('playerService',
        [
            '$http', function ($http) {

                var service = this;

                service.searchPlayers = searchPlayers;
                service.getPlayerDetail = getPlayerDetail;

                function searchPlayers(searchPlayerCommand) {
                    return $http({
                        url: abp.appPath + 'api/services/app/player/SearchPlayers',
                        method: 'POST',
                        data: JSON.stringify(searchPlayerCommand || {})
                    });
                }

                function getPlayerDetail(getPlayerDetailCommand) {
                    return $http({
                        url: abp.appPath + 'api/services/app/player/GetPlayerDetail',
                        method: 'POST',
                        data: JSON.stringify(getPlayerDetailCommand || {})
                    });
                }
            }
        ]);
})((abp || (abp = {})), (angular || undefined));