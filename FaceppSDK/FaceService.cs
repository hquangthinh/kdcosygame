﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using log4net;
using System.IO;


namespace FaceppSDK
{
    /// <summary>
    /// Faceplusplus C# SDK
    /// http://faceplusplus.com
    /// </summary>
    public class FaceService : ISerive
    {
        #region Member list
        private ILog logger = LogManager.GetLogger(typeof(FaceService));
        private string app_key;
        private string app_secret;
        public IHttpMethod HttpMethod { get; private set; }
        #endregion
        #region Constructor
        /// <summary>
        /// Create a service class FaceService
        /// </summary>
        /// <param name="app_key">API Key</param>
        /// <param name="app_secret">API Secret</param>
        /// <returns></returns>
        public FaceService(string _app_key, string _app_secret)
        {
            app_key = _app_key;
            app_secret = _app_secret;
            HttpMethod = new HttpMethods();
        }
        /// <summary>
        /// Create a service class FaceService
        /// </summary>
        public FaceService()
        {
        }
        #endregion
        #region detection
        /// <summary>
        /// Sync detect a given image (Image) in all faces (Face) position and corresponding facial attributes
        /// Currently facial attributes include sex (gender), age, (age) and race (race)
        /// If the result is not a member of any face_id faceset / person among the expire after 72 hours is automatically cleared
        /// </summary>
        /// <param name="url">URL of the image to be detected</param>
        /// <param name="mode">Detection mode can be normal (default) or oneface. In oneface mode, the detector only to find the biggest picture of a face.</param>
        /// <param name="attribute">It may be all (default) or none or a comma-delimited list of attributes. Currently supported attributes include: gender, age, race</param>
        /// <param name="tag">Photos can be detected in the Face to specify each one a 255-byte string as a tag, tag information can be / info / get_face inquiry</param>
        /// <returns>Returns a Detect Result, For details, see Face Entity.cs Code</returns>
        public DetectResult Detection_Detect(string url, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"url", url}
            };
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<DetectResult>(Constants.URL_DETECT, dictionary);
        }
        /// <summary>
        /// Asynchronous detect a given image (Image) in all faces (Face) position and corresponding facial attributes
        /// Currently face attributes including sex (gender), age, (age) and race (race)
        /// If the result is not a member of any face_id faceset / person among the expire after 72 hours is automatically cleared
        /// </summary>
        /// <param name="url">URL of the image to be detected</param>
        /// <param name="mode">Detection mode can be normal (default) or oneface. In oneface mode, the detector only to find the biggest picture of a face.</param>
        /// <param name="attribute">It may be all (default) or none or a comma-delimited list of attributes. Currently supported attributes include: gender, age, race</param>
        /// <param name="tag">Photos can be detected in the Face to specify each one a 255-byte string as a tag, tag information can be / info / get_face inquiry</param>
        /// <returns>Returns a DetectAsyncResult, contains a string session_id</returns>
        public AsyncResult Detection_Detect_Async(string url, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"url", url}
            };
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            dictionary.Add("async", "true");
            return HttpGet<AsyncResult>(Constants.URL_DETECT, dictionary);
        }
        /// <summary>
        /// Sync detect a given image (Image) in all faces (Face) position and corresponding facial attributes
        /// Currently face attributes including sex (gender), age, (age) and race (race)
        /// If the result is not a member of any face_id faceset / person among the expire after 72 hours is automatically cleared
        /// </summary>
        /// <param name="filepath">Image file path to be detected</param>
        /// <param name="mode">Detection mode can be normal (default) or oneface. In oneface mode, the detector only to find the biggest picture of a face.</param>
        /// <param name="attribute">It may be all (default) or none or a comma-delimited list of attributes. Currently supported attributes include: gender, age, race</param>
        /// <param name="tag">Photos can be detected in the Face to specify each one a 255-byte string as a tag, tag information can be / info / get_face inquiry</param>
        /// <returns>Returns a Detect Result, For details, see Face Entity.cs Code</returns>
        public DetectResult Detection_DetectImg(string filepath, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object> { };
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpPost<DetectResult>(Constants.URL_DETECT, dictionary, byData);
        }
        /// <summary>
        /// Sync detect a given image (Image) in all faces (Face) position and corresponding facial attributes
        /// Currently face attributes including sex (gender), age, (age) and race (race)
        /// If the result is not a member of any face_id faceset / person among the expire after 72 hours is automatically cleared
        /// </summary>
        /// <param name="filepath">Image file path to be detected</param>
        /// <param name="mode">Detection mode can be normal (default) or oneface. In oneface mode, the detector only to find the biggest picture of a face.</param>
        /// <param name="attribute">It may be all (default) or none or a comma-delimited list of attributes. Currently supported attributes include: gender, age, race</param>
        /// <param name="tag">Photos can be detected in the Face to specify each one a 255-byte string as a tag, tag information can be / info / get_face inquiry</param>
        /// <returns>Returns a DetectAsyncResult, contains a string session_id</returns>
        public AsyncResult Detection_DetectImg_Async(string filepath, string mode = "", string attribute = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object> { };
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            if (mode != "") dictionary.Add("mode", mode);
            if (attribute != "") dictionary.Add("attribute", attribute);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpPost<AsyncResult>(Constants.URL_DETECT, dictionary, byData);
        }
        #endregion
        #region recognition
        public CompareResult Recognition_Compare(string face_id1, string face_id2)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"face_id1", face_id1},
                {"face_id2", face_id2}
            };
            return HttpGet<CompareResult>(Constants.URL_COMPARE, dictionary);
        }
       
	    public IdentifyResult Recognition_IdentifyUrlById(string group_id, string url = "", string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            if (url != "") dictionary.Add("url", url);
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            return HttpGet<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
        }
        
        public AsyncResult Recognition_IdentifyUrlById_Async(string group_id, string url = "", string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            if (url != "") dictionary.Add("url", url);
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            return HttpGet<AsyncResult>(Constants.URL_RECOGNIZE, dictionary);
        }
        
        public IdentifyResult Recognition_IdentifyUrlByName(string group_name, string url, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (url != "") dictionary.Add("url", url);
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            return HttpGet<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
        }
        
        public IdentifyResult Recognition_IdentifyUrlByName_Async(string group_name, string url, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (url != "") dictionary.Add("url", url);
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            return HttpGet<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
        }
        
        public IdentifyResult Recognition_IdentifyImgById(string group_id, string filepath, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            if (filepath == "") return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary, byData);
        }
        
        public IdentifyResult Recognition_IdentifyImgById_Async(string group_id, string filepath, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            if (filepath == "") return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary, byData);
        }
        
        public IdentifyResult Recognition_IdentifyImgByName(string group_name, string filepath, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            if (filepath == "") return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary, byData);
        }
        
        public IdentifyResult Recognition_IdentifyImgByName_Async(string group_name, string filepath, string key_face_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (key_face_id != "") dictionary.Add("key_face_id", key_face_id);
            if (filepath == "") return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary);
            FileStream fs = new FileStream(filepath, FileMode.Open);
            byte[] byData = new byte[fs.Length];
            fs.Read(byData, 0, byData.Length);
            fs.Close();
            return HttpPost<IdentifyResult>(Constants.URL_RECOGNIZE, dictionary, byData);
        }
        
        public SearchResult Recognition_SearchById(string key_face_id, string faceset_id, int count = 3)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"key_face_id", key_face_id},
                {"faceset_id", faceset_id},
                {"count", count}
            };
            return HttpGet<SearchResult>(Constants.URL_SEARCH, dictionary);
        }
        
        public AsyncResult Recognition_SearchById_Async(string key_face_id, string faceset_id, int count = 3)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"key_face_id", key_face_id},
                {"faceset_id", faceset_id},
                {"count", count}
            };
            dictionary.Add("async", "true");
            return HttpGet<AsyncResult>(Constants.URL_SEARCH, dictionary);
        }
        
        public SearchResult Recognition_SearchByName(string key_face_id, string faceset_name, int count = 50)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"key_face_id", key_face_id},
                {"faceset_name", faceset_name},
                {"count", count}
            };
            return HttpGet<SearchResult>(Constants.URL_SEARCH, dictionary);
        }
        
        public AsyncResult Recognition_SearchByName_Async(string key_face_id, string faceset_name, int count = 50)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"key_face_id", key_face_id},
                {"faceset_name", faceset_name},
                {"count", count}
            };
            return HttpGet<AsyncResult>(Constants.URL_SEARCH, dictionary);
        }
        
        public VerifyResult Recognition_VerifyById(string face_id, string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"face_id", face_id},
                {"person_id", person_id}
            };
            return HttpGet<VerifyResult>(Constants.URL_VERIFY, dictionary);
        }
        
        public VerifyResult Recognition_VerifyByName(string face_id, string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"face_id", face_id},
                {"person_name", person_name}
            };
            return HttpGet<VerifyResult>(Constants.URL_VERIFY, dictionary);
        }
        #endregion
        #region person
        
        public ManageResult Person_AddFaceById(string person_id, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_id", person_id},
                {"face_id", face_id}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_ADDFACE, dictionary);
        }
        
        public ManageResult Person_AddFaceByName(string person_name, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_name", person_name},
                {"face_id", face_id}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_ADDFACE, dictionary);
        }
        
        public PersonBasicInfo Person_Create(string person_name = "", string face_id = "", string tag = "", string group_id = "", string group_name = "")
        {
            var dictonary = new Dictionary<object, object> { };
            if (person_name != "") dictonary.Add("person_name", person_name);
            if (face_id != "") dictonary.Add("face_id", face_id);
            if (tag != "") dictonary.Add("tag", tag);
            if (group_id != "") dictonary.Add("group_id", group_id);
            if (group_name != "") dictonary.Add("group_name", group_name);
            return HttpGet<PersonBasicInfo>(Constants.URL_PERSON_CREATE, dictonary);
        }
        
        public ManageResult Person_DeleteById(string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_id", person_id}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_DELETE, dictionary);
        }
        
        public ManageResult Person_DeleteByName(string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_name", person_name}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_DELETE, dictionary);
        }
        
        public PersonInfo Person_GetInfoById(string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_id", person_id}
            };
            return HttpGet<PersonInfo>(Constants.URL_PERSON_GETINFO, dictionary);
        }
        
        public PersonInfo Person_GetInfoByName(string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_name", person_name}
            };
            return HttpGet<PersonInfo>(Constants.URL_PERSON_GETINFO, dictionary);
        }
        
        public ManageResult Person_RemoveFaceById(string person_id, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_id", person_id},
                {"face_id", face_id}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_REMOVEFACE, dictionary);
        }
       
        public ManageResult Person_RemoveFaceByName(string person_name, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_name", person_name},
                {"face_id", face_id}
            };
            return HttpGet<ManageResult>(Constants.URL_PERSON_REMOVEFACE, dictionary);
        }
        
        public PersonBasicInfo Person_SetInfoById(string person_id, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_id", person_id}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<PersonBasicInfo>(Constants.URL_PERSON_SETINFO, dictionary);
        }
        
        public PersonBasicInfo Person_SetInfoByName(string person_name, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"person_name", person_name}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<PersonBasicInfo>(Constants.URL_PERSON_SETINFO, dictionary);
        }
        #endregion
        #region group
        
        public ManageResult Group_AddPersonByGroupIdPersonId(string group_id, string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id},
                {"person_id", person_id}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_ADDPERSON, dictionary);
        }
        
        public ManageResult Group_AddPersonByGroupIdPersonName(string group_id, string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id},
                {"person_name", person_name}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_ADDPERSON, dictionary);
        }
        
        public ManageResult Group_AddPersonByGroupNamePersonName(string group_name, string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name},
                {"person_name", person_name}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_ADDPERSON, dictionary);
        }
       
        public ManageResult Group_AddPersonByGroupNamePersonId(string group_name, string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name},
                {"person_id", person_id}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_ADDPERSON, dictionary);
        }
                
        public ManageResult Group_AddPerson(string group_id = "", string group_name = "", string person_id = "", string person_name = "")
        {
            var dictionary = new Dictionary<object, object> { };
            ManageResult res = new ManageResult();
            if ((group_id == "") == (group_name == "") || (person_id == "") == (person_name == ""))
            {
                res.success = false;
            }
            else
            {
                if (group_id != "")
                {
                    if (person_id != "") res = Group_AddPersonByGroupIdPersonId(group_id, person_id);
                    else res = Group_AddPersonByGroupIdPersonName(group_id, person_name);
                }
                else
                {
                    if (person_id != "") res = Group_AddPersonByGroupNamePersonId(group_name, person_id);
                    else res = Group_AddPersonByGroupNamePersonName(group_name, person_name);
                }
            }
            return res;
        }
        
        public GroupBasicInfo Group_CreateByIdList(string group_name, string tag = "", string person_id = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (tag != "") dictionary.Add("tag", tag);
            if (person_id != "") dictionary.Add("person_id", person_id);
            return HttpGet<GroupBasicInfo>(Constants.URL_GROUP_CREATE, dictionary);
        }
        
        public GroupBasicInfo Group_CreateByNameList(string group_name, string tag = "", string person_name = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (tag != "") dictionary.Add("tag", tag);
            if (person_name != "") dictionary.Add("person_name", person_name);
            return HttpGet<GroupBasicInfo>(Constants.URL_GROUP_CREATE, dictionary);
        }
        
        public ManageResult Group_DeleteById(string group_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_DELETE, dictionary);
        }
        
        public ManageResult Group_DeleteByName(string group_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_DELETE, dictionary);
        }
       
        public GroupInfo Group_GetInfoById(string group_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            return HttpGet<GroupInfo>(Constants.URL_GROUP_GETINFO, dictionary);
        }
        
        public GroupInfo Group_GetInfoByName(string group_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            return HttpGet<GroupInfo>(Constants.URL_GROUP_GETINFO, dictionary);
        }
        
        public ManageResult Group_RemovePersonByGroupIdPersonId(string group_id, string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id},
                {"person_id", person_id}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_REMOVEPERSON, dictionary);
        }
        
        public ManageResult Group_RemovePersonByGroupIdPersonName(string group_id, string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id},
                {"person_name", person_name}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_REMOVEPERSON, dictionary);
        }
        
        public ManageResult Group_RemovePersonByGroupNamePersonName(string group_name, string person_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name},
                {"person_name", person_name}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_REMOVEPERSON, dictionary);
        }
        
        public ManageResult Group_RemovePersonByGroupNamePersonId(string group_name, string person_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name},
                {"person_id", person_id}
            };
            return HttpGet<ManageResult>(Constants.URL_GROUP_REMOVEPERSON, dictionary);
        }
        
        public ManageResult Group_RemovePerson(string group_id = "", string group_name = "", string person_id = "", string person_name = "")
        {
            var dictionary = new Dictionary<object, object> { };
            ManageResult res = new ManageResult();
            if ((group_id == "") == (group_name == "") || (person_id == "") == (person_name == ""))
            {
                res.success = false;
            }
            else
            {
                if (group_id != "")
                {
                    if (person_id != "") res = Group_RemovePersonByGroupIdPersonId(group_id, person_id);
                    else res = Group_RemovePersonByGroupIdPersonName(group_id, person_name);
                }
                else
                {
                    if (person_id != "") res = Group_RemovePersonByGroupNamePersonId(group_name, person_id);
                    else res = Group_RemovePersonByGroupNamePersonName(group_name, person_name);
                }
            }
            return res;
        }
        
        public GroupBasicInfo Group_SetInfoById(string group_id, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<GroupBasicInfo>(Constants.URL_GROUP_SETINFO, dictionary);
        }
        
        public GroupBasicInfo Group_SetInfoByName(string group_name, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<GroupBasicInfo>(Constants.URL_GROUP_SETINFO, dictionary);
        }
        #endregion
        #region FaceSet
        
        public FaceSetCreateResult FaceSet_Create(string faceset_name = "", string face_id = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>();
            if (faceset_name != "") dictionary.Add("faceset_name", faceset_name);
            if (face_id != "") dictionary.Add("face_id", face_id);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<FaceSetCreateResult>(Constants.URL_FACESET_CREATE, dictionary);
        }
        
        public FaceSetDeleteResult FaseSet_DeleteByName(string faceset_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name} 
            };
            return HttpGet<FaceSetDeleteResult>(Constants.URL_FACESET_DELETE, dictionary);
        }
        
        public FaceSetDeleteResult FaseSet_DeleteById(string faceset_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id} 
            };
            return HttpGet<FaceSetDeleteResult>(Constants.URL_FACESET_DELETE, dictionary);
        }
        
        public FaceSetAddResult FaceSet_AddByName(string faceset_name, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name},
                {"face_id", face_id}
            };
            return HttpGet<FaceSetAddResult>(Constants.URL_FACESET_ADDFACE, dictionary);
        }
        
        public FaceSetAddResult FaceSet_AddById(string faceset_id, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id},
                {"face_id", face_id}
            };
            return HttpGet<FaceSetAddResult>(Constants.URL_FACESET_ADDFACE, dictionary);
        }
        
        public FaceSetRemoveResult FaceSet_RemoveFaceById(string faceset_id, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id},
                {"face_id", face_id}
            };
            return HttpGet<FaceSetRemoveResult>(Constants.URL_FACESET_REMOVEFACE, dictionary);
        }
       
        public FaceSetRemoveResult FaceSet_RemoveFaceByName(string faceset_name, string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name},
                {"face_id", face_id}
            };
            return HttpGet<FaceSetRemoveResult>(Constants.URL_FACESET_REMOVEFACE, dictionary);
        }
        
        public FaceSetSetInfoResult FaceSet_SetInfoById(string faceset_id, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<FaceSetSetInfoResult>(Constants.URL_FACESET_SETINFO, dictionary);
        }
       
        public FaceSetSetInfoResult FaceSet_SetInfoByName(string faceset_name, string name = "", string tag = "")
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name}
            };
            if (name != "") dictionary.Add("name", name);
            if (tag != "") dictionary.Add("tag", tag);
            return HttpGet<FaceSetSetInfoResult>(Constants.URL_FACESET_SETINFO, dictionary);
        }
        
        public FaceSetInfoResult FaceSet_GetInfoById(string faceset_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id}
            };
            return HttpGet<FaceSetInfoResult>(Constants.URL_FACESET_GET_INFO, dictionary);
        }
        
        public FaceSetInfoResult FaceSet_GetInfoByName(string faceset_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name}
            };
            return HttpGet<FaceSetInfoResult>(Constants.URL_FACESET_GET_INFO, dictionary);
        }
        #endregion
        #region train
        
        public AsyncResult Train_VerifyById(string person_id)
        {
            var dictionary = new Dictionary<object, object> {
                {"person_id", person_id}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_VERIFY, dictionary);
        }
        
        public AsyncResult Train_VerifyByName(string person_name)
        {
            var dictionary = new Dictionary<object, object> {
                {"person_name", person_name}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_VERIFY, dictionary);
        }       
        
        public AsyncResult Train_SearchById(string faceset_id)
        {
            var dictionary = new Dictionary<object, object> {
                {"faceset_id", faceset_id}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_SEARCH, dictionary);
        }
       
        public AsyncResult Train_SearchByName(string faceset_name)
        {
            var dictionary = new Dictionary<object, object> {
                {"faceset_name", faceset_name}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_SEARCH, dictionary);
        }
       
        public AsyncResult Train_IdentifyById(string group_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_id", group_id}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_IDENTIFY, dictionary);
        }
        
        public AsyncResult Train_IdentifyByName(string group_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"group_name", group_name}
            };
            return HttpGet<AsyncResult>(Constants.URL_TRAIN_IDENTIFY, dictionary);
        }
        #endregion
        #region grouping
        
        public AsyncResult Grouping_GroupingById(string faceset_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_id", faceset_id}
            };
            return HttpGet<AsyncResult>(Constants.URL_GROUPING_GROUPING, dictionary);
        }
        
        public AsyncResult Grouping_GroupingByName(string faceset_name)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"faceset_name", faceset_name}
            };
            return HttpGet<AsyncResult>(Constants.URL_GROUPING_GROUPING, dictionary);
        }
        #endregion
        #region info
        
        public AppInfo Info_GetApp()
        {
            var dictionary = new Dictionary<object, object> { };
            return HttpGet<AppInfo>(Constants.URL_INFO_GETAPP, dictionary);
        }
        
        public FaceInfoList Info_GetFace(string face_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"face_id", face_id}
            };
            return HttpGet<FaceInfoList>(Constants.URL_INFO_GETFACE, dictionary);
        }
        
        public GroupInfoList Info_GetGroupList()
        {
            var dictionary = new Dictionary<object, object> { };
            return HttpGet<GroupInfoList>(Constants.URL_INFO_GETGROUPLIST, dictionary);
        }
        
        public ImgInfo Info_GetImage(string img_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"img_id", img_id}
            };
            return HttpGet<ImgInfo>(Constants.URL_INFO_GETIMAGE, dictionary);
        }
        
        public PersonInfoList Info_GetPersonList()
        {
            var dictionary = new Dictionary<object, object> { };
            return HttpGet<PersonInfoList>(Constants.URL_INFO_GETPERSONLIST, dictionary);
        }
        
        public QuotaInfo Info_GetQuota()
        {
            var dictionary = new Dictionary<object, object> { };
            return HttpGet<QuotaInfo>(Constants.URL_INFO_GETQUOTA, dictionary);
        }
       
        public DetectSessionInfo Info_GetDetectSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<DetectSessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public CompareSessionInfo Info_GetCompareSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<CompareSessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public RecognizeSessionInfo Info_GetRecognizeSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<RecognizeSessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public SearchSessionInfo Info_GetSearchSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<SearchSessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public TrainSessionInfo Info_GetTrainSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<TrainSessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
       
        public VerifySessionInfo Info_GetVerifySession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<VerifySessionInfo>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public GroupingResult Info_GetGroupingSession(string session_id)
        {
            var dictionary = new Dictionary<object, object>
            {
                {"session_id", session_id}
            };
            return HttpGet<GroupingResult>(Constants.URL_INFO_GETSESSION, dictionary);
        }
        
        public FaceSetListInfo Info_GetFacesetList()
        {
            var dictionary = new Dictionary<object, object>();
            return HttpGet<FaceSetListInfo>(Constants.URL_INFO_GET_FACESETLIST, dictionary);
        }
        #endregion
        #region HTTP请求
        public T HttpGet<T>(string url, IDictionary<object, object> dictionary) where T : class
        {
            dictionary.Add("api_key", app_key);
            dictionary.Add("api_secret", app_secret);
            var query = dictionary.ToQueryString();
            logger.Error(url + "?" + query);
            var json = HttpMethod.HttpGet(url + "?" + query);
            return json.ToEntity<T>("json");
        }

        public string HttpGet(string url, IDictionary<object, object> dictionary)
        {
            dictionary.Add("api_key", app_key);
            dictionary.Add("api_secret", app_secret);
            var query = dictionary.ToQueryString();
            logger.Error(url + "?" + query);
            return HttpMethod.HttpGet(url + "?" + query);
        }
        public T HttpPost<T>(string url, IDictionary<object, object> dictionary) where T : class
        {
            return HttpPost<T>(url, dictionary, null);
        }

        private T HttpPost<T>(string url, IDictionary<object, object> dictionary, byte[] file) where T : class
        {
            dictionary.Add("api_key", app_key);
            dictionary.Add("api_secret", app_secret);
            var query = dictionary.ToQueryString();
            logger.Error(url);
            logger.Error(query);
            var json = string.Empty;
            if (file != null)
            {
                json = HttpMethod.HttpPost(url, dictionary, file);
            }
            else
            {
                json = HttpMethod.HttpPost(url, query);
            }

            return json.ToEntity<T>();
        }

        private string HttpPost(string url, IDictionary<object, object> dictionary)
        {
            dictionary.Add("api_key", app_key);
            dictionary.Add("api_secret", app_secret);
            var query = dictionary.ToQueryString();
            logger.Error(url);
            logger.Error(query);
            return HttpMethod.HttpPost(url, query);
        }
        #endregion
    }
}
