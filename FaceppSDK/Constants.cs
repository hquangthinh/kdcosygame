﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FaceppSDK
{
    class Constants
    {
        public static string FACE_KEY = "2affcadaeddd18f422375adc869f3991";
        public static string FACE_SECRET = "EsU9hmgweuz8U-nwv6s4JP-9AJt64vhz";
	
	    public static string URL_DETECT = "https://apius.faceplusplus.com/v2/detection/detect"; 
	    public static string URL_COMPARE = "https://apius.faceplusplus.com/v2/recognition/compare"; 
	    public static string URL_RECOGNIZE = "https://apius.faceplusplus.com/v2/recognition/recognize"; 
	    public static string URL_SEARCH = "https://apius.faceplusplus.com/v2/recognition/search"; 
	    public static string URL_TRAIN = "https://apius.faceplusplus.com/v2/recognition/train"; 
	    public static string URL_VERIFY = "https://apius.faceplusplus.com/v2/recognition/verify"; 
	
	    public static string URL_PERSON_ADDFACE = "https://apius.faceplusplus.com/v2/person/add_face";
	    public static string URL_PERSON_CREATE = "https://apius.faceplusplus.com/v2/person/create";
	    public static string URL_PERSON_DELETE = "https://apius.faceplusplus.com/v2/person/delete";
	    public static string URL_PERSON_GETINFO = "https://apius.faceplusplus.com/v2/person/get_info";
	    public static string URL_PERSON_REMOVEFACE = "https://apius.faceplusplus.com/v2/person/remove_face";
	    public static string URL_PERSON_SETINFO = "https://apius.faceplusplus.com/v2/person/set_info";
	
	    public static string URL_GROUP_ADDPERSON = "https://apius.faceplusplus.com/v2/group/add_person";
	    public static string URL_GROUP_CREATE = "https://apius.faceplusplus.com/v2/group/create";
	    public static string URL_GROUP_DELETE = "https://apius.faceplusplus.com/v2/group/delete";
	    public static string URL_GROUP_GETINFO = "https://apius.faceplusplus.com/v2/group/get_info";
	    public static string URL_GROUP_REMOVEPERSON = "https://apius.faceplusplus.com/v2/group/remove_person";
	    public static string URL_GROUP_SETINFO = "https://apius.faceplusplus.com/v2/group/set_info";

        public static string URL_INFO_GETAPP = "https://apius.faceplusplus.com/v2/info/get_app";
        public static string URL_INFO_GETFACE = "https://apius.faceplusplus.com/v2/info/get_face";
        public static string URL_INFO_GETGROUPLIST = "https://apius.faceplusplus.com/v2/info/get_group_list";
        public static string URL_INFO_GETIMAGE = "https://apius.faceplusplus.com/v2/info/get_image";
        public static string URL_INFO_GETPERSONLIST = "https://apius.faceplusplus.com/v2/info/get_person_list";
        public static string URL_INFO_GETQUOTA = "https://apius.faceplusplus.com/v2/info/get_quota";
        public static string URL_INFO_GETSESSION = "https://apius.faceplusplus.com/v2/info/get_session";
        public static string URL_INFO_GET_FACESETLIST = "https://apius.faceplusplus.com/v2/info/get_faceset_list";

        public static string URL_FACESET_CREATE = "https://apius.faceplusplus.com/v2/faceset/create";
        public static string URL_FACESET_DELETE = "https://apius.faceplusplus.com/v2/faceset/delete";
        public static string URL_FACESET_ADDFACE = "https://apius.faceplusplus.com/v2/faceset/add_face";
        public static string URL_FACESET_REMOVEFACE = "https://apius.faceplusplus.com/v2/faceset/remove_face";
        public static string URL_FACESET_SETINFO = "https://apius.faceplusplus.com/v2/faceset/set_info";
        public static string URL_FACESET_GET_INFO = "https://apius.faceplusplus.com/v2/faceset/get_info";

        public static string URL_TRAIN_VERIFY = "https://apius.faceplusplus.com/v2/train/verify";
        public static string URL_TRAIN_SEARCH = "https://apius.faceplusplus.com/v2/train/search";
        public static string URL_TRAIN_IDENTIFY = "https://apius.faceplusplus.com/v2/train/identify";

        public static string URL_GROUPING_GROUPING = "https://apius.faceplusplus.com/v2/grouping/grouping";
    }
}
