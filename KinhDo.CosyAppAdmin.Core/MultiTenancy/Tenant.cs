﻿using Abp.MultiTenancy;
using KinhDo.CosyAppAdmin.Users;

namespace KinhDo.CosyAppAdmin.MultiTenancy
{
    public class Tenant : AbpTenant<User>
    {
        public Tenant()
        {
            
        }

        public Tenant(string tenancyName, string name)
            : base(tenancyName, name)
        {
        }
    }
}