﻿using Abp.Authorization;
using KinhDo.CosyAppAdmin.Authorization.Roles;
using KinhDo.CosyAppAdmin.MultiTenancy;
using KinhDo.CosyAppAdmin.Users;

namespace KinhDo.CosyAppAdmin.Authorization
{
    public class PermissionChecker : PermissionChecker<Tenant, Role, User>
    {
        public PermissionChecker(UserManager userManager)
            : base(userManager)
        {

        }
    }
}
