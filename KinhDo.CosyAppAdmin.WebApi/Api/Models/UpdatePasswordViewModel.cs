﻿using System.ComponentModel.DataAnnotations;
using Abp.Auditing;
using Abp.Authorization.Users;
using KinhDo.CosyAppAdmin.Users;

namespace KinhDo.CosyAppAdmin.Api.Models
{
    public class UpdatePasswordViewModel
    {
        [StringLength(AbpUserBase.MaxUserNameLength)]
        public string UserName { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        [DisableAuditing]
        [Required]
        public string Password { get; set; }

        [StringLength(User.MaxPlainPasswordLength)]
        [DisableAuditing]
        [Required]
        public string PasswordConfirm { get; set; }
    }
}
