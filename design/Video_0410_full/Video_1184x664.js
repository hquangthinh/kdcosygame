(function (lib, img, cjs) {

var p; // shortcut to reference prototypes

// library properties:
lib.properties = {
	width: 1180,
	height: 664,
	fps: 24,
	color: "#F9F3DE",
	manifest: [
		{src:"images/_001_USER_329x76.png", id:"_001_USER_329x76"},
		{src:"images/_002_USER_PHOTO_235x235.jpg", id:"_002_USER_PHOTO_235x235"},
		{src:"images/_003_USER_NUMBER_274x105.png", id:"_003_USER_NUMBER_274x105"},
		{src:"images/_004_f5_hinh1_235x235.jpg", id:"_004_f5_hinh1_235x235"},
		{src:"images/_005_f5_hinh2_235x235.jpg", id:"_005_f5_hinh2_235x235"},
		{src:"images/_006_f5_hinh3_235x235.jpg", id:"_006_f5_hinh3_235x235"},
		{src:"images/_007_f5_hinh4_235x235.jpg", id:"_007_f5_hinh4_235x235"},
		{src:"images/_008_f5_hinh5_235x235.jpg", id:"_008_f5_hinh5_235x235"},
		{src:"images/_009_f6_Number_430x134.png", id:"_009_f6_Number_430x134"},
		{src:"images/_010_f9_hinh1_280x280.jpg", id:"_010_f9_hinh1_280x280"},
		{src:"images/_011_f9_hinh2_280x280.jpg", id:"_011_f9_hinh2_280x280"},
		{src:"images/_012_f9_hinh3_280x280.jpg", id:"_012_f9_hinh3_280x280"},
		{src:"images/_013_f11_NUMBER_275x145.png", id:"_013_f11_NUMBER_275x145"},
		{src:"images/_014_f11_KETQUA1_58x33.png", id:"_014_f11_KETQUA1_58x33"},
		{src:"images/_015_f11_KETQUA2_58x33.png", id:"_015_f11_KETQUA2_58x33"},
		{src:"images/_016_f11_KETQUA3_58x33.png", id:"_016_f11_KETQUA3_58x33"},
		{src:"images/_017_f17_NUMBER_223x137.png", id:"_017_f17_NUMBER_223x137"},
		{src:"images/_018_f17_hinh1_235x235.jpg", id:"_018_f17_hinh1_235x235"},
		{src:"images/_019_f17_hinh2_235x235.jpg", id:"_019_f17_hinh2_235x235"},
		{src:"images/_020_f17_hinh3_235x235.jpg", id:"_020_f17_hinh3_235x235"},
		{src:"images/_021_f22_NUMBER_220x160.png", id:"_021_f22_NUMBER_220x160"},
		{src:"images/_022_f22_hinh1_408x283.jpg", id:"_022_f22_hinh1_408x283"},
		{src:"images/_023_f22_hinh2_283x408.jpg", id:"_023_f22_hinh2_283x408"},
		{src:"images/_024_f22_hinh3_408x283.jpg", id:"_024_f22_hinh3_408x283"},
		{src:"images/_025_f27_USER_288x288.jpg", id:"_025_f27_USER_288x288"},
		{src:"images/_026_f28_NUMBER_218x132.png", id:"_026_f28_NUMBER_218x132"},
		{src:"images/_027_f29_hinh1_300x300.jpg", id:"_027_f29_hinh1_300x300"},
		{src:"images/_028_f29_hinh2_300x300.jpg", id:"_028_f29_hinh2_300x300"},
		{src:"images/_029_f29_hinh3_300x300.jpg", id:"_029_f29_hinh3_300x300"},
		{src:"images/_030_f29_hinh4_300x300.jpg", id:"_030_f29_hinh4_300x300"},
		{src:"images/_031_f29_hinh5_300x300.jpg", id:"_031_f29_hinh5_300x300"},
		{src:"images/f10_1.png", id:"f10_1"},
		{src:"images/f10_2.png", id:"f10_2"},
		{src:"images/f10_heart1.png", id:"f10_heart1"},
		{src:"images/f10_heart2.png", id:"f10_heart2"},
		{src:"images/f10_heart3.png", id:"f10_heart3"},
		{src:"images/f10_temp.jpg", id:"f10_temp"},
		{src:"images/f11_1.png", id:"f11_1"},
		{src:"images/f11_2.png", id:"f11_2"},
		{src:"images/f11_21.png", id:"f11_21"},
		{src:"images/f11_22.png", id:"f11_22"},
		{src:"images/f11_23.png", id:"f11_23"},
		{src:"images/f11_24.png", id:"f11_24"},
		{src:"images/f11_25.png", id:"f11_25"},
		{src:"images/f11_icon1.png", id:"f11_icon1"},
		{src:"images/f11_icon2.png", id:"f11_icon2"},
		{src:"images/f11_icon3.png", id:"f11_icon3"},
		{src:"images/f11_line1.png", id:"f11_line1"},
		{src:"images/f11_line2.png", id:"f11_line2"},
		{src:"images/f11_temp.jpg", id:"f11_temp"},
		{src:"images/f11_text.png", id:"f11_text"},
		{src:"images/f16_1.png", id:"f16_1"},
		{src:"images/f16_2.png", id:"f16_2"},
		{src:"images/f16_3.png", id:"f16_3"},
		{src:"images/f16_4.png", id:"f16_4"},
		{src:"images/f16_5.png", id:"f16_5"},
		{src:"images/f16_text1.png", id:"f16_text1"},
		{src:"images/f17_1.png", id:"f17_1"},
		{src:"images/f17_2.png", id:"f17_2"},
		{src:"images/f17_gra1.png", id:"f17_gra1"},
		{src:"images/f17_gra2.png", id:"f17_gra2"},
		{src:"images/f18_BG.jpg", id:"f18_BG"},
		{src:"images/f1_1.png", id:"f1_1"},
		{src:"images/f1_2.png", id:"f1_2"},
		{src:"images/f1_rem1.jpg", id:"f1_rem1"},
		{src:"images/f1_rem2.png", id:"f1_rem2"},
		{src:"images/f22_1.png", id:"f22_1"},
		{src:"images/f22_2.png", id:"f22_2"},
		{src:"images/f22_22.png", id:"f22_22"},
		{src:"images/f22_3.png", id:"f22_3"},
		{src:"images/f22_4.png", id:"f22_4"},
		{src:"images/f22_5.png", id:"f22_5"},
		{src:"images/f22_bg_text0.png", id:"f22_bg_text0"},
		{src:"images/f22_bgFrame.png", id:"f22_bgFrame"},
		{src:"images/f22_btn.png", id:"f22_btn"},
		{src:"images/f22_text0.png", id:"f22_text0"},
		{src:"images/f22_text1.png", id:"f22_text1"},
		{src:"images/f22_text2.png", id:"f22_text2"},
		{src:"images/f22_Transition.png", id:"f22_Transition"},
		{src:"images/f22_Transition0.png", id:"f22_Transition0"},
		{src:"images/f27_1.png", id:"f27_1"},
		{src:"images/f27_2.png", id:"f27_2"},
		{src:"images/f27_USER.jpg", id:"f27_USER"},
		{src:"images/f28_1.png", id:"f28_1"},
		{src:"images/f28_2.png", id:"f28_2"},
		{src:"images/f28_3.png", id:"f28_3"},
		{src:"images/f28_4.png", id:"f28_4"},
		{src:"images/f28_5.png", id:"f28_5"},
		{src:"images/f28_text.png", id:"f28_text"},
		{src:"images/f29_1.png", id:"f29_1"},
		{src:"images/f29_2.png", id:"f29_2"},
		{src:"images/f29_3.png", id:"f29_3"},
		{src:"images/f29_text.png", id:"f29_text"},
		{src:"images/f2_1.png", id:"f2_1"},
		{src:"images/f2_2.png", id:"f2_2"},
		{src:"images/f2_3.png", id:"f2_3"},
		{src:"images/f2_41.png", id:"f2_41"},
		{src:"images/f2_51.png", id:"f2_51"},
		{src:"images/f30_1.png", id:"f30_1"},
		{src:"images/f30_2.png", id:"f30_2"},
		{src:"images/f30_3.png", id:"f30_3"},
		{src:"images/f30_text1.png", id:"f30_text1"},
		{src:"images/f30_text2.png", id:"f30_text2"},
		{src:"images/f32_1.png", id:"f32_1"},
		{src:"images/f32_2.png", id:"f32_2"},
		{src:"images/f32_3.png", id:"f32_3"},
		{src:"images/f32_4.png", id:"f32_4"},
		{src:"images/f32_5.png", id:"f32_5"},
		{src:"images/f32_6.png", id:"f32_6"},
		{src:"images/f32_temp.jpg", id:"f32_temp"},
		{src:"images/f32_text.png", id:"f32_text"},
		{src:"images/f33.png", id:"f33"},
		{src:"images/f3_face1.png", id:"f3_face1"},
		{src:"images/f3_face2.png", id:"f3_face2"},
		{src:"images/f3_face3.png", id:"f3_face3"},
		{src:"images/f3_face4.png", id:"f3_face4"},
		{src:"images/f3_face5.png", id:"f3_face5"},
		{src:"images/f3_face6.png", id:"f3_face6"},
		{src:"images/f3_face7.png", id:"f3_face7"},
		{src:"images/f3_face8.png", id:"f3_face8"},
		{src:"images/f3_face9.png", id:"f3_face9"},
		{src:"images/f3_tia.png", id:"f3_tia"},
		{src:"images/f4_banh1.png", id:"f4_banh1"},
		{src:"images/f4_banh2.png", id:"f4_banh2"},
		{src:"images/f4_Xanh.png", id:"f4_Xanh"},
		{src:"images/f5_BG.png", id:"f5_BG"},
		{src:"images/f5_item1.png", id:"f5_item1"},
		{src:"images/f5_item2.png", id:"f5_item2"},
		{src:"images/f5_item3.png", id:"f5_item3"},
		{src:"images/f5_item4.png", id:"f5_item4"},
		{src:"images/f5_item5.png", id:"f5_item5"},
		{src:"images/f5_item6.png", id:"f5_item6"},
		{src:"images/f5_Temp.png", id:"f5_Temp"},
		{src:"images/f5_text.png", id:"f5_text"},
		{src:"images/f6_1.png", id:"f6_1"},
		{src:"images/f6_2.png", id:"f6_2"},
		{src:"images/f6_BANH.png", id:"f6_BANH"},
		{src:"images/f6_bg.png", id:"f6_bg"},
		{src:"images/f6_item1.png", id:"f6_item1"},
		{src:"images/f6_temp.jpg", id:"f6_temp"},
		{src:"images/f9_1.png", id:"f9_1"},
		{src:"images/f9_2.png", id:"f9_2"},
		{src:"images/f9_3.png", id:"f9_3"},
		{src:"images/f9_4.png", id:"f9_4"},
		{src:"images/f9_5.png", id:"f9_5"},
		{src:"images/f9_temp.jpg", id:"f9_temp"},
		{src:"sounds/test.mp3", id:"test"}
	]
};



// symbols:



(lib._001_USER_329x76 = function() {
	this.initialize(img._001_USER_329x76);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,329,76);


(lib._002_USER_PHOTO_235x235 = function() {
	this.initialize(img._002_USER_PHOTO_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._003_USER_NUMBER_274x105 = function() {
	this.initialize(img._003_USER_NUMBER_274x105);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,274,105);


(lib._004_f5_hinh1_235x235 = function() {
	this.initialize(img._004_f5_hinh1_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._005_f5_hinh2_235x235 = function() {
	this.initialize(img._005_f5_hinh2_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._006_f5_hinh3_235x235 = function() {
	this.initialize(img._006_f5_hinh3_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._007_f5_hinh4_235x235 = function() {
	this.initialize(img._007_f5_hinh4_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._008_f5_hinh5_235x235 = function() {
	this.initialize(img._008_f5_hinh5_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._009_f6_Number_430x134 = function() {
	this.initialize(img._009_f6_Number_430x134);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,430,134);


(lib._010_f9_hinh1_280x280 = function() {
	this.initialize(img._010_f9_hinh1_280x280);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,280);


(lib._011_f9_hinh2_280x280 = function() {
	this.initialize(img._011_f9_hinh2_280x280);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,280);


(lib._012_f9_hinh3_280x280 = function() {
	this.initialize(img._012_f9_hinh3_280x280);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,280,280);


(lib._013_f11_NUMBER_275x145 = function() {
	this.initialize(img._013_f11_NUMBER_275x145);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,275,145);


(lib._014_f11_KETQUA1_58x33 = function() {
	this.initialize(img._014_f11_KETQUA1_58x33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib._015_f11_KETQUA2_58x33 = function() {
	this.initialize(img._015_f11_KETQUA2_58x33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib._016_f11_KETQUA3_58x33 = function() {
	this.initialize(img._016_f11_KETQUA3_58x33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib._017_f17_NUMBER_223x137 = function() {
	this.initialize(img._017_f17_NUMBER_223x137);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,223,137);


(lib._018_f17_hinh1_235x235 = function() {
	this.initialize(img._018_f17_hinh1_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._019_f17_hinh2_235x235 = function() {
	this.initialize(img._019_f17_hinh2_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._020_f17_hinh3_235x235 = function() {
	this.initialize(img._020_f17_hinh3_235x235);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib._021_f22_NUMBER_220x160 = function() {
	this.initialize(img._021_f22_NUMBER_220x160);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,220,160);


(lib._022_f22_hinh1_408x283 = function() {
	this.initialize(img._022_f22_hinh1_408x283);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,408,283);


(lib._023_f22_hinh2_283x408 = function() {
	this.initialize(img._023_f22_hinh2_283x408);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,283,408);


(lib._024_f22_hinh3_408x283 = function() {
	this.initialize(img._024_f22_hinh3_408x283);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,408,283);


(lib._025_f27_USER_288x288 = function() {
	this.initialize(img._025_f27_USER_288x288);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,288,288);


(lib._026_f28_NUMBER_218x132 = function() {
	this.initialize(img._026_f28_NUMBER_218x132);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,218,132);


(lib._027_f29_hinh1_300x300 = function() {
	this.initialize(img._027_f29_hinh1_300x300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib._028_f29_hinh2_300x300 = function() {
	this.initialize(img._028_f29_hinh2_300x300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib._029_f29_hinh3_300x300 = function() {
	this.initialize(img._029_f29_hinh3_300x300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib._030_f29_hinh4_300x300 = function() {
	this.initialize(img._030_f29_hinh4_300x300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib._031_f29_hinh5_300x300 = function() {
	this.initialize(img._031_f29_hinh5_300x300);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,300,300);


(lib.f10_1 = function() {
	this.initialize(img.f10_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,715,163);


(lib.f10_2 = function() {
	this.initialize(img.f10_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,150,149);


(lib.f10_heart1 = function() {
	this.initialize(img.f10_heart1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,102,90);


(lib.f10_heart2 = function() {
	this.initialize(img.f10_heart2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,83,73);


(lib.f10_heart3 = function() {
	this.initialize(img.f10_heart3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,48,45);


(lib.f10_temp = function() {
	this.initialize(img.f10_temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f11_1 = function() {
	this.initialize(img.f11_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,332,220);


(lib.f11_2 = function() {
	this.initialize(img.f11_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,685,298);


(lib.f11_21 = function() {
	this.initialize(img.f11_21);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,352,351);


(lib.f11_22 = function() {
	this.initialize(img.f11_22);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,514,169);


(lib.f11_23 = function() {
	this.initialize(img.f11_23);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,459,199);


(lib.f11_24 = function() {
	this.initialize(img.f11_24);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,422,155);


(lib.f11_25 = function() {
	this.initialize(img.f11_25);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,52,38);


(lib.f11_icon1 = function() {
	this.initialize(img.f11_icon1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,113);


(lib.f11_icon2 = function() {
	this.initialize(img.f11_icon2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,96,90);


(lib.f11_icon3 = function() {
	this.initialize(img.f11_icon3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,117,112);


(lib.f11_line1 = function() {
	this.initialize(img.f11_line1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,138,73);


(lib.f11_line2 = function() {
	this.initialize(img.f11_line2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,81,491);


(lib.f11_temp = function() {
	this.initialize(img.f11_temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f11_text = function() {
	this.initialize(img.f11_text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,421,156);


(lib.f16_1 = function() {
	this.initialize(img.f16_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,138,190);


(lib.f16_2 = function() {
	this.initialize(img.f16_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,255,171);


(lib.f16_3 = function() {
	this.initialize(img.f16_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,116,116);


(lib.f16_4 = function() {
	this.initialize(img.f16_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,143,146);


(lib.f16_5 = function() {
	this.initialize(img.f16_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,178,181);


(lib.f16_text1 = function() {
	this.initialize(img.f16_text1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,900,474);


(lib.f17_1 = function() {
	this.initialize(img.f17_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,708,650);


(lib.f17_2 = function() {
	this.initialize(img.f17_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,291,128);


(lib.f17_gra1 = function() {
	this.initialize(img.f17_gra1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,88,79);


(lib.f17_gra2 = function() {
	this.initialize(img.f17_gra2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,272,256);


(lib.f18_BG = function() {
	this.initialize(img.f18_BG);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f1_1 = function() {
	this.initialize(img.f1_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,441,284);


(lib.f1_2 = function() {
	this.initialize(img.f1_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,849,305);


(lib.f1_rem1 = function() {
	this.initialize(img.f1_rem1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f1_rem2 = function() {
	this.initialize(img.f1_rem2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f22_1 = function() {
	this.initialize(img.f22_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,216,280);


(lib.f22_2 = function() {
	this.initialize(img.f22_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,204,69);


(lib.f22_22 = function() {
	this.initialize(img.f22_22);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,131,111);


(lib.f22_3 = function() {
	this.initialize(img.f22_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,297,200);


(lib.f22_4 = function() {
	this.initialize(img.f22_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,624,214);


(lib.f22_5 = function() {
	this.initialize(img.f22_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,102,254);


(lib.f22_bg_text0 = function() {
	this.initialize(img.f22_bg_text0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,702,438);


(lib.f22_bgFrame = function() {
	this.initialize(img.f22_bgFrame);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,460,328);


(lib.f22_btn = function() {
	this.initialize(img.f22_btn);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,38,38);


(lib.f22_text0 = function() {
	this.initialize(img.f22_text0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,552,226);


(lib.f22_text1 = function() {
	this.initialize(img.f22_text1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,465,55);


(lib.f22_text2 = function() {
	this.initialize(img.f22_text2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,367,101);


(lib.f22_Transition = function() {
	this.initialize(img.f22_Transition);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,383);


(lib.f22_Transition0 = function() {
	this.initialize(img.f22_Transition0);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,507);


(lib.f27_1 = function() {
	this.initialize(img.f27_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,683,124);


(lib.f27_2 = function() {
	this.initialize(img.f27_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,335,443);


(lib.f27_USER = function() {
	this.initialize(img.f27_USER);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,288,288);


(lib.f28_1 = function() {
	this.initialize(img.f28_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,590,258);


(lib.f28_2 = function() {
	this.initialize(img.f28_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,121);


(lib.f28_3 = function() {
	this.initialize(img.f28_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,532,435);


(lib.f28_4 = function() {
	this.initialize(img.f28_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,103,98);


(lib.f28_5 = function() {
	this.initialize(img.f28_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,419,396);


(lib.f28_text = function() {
	this.initialize(img.f28_text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,487,281);


(lib.f29_1 = function() {
	this.initialize(img.f29_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,73,109);


(lib.f29_2 = function() {
	this.initialize(img.f29_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,103,73);


(lib.f29_3 = function() {
	this.initialize(img.f29_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,231,239);


(lib.f29_text = function() {
	this.initialize(img.f29_text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,525,182);


(lib.f2_1 = function() {
	this.initialize(img.f2_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f2_2 = function() {
	this.initialize(img.f2_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,636,269);


(lib.f2_3 = function() {
	this.initialize(img.f2_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,238,78);


(lib.f2_41 = function() {
	this.initialize(img.f2_41);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,273,325);


(lib.f2_51 = function() {
	this.initialize(img.f2_51);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,273,325);


(lib.f30_1 = function() {
	this.initialize(img.f30_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,599,306);


(lib.f30_2 = function() {
	this.initialize(img.f30_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,612,148);


(lib.f30_3 = function() {
	this.initialize(img.f30_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,86,114);


(lib.f30_text1 = function() {
	this.initialize(img.f30_text1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,397,209);


(lib.f30_text2 = function() {
	this.initialize(img.f30_text2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,778,125);


(lib.f32_1 = function() {
	this.initialize(img.f32_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,777,418);


(lib.f32_2 = function() {
	this.initialize(img.f32_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,848,275);


(lib.f32_3 = function() {
	this.initialize(img.f32_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,277,390);


(lib.f32_4 = function() {
	this.initialize(img.f32_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,174,177);


(lib.f32_5 = function() {
	this.initialize(img.f32_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,241,228);


(lib.f32_6 = function() {
	this.initialize(img.f32_6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,170,157);


(lib.f32_temp = function() {
	this.initialize(img.f32_temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f32_text = function() {
	this.initialize(img.f32_text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,591,124);


(lib.f33 = function() {
	this.initialize(img.f33);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,593,426);


(lib.f3_face1 = function() {
	this.initialize(img.f3_face1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face2 = function() {
	this.initialize(img.f3_face2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face3 = function() {
	this.initialize(img.f3_face3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face4 = function() {
	this.initialize(img.f3_face4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face5 = function() {
	this.initialize(img.f3_face5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face6 = function() {
	this.initialize(img.f3_face6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face7 = function() {
	this.initialize(img.f3_face7);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face8 = function() {
	this.initialize(img.f3_face8);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face9 = function() {
	this.initialize(img.f3_face9);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_tia = function() {
	this.initialize(img.f3_tia);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,770,252);


(lib.f4_banh1 = function() {
	this.initialize(img.f4_banh1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,174,327);


(lib.f4_banh2 = function() {
	this.initialize(img.f4_banh2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,159,324);


(lib.f4_Xanh = function() {
	this.initialize(img.f4_Xanh);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,279,133);


(lib.f5_BG = function() {
	this.initialize(img.f5_BG);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f5_item1 = function() {
	this.initialize(img.f5_item1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,93,89);


(lib.f5_item2 = function() {
	this.initialize(img.f5_item2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,70,72);


(lib.f5_item3 = function() {
	this.initialize(img.f5_item3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,102,97);


(lib.f5_item4 = function() {
	this.initialize(img.f5_item4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,283,273);


(lib.f5_item5 = function() {
	this.initialize(img.f5_item5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,204,175);


(lib.f5_item6 = function() {
	this.initialize(img.f5_item6);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,251,376);


(lib.f5_Temp = function() {
	this.initialize(img.f5_Temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f5_text = function() {
	this.initialize(img.f5_text);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,368,168);


(lib.f6_1 = function() {
	this.initialize(img.f6_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,271,117);


(lib.f6_2 = function() {
	this.initialize(img.f6_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,494,159);


(lib.f6_BANH = function() {
	this.initialize(img.f6_BANH);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,551,471);


(lib.f6_bg = function() {
	this.initialize(img.f6_bg);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,780,664);


(lib.f6_item1 = function() {
	this.initialize(img.f6_item1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,57,57);


(lib.f6_temp = function() {
	this.initialize(img.f6_temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f9_1 = function() {
	this.initialize(img.f9_1);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,356,351);


(lib.f9_2 = function() {
	this.initialize(img.f9_2);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,446,429);


(lib.f9_3 = function() {
	this.initialize(img.f9_3);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,156,498);


(lib.f9_4 = function() {
	this.initialize(img.f9_4);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,164,534);


(lib.f9_5 = function() {
	this.initialize(img.f9_5);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,157,379);


(lib.f9_temp = function() {
	this.initialize(img.f9_temp);
}).prototype = p = new cjs.Bitmap();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.Tween47 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_text2();
	this.instance.setTransform(-389,-62.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-389,-62.5,778,125);


(lib.Tween46 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_text2();
	this.instance.setTransform(-389,-62.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-389,-62.5,778,125);


(lib.Tween45 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_1();
	this.instance.setTransform(-36.5,-54.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-36.5,-54.5,73,109);


(lib.Tween44 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_1();
	this.instance.setTransform(-36.5,-54.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-36.5,-54.5,73,109);


(lib.Tween43 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_text();
	this.instance.setTransform(-262.5,-91);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-262.5,-91,525,182);


(lib.Tween42 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_text();
	this.instance.setTransform(-262.5,-91);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-262.5,-91,525,182);


(lib.Tween41 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_text();
	this.instance.setTransform(-262.5,-91);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-262.5,-91,525,182);


(lib.Tween40 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_text();
	this.instance.setTransform(-262.5,-91);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-262.5,-91,525,182);


(lib.Tween39 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_22();
	this.instance.setTransform(-65.5,-55.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-65.5,-55.5,131,111);


(lib.Tween38 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_22();
	this.instance.setTransform(-65.5,-55.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-65.5,-55.5,131,111);


(lib.Tween34 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f18_BG();
	this.instance.setTransform(-590,-332);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-590,-332,1180,664);


(lib.Tween33 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f18_BG();
	this.instance.setTransform(-590,-332);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-590,-332,1180,664);


(lib.Tween29 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f16_text1();
	this.instance.setTransform(-450,-237);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-450,-237,900,474);


(lib.Tween28 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-27,-52.7,0.759,0.759,35);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-58.7,-52.7,117.6,105.4);


(lib.Tween27 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-27,-52.7,0.759,0.759,35);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-58.7,-52.7,117.6,105.4);


(lib.Tween26 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-66.1,19.7,0.885,0.885,-44.4);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-66.1,-65.8,132.4,131.6);


(lib.Tween25 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-66.1,19.7,0.885,0.885,-44.4);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-66.1,-65.8,132.4,131.6);


(lib.Tween24 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-69,-36.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-69,-36.5,138,73);


(lib.Tween23 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line1();
	this.instance.setTransform(-69,-36.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-69,-36.5,138,73);


(lib.Tween22 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_text();
	this.instance.setTransform(-210.5,-78);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-210.5,-78,421,156);


(lib.Tween20 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_text();
	this.instance.setTransform(-210.5,-78);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-210.5,-78,421,156);


(lib.Tween14 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f9_1();
	this.instance.setTransform(-250,-146.6,1.16,1.16,-14.2);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-250,-247.9,500.1,495.9);


(lib.Tween13 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f9_1();
	this.instance.setTransform(-250,-146.6,1.16,1.16,-14.2);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-250,-247.9,500.1,495.9);


(lib.Tween11 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f6_item1();
	this.instance.setTransform(-28.5,-28.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-28.5,-28.5,57,57);


(lib.Tween8 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_1();
	this.instance.setTransform(-590,-332);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-590,-332,1180,664);


(lib.Tween7 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_1();
	this.instance.setTransform(-590,-332);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-590,-332,1180,664);


(lib.Tween4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_2();
	this.instance.setTransform(-318,-134.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-318,-134.5,636,269);


(lib.Tween3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_2();
	this.instance.setTransform(-318,-134.5);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-318,-134.5,636,269);


(lib.Tween2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_3();
	this.instance.setTransform(-119,-39);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-119,-39,238,78);


(lib.Tween1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_3();
	this.instance.setTransform(-119,-39);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-119,-39,238,78);


(lib.rem2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f1_rem2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.frame22 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("EhlYALWQgxgpAAhfQAAg/AdgcQAcgcBTgVIDjgwQDBgpC5gzQAtgMBZgIQB1gMAjgFQC9gdBahoQCKidGxgrQCDgNCmgCICfgDQCEgUD9gwIGBhIQB3gTO2iAIQFiKQDdggEWg0QEgg1CGgUQH/hOPfgyINcAAQDBAABXBRQA+A6AABZQAABqhpBUQiCBnkBAiITaAAIHggtQBciJBiAHILigKQAigRBHgNIXKAAQEEgpCOCtQAxA9AcBOQAVA7AAAmQAAAwgiA8QgsBRhTA9QjoCrm/gpQADATgNAiQgOAkgbAhQhGBXhlABMgoXAAAQhGgVg7gMQhugWjzAAQksAAw+CLIsEBlQl5AwhqAHIzzAFIhLAPMhCpAAFIjkgMQi2gIAAAeIAAAEQiugJhDg2g");
	var mask_graphics_2 = new cjs.Graphics().p("EhOtAXxQhvgUgzhCQgmgxAAhBQAAgzAagvQAnhFBVgcQoOgtikhTQhCghgVguQgNgcAAg6QAAhzCGhDQBvg3DtgkQBJgKGXgtQFFgjDNgqQgagigMgfQgMggAAgmQAAhPA8gyQB/hrFhAfQgNgTgFgcI+EACIjkgMQi2gIAAAeIAAAEQiugJhDg0QgxgpAAhfQAAg/AdgcQAcgcBTgVIDjgwQDBgpC5gzQAtgMBZgIQB1gMAjgFQC9gdBahoQCKifGxgrQCDgNCmgCICfgDQCEgUD9gwIGBhIQB3gTO2iAIQFiKQDdggEWg0QEgg1CGgUQH/hOPfgyINcAAQDBAABXBRQA+A6AABZQAABqhpBUQiCBnkBAiITaAAIHggtQBciJBiAHILigKQAigRBHgNIXKAAQEEgpCOCtQAxA9AcBOQAVA7AAAmQAAAwgiA+QgsBRhTA9QjoCrm/gpQADARgKAbIAdAFQBUASAjAmQAnAqAABTQAACSm2BdQlFBHtlBdQ1HCRmeA0QyPCTvaDCIwTAFQqhBv1FE7QrECljWAwQnKBljuAjg");
	var mask_graphics_4 = new cjs.Graphics().p("EhSJAXxQhvgUgzhCQgmgxAAhBQAAgzAagvQAnhFBVgcQoOgtikhTQhCghgVguQgNgcAAg6QAAhzCGhDQBvg3DtgkQBJgKGXgtQFFgjDNgqQgagigMgfQgMggAAgmQAAhPA8gyQB/hrFhAfQgNgTgFgcI+EACIjkgMQi2gIAAAeIAAAEQiugJhDg0QgxgpAAhfQAAg/AdgcQAcgcBTgVIDjgwQDBgpC5gzQAtgMBZgIQB1gMAjgFQC9gdBahoQCKifGxgrQCDgNCmgCICfgDQCEgUD9gwIGBhIQB3gTO2iAIQFiKQDdggEWg0QEgg1CGgUQIBhOPdgyINcAAQDBAABXBRQA+A6AABZQAABqhpBUQiCBnkBAiITaAAIHggtQBciJBiAHILigKQAigRBHgNIXKAAQEEgpCOCtQAxA9AcBOQAVA7AAAmQAAAwgiA+QgsBRhTA9QjoCrm/gpQADARgKAbIAdAFQBUASAjAmQAbAcAIAwQALAZADAiQAEA3gJCHQAAAgA+A1QANAMB7BdQBcBFAsAyQA9BGAAA+QAAA1gfAuQgfAsg/AqQHmAPC/BVQCUBCAACBQAABuhiA1QhrA6kdAdQhDAGgcAnQgMARgUBEQgSA8ghAgQgxAwhnAUI7TAFIhVAPQgwgFipgbQi/gehkgNI6rgFIiHgfQhPgSg4gGIsqgFIh3ALQh/AMhxgHQlvgYjXjZQg7g7iFgdIiPgeQhlgVhZgaQhrghjkg1QijgmhhgZQqDB5woD5QrECljWAwQnKBljuAjg");
	var mask_graphics_6 = new cjs.Graphics().p("EhYPAc2QgXgfgGgnQgBgJAAgcQAAgqAzg0QBNhQAlg8Ql7gziJgUQjbgghlgcQh1gggugxQgugwAAhVQAAhLAYgiQAegsBXgWQCagoJsgKIC8gBQgDgRAAgRQAAgzAagvQAnhGBVgcQoOgsikhTQhCghgVguQgNgdAAg5QAAhzCGhEQBvg3DtgjQBJgLGXgsQFFgkDNgpQgagggMggQgMgfAAgmQAAhPA8gzQB/hrFhAgQgNgUgFgbI+EACIjkgMQi2gJAAAfIAAADQiugIhDg3QgxgoAAhfQAAg/AdgdQAcgbBTgVIDjgxQDBgpC5gyQAtgMBZgIQB1gMAjgFQC9geBahnQCKifGxgrQCDgNCmgDICfgCQCEgVD9gvIGBhIQB3gUO2h/IQFiKQDdggEWg0QEgg1CGgVQIBhNPdgyINcAAQDBAABXBRQA+A6AABYQAABqhpBUQiCBokBAiITaAAIHggtQBciJBiAHILigKQAigRBHgNIXKAAQEEgqCOCuQAxA8AcBPQAVA7AAAmQAAAwgiA+QgsBQhTA+QjoCqm/goQADAQgKAcIAdAFQBUASAjAmQAbAcAIAwQALAZADAiQAEA3gJCHQAAAiA+A0QANAMB7BdQBcBGAsAyQA9BGAAA8QAAA1gfAtQgfAsg/ArQHmAPC/BVQCUBCAACAQAABvhiA1QhrA5kdAdQhDAHgcAnQgMARgUBEQgSA7ghAhQgxAvhnAUI7TAGIhVAPQgwgGipgaQi/gehkgOI55gEQhnAYi/AiQjMAkhIAaQjLA3iVArQkeBRhcA3QhEAqg5AUQg/AVhnANQhLAJqoAuQxlB3xjBxUgi6ADhgHWAAAQjZAAhJhgg");
	var mask_graphics_8 = new cjs.Graphics().p("Ak0fcQlAgPhMgWQgogahSgMIvtgFQighhifg8Qg7gXifgvQhegcgggcQgbgagJgvIrABHUgi7ADhgHWAAAQjZAAhIhgQgXgfgGgnQgCgJABgcQgBgqA0g0QBNhQAkg8Ql6gziJgUQjbgghlgcQh1gggvgxQgtgwgBhVQAAhLAZgiQAegsBXgWQCagoJsgKIC7gBQgDgRAAgRQABgzAagvQAnhGBVgcQoOgsikhTQhDghgUguQgNgdgBg5QAAhzCGhEQBvg3DugjQBIgLGXgsQFFgiDOgpQgbgigLggQgMgfAAgmQAAhPA8gzQB/hrFhAgQgNgUgFgbI+EACIjkgMQi3gJAAAfIAAADQiugIhCg3QgygoABhfQAAg/AcgdQAdgbBSgVIDkgxQDAgpC6gyQAtgMBYgIQB1gMAkgFQC9geBZhnQCLifGxgrQCCgNCngDICfgCQCEgVD9gvIGBhIQB2gUO3h/IQEiKQDdggEXg0QEgg1CFgVQIBhNPegyINbAAQDBAABXBRQA/A6AABYQAABqhqBUQiCBokBAiITbAAIHggtQBbiJBjAHILigKQAigRBHgNIXKAAQEEgqCOCuQAxA8AcBPQAVA7AAAmQAAAwgiA+QgsBQhTA+QjpCqm+goQADAQgKAcIAdAFQBTASAkAmQAaAcAJAwQALAZACAiQAFA3gJCHQAAAiA+A0QANAMB7BdQBcBGArAyQA+BGAAA9QAAA0ggAtQgeAsg/ArQHmAPC/BVQCUBCAACAQAABvhiA1QhrA5kdAdQhEAHgbAnQgMARgVBEQgRA7giAhQgwAvhnAUIlrABQBnA8AeBKIfdAGQBuATA0BEQAlAxABA/QgBBBglAyQg0BChuATIoEAKIAKAZQAFATAAAgQAABHgcAyQgyBVh8AJI0UAEIksAmI3uAFQiMAfhOAYQg2AUgQAEQgTAEg+gBIj7gCQisAAhqAXQhWAagrAGIpYAFIi0APg");
	var mask_graphics_10 = new cjs.Graphics().p("Egz/AorIg5g4IpQgFQiOgagmhUQgIgRgNgzQgJgngPgSQiUAfiaAdQkzA3iwAAQiTAAgqh4QgOgoAAguQgBghAEgQIoBgFQhugSg0hDQgmgyABhBQAAhGAtgzQBMhYC8gBQgehFgpiUQgniNAAggQABhPBmg3QjiAOh1AAQjZAAhIhgQgXgfgGgnQgCgJABgcQgBgqA0g0QBNhQAkg8Ql6gziJgUQjbgghlgcQh1gggvgxQgtgwgBhVQAAhLAZgiQAegsBXgWQCagoJsgKIC7gBQgDgRAAgRQABgzAagvQAnhGBVgcQoOgqikhTQhDghgUguQgNgdgBg5QAAhzCGhEQBvg3DugjQBIgLGXgsQFFgkDOgpQgbgigLggQgMgfAAgmQAAhPA8gzQB/hrFhAgQgNgUgFgbI+EACIjkgMQi3gJAAAfIAAADQiugIhCg3QgygoABhfQAAg/AcgdQAdgbBSgVIDkgxQDAgpC6gyQAtgMBYgIQB1gMAkgFQC9geBZhnQCLifGxgrQCCgNCngDICfgCQCEgVD9gvIGBhIQB2gUO3h/IQEiKQDdggEXg0QEgg1CFgVQIBhNPegyINbAAQDBAABXBRQA/A6AABYQAABqhqBUQiCBokBAiITbAAIHggtQBbiJBjAHILigKQAigRBHgNIXKAAQEEgqCOCuQAxA8AcBPQAVA7AAAmQAAAwgiA+QgsBQhTA+QjpCqm+goQADAQgKAcIAdAFQBTASAkAmQAaAcAJAwQALAZACAiQAFA3gJCHQAAAiA+A0QANAMB7BdQBcBGArAyQA+BGAAA9QAAA2ggAtQgeAsg/ArQHmAPC/BVQCUBCAACAQAABvhiA1QhrA3kdAdQhEAHgbAnQgMARgVBEQgRA7giAhQgwAvhnAUIlrABQBnA8AeBKIfdAGQBuATA0BEQAlAxABA/QgBBBglAyQg0BChuATIoEAKIAKAZQAFATAAAgQAABHgcAyQgyBVh8AJI0UAEIksAmI3uAFQiMAfhOAYQg2AUgQAEQgTAEg+gBIj7gCQisAAhqAXQhWAagrAGIkNACQhFBCikAzQjVBCnFA6QoSA9j4AiQmtA7i+BJIrcEfQk7B8jJA7Qj3BKlGA3Qk/A1pKBEQgYArg7AZQg1AWg7AAQhPAAg2gng");
	var mask_graphics_12 = new cjs.Graphics().p("Egz/AorIg5g4IpQgFQiOgagmhUQgIgRgNgzQgJgngPgSQiUAfiaAdQkzA3iwAAQiTAAgqh4QgOgoAAguQgBghAEgQIoBgFQhugSg0hDQgmgyABhBQAAhGAtgzQBMhYC8gBQgehFgpiUQgniNAAggQABhPBmg3QjiAOh1AAQjZAAhIhgQgXgfgGgnQgCgJABgcQgBgqA0g0QBNhQAkg8Ql6gziJgUQjbgghlgcQh1gggvgxQgtgwgBhVQAAhLAZgiQAegsBXgWQCagoJsgKIC7gBQgDgRAAgRQABgzAagvQAnhGBVgcQoOgqikhTQhDghgUguQgNgdgBg5QAAhzCGhEQBvg3DugjQBIgLGXgsQFFgkDOgpQgbgigLggQgMgfAAgmQAAhPA8gzQB/hrFhAgQgNgUgFgbI+EACIjkgMQi3gJAAAfIAAADQiugIhCg3QgygoABhfQAAg/AcgdQAdgbBSgVIDkgxQDAgpC6gyQAtgMBYgIQB1gMAkgFQC9geBZhnQCLifGxgrQCCgNCngDICfgCQCEgVD9gvIGBhIQB2gUO3h/IQEiKQDdggEXg0QEgg1CFgVQIBhNPegyINbAAQDBAABXBRQA/A6AABYQAABqhqBUQiCBokBAiITbAAIHggtQBbiJBjAHILigKQAigRBHgNIXKAAQEEgqCOCuQAxA8AcBPQAVA7AAAmQAAAwgiA+QgsBQhTA+QjpCqm+goQADAQgKAcIAdAFQBTASAkAmQAaAcAJAwQALAZACAiQAFA3gJCHQAAAiA+A0QANAMB7BdQBcBGArAyQA+BGAAA9QAAA2ggAtQgeAsg/ArQHmAPC/BVQCUBCAACAQAABvhiA1QhrA3kdAdQhEAHgbAnQgMARgVBEQgRA7giAhQgwAvhnAUIlrABQBnA8AeBKIfdAGQBuATA0BEQAlAxABA/QgBBBglAyQg0BChuATIoEAKIAKAZQAFATAAAgQAABHgcAyQgyBVh8AJIssADQg/ArhiAMIIVAEQCdA0BJCDQAiA6gBA4QAAA6gmAoQFugyCEB3QBCA9gGBLQgBBCgkAxQgzBChxATInxAFIohAjItQAFIiSAVQiOAWgVAMQA/ArAcAvQAXAoABAyQgBBCgkAxQg0BDhvASMguWAAAQhvgLgugGQhNgJjcgxMgjGgAFQgRgChEglQhEgmgRgSQgRgRgWgLQjjA/kfAxQk/A1pKBEQgYArg7AZQg1AWg7AAQhPAAg2gng");
	var mask_graphics_14 = new cjs.Graphics().p("Eh03AxRQhwgSgzhDQgkgxgBhCQABlEZ2kEQIEhRK4hMII5g+Qj4AOhohgQgqgmgNg0IgGgvIAAgVQiKgDgph0QgOgoAAgvQgBggAEgQIoBgFQhugTg0hCQgmgyABhBQAAhGAtgzQBMhYC8gBQgehGgpiTQgniNAAggQABhPBmg3QjiAOh1AAQjZAAhIhhQgXgfgGgmQgCgJABgcQgBgqA0g1QBNhPAkg9Ql6gyiJgUQjbgghlgcQh1gggvgxQgtgwgBhUQAAhKAZgjQAegrBXgWQCagoJsgLIC7gBQgDgQAAgSQABgyAagvQAnhGBVgcQoOgtikhSQhDgigUgtQgNgdgBg5QAAh0CGhDQBvg3DugjQBIgLGXgsQFFgkDOgpQgbgigLggQgMggAAglQAAhPA8gzQB/hrFhAgQgNgUgFgcI+EADIjkgMQi3gJAAAfIAAADQiugJhCg2QgygoABhfQAAg/AcgdQAdgbBSgVIDkgxQDAgpC6gyQAtgMBYgJQB1gLAkgFQC9geBZhnQCLifGxgrQCCgNCngDICfgCQCEgVD9gvIGBhIQB2gUO3h/IQEiKQDdghEXgzQEgg2CFgUQIBhNPegyINbAAQDBAABXBRQA/A6AABYQAABqhqBUQiCBnkBAjITbAAIHggtQBbiJBjAHILigKQAigRBHgNIXKAAQEEgqCOCuQAxA8AcBOQAVA8AAAlQAAAwgiA/QgsBQhTA9QjpCrm+goQADAQgKAcIAdAFQBTASAkAmQAaAcAJAwQALAYACAjQAFA3gJCHQAAAhA+A1QANAMB7BdQBcBGArAyQA+BGAAA9QAAA2ggAtQgeAsg/AqQHmAQC/BVQCUBCAACAQAABvhiA1QhrA5kdAdQhEAHgbAnQgMAQgVBEQgRA8giAgQgwAwhnAUIlrABQBnA8AeBKIfdAFQBuAUA0BBQAlAyABA/QgBBBglAxQg0BChuAUIoEAKIAKAZQAFATAAAfQAABIgcAyQgyBVh8AIIssAEQg/ArhiAMIIVAEQCdA0BJCCQAiA7gBA3QAAA7gmAoQFugyCEB3QBCA9gGBLQgBBCgkAxQgzBChxATInxAFIohAjItQAFIiSAVQiOAVgVANQA/AqAcAwQAXAoABAyQgBBCgkAxQg0BChvATMgkhAAAQgaA4AAAsQAABEgfAeQghAghgAUQhFAOjUAaQkrAmi7AbQjnAhh7AkQhIAVg9AgQgwAagtAHQhFAMirAAUgFLAAAgiLAEkI4iDSQr/BnimAPgEgsRAkHQp5BmjmAtQklA753DnISzAAQJNgvJthMQFegrJQhPIAGgKQhHgqhQhCQhMg+gdgRQg1geg9gBIgCAAQhGAAhsAkg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:615.9,y:10.9}).wait(2).to({graphics:mask_graphics_2,x:615.9,y:84.1}).wait(2).to({graphics:mask_graphics_4,x:637.9,y:84.1}).wait(2).to({graphics:mask_graphics_6,x:637.9,y:126.3}).wait(2).to({graphics:mask_graphics_8,x:696,y:133.3}).wait(2).to({graphics:mask_graphics_10,x:696,y:196.3}).wait(2).to({graphics:mask_graphics_12,x:696,y:196.3}).wait(2).to({graphics:mask_graphics_14,x:662,y:247.4}).wait(185));

	// Layer 1
	this.instance = new lib.f22_Transition0();

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({_off:true},190).wait(9));

	// Layer 3
	this.shape = new cjs.Shape();
	this.shape.graphics.f("rgba(255,212,82,0)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape.setTransform(590,332);

	this.shape_1 = new cjs.Shape();
	this.shape_1.graphics.f("rgba(255,212,82,0.125)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_1.setTransform(590,332);

	this.shape_2 = new cjs.Shape();
	this.shape_2.graphics.f("rgba(255,212,82,0.251)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_2.setTransform(590,332);

	this.shape_3 = new cjs.Shape();
	this.shape_3.graphics.f("rgba(255,212,82,0.376)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_3.setTransform(590,332);

	this.shape_4 = new cjs.Shape();
	this.shape_4.graphics.f("rgba(255,212,82,0.502)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_4.setTransform(590,332);

	this.shape_5 = new cjs.Shape();
	this.shape_5.graphics.f("rgba(255,212,82,0.624)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_5.setTransform(590,332);

	this.shape_6 = new cjs.Shape();
	this.shape_6.graphics.f("rgba(255,212,82,0.749)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_6.setTransform(590,332);

	this.shape_7 = new cjs.Shape();
	this.shape_7.graphics.f("rgba(255,212,82,0.875)").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_7.setTransform(590,332);

	this.shape_8 = new cjs.Shape();
	this.shape_8.graphics.f("#FFD452").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape_8.setTransform(590,332);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.shape}]},9).to({state:[{t:this.shape_1}]},1).to({state:[{t:this.shape_2}]},1).to({state:[{t:this.shape_3}]},1).to({state:[{t:this.shape_4}]},1).to({state:[{t:this.shape_5}]},1).to({state:[{t:this.shape_6}]},1).to({state:[{t:this.shape_7}]},1).to({state:[{t:this.shape_8}]},1).wait(182));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1180,89.9);


(lib.f22_trasition = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_Transition();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,383);


(lib.f22_text2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_text2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,367,101);


(lib.f22_text1_1 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib._021_f22_NUMBER_220x160();
	this.instance.setTransform(132.3,-87.9);

	// Layer 1
	this.instance_1 = new lib.f22_text1();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-87.9,465,160);


(lib.f22_text0_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_text0();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,552,226);


(lib.f22_frame = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_bgFrame();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,460,328);


(lib.f22_bgtext = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_bg_text0();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,702,438);


(lib.f22_5_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,102,254);


(lib.f22_4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,624,214);


(lib.f22_3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,297,200);


(lib.f22_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,204,69);


(lib.f22_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f22_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,216,280);


(lib.f18_BG_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f18_BG();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f17_text1_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f16_text1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,900,474);


(lib.f17_gra2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f17_gra2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,272,256);


(lib.f17_gra1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f17_gra1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,88,79);


(lib.f17_2_1 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib._017_f17_NUMBER_223x137();
	this.instance.setTransform(141.8,-90.5);

	// Layer 1
	this.instance_1 = new lib.f17_2();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,-90.5,364.8,218.5);


(lib.f17_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f17_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,708,650);


(lib.f16_Gaptrong = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f16_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,116,116);


(lib.f16_5_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f16_5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,178,181);


(lib.f16_4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f16_4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,143,146);


(lib.f11_NUMBER = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._013_f11_NUMBER_275x145();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,275,145);


(lib.f11_line2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_line2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,81,491);


(lib.f11_KETQUA3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._016_f11_KETQUA3_58x33();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib.f11_KETQUA2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._015_f11_KETQUA2_58x33();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib.f11_KETQUA1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._014_f11_KETQUA1_58x33();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,58,33);


(lib.f11_icon3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_icon3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,117,112);


(lib.f11_icon2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_icon2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,96,90);


(lib.f11_icon1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_icon1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,117,113);


(lib.f11_25_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_25();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,52,38);


(lib.f11_21_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f11_21();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,352,351);


(lib.f10_h3_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f10_heart3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,48,45);


(lib.f10_h2_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f10_heart2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,83,73);


(lib.f10_h1_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f10_heart1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,102,90);


(lib.f10_2_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f10_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,150,149);


(lib.f10_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f10_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,715,163);


(lib.f9_banh2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f4_banh2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,159,324);


(lib.f9_banh1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f4_banh1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,174,327);


(lib.f9_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f9_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,446,429);


(lib.f6_Number = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._009_f6_Number_430x134();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,430,134);


(lib.f6_item1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f6_item1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,57,57);


(lib.f6_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f6_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,494,159);


(lib.f6_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f6_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,271,117);


(lib.f5_text_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_text();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,368,168);


(lib.f5_item6_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,251,376);


(lib.f5_item5_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,204,175);


(lib.f5_item4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,283,273);


(lib.f5_item3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,102,97);


(lib.f5_item2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,70,72);


(lib.f5_item1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_item1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,93,89);


(lib.f5_bg_white = function() {
	this.initialize();

	// Layer 1
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("A0TV3MAAAgruMAonAAAMAAAArug");
	this.shape.setTransform(130,140);

	this.addChild(this.shape);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,260,280);


(lib.f5_BG_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f5_BG();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f4_Xanh_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f4_Xanh();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,279,133);


(lib.f4_banh2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f4_banh2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,159,324);


(lib.f4_banh1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f4_banh1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,174,327);


(lib.f3_tia_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_tia();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,770,252);


(lib.f3_face9_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face9();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face8_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face8();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face7_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face7();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face6_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face5_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face4_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face3_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face2_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f3_face1_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f3_face1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,210,210);


(lib.f2_52 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._003_USER_NUMBER_274x105();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,274,105);


(lib.f1_rem1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f1_rem1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f1_42 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._002_USER_PHOTO_235x235();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,235,235);


(lib.f1_3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._001_USER_329x76();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,329,76);


(lib.f1_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f1_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,849,305);


(lib.f1_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f1_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,441,284);


(lib.f33_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f33();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,593,426);


(lib.f32_text_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_text();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,591,124);


(lib.f32_6_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,277,390);


(lib.f32_5_in = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_6();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,170,157);


(lib.f32_4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_5();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,241,228);


(lib.f32_3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,174,177);


(lib.f32_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,848,275);


(lib.f32_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f32_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,777,418);


(lib.f30_text1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_text1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,397,209);


(lib.f30_BG = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f2_1();

	// Layer 2
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#F8F1DE").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape.setTransform(590,332);

	this.addChild(this.shape,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664.1);


(lib.f30_32 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,86,114);


(lib.f30_3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,612,148);


(lib.f30_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f30_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,599,306);


(lib.f29_3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,231,239);


(lib.f29_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f29_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,103,73);


(lib.f28_Cosy = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f28_5();
	this.instance.setTransform(0,0,0.884,0.884);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,370.3,350);


(lib.f28_4_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f28_4();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,103,98);


(lib.f28_3_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f28_3();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,532,435);


(lib.f28_2_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f28_2();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,157,121);


(lib.f28_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f28_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,590,258);


(lib.f27_text = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib._026_f28_NUMBER_218x132();
	this.instance.setTransform(-4,84);

	// Layer 1
	this.instance_1 = new lib.f28_text();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,0,491,281);


(lib.f27_2_in = function() {
	this.initialize();

	// Layer 4
	this.instance = new lib._001_USER_329x76();
	this.instance.setTransform(-0.4,320.1);

	// Layer 3 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	mask.graphics.p("A06WgQhlAAAAhmMAAAgp0QAAhkBlAAMAp1AAAQBlAAgBBkMAAAAp0QABBmhlAAg");
	mask.setTransform(167.5,168);

	// Layer 2
	this.instance_1 = new lib._025_f27_USER_288x288();
	this.instance_1.setTransform(23.5,24);

	this.instance_1.mask = mask;

	// Layer 1
	this.instance_2 = new lib.f27_2();

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-0.4,0,335.5,443);


(lib.f27_1_1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.f27_1();

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,683,124);


(lib.Tween30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f17_text1_in();
	this.instance.setTransform(0,0,1,1,0,0,0,450,237);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:3,y:-2},6).to({x:0,y:0},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-450,-237,900,474);


(lib.Tween12 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f6_item1_1();
	this.instance.setTransform(0,0,1,1,0,0,0,28.5,28.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:1,y:-1.5},5).to({x:0,y:0},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-28.5,-28.5,57,57);


(lib.f22_hinh3 = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.f22_btn();
	this.instance.setTransform(275.4,12.1);

	// Layer 2
	this.instance_1 = new lib._024_f22_hinh3_408x283();
	this.instance_1.setTransform(26.4,25.8);

	// Layer 1
	this.instance_2 = new lib.f22_frame();
	this.instance_2.setTransform(230,164,1,1,0,0,0,230,164);
	this.instance_2.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,4);

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,468,336);


(lib.f22_hinh2 = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.f22_btn();
	this.instance.setTransform(6.1,87.2);

	// Layer 2
	this.instance_1 = new lib._023_f22_hinh2_283x408();
	this.instance_1.setTransform(25.7,29.3);

	// Layer 1
	this.instance_2 = new lib.f22_frame();
	this.instance_2.setTransform(164,230,1,1,-90,0,0,230,164);
	this.instance_2.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,4);

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,336,468);


(lib.f22_hinh1 = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.f22_btn();
	this.instance.setTransform(319.5,18.1);

	// Layer 1
	this.instance_1 = new lib._022_f22_hinh1_408x283();
	this.instance_1.setTransform(26,28.1);

	// Layer 2
	this.instance_2 = new lib.f22_frame();
	this.instance_2.setTransform(230,164,1,1,0,0,0,230,164);
	this.instance_2.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,4);

	this.addChild(this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-2,-2,468,336);


(lib.f22_choe = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f22_1_1();
	this.instance.setTransform(70.6,118.2,0.598,0.598,0,0,0,108,140);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.74,scaleY:0.74,x:79.6,y:103.1,alpha:1},8,cjs.Ease.get(1)).to({scaleX:0.79,scaleY:0.79,y:103.2},5).to({scaleX:0.74,scaleY:0.74,y:103.1},5).to({scaleX:0.79,scaleY:0.79,y:103.2},5).to({scaleX:0.74,scaleY:0.74,y:103.1},5).to({scaleX:0.79,scaleY:0.79,y:103.2},5).to({scaleX:0.74,scaleY:0.74,y:103.1},5).to({scaleX:0.79,scaleY:0.79,y:103.2},5).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(6,34.5,129.1,167.4);


(lib.f18_hình2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._019_f17_hinh2_235x235();
	this.instance.setTransform(12.5,12);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f18_hinh3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._020_f17_hinh3_235x235();
	this.instance.setTransform(12.5,13);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f18_hinh1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._018_f17_hinh1_235x235();
	this.instance.setTransform(12.5,12);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f11_25_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f11_25_in();
	this.instance.setTransform(30,29.1,0.538,0.538,0,0,0,26,19.1);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:19,scaleX:1,scaleY:1,x:26,y:19,alpha:1},6).to({x:25,y:16},3).to({x:26,y:19},2).to({x:25,y:16},3).to({x:26,y:19},3).to({x:25,y:16},3).to({x:26,y:19},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(16,18.8,28,20.5);


(lib.f10_heart3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f10_h3_in();
	this.instance.setTransform(24,22.5,1,1,0,0,0,24,22.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:28,y:16.5},4).to({x:18,y:18.5},4).to({x:24,y:22.5},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,48,45);


(lib.f10_heart2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f10_h2_in();
	this.instance.setTransform(41.5,36.5,1,1,0,0,0,41.5,36.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:37.5,y:40.5},4).to({x:45.5,y:38.5},4).to({x:41.5,y:36.5},4).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,83,73);


(lib.f10_heart1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f10_h1_in();
	this.instance.setTransform(51,45,1,1,0,0,0,51,45);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:47,y:39},6).to({x:45.3,y:46.5},4).to({x:51,y:45},3).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,102,90);


(lib.f10_2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f10_2_in();
	this.instance.setTransform(104,47.5,0.573,0.573,0,0,0,75,74.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:75,y:74.5,alpha:1},9).to({scaleX:1.11,scaleY:1.11,x:70,y:82.5},6).to({scaleX:1.21,scaleY:1.21,x:49,y:89.5,alpha:0},4).wait(11));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(61.1,4.9,85.9,85.4);


(lib.f9_hinh3 = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.f9_3();
	this.instance.setTransform(448,126.4,0.717,0.717,53.2);

	// Layer 1
	this.instance_1 = new lib._012_f9_hinh3_280x280();
	this.instance_1.setTransform(20,20);

	// Layer 2
	this.instance_2 = new lib.f5_bg_white();
	this.instance_2.setTransform(160,172.3,1.231,1.231,0,0,0,130,140);
	this.instance_2.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	// Layer 4
	this.instance_3 = new lib.f9_4();
	this.instance_3.setTransform(-56.8,-60.3,0.807,0.807,7.2);

	this.addChild(this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-110.9,-60.3,626,490.4);


(lib.f9_hinh2 = function() {
	this.initialize();

	// Layer 3
	this.instance = new lib.f9_5();
	this.instance.setTransform(275.6,32.9,0.922,0.922,-5.9);

	this.instance_1 = new lib.f9_5();
	this.instance_1.setTransform(9.7,360.7,0.713,0.713,174.1);

	// Layer 1
	this.instance_2 = new lib._011_f9_hinh2_280x280();
	this.instance_2.setTransform(20,18);

	// Layer 2
	this.instance_3 = new lib.f5_bg_white();
	this.instance_3.setTransform(160,172.3,1.231,1.231,0,0,0,130,140);
	this.instance_3.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-129.6,-4,585.3,384.3);


(lib.f9_hinh1 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.f9_5();
	this.instance.setTransform(-54.7,94.5,0.575,0.575,-117);

	this.instance_1 = new lib.f9_5();
	this.instance_1.setTransform(312.4,111.5,0.795,0.795,27.8);

	// Layer 1
	this.instance_2 = new lib._010_f9_hinh1_280x280();
	this.instance_2.setTransform(20,20);

	this.instance_3 = new lib.f5_bg_white();
	this.instance_3.setTransform(160,172.3,1.231,1.231,0,0,0,130,140);
	this.instance_3.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-95.8,-84.9,518.6,521.4);


(lib.f9_Hinh_Graphic = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f9_hinh1();
	this.instance.setTransform(160,172.3,1,1,0,0,0,160,172.3);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({rotation:14,x:200},25).wait(275));

	// Layer 5
	this.instance_1 = new lib.f9_hinh3();
	this.instance_1.setTransform(2023.9,417.8,1,1,0,0,0,140,140);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(127).to({_off:false},0).to({regY:140.1,rotation:8.5},29).to({regX:139.9,regY:140.2,scaleX:1.1,scaleY:1.1,rotation:-1,y:417.9},44).wait(100));

	// Layer 4
	this.instance_2 = new lib.f9_hinh2();
	this.instance_2.setTransform(1141.8,310.2,1,1,-2.5,0,0,140,140);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(59).to({_off:false},0).wait(241));

	// Layer 2
	this.instance_3 = new lib.f9_2_1();
	this.instance_3.setTransform(372.1,239,1,1,-6.7,0,0,223,214.4);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(34).to({_off:false},0).to({regY:214.5,scaleX:0.76,scaleY:0.76,rotation:13.8,x:534.3,y:283.9,alpha:1},17,cjs.Ease.get(1)).wait(26).to({regX:223.1,regY:214.4,scaleX:0.9,scaleY:0.9,rotation:-5.7,x:678.4,y:293.9},9,cjs.Ease.get(1)).wait(214));

	// Layer 3
	this.instance_4 = new lib.Tween13("synched",0);
	this.instance_4.setTransform(194.1,196.1,1,1,25.2);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.instance_5 = new lib.Tween14("synched",0);
	this.instance_5.setTransform(-198,184.1,0.853,0.853);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_4}]},20).to({state:[{t:this.instance_5}]},17).wait(263));
	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(20).to({_off:false},0).to({_off:true,scaleX:0.85,scaleY:0.85,rotation:0,x:-198,y:184.1,alpha:1},17,cjs.Ease.get(1)).wait(263));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-95.8,-84.9,518.6,521.4);


(lib.f6_BanhXoay_In = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Tween11("synched",0);
	this.instance.setTransform(43.1,37.1,0.368,0.368);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.instance_1 = new lib.Tween12("synched",0);
	this.instance_1.setTransform(11,69.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},14).to({state:[{t:this.instance_1}]},8).wait(178));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(14).to({_off:false},0).to({_off:true,scaleX:1,scaleY:1,x:11,y:69.1,alpha:1},8,cjs.Ease.get(1)).wait(178));

	// Layer 1
	this.instance_2 = new lib.f6_BANH();
	this.instance_2.setTransform(0,0,0.161,0.161);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(200));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,88.8,75.9);


(lib.f6_BanhXoay = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 6
	this.instance = new lib.f6_BanhXoay_In();
	this.instance.setTransform(-258.3,-1.4,1,1,-2.8,0,0,44.3,37.9);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regX:44.4,rotation:-57.9,guide:{path:[-258.2,-1.3,-143.6,-32,-38.2,4.7,84.4,47.2,123.1,155.3,152.3,237.3,120.7,320.5]}},99).wait(1));

	// Layer 5
	this.instance_1 = new lib.f6_BanhXoay_In();
	this.instance_1.setTransform(-526.4,228.8,1,1,113,0,0,44.3,37.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({rotation:31.6,guide:{path:[-526.3,228.7,-515.7,198.8,-497.3,168.7,-429.6,58.3,-295,10.2,-160.6,-37.9,-38.2,4.7,4.9,19.7,37.6,42.7]}},99).wait(1));

	// Layer 4
	this.instance_2 = new lib.f6_BanhXoay_In();
	this.instance_2.setTransform(-375.9,534.7,1,1,38.5,0,0,44.4,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:44.3,rotation:7.2,guide:{path:[-375.8,534.5,-489.3,491.1,-526.3,387.6,-565,279.3,-497.3,168.9,-429.6,58.3,-295,10.2,-290.5,8.6,-286,7.1]}},99).wait(1));

	// Layer 3
	this.instance_3 = new lib.f6_BanhXoay_In();
	this.instance_3.setTransform(-16.2,487.9,1,1,22.7,0,0,44.4,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({rotation:107.2,guide:{path:[-16.2,487.7,-57.5,514.8,-108.3,532.9,-242.9,581,-365.1,538.4,-487.6,495.9,-526.3,387.6,-556.9,302.1,-521.1,215.4]}},99).wait(1));

	// Layer 2
	this.instance_4 = new lib.f6_BanhXoay_In();
	this.instance_4.setTransform(123.1,314.3,0.91,0.91,0,0,0,44.4,37.9);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({regY:38,rotation:27.7,guide:{path:[123,314.2,112.4,344.1,94,374.2,26.3,484.8,-108.3,532.9,-242.7,581,-365.1,538.4,-370.1,536.7,-374.9,534.9]}},99).wait(1));

	// Layer 1
	this.instance_5 = new lib.f6_BanhXoay_In();
	this.instance_5.setTransform(39.9,44.2,1,1,-17.5,0,0,44.4,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({regY:37.9,rotation:0,guide:{path:[40,44.3,98.4,86.6,123,155.6,161.7,263.7,93.9,374.3,50.2,446,-21.8,491.5]}},99).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-578.7,-41.4,742.2,633.4);


(lib.f6_Banh_In = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.f6_1_1();
	this.instance.setTransform(288.1,112,0.107,0.107,0,0,0,135.5,58.6);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:58.5,scaleX:1.19,scaleY:1.19,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},2).wait(170));

	// Layer 3
	this.instance_1 = new lib.f6_Number();
	this.instance_1.setTransform(280.1,259,7.756,7.756,0,0,0,215,67);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({_off:false},0).to({scaleX:0.91,scaleY:0.91,y:259.1,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1.06,scaleY:1.06},2).to({scaleX:1,scaleY:1},2).wait(159));

	// Layer 4
	this.instance_2 = new lib.f6_2_1();
	this.instance_2.setTransform(319.6,381.6,0.077,0.077,0,0,0,247,79.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(5).to({_off:false},0).to({scaleX:1.11,scaleY:1.11,alpha:1},7,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},3).wait(165));

	// Layer 1
	this.instance_3 = new lib.f6_BANH();

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(180));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,551,471);


(lib.f6_Banh = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f6_Banh_In();
	this.instance.setTransform(275.5,235.5,1,1,0,0,0,275.5,235.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).to({x:279.5,y:231.5},7).to({x:275.5,y:235.5},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,551,471);


(lib.f5_text_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f5_text_in();
	this.instance.setTransform(184,84,1,1,0,0,0,184,84);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:187.4,y:82.3},5).to({x:184,y:84},6).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,368,168);


(lib.f5_hinh5 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._008_f5_hinh5_235x235();
	this.instance.setTransform(12.5,11.7);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f5_hinh4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._007_f5_hinh4_235x235();
	this.instance.setTransform(12.5,10.8);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f5_hinh3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._006_f5_hinh3_235x235();
	this.instance.setTransform(12.5,10.6);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f5_hinh2 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib._005_f5_hinh2_235x235();
	this.instance.setTransform(12.5,11.5);

	// Layer 1
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f5_hinh1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._004_f5_hinh1_235x235();
	this.instance.setTransform(12.5,12.1);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(130,140,1,1,0,0,0,130,140);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,272,292);


(lib.f5_5Hinh_ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 13
	this.instance = new lib.f5_item6_1();
	this.instance.setTransform(763.4,431.5,1,1,0,0,0,125.5,188);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(79).to({_off:false},0).to({alpha:1},27).wait(48).to({alpha:0},11).to({_off:true},6).wait(29));

	// Layer 12
	this.instance_1 = new lib.f5_item5_1();
	this.instance_1.setTransform(742.5,197.3,1,1,0,0,0,102,87.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(62).to({_off:false},0).to({alpha:1},23).wait(56).to({alpha:0},10).to({_off:true},20).wait(29));

	// Layer 11
	this.instance_2 = new lib.f5_item4_1();
	this.instance_2.setTransform(-11.2,497.9,1,1,0,0,0,141.5,136.5);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(41).to({_off:false},0).to({alpha:1},23).wait(82).to({alpha:0},11).to({_off:true},36).wait(7));

	// Layer 10
	this.instance_3 = new lib.f5_item3_1();
	this.instance_3.setTransform(776.1,535.8,1,1,0,0,0,51,48.5);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(88).to({_off:false},0).to({alpha:1},15).wait(30).to({alpha:0},14).to({_off:true},31).wait(22));

	// Layer 9
	this.instance_4 = new lib.f5_item2_1();
	this.instance_4.setTransform(359.9,485.2,1,1,0,0,0,35,36);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(102).to({_off:false},0).to({alpha:1},11).wait(49).to({alpha:0},8).to({_off:true},1).wait(29));

	// Layer 8
	this.instance_5 = new lib.f5_item1_1();
	this.instance_5.setTransform(-53.9,309.3,1,1,0,0,0,46.5,44.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(74).to({_off:false},0).to({alpha:1},12).wait(53).to({alpha:0},8).to({_off:true},24).wait(29));

	// Layer 6
	this.instance_6 = new lib.f5_hinh5();
	this.instance_6.setTransform(357.3,898.9,0.113,0.113,20.2,0,0,117.9,117.4);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(26).to({_off:false},0).to({scaleX:0.57,scaleY:0.57,rotation:20.1,x:413.5,y:681.9},101).to({regX:117.6,regY:117.5,scaleX:0.72,scaleY:0.72,rotation:20.2,x:432.1,y:610.1},72).wait(1));

	// Layer 5
	this.instance_7 = new lib.f5_hinh4();
	this.instance_7.setTransform(387.8,851.5,0.103,0.103,39.2,0,0,116.7,117.6);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(19).to({_off:false},0).to({scaleX:0.57,scaleY:0.57,rotation:39.1,x:573.7,y:518.6},100).to({regX:117.5,regY:117.4,scaleX:0.72,scaleY:0.72,rotation:39.2,x:632.1,y:413.9},80).wait(1));

	// Layer 4
	this.instance_8 = new lib.f5_hinh3();
	this.instance_8.setTransform(339,842.4,0.125,0.125,-9.9,0,0,117.7,117.4);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(13).to({_off:false},0).to({regY:117.3,scaleX:0.77,scaleY:0.77,rotation:-9.8,x:209.1,y:545.2},98).to({regX:117.3,regY:117.6,scaleX:0.96,scaleY:0.96,rotation:-9.9,x:170,y:455.7},88).wait(1));

	// Layer 2
	this.instance_9 = new lib.f5_hinh2();
	this.instance_9.setTransform(361.3,820.2,0.103,0.103,24.9,0,0,130.1,137.3);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(6).to({_off:false},0).to({scaleX:0.72,scaleY:0.72,rotation:24.8,x:575.2,y:252.8},98).to({regX:130,regY:137.5,scaleX:0.9,scaleY:0.9,rotation:24.9,x:636.4,y:90.3},95).wait(1));

	// Layer 1
	this.instance_10 = new lib.f5_hinh1();
	this.instance_10.setTransform(308.4,787.2,0.182,0.182,-20.5,0,0,117.4,117.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_10).to({regX:117.5,regY:117.6,scaleX:0.87,scaleY:0.87,rotation:-27.4,x:97.3,y:233},96).to({regY:117.5,scaleX:1.06,scaleY:1.06,rotation:-29.4,x:39.1,y:80.1},103).wait(1));

	// Layer 7
	this.instance_11 = new lib.f5_text_1();
	this.instance_11.setTransform(385.6,335.4,0.088,0.088,0,0,0,184,83.8);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(77).to({_off:false},0).to({regY:84,scaleX:1.11,scaleY:1.11,x:385.7,alpha:1},8).to({scaleX:0.92,scaleY:0.92,x:385.6},2).to({scaleX:1.1,scaleY:1.1},2).to({scaleX:1,scaleY:1},1).to({_off:true},100).wait(10));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-220.3,0,1180,830.1);


(lib.f4_BANH = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f4_banh1_1();
	this.instance.setTransform(87,163.5,1,1,0,0,0,87,163.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({x:37.7,y:159.9},3,cjs.Ease.get(-0.74)).to({x:41.1},2).wait(3).to({scaleX:0.93,scaleY:0.93,x:72.6,y:158.5},5).wait(127));

	// Layer 2
	this.instance_1 = new lib.f4_banh2_1();
	this.instance_1.setTransform(217,164.6,1,1,0,0,0,79.5,162);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(9).to({x:264,y:160.2},3,cjs.Ease.get(-0.84)).to({x:261,y:162},2).wait(3).to({regY:162.1,scaleX:0.91,scaleY:0.91,rotation:17.4,x:219,y:181.7},5).wait(127));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,296.5,327);


(lib.f3_tia_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_tia_in();
	this.instance.setTransform(385,126,0.789,0.789,0,0,0,384.9,126);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:0.81,scaleY:0.81,y:122,alpha:1},2).to({scaleX:0.83,scaleY:0.83,y:117.8},2).to({scaleX:0.87,scaleY:0.87,y:122.7,alpha:0},3).wait(3));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(81.3,26.6,607.5,198.8);


(lib.f3_face10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face1_in();
	this.instance.setTransform(-230.7,295.8,0.144,0.144,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(8).to({_off:false},0).to({scaleX:1.37,scaleY:1.37,x:382,y:-98.7},25).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face9_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face9_in();
	this.instance.setTransform(35.3,131.6,0.065,0.065,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(27).to({_off:false},0).to({scaleX:1.07,scaleY:1.07,x:660.9,y:-36.3},20).wait(15));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face8_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face8_in();
	this.instance.setTransform(-124.3,361.9,0.083,0.083,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(20).to({_off:false},0).to({scaleX:1,scaleY:1,x:546.3,y:13.3},22).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face7_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face7_in();
	this.instance.setTransform(82.1,427,0.083,0.083,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(17).to({_off:false},0).to({scaleX:1,scaleY:1,x:206,y:-147.2},20).wait(12));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face6_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face6_in();
	this.instance.setTransform(-128.9,272.9,0.109,0.109,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({scaleX:1,scaleY:1,x:277.5,y:-112.4},22).wait(9));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face5_in();
	this.instance.setTransform(105,105,0.126,0.126,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(22).to({_off:false},0).to({regX:105.1,scaleX:1,scaleY:1,rotation:-27.7,x:-526,y:-98.6},20).wait(8));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face4_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face4_in();
	this.instance.setTransform(256.4,174.7,0.109,0.109,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(19).to({_off:false},0).to({scaleX:1,scaleY:1,x:-418.8,y:77.6},28).wait(13));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face3_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face3_in();
	this.instance.setTransform(320.6,228,0.118,0.118,0,0,0,105.1,105.1);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(11).to({_off:false},0).to({regX:105,regY:105,scaleX:1,scaleY:1,x:-277.5,y:-162.9},32).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face2_in();
	this.instance.setTransform(199.5,220.6,0.127,0.127,0,0,0,105,105);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(9).to({_off:false},0).to({scaleX:1,scaleY:1,x:-71.1,y:-154.6},24).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = null;


(lib.f3_face1_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f3_face1_in();
	this.instance.setTransform(364.6,255.4,0.1,0.1,0,0,0,105,105);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:105.1,scaleX:0.72,scaleY:0.72,x:264.5,y:108.8},14,cjs.Ease.get(-0.36)).to({scaleX:1.07,scaleY:1.07,x:261.8,y:-124.2},15).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(354.1,244.9,21,21);


(lib.f2_51_1 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.f2_52();
	this.instance.setTransform(137,190,1,1,0,0,0,137,52.5);

	// Layer 1
	this.instance_1 = new lib.f2_51();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,274,325);


(lib.F2_41 = function() {
	this.initialize();

	// Layer 2
	this.instance = new lib.f1_42();
	this.instance.setTransform(136.5,144.5,1,1,0,0,0,117.5,117.5);

	// Layer 1
	this.instance_1 = new lib.f2_41();

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,273,325);


(lib.bando = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.f16_5_1();
	this.instance.setTransform(46,30,0.448,0.448,0,0,0,89,90.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(25).to({_off:false},0).to({scaleX:1.07,scaleY:1.07,y:10,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},2).wait(145));

	// Layer 1
	this.instance_1 = new lib.f16_1();

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(180));

	// Layer 7
	this.instance_2 = new lib.f16_4_1();
	this.instance_2.setTransform(190,180.1,1,1,0,0,180,71.5,73);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(21).to({_off:false},0).to({x:216,y:181.1,alpha:1},4).wait(155));

	// Layer 6
	this.instance_3 = new lib.f16_4_1();
	this.instance_3.setTransform(-54,180.1,1,1,0,0,0,71.5,73);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(21).to({_off:false},0).to({x:-72,y:181.1,alpha:1},4).wait(155));

	// Layer 4
	this.instance_4 = new lib.f16_Gaptrong();
	this.instance_4.setTransform(6.1,194.1,1,1,0,0,180,58,58);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(19).to({alpha:0},4).wait(157));

	// Layer 3
	this.instance_5 = new lib.f16_Gaptrong();
	this.instance_5.setTransform(140.1,194.1,1,1,0,0,0,58,58);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(19).to({alpha:0},4).wait(157));

	// Layer 2
	this.instance_6 = new lib.f16_2();
	this.instance_6.setTransform(-55.5,142.6);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(180));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-55.5,0,255,313.6);


(lib.Frame30 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Tween46("synched",0);
	this.instance.setTransform(586.2,512.1);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.instance_1 = new lib.Tween47("synched",0);
	this.instance_1.setTransform(586.2,472.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},18).to({state:[{t:this.instance_1}]},10).wait(172));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(18).to({_off:false},0).to({_off:true,y:472.1,alpha:1},10,cjs.Ease.get(1)).wait(172));

	// Layer 2
	this.instance_2 = new lib.f30_text1_1();
	this.instance_2.setTransform(586.2,243.1,0.078,0.078,0,0,0,198.5,104.4);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(9).to({_off:false},0).to({regY:104.6,scaleX:1.1,scaleY:1.1,y:243.2,alpha:1},9,cjs.Ease.get(1)).to({regY:104.5,scaleX:0.95,scaleY:0.95,y:243.1},2).to({scaleX:1,scaleY:1},2).wait(178));

	// Layer 1
	this.instance_3 = new lib.f30_1_1();
	this.instance_3.setTransform(586.2,243,0.301,0.301,0,0,0,299.5,153);
	this.instance_3.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).to({scaleX:1,scaleY:1,alpha:1},12,cjs.Ease.get(1)).wait(188));

	// Layer 5
	this.instance_4 = new lib.f30_32();
	this.instance_4.setTransform(900.4,400.1,0.343,0.343,0,0,0,43,57);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(33).to({_off:false},0).to({scaleX:1,scaleY:1,x:919.2,y:367.1,alpha:1},11,cjs.Ease.get(1)).wait(156));

	// Layer 4
	this.instance_5 = new lib.f30_3_1();
	this.instance_5.setTransform(577.1,452.1,1,1,0,0,0,306,74);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(24).to({_off:false},0).to({alpha:1},13,cjs.Ease.get(1)).wait(163));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(496.1,197,180.1,92);


(lib.f32_5_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f32_5_in();
	this.instance.setTransform(81,105.5,0.516,0.516,0,0,0,85,78.5);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({scaleX:1,scaleY:1,x:93,y:76.5,alpha:1},8,cjs.Ease.get(1)).wait(5).to({scaleX:1.22,scaleY:1.22,x:103.4,y:62.5,alpha:0},4).wait(7));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(37.2,65,87.7,81);


(lib.f29_hinh5 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._031_f29_hinh5_300x300();
	this.instance.setTransform(18.5,18);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(168.5,180.7,1.296,1.289,0,0,0,130,140.1);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,349,373);


(lib.f29_hinh4 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._030_f29_hinh4_300x300();
	this.instance.setTransform(18.5,18);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(168.5,180.7,1.296,1.289,0,0,0,130,140.1);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,349,373);


(lib.f29_hinh3 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._029_f29_hinh3_300x300();
	this.instance.setTransform(18.5,16);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(168.5,180.7,1.296,1.289,0,0,0,130,140.1);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,349,373);


(lib.f29_hinh2 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._028_f29_hinh2_300x300();
	this.instance.setTransform(18.5,18);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(168.5,180.7,1.296,1.289,0,0,0,130,140.1);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,349,373);


(lib.f29_hinh1 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib._027_f29_hinh1_300x300();
	this.instance.setTransform(18.5,15);

	// Layer 2
	this.instance_1 = new lib.f5_bg_white();
	this.instance_1.setTransform(168.5,180.7,1.296,1.289,0,0,0,130,140.1);
	this.instance_1.shadow = new cjs.Shadow("rgba(204,204,204,1)",0,0,7);

	this.addChild(this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-4,-4,349,373);


(lib.f29_h5 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f29_hinh5();
	this.instance.setTransform(147,153.3,0.827,0.827,-11.9,0,0,150,150);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:149,y:151.3},10).to({x:147,y:153.3},7).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-4,346,361);


(lib.f29_h4 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f29_hinh4();
	this.instance.setTransform(132.3,126.1,0.744,0.744,8,0,0,150.1,150.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:133.3,y:128.1},8).to({x:132.3,y:126.1},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-4,297,312);


(lib.f29_h3 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f29_hinh3();
	this.instance.setTransform(131.7,137.9,0.727,0.727,-13.5,0,0,150.1,150.1);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:129.7,y:136.9},10).to({x:131.7,y:137.9},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-4,311,324);


(lib.f29_h2 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f29_hinh2();
	this.instance.setTransform(183.2,192.6,1,1,-14.7,0,0,150,150);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:186.2,y:189.6},7).to({x:183.2,y:192.6},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-4,429,446);


(lib.f29_h1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f29_hinh1();
	this.instance.setTransform(164.3,154.3,0.88,0.88,10.7,0,0,150.1,150);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:161.3,y:152.3},10).to({x:164.3,y:154.3},8).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-4,-4,362,379);


(lib.f27_2_1 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.f27_2_in();
	this.instance.setTransform(167.5,221.5,1,1,0,0,0,167.5,221.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:170.5,y:218.5},9).to({x:167.5,y:221.5},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,0,335.5,443);


(lib.frame22_ = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 5
	this.instance = new lib.f22_hinh3();
	this.instance.setTransform(-199.2,978.5,1,1,13.7,0,0,230.1,164);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(40).to({_off:false},0).to({rotation:11,x:-176.9,y:948.4,alpha:1},10,cjs.Ease.get(1)).to({_off:true},130).wait(19));

	// Layer 10
	this.instance_1 = new lib.f22_3_1();
	this.instance_1.setTransform(-48,1123.7,1,1,17.2,0,0,148.5,100);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(51).to({_off:false},0).to({_off:true},129).wait(19));

	// Layer 9
	this.instance_2 = new lib.f22_2_1();
	this.instance_2.setTransform(-338.6,713.1,0.871,0.871,15.8,0,0,101.9,34.6);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(46).to({_off:false},0).to({_off:true},134).wait(19));

	// Layer 4
	this.instance_3 = new lib.f22_hinh2();
	this.instance_3.setTransform(-126.1,636.6,0.914,0.914,-19.4,0,0,164,230);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(29).to({_off:false},0).to({rotation:-12.7,x:-116.1,y:626.6,alpha:1},11,cjs.Ease.get(1)).to({_off:true},140).wait(19));

	// Layer 14
	this.instance_4 = new lib.f22_text2_1();
	this.instance_4.setTransform(298.4,813.2,0.079,0.079,0,0,0,183.7,50.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(94).to({_off:false},0).to({regX:183.5,scaleX:1.13,scaleY:1.13,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},2).wait(95));

	// Layer 16
	this.instance_5 = new lib.f22_choe();
	this.instance_5.setTransform(443.6,545.2,1,1,0,0,0,79.5,103.1);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(98).to({_off:false},0).wait(101));

	// Layer 13
	this.instance_6 = new lib.f22_text1_1();
	this.instance_6.setTransform(318.4,629,1,1,0,0,0,232.5,27.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(90).to({_off:false},0).to({y:679,alpha:1},8,cjs.Ease.get(1)).to({_off:true},97).wait(4));

	// Layer 12
	this.instance_7 = new lib.f22_5_1();
	this.instance_7.setTransform(-89.1,827.2,1,1,0,0,0,51,127);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(79).to({_off:false},0).to({x:556.9,y:825.3},11,cjs.Ease.get(1)).to({_off:true},94).wait(15));

	// Layer 15 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_79 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_80 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_81 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_82 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_83 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_84 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_85 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_86 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_87 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_88 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_89 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");
	var mask_graphics_90 = new cjs.Graphics().p("Egy6gScMBl1AAAMgGpAjeMhfMABbg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:null,x:0,y:0}).wait(79).to({graphics:mask_graphics_79,x:-371.7,y:818.3}).wait(1).to({graphics:mask_graphics_80,x:-264.1,y:818.3}).wait(1).to({graphics:mask_graphics_81,x:-166.7,y:818.3}).wait(1).to({graphics:mask_graphics_82,x:-79.6,y:818.3}).wait(1).to({graphics:mask_graphics_83,x:-2.8,y:818.3}).wait(1).to({graphics:mask_graphics_84,x:63.9,y:818.3}).wait(1).to({graphics:mask_graphics_85,x:120.2,y:818.3}).wait(1).to({graphics:mask_graphics_86,x:166.3,y:818.3}).wait(1).to({graphics:mask_graphics_87,x:202.2,y:818.3}).wait(1).to({graphics:mask_graphics_88,x:227.8,y:818.3}).wait(1).to({graphics:mask_graphics_89,x:243.2,y:818.3}).wait(1).to({graphics:mask_graphics_90,x:248.3,y:818.3}).wait(109));

	// Layer 11
	this.instance_8 = new lib.f22_4_1();
	this.instance_8.setTransform(264.5,821.3,1,1,0,0,0,312,107);
	this.instance_8._off = true;

	this.instance_8.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(79).to({_off:false},0).to({_off:true},105).wait(15));

	// Layer 8
	this.instance_9 = new lib.f22_1_1();
	this.instance_9.setTransform(-292.5,482.8,0.197,0.197,0,-43.6,136.4,107.8,140.3);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(40).to({_off:false},0).to({regY:140.2,scaleX:0.43,scaleY:0.43,x:-344.5,y:474.7,alpha:1},6,cjs.Ease.get(1)).to({x:-336.5,y:476.7},2).to({_off:true},134).wait(17));

	// Layer 3
	this.instance_10 = new lib.f22_hinh1();
	this.instance_10.setTransform(-146.2,172.4,1,1,-1.8,0,0,204,141.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(18).to({_off:false},0).to({rotation:-11,x:-198.3,y:178.3,alpha:1},12,cjs.Ease.get(1)).to({_off:true},150).wait(19));

	// Layer 6
	this.instance_11 = new lib.Tween38("synched",0);
	this.instance_11.setTransform(-17.9,404.5);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.instance_12 = new lib.Tween39("synched",0);
	this.instance_12.setTransform(-3.9,392.5);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_11}]},30).to({state:[{t:this.instance_12}]},9).to({state:[]},145).wait(15));
	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(30).to({_off:false},0).to({_off:true,x:-3.9,y:392.5,alpha:1},9,cjs.Ease.get(1)).wait(160));

	// Layer 2
	this.instance_13 = new lib.f22_text0_1();
	this.instance_13.setTransform(362.5,182.3,0.129,0.129,0,0,0,276,113.1);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(8).to({_off:false},0).to({regX:275.9,regY:113,scaleX:1.11,scaleY:1.11,x:362.4,alpha:1},8,cjs.Ease.get(1)).to({regX:276,scaleX:1,scaleY:1,x:362.5},2,cjs.Ease.get(-0.15)).to({_off:true},161).wait(20));

	// Layer 1
	this.instance_14 = new lib.f22_bgtext();
	this.instance_14.setTransform(351,219,1,1,0,0,0,351,219);
	this.instance_14.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).to({alpha:1},11,cjs.Ease.get(1)).to({_off:true},169).wait(19));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-449,-41.6,1180,664);


(lib.Frame17 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 10
	this.instance = new lib.f17_gra2_1();
	this.instance.setTransform(563.7,-98.9,1,1,0,0,0,136,128);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(88).to({_off:false},0).to({x:589.8,y:-124.2,alpha:1},12,cjs.Ease.get(1)).wait(41).to({alpha:0},13).wait(36));

	// Layer 9
	this.instance_1 = new lib.f17_gra1_1();
	this.instance_1.setTransform(-230.3,-21.8,1,1,0,0,0,44,39.5);
	this.instance_1.alpha = 0;
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(80).to({_off:false},0).to({x:-274.4,y:-34,alpha:1},13,cjs.Ease.get(1)).wait(33).to({alpha:0},12).wait(52));

	// Layer 7
	this.instance_2 = new lib.f18_hinh3();
	this.instance_2.setTransform(367.6,380,0.934,0.934,26.7,0,0,117.5,117.6);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(80).to({_off:false},0).to({x:522.8,y:199.7,alpha:1},13,cjs.Ease.get(1)).wait(97));

	// Layer 8
	this.instance_3 = new lib.f17_2_1();
	this.instance_3.setTransform(112.1,66.1,4.04,4.04,0,0,0,145.5,64);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(80).to({_off:false},0).to({scaleX:0.86,scaleY:0.86,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},3).wait(99));

	// Layer 2
	this.instance_4 = new lib.f4_banh1_1();
	this.instance_4.setTransform(96.2,541.5,1,1,0,0,0,87,163.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({scaleX:1.35,scaleY:1.35,x:75.3,y:271.2},11,cjs.Ease.get(1)).wait(3).to({rotation:-21.5,x:-404.4,y:256.3},8,cjs.Ease.get(1)).wait(27).to({regY:163.6,scaleX:0.74,scaleY:0.74,x:-118.9,y:154.3},15,cjs.Ease.get(1)).wait(126));

	// Layer 1
	this.instance_5 = new lib.f4_banh2_1();
	this.instance_5.setTransform(224.3,545,1,1,0,0,0,79.5,162);

	this.timeline.addTween(cjs.Tween.get(this.instance_5).to({scaleX:1.35,scaleY:1.35,x:247.8,y:275.9},11,cjs.Ease.get(1)).wait(3).to({regY:162.1,rotation:20,x:750.4,y:241.8},8,cjs.Ease.get(1)).wait(27).to({regX:79.4,scaleX:0.71,scaleY:0.71,rotation:40.2,x:325.2,y:190.7},15,cjs.Ease.get(1)).wait(126));

	// Layer 3
	this.instance_6 = new lib.Tween29("synched",0);
	this.instance_6.setTransform(153.3,197.3,0.203,0.203);
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween30("synched",0);
	this.instance_7.setTransform(174.3,32.1,1.042,1.042);
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(19).to({_off:false},0).to({_off:true,scaleX:1.04,scaleY:1.04,x:174.3,y:32.1},8,cjs.Ease.get(1)).wait(163));
	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(19).to({_off:false},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1,y:33.1},2).wait(20).to({startPosition:0},0).to({scaleX:0.37,scaleY:0.37,x:120.3,y:40.1},15,cjs.Ease.get(1)).wait(10).to({startPosition:10},0).to({scaleX:0.39,scaleY:0.39,startPosition:12},2).to({scaleX:0.05,scaleY:0.05,alpha:0,startPosition:3},5,cjs.Ease.get(-1)).wait(109));

	// Layer 4
	this.instance_8 = new lib.f17_1_1();
	this.instance_8.setTransform(107.1,70.1,2.929,2.929,0,0,0,354,325);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(49).to({_off:false},0).to({scaleX:1,scaleY:1},15,cjs.Ease.get(1)).wait(126));

	// Layer 6
	this.instance_9 = new lib.f18_hình2();
	this.instance_9.setTransform(349.7,-113.2,0.831,0.831,-8,0,0,117.5,117.4);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(73).to({_off:false},0).to({regY:117.5,rotation:38.7,x:615.9,y:-193.2,alpha:1},13,cjs.Ease.get(1)).wait(104));

	// Layer 5
	this.instance_10 = new lib.f18_hinh1();
	this.instance_10.setTransform(-115.8,-113,0.829,0.829,4.5,0,0,117.6,117.5);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(64).to({_off:false},0).to({regX:117.5,rotation:-20.5,x:-271.3,y:-183.1,alpha:1},10,cjs.Ease.get(1)).wait(116));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(9.2,378,294.6,329);


(lib.frame16 = function() {
	this.initialize();

	// Layer 1
	this.instance = new lib.bando();
	this.instance.setTransform(69,95,1,1,0,0,0,69,95);

	this.addChild(this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(-55.5,0,255,313.6);


(lib.frame10 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.f10_heart3_1();
	this.instance.setTransform(83,47,1,1,0,0,0,24,22.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(16).to({x:378,y:35},9).to({x:373},2).wait(113));

	// Layer 2
	this.instance_1 = new lib.f10_heart2_1();
	this.instance_1.setTransform(21.5,8,1,1,0,0,0,41.5,36.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(13).to({x:-322.5},9,cjs.Ease.get(1)).to({x:-315.5},2).wait(116));

	// Layer 1
	this.instance_2 = new lib.f10_heart1_1();
	this.instance_2.setTransform(119,-21.5,1,1,0,0,0,51,45);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(13).to({x:308,y:-24.5},9,cjs.Ease.get(1)).to({x:300},2).wait(116));

	// Layer 5
	this.instance_3 = new lib.f10_1_1();
	this.instance_3.setTransform(86,50,0.067,0.067,0,0,0,357.9,81.5);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(13).to({_off:false},0).to({regX:357.5,scaleX:1.02,scaleY:1.02},6).to({scaleX:1,scaleY:1},3).wait(118));

	// Layer 6
	this.instance_4 = new lib.f10_2_1();
	this.instance_4.setTransform(-283.1,146,1,1,0,0,0,75,74.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(22).to({_off:false},0).wait(118));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-583.9,-302,1180,664);


(lib.frame9 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.f9_banh1();
	this.instance.setTransform(116.4,163.3,1.5,1.5,0,0,0,87,163.5);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-101.6,y:164.8},9,cjs.Ease.get(1)).to({x:-110.6},6,cjs.Ease.get(-1)).to({scaleX:2.23,scaleY:2.23,x:-633.7},8,cjs.Ease.get(-1)).to({_off:true},1).wait(296));

	// Layer 2
	this.instance_1 = new lib.f9_banh2();
	this.instance_1.setTransform(311.5,167,1.5,1.5,0,0,0,79.5,162);

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({x:547.5},9,cjs.Ease.get(1)).to({x:555.5},6,cjs.Ease.get(-1)).to({scaleX:2.27,scaleY:2.27,x:1064.7},8,cjs.Ease.get(-1)).to({_off:true},1).wait(296));

	// Layer 3
	this.instance_2 = new lib.f9_Hinh_Graphic();
	this.instance_2.setTransform(214.1,140.1,0.617,0.617,0,0,0,140.1,140.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).to({regX:140,regY:140,scaleX:1,scaleY:1,x:224.1,y:140},9,cjs.Ease.get(1)).to({x:204.1},12).to({regX:139.9,scaleX:1.08,scaleY:1.08,rotation:-2.5,x:202,y:120},51).to({regX:140,rotation:-18.1,x:-686.1,y:140},11,cjs.Ease.get(1)).to({rotation:-8.4},61).to({x:-1846.1},13,cjs.Ease.get(1)).to({x:-1860.1},53).to({scaleX:1.6,scaleY:1.6,x:-2868.1,y:88,alpha:0},11,cjs.Ease.get(-1)).wait(99));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-14.1,-82,444.9,492);


(lib.frame6 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.f6_Banh();
	this.instance.setTransform(588.2,332.1,0.041,0.041,0,0,0,275.5,235.6);
	this.instance.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance).to({regY:235.5,scaleX:1.09,scaleY:1.09,alpha:1},7,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},3).to({_off:true},81).wait(789));

	// Layer 6
	this.instance_1 = new lib.f6_BanhXoay();
	this.instance_1.setTransform(593.1,316.6,0.267,0.267,0,0,0,-195.8,255.1);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(12).to({_off:false},0).to({regX:-195.7,regY:255,scaleX:1.02,scaleY:1.02},9).to({scaleX:1,scaleY:1},2).wait(65).to({scaleX:0.96,scaleY:0.96},2).to({regX:-195.6,scaleX:2.4,scaleY:2.4,x:593.4,y:316.7,alpha:0},6,cjs.Ease.get(-1)).wait(784));

	// Layer 1
	this.instance_2 = new lib.f6_bg();
	this.instance_2.setTransform(214,0);

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(880));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f11_BANG_ROLL = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 25
	this.instance = new lib.f11_KETQUA3();
	this.instance.setTransform(167.1,-106,1,1,0,0,0,29,16.5);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(64).to({_off:false},0).to({_off:true},139).wait(3));

	// Layer 22
	this.instance_1 = new lib.f11_KETQUA2();
	this.instance_1.setTransform(844.2,-65,1,1,0,0,0,29,16.5);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(53).to({_off:false},0).to({_off:true},147).wait(6));

	// Layer 21
	this.instance_2 = new lib.Tween25("synched",0);
	this.instance_2.setTransform(761.1,-46.6);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.instance_3 = new lib.Tween26("synched",0);
	this.instance_3.setTransform(761.1,-46.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_2}]},44).to({state:[{t:this.instance_3}]},9).to({state:[]},150).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(44).to({_off:false},0).to({_off:true,alpha:1},9,cjs.Ease.get(1)).wait(153));

	// Layer 24
	this.instance_4 = new lib.f11_KETQUA1();
	this.instance_4.setTransform(37,-27,1,1,0,0,0,29,16.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(39).to({_off:false},0).to({_off:true},164).wait(3));

	// Layer 20
	this.instance_5 = new lib.Tween23("synched",0);
	this.instance_5.setTransform(127.1,-6);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.instance_6 = new lib.Tween24("synched",0);
	this.instance_6.setTransform(127.1,-6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_5}]},32).to({state:[{t:this.instance_6}]},7).to({state:[]},164).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(32).to({_off:false},0).to({_off:true,alpha:1},7,cjs.Ease.get(1)).wait(167));

	// Layer 18
	this.instance_7 = new lib.Tween22("synched",0);
	this.instance_7.setTransform(453.1,161.1,0.051,0.051);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.instance_8 = new lib.Tween20("synched",0);
	this.instance_8.setTransform(453.1,161.1,1.148,1.148);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(13).to({_off:false},0).to({_off:true,scaleX:1.15,scaleY:1.15,alpha:1},7,cjs.Ease.get(1)).wait(186));
	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(13).to({_off:false},7,cjs.Ease.get(1)).to({scaleX:0.93,scaleY:0.93},3).to({scaleX:1,scaleY:1},3).to({_off:true},177).wait(3));

	// Layer 2
	this.instance_9 = new lib.f11_2();
	this.instance_9.setTransform(193.7,5.1);

	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({_off:true},200).wait(6));

	// Layer 8
	this.instance_10 = new lib.f11_25_1();
	this.instance_10.setTransform(664.7,-82.5,0.755,0.755,82.2,0,0,30,28.9);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(25).to({_off:false},0).to({_off:true},175).wait(6));

	// Layer 7
	this.instance_11 = new lib.f11_25_1();
	this.instance_11.setTransform(197.1,16.6,0.429,0.429,-32.7,0,0,30.1,29);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(22).to({_off:false},0).wait(10).to({alpha:0},5).to({_off:true},163).wait(6));

	// Layer 16
	this.instance_12 = new lib.f11_icon1_1();
	this.instance_12.setTransform(368.1,88.1,0.136,0.136,0,0,0,58.5,56.6);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(25).to({_off:false},0).to({regX:58.6,regY:56.5,scaleX:0.56,scaleY:0.56,alpha:1},6,cjs.Ease.get(1)).to({_off:true},170).wait(5));

	// Layer 13
	this.instance_13 = new lib.f11_icon1_1();
	this.instance_13.setTransform(615.2,-0.9,0.097,0.097,0,0,0,58.6,56.5);
	this.instance_13.alpha = 0;
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(17).to({_off:false},0).to({regX:58.5,scaleX:1,scaleY:1,alpha:1},7,cjs.Ease.get(1)).to({_off:true},176).wait(6));

	// Layer 14
	this.instance_14 = new lib.f11_icon3_1();
	this.instance_14.setTransform(546.1,30,0.107,0.107,0,0,0,58.4,56.1);
	this.instance_14.alpha = 0;
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(20).to({_off:false},0).to({regX:58.5,regY:56,scaleX:0.82,scaleY:0.82,x:546.2,alpha:1},7,cjs.Ease.get(1)).to({_off:true},174).wait(5));

	// Layer 10
	this.instance_15 = new lib.f11_icon3_1();
	this.instance_15.setTransform(272.1,80,0.068,0.068,0,0,0,58.5,56.3);
	this.instance_15.alpha = 0;
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(14).to({_off:false},0).to({regY:56,scaleX:1,scaleY:1,alpha:1},6,cjs.Ease.get(1)).to({_off:true},180).wait(6));

	// Layer 6
	this.instance_16 = new lib.f11_25_1();
	this.instance_16.setTransform(332.1,-150,1,1,0,0,0,26,19);
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(19).to({_off:false},0).to({_off:true},181).wait(6));

	// Layer 9
	this.instance_17 = new lib.f11_NUMBER();
	this.instance_17.setTransform(428.1,-19,0.055,0.055,0,0,0,137.8,72.5);
	this.instance_17.alpha = 0;
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(19).to({_off:false},0).to({regX:137.5,scaleX:1.03,scaleY:1.03,alpha:1},8,cjs.Ease.get(1)).to({regX:137.6,scaleX:0.94,scaleY:0.94,x:428.2},2).to({regX:137.5,scaleX:1,scaleY:1,x:428.1},2).to({_off:true},169).wait(6));

	// Layer 3
	this.instance_18 = new lib.f11_21_1();
	this.instance_18.setTransform(444.1,94.5,1,1,0,0,0,176,175.5);
	this.instance_18.alpha = 0;
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(10).to({_off:false},0).to({y:14.5,alpha:1},9,cjs.Ease.get(1)).to({_off:true},181).wait(6));

	// Layer 11
	this.instance_19 = new lib.f11_icon2_1();
	this.instance_19.setTransform(243.1,30,0.132,0.132,0,0,0,48.1,45);
	this.instance_19.alpha = 0;
	this.instance_19._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(18).to({_off:false},0).to({regX:48,scaleX:1,scaleY:1,alpha:1},8,cjs.Ease.get(1)).to({_off:true},175).wait(5));

	// Layer 17
	this.instance_20 = new lib.f11_icon3_1();
	this.instance_20.setTransform(630.2,-53,0.68,0.68,0,0,0,58.6,56);
	this.instance_20.alpha = 0;
	this.instance_20._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_20).wait(26).to({_off:false},0).to({alpha:1},7,cjs.Ease.get(1)).to({_off:true},168).wait(5));

	// Layer 15
	this.instance_21 = new lib.f11_icon2_1();
	this.instance_21.setTransform(679.2,-3.9,0.061,0.061,0,0,0,48.3,45);
	this.instance_21.alpha = 0;
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(22).to({_off:false},0).to({regX:48,scaleX:0.69,scaleY:0.69,alpha:1},7,cjs.Ease.get(1)).to({_off:true},172).wait(5));

	// Layer 12
	this.instance_22 = new lib.f11_icon1_1();
	this.instance_22.setTransform(257.1,-24,0.624,0.624,0,0,0,58.5,56.5);
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(22).to({_off:false},0).to({_off:true},178).wait(6));

	// Layer 26
	this.instance_23 = new lib.f11_line2_1();
	this.instance_23.setTransform(458,-372,1,1,0,0,0,40.5,245.5);
	this.instance_23.alpha = 0;
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(64).to({_off:false},0).to({alpha:1},11,cjs.Ease.get(1)).wait(131));

	// Layer 23
	this.instance_24 = new lib.Tween27("synched",0);
	this.instance_24.setTransform(222,-60.3);
	this.instance_24.alpha = 0;
	this.instance_24._off = true;

	this.instance_25 = new lib.Tween28("synched",0);
	this.instance_25.setTransform(222,-60.3);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_24}]},56).to({state:[{t:this.instance_25}]},8).to({state:[]},139).wait(3));
	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(56).to({_off:false},0).to({_off:true,alpha:1},8,cjs.Ease.get(1)).wait(142));

	// Layer 1
	this.instance_26 = new lib.f11_1();

	this.timeline.addTween(cjs.Tween.get(this.instance_26).to({_off:true},200).wait(6));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,878.7,303.1);


(lib.f3_faces = function() {
	this.initialize();

	// Layer 11
	this.instance = new lib.f3_face10();
	this.instance.setTransform(1034.8,174.3,1,1,0,0,0,105,105);

	// Layer 10
	this.instance_1 = new lib.f3_face9_1();
	this.instance_1.setTransform(769.7,395.4,1,1,0,0,0,105,105);

	// Layer 9
	this.instance_2 = new lib.f3_face8_1();
	this.instance_2.setTransform(855.9,263.3,1,1,0,0,0,105,105);

	// Layer 8
	this.instance_3 = new lib.f3_face7_1();
	this.instance_3.setTransform(748.6,134,1,1,0,0,0,105,105);

	// Layer 7
	this.instance_4 = new lib.f3_face6_1();
	this.instance_4.setTransform(922,131.2,1,1,0,0,0,105,105);

	// Layer 6
	this.instance_5 = new lib.f3_face5_1();
	this.instance_5.setTransform(506.4,443.1,1,1,0,0,0,105,105);

	// Layer 5
	this.instance_6 = new lib.f3_face4_1();
	this.instance_6.setTransform(387.2,370.9,1,1,0,0,0,105,105);

	// Layer 4
	this.instance_7 = new lib.f3_face3_1();
	this.instance_7.setTransform(286.2,322.9,1,1,0,0,0,105,105);

	// Layer 3
	this.instance_8 = new lib.f3_face2_1();
	this.instance_8.setTransform(393.6,174.3,1,1,0,0,0,105,105);

	// Layer 2
	this.instance_9 = new lib.f3_face1_1();
	this.instance_9.setTransform(233,105,1,1,0,0,0,105,105);

	this.addChild(this.instance_9,this.instance_8,this.instance_7,this.instance_6,this.instance_5,this.instance_4,this.instance_3,this.instance_2,this.instance_1,this.instance);
}).prototype = p = new cjs.Container();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.f2_xxx = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 3
	this.instance = new lib.Tween1("synched",0);
	this.instance.setTransform(590,486.2,5.171,5.171);
	this.instance._off = true;

	this.instance_1 = new lib.Tween2("synched",0);
	this.instance_1.setTransform(590,486.2,0.923,0.923);
	this.instance_1._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(15).to({_off:false},0).to({_off:true,scaleX:0.92,scaleY:0.92},8,cjs.Ease.get(1)).wait(337));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(15).to({_off:false},8,cjs.Ease.get(1)).to({scaleX:1.09,scaleY:1.09},2).to({scaleX:1,scaleY:1},2).wait(94).to({startPosition:0},0).to({y:1120.1},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 9
	this.instance_2 = new lib.f4_Xanh_1();
	this.instance_2.setTransform(591,417.7,1,2.473,0,0,0,139.5,131.1);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(87).to({_off:false},0).to({scaleY:2.18,alpha:1},1).to({scaleY:0.42},11,cjs.Ease.get(1)).wait(22).to({y:1051.7},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 10
	this.instance_3 = new lib.f4_BANH();
	this.instance_3.setTransform(593.1,403.6,0.066,0.066,0,0,0,146.7,164.1);
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(96).to({_off:false},0).to({regX:147,regY:164.2,scaleX:1.09,scaleY:1.09,rotation:-12.7,x:586.5,y:245.8},6,cjs.Ease.get(1)).to({regX:147.1,regY:164.1,scaleX:1,scaleY:1,x:586.6,y:262.2},2).wait(17).to({y:896.2},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 11
	this.instance_4 = new lib.f5_5Hinh_();
	this.instance_4.setTransform(384.5,-415,1,1,0,0,0,130,137.5);
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(116).to({_off:false},0).wait(16).to({y:74.8},46).wait(182));

	// Layer 6
	this.instance_5 = new lib.f2_51_1();
	this.instance_5.setTransform(591.5,256.1,0.004,1,0,0,0,137.1,162.5);
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(42).to({_off:false},0).to({regX:136.6,scaleX:1.02,x:592.3},4,cjs.Ease.get(1)).to({regX:136.5,scaleX:1,x:592.2},2).wait(39).to({regX:137.4,regY:324.9,x:593.1,y:418.5},0).to({scaleY:0.65,alpha:0},3).to({scaleY:0.17},8).wait(23).to({y:1052.5},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 5
	this.instance_6 = new lib.F2_41();
	this.instance_6.setTransform(592.2,338.6,1,1,0,0,0,136.5,162.5);

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:256.1},11,cjs.Ease.get(1)).wait(27).to({regX:136.6,scaleX:0,x:591.5},4,cjs.Ease.get(1)).to({_off:true},79).wait(239));

	// Layer 4
	this.instance_7 = new lib.f1_3();
	this.instance_7.setTransform(590,676.7,1,1,0,0,0,164.5,38);

	this.timeline.addTween(cjs.Tween.get(this.instance_7).to({y:594.2},11,cjs.Ease.get(1)).wait(110).to({y:1228.1},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 2
	this.instance_8 = new lib.Tween3("synched",0);
	this.instance_8.setTransform(590,606.7);

	this.instance_9 = new lib.Tween4("synched",0);
	this.instance_9.setTransform(590,524.2);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).to({_off:true,y:524.2},11,cjs.Ease.get(1)).wait(349));
	this.timeline.addTween(cjs.Tween.get(this.instance_9).to({_off:false},11,cjs.Ease.get(1)).wait(110).to({startPosition:0},0).to({y:1158.1},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 8
	this.instance_10 = new lib.f3_faces();
	this.instance_10.setTransform(489.1,174.3,1,1,0,0,0,489.1,174.3);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(48).to({_off:false},0).wait(85).to({alpha:0},21).to({_off:true},1).wait(205));

	// Layer 7
	this.instance_11 = new lib.f3_tia_1();
	this.instance_11.setTransform(588,304.6,0.272,0.272,0,0,0,385,126);
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(20).to({_off:false},0).to({scaleX:1,scaleY:1},6,cjs.Ease.get(1)).wait(95).to({y:938.6},45,cjs.Ease.get(-0.2)).wait(194));

	// Layer 1
	this.instance_12 = new lib.f2_1();

	this.instance_13 = new lib.Tween7("synched",0);
	this.instance_13.setTransform(590,332);
	this.instance_13._off = true;

	this.instance_14 = new lib.Tween8("synched",0);
	this.instance_14.setTransform(590,966);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[{t:this.instance_12}]}).to({state:[{t:this.instance_13}]},121).to({state:[{t:this.instance_14}]},45).wait(194));
	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(121).to({_off:false},0).to({_off:true,y:966},45,cjs.Ease.get(-0.2)).wait(194));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1180,741.2);


(lib.Frame32 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 8
	this.instance = new lib.f32_text_1();
	this.instance.setTransform(586.1,453.1,1,1,0,0,0,295.5,62);
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({_off:false},0).wait(156));

	// Layer 3
	this.instance_1 = new lib.f32_2_1();
	this.instance_1.setTransform(597.1,519.1,1,1,0,0,0,424,137.5);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:469.1,alpha:1},11,cjs.Ease.get(1)).wait(169));

	// Layer 6
	this.instance_2 = new lib.f32_5_1();
	this.instance_2.setTransform(780.2,205.1,1,1,0,0,0,85,78.5);
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(23).to({_off:false},0).wait(157));

	// Layer 5
	this.instance_3 = new lib.f32_4_1();
	this.instance_3.setTransform(678.2,478.1,1,1,0,0,0,120.5,114);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(15).to({_off:false},0).to({y:342.1,alpha:1},9,cjs.Ease.get(1)).to({y:348.1},2).wait(154));

	// Layer 4
	this.instance_4 = new lib.f32_3_1();
	this.instance_4.setTransform(516.1,480.1,1,1,0,0,0,87,88.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(8).to({_off:false},0).to({y:326.1,alpha:1},9).to({y:330.1},2).wait(161));

	// Layer 7
	this.instance_5 = new lib.f32_6_1();
	this.instance_5.setTransform(615.1,472.1,1,1,0,0,0,138.5,195);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(27).to({_off:false},0).to({y:232.1,alpha:1},9,cjs.Ease.get(1)).wait(144));

	// Layer 2
	this.instance_6 = new lib.f32_1_1();
	this.instance_6.setTransform(592.1,450.1,1,1,0,0,0,388.5,209);
	this.instance_6.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).to({y:420.1,alpha:1},11,cjs.Ease.get(1)).wait(169));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.frame30_Out = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Frame30();
	this.instance.setTransform(-196.6,-44,1,1,0,0,0,299.5,153);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({x:-194.6,y:-46},11).to({x:-196.6,y:-44},11).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,180.1,92);


(lib.Frame27 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 2
	this.instance = new lib.f27_1_1();
	this.instance.setTransform(165.7,-61,3.475,3.475,0,0,0,341.4,62);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(24).to({_off:false},0).to({regX:341.5,scaleX:0.92,scaleY:0.92,x:166,alpha:1},8,cjs.Ease.get(1)).to({scaleX:1,scaleY:1},2).wait(27).to({regX:341.4,scaleX:1.03,scaleY:1.03,x:165.9},2).to({regX:341.2,regY:62.2,scaleX:0.08,scaleY:0.08,alpha:0},5,cjs.Ease.get(-1)).to({_off:true},9).wait(123));

	// Layer 1
	this.instance_1 = new lib.f27_2_1();
	this.instance_1.setTransform(167.5,281.5,1,1,0,0,0,167.5,221.5);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:221.5,alpha:1},13,cjs.Ease.get(1)).wait(55).to({x:177.5},2).to({x:-122.5,y:201.5},7,cjs.Ease.get(1)).wait(123));

	// Layer 8
	this.instance_2 = new lib.f28_4_1();
	this.instance_2.setTransform(490.1,415.1,0.262,0.262,0,0,0,51.5,49);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(114).to({_off:false},0).to({scaleX:1,scaleY:1,x:455.1,y:411.1,alpha:1},11,cjs.Ease.get(1)).wait(75));

	// Layer 7
	this.instance_3 = new lib.f28_Cosy();
	this.instance_3.setTransform(863.2,315.1,1,1,0,0,0,185.2,175);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(106).to({_off:false},0).to({x:573.2,alpha:1},12,cjs.Ease.get(1)).wait(82));

	// Layer 6
	this.instance_4 = new lib.f27_text();
	this.instance_4.setTransform(305.1,106,0.046,0.046,0,0,0,243.2,140.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(98).to({_off:false},0).to({regX:243.5,scaleX:1.04,scaleY:1.04,alpha:1},10,cjs.Ease.get(1)).to({scaleX:0.96,scaleY:0.96},2).to({scaleX:1,scaleY:1},2).wait(28).to({scaleX:1.06,scaleY:1.06},3).to({scaleX:1,scaleY:1},3).wait(54));

	// Layer 5
	this.instance_5 = new lib.f28_3_1();
	this.instance_5.setTransform(373.8,149,1,1,-56.2,0,0,266.1,217.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(93).to({_off:false},0).to({regX:266,rotation:0,x:408.1,y:236,alpha:1},13,cjs.Ease.get(1)).wait(94));

	// Layer 4
	this.instance_6 = new lib.f28_2_1();
	this.instance_6.setTransform(409.1,-50,1,1,0,0,0,78.5,60.5);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(86).to({_off:false},0).to({x:498.1,y:5,alpha:1},12,cjs.Ease.get(1)).wait(102));

	// Layer 3
	this.instance_7 = new lib.f28_1_1();
	this.instance_7.setTransform(115,24.1,1,1,-4.2,0,0,295,129);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(77).to({_off:false},0).to({rotation:0,x:176,y:6,alpha:1},12,cjs.Ease.get(1)).wait(111));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-0.4,60,335.5,443);


(lib.Fram29 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 9
	this.instance = new lib.Tween43("synched",0);
	this.instance.setTransform(559.2,362.6,0.054,0.054);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.instance_1 = new lib.Tween42("synched",0);
	this.instance_1.setTransform(559.2,362.6,1.084,1.084);
	this.instance_1._off = true;

	this.instance_2 = new lib.Tween40("synched",0);
	this.instance_2.setTransform(559.2,362.6,0.954,0.954);
	this.instance_2._off = true;

	this.instance_3 = new lib.Tween41("synched",0);
	this.instance_3.setTransform(559.2,362.6);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance}]},34).to({state:[{t:this.instance_1}]},8).to({state:[{t:this.instance_2}]},2).to({state:[{t:this.instance_3}]},2).wait(114));
	this.timeline.addTween(cjs.Tween.get(this.instance).wait(34).to({_off:false},0).to({_off:true,scaleX:1.08,scaleY:1.08,alpha:1},8,cjs.Ease.get(0.87)).wait(118));
	this.timeline.addTween(cjs.Tween.get(this.instance_1).wait(34).to({_off:false},8,cjs.Ease.get(0.87)).to({_off:true,scaleX:0.95,scaleY:0.95},2).wait(116));
	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(42).to({_off:false},2).to({_off:true,scaleX:1,scaleY:1},2).wait(114));

	// Layer 8
	this.instance_4 = new lib.f29_3_1();
	this.instance_4.setTransform(620.1,149.1,0.584,0.584,0,0,0,115.5,119.5);
	this.instance_4.alpha = 0;
	this.instance_4._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_4).wait(48).to({_off:false},0).to({scaleX:1,scaleY:1,x:674.2,y:104.1,alpha:1},12,cjs.Ease.get(1)).wait(100));

	// Layer 7
	this.instance_5 = new lib.f29_2_1();
	this.instance_5.setTransform(363.1,273.9,1,1,0,0,0,51.5,36.5);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(38).to({_off:false},0).to({x:333.1,alpha:1},11).wait(111));

	// Layer 6
	this.instance_6 = new lib.Tween44("synched",0);
	this.instance_6.setTransform(-216,399.1);
	this.instance_6.alpha = 0;
	this.instance_6._off = true;

	this.instance_7 = new lib.Tween45("synched",0);
	this.instance_7.setTransform(-237,393.1);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_6}]},28).to({state:[{t:this.instance_7}]},10).wait(122));
	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(28).to({_off:false},0).to({_off:true,x:-237,y:393.1,alpha:1},10).wait(122));

	// Layer 4
	this.instance_8 = new lib.f29_h4();
	this.instance_8.setTransform(156.4,373.9,1,1,0,0,0,142.7,150.3);
	this.instance_8.alpha = 0;
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(28).to({_off:false},0).to({x:165.4,y:326.9,alpha:1},13,cjs.Ease.get(1)).wait(119));

	// Layer 2
	this.instance_9 = new lib.f29_h2();
	this.instance_9.setTransform(455.8,101.5,1,1,0,0,0,208.8,217.5);
	this.instance_9.alpha = 0;
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(9).to({_off:false},0).to({x:439.8,y:76.5,alpha:1},13,cjs.Ease.get(1)).wait(138));

	// Layer 3
	this.instance_10 = new lib.f29_h3();
	this.instance_10.setTransform(-73.8,340.8,1,1,0,0,0,149.8,156.3);
	this.instance_10.alpha = 0;
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(19).to({_off:false},0).to({x:-79.8,y:308.8,alpha:1},13,cjs.Ease.get(1)).wait(128));

	// Layer 1
	this.instance_11 = new lib.f29_h1();
	this.instance_11.setTransform(141,183.7,1,1,0,0,0,175.2,183.7);
	this.instance_11.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).to({x:151,y:127.5,alpha:1},13).wait(147));

	// Layer 5
	this.instance_12 = new lib.f29_h5();
	this.instance_12.setTransform(-68.7,103.5,1,1,0,0,0,167.2,174.8);
	this.instance_12.alpha = 0;
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(34).to({_off:false},0).to({x:-81.7,y:62.5,alpha:1},15,cjs.Ease.get(1)).wait(111));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(-38.2,-4,362,379);


(lib.Frame11 = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 9 (mask)
	var mask = new cjs.Shape();
	mask._off = true;
	var mask_graphics_0 = new cjs.Graphics().p("AkDNBQgGgMgehNQgUg2gdghQgEAAgTAFQgSAFgOAAQg6AAgbgvQgPgcAAgdQAAhYAehBQA0hxCAAAQBLAAAhAPQARAIASAMIAbgFIEsgCQBLgfAagrQALgTAEgbQgkhGgohzQgfhbgdhrQgWhXAAgKQgBhkABgaQAAghAKgXQAqhQBEiwQAIgWABghIAAg4QAAhGAmglQBdhaAwBLQANAVAIAcQASAiAOAzQgNAhgKA0QgOBHgEAKQgEB3gRCwIgdEiQgQCuglB5QgzCihnB+Qg+BLhqDEQg7BvgfBNQiZg1gyhfg");
	var mask_graphics_1 = new cjs.Graphics().p("AjMPgQgFgNAAg+Qivg1g2hoQgGgMgehNQgVg2gdghQgEAAgSAFQgTAFgOAAQg6AAgagvQgPgcgBgdQAAhYAehBQA1hxB/AAQBMAAAgAPQASAIARAMIAbgFIEtgCQBLgfAagrQALgTAEgbQglhGgohxQgfhdgahrQgXhXAAgKQAAhkAAgaQABghAJgXQAphQBDiwQAIgWABghIAAg4QAAhGAnglQBdhaAvBLQANAVAJAcQAdA4ATBnQBTBxBOA9QApAfBBAfQAsAVANAYQASAhAABbQAAC+hOCwQg/CNimDpQgYAihJA8IhnBUQgqAlgWA2QgOAggTBAQgUA4gSBRQgUBdgGAUQgOA1gZAYQgdAcg0AAQhMAAgTg+g");
	var mask_graphics_2 = new cjs.Graphics().p("ALoSXQgngFgdAAQhaAAgWg/QgHgSgCgdQgCgZgCgEIgCgHQgKAEgSAFQgXAGgRAAQgiAAgZgKQgVgJgKgOIgDAgQgEAhgKAaQgeBThOAAQiAAAhihfQgYgXgtg3QgngvgVgQQgNAFgLATIgTAjQgbAvg1AAQh4AAglh7IgBgEQgSAGgYAAQhNAAgTg+QgEgNAAg/Qivg1g2hoQgGgLgehNQgVg2gdghQgEAAgTAFQgSAFgOAAQg6AAgagwQgQgbAAgdQAAhZAehBQA0hwCAAAQBLAAAhAPQARAIASAMIAbgFIEvgDQBKgeAagrQAMgTADgcQgkhDgoh0QgfhdgchrQgXhXAAgJQgBhlABgZQABgiAJgWQArhQBDixQAIgWABggIAAg5QAAhGAngkQBdhaAvBLQANAVAIAbQAcA4ATBnQBSByBOA8QApAgBBAeQAtAWANAYQASAfAABTQAagDAdAAIAXgZQAdgcAbgPQBagzBCBUQASAcAMA0QAIAdAMA9IBAgMQAygIAQAAQAtAAAeAjQAXAcACAdIAAHmQAXBgAFCQQACAxAAC/IAFBgQAEBpgJBUQgcEKiOAAQgWAAgogFg");
	var mask_graphics_3 = new cjs.Graphics().p("AMbS2Igii2QgShigSgiQgjg/hUhHQgZgVgUgUQglDQh8AAQgXAAgogFQgngFgdAAQhZAAgXg/QgHgSgCgdQgBgZgCgEIgDgHQgKAEgSAFQgXAGgQAAQgjAAgZgKQgTgJgJgOIgEAgQgEAhgJAaQgfBThNAAQiBAAhihfQgYgXgug3QgogvgVgQQgNAFgKATIgUAjQgbAvg0AAQh5AAglh7IgBgEQgSAGgYAAQhMAAgTg+QgFgNAAg/Qivg1g2hoQgGgLgehNQgVg2gdghQgEAAgSAFQgTAFgOAAQg6AAgagwQgPgbgBgdQAAhZAeg/QA1hwB/AAQBMAAAgAPQASAIARAMIAbgFIEvgDQBLgeAagrQALgTAEgcQglhFgoh0QgfhdgchrQgXhXAAgJQAAhlAAgZQABgiAJgWQArhQBDixQAIgWABggIAAg5QAAhGAngkQBdhaAvBLQANAVAJAbQAdA4ATBnQBTByBOA8QApAgBBAeQAsAWANAYQASAfAABTQAagDAbAAIAYgZQAcgcAbgPQBagzBCBUQASAcANA0QAHAdANA9IA/gMQAygIAQAAQAkAAAaAWIAIgMQBEhkA6AAQAcAAApAbQAUAOARAOIB9gvQCFgyA/gNQBFgPAYgwIASgrQANgaAVgSQBqhbAwCUQAZBLAEBoQgBAOgdBTQgiBdgLBbQgNBXgMC/QgIBmgmCnIhAEUQgkCtgTDkQgLB/gODZQgPCngcBIQghBahFAAQhuAAgyjlg");
	var mask_graphics_4 = new cjs.Graphics().p("ATeYsQgggeADgUQjQgghIgwQiGgLiUhvQgxglgiglQhogIgwjcIgii2QgShigSgiQgjg/hUhHQgZgVgUgUQglDQh8AAQgXAAgmgFQgngFgdAAQhZAAgXg/QgHgSgCgdQgBgZgCgEIgDgHQgKAEgSAFQgXAGgQAAQgjAAgZgKQgVgJgJgOIgEAgQgEAhgJAaQgfBThNAAQiBAAhihfQgYgXgug3QgogvgVgQQgNAFgKATIgUAjQgbAvg0AAQh5AAglh7IgBgEQgSAGgYAAQhMAAgTg+QgFgNAAg/Qivg1g2hoQgGgLgehNQgVg2gdghQgEAAgSAFQgTAFgOAAQg6AAgaguQgPgbgBgdQAAhZAehBQA1hwB/AAQBMAAAgAPQASAIARAMIAbgFIEvgDQBLgeAagrQALgTAEgcQglhFgoh0QgfhdgchrQgXhXAAgJQAAhlAAgZQABgiAJgWQArhQBDixQAIgWABggIAAg5QAAhGAngkQBdhaAvBLQANAVAJAbQAdA4ATBnQBTByBOA8QApAgBBAeQAsAWANAYQASAfAABTQAagDAdAAIAYgZQAcgcAbgPQBagzBCBUQASAcANA0QAHAdANA9IA/gMQAwgIAQAAQAkAAAaAWIAIgMQBEhkA6AAQAcAAApAbQAUAOARAOIB9gvQCFgyA/gNQBFgPAYgwIASgrQANgaAVgSQBhhVAxB4QAWgKAeAAQAdAAAoAWQBqikB+CUQApAwAuBXIA5BtQCiAaBNDzQAyCjAADBQABAUgGAbQgFAdgJAVIAAGEIgKAeIAAIOQAFD+giDLQgyEoh5AAQggAAgegaIgSAQQhqBagZAAQgrAAgjgjg");
	var mask_graphics_5 = new cjs.Graphics().p("AZkYgQgGgTgJgtQgHgjgIgJQg3hDhlgHQhtADgqgJIAAAdQAAAfgFAXQgSBNhSAAQhNAAgzhnQgpA/g4AAQggAAgegaIgSAQQhpBagZAAQgsAAgjgjQgfgeACgUQjPgghJgwQiFgLiVhvQgxglgiglQhlgIgxjcIgii2QgShigSgiQgjg/hUhHQgYgVgUgUQglDQh9AAQgWAAgogFQgogFgcAAQhaAAgXg/QgHgSgBgdQgCgZgCgEIgDgHQgJAEgTAFQgWAGgRAAQgiAAgZgKQgWgJgJgOIgDAgQgFAhgJAaQgfBThNAAQiBAAhihfQgYgXgug3QgngvgWgQQgMAFgLATIgTAjQgcAvg0AAQh5AAgkh7IgBgEQgTAGgXAAQhNAAgTg+QgEgNAAg/Qiwg1g2hoQgGgLgehNQgVg2gcgfQgFAAgSAFQgSAEgOAAQg7AAgagvQgPgbAAgdQAAhZAehBQA0hwCAAAQBLAAAhAPQARAIARAMIAcgFIEugDQBLgeAagrQALgTAEgcQglhFgnh0QgghdgchrQgWhXAAgJQgBhlABgZQAAgiAJgWQArhQBEixQAIgWABggIgBg5QABhGAmgkQBdhaAvBLQAOAVAIAbQAeA4ASBnQBTByBOA8QApAgBBAeQAtAWANAYQARAfABBTQAZgDAeAAIAXgZQAcgcAcgPQBZgzBDBUQARAcANA0QAHAdANA9IBAgMQAxgIARAAQAkAAAZAWIAIgMQBEhkA6AAQAcAAApAbQAUAOARAOIB9gvQCDgyA/gNQBGgPAXgwIASgrQANgaAVgSQBihVAxB4QAWgKAdAAQAdAAApAWQBpikB+CUQApAwAuBXIA5BtQCjAaBMDzIALAlIAKgBQAogHANgcQAGgNAFgrQADgkAPgPQAWgXA4AAQA2AAAcAxQASAfAAAZIAAACQBZgRCUgMQBwgTAphNQAnhLAYgWQAUgTAUALQANAGAfAZQAhAWAoAAQBeAABMFCQA0DcAaEWQAXDpAKC2QAKC+AADWIgCCoQAACoACBZQABA0grA5Qg0A5gQAdQAAAHAFAiQAEAlgEAbQgNBVhdAAQh2AAgIh+QgDgmAIg4IAIg8QgDAAgIgFIgTgKQgoBig8BnQhTCQgsAAQhUAAgdhSg");
	var mask_graphics_6 = new cjs.Graphics().p("APxYgQgGgTgJgtQgHgjgIgJQg3hDhlgHQhtADgqgJIAAAdQAAAfgFAXQgSBNhSAAQhNAAgzhnQgpA/g4AAQggAAgegaIgSAQQhpBagZAAQgsAAgjgjQgfgeACgUQjNgghJgwQiFgLiVhvQgxglgiglQhngIgxjcIgii2QgShigSgiQgjg/hUhHQgYgVgUgUQglDQh9AAQgWAAgogFQgogFgcAAQhaAAgXg/QgHgSgBgdQgCgZgCgEIgDgHQgJAEgTAFQgWAGgRAAQgiAAgZgKQgWgJgJgOIgDAgQgFAhgJAaQgfBThNAAQiBAAhihfQgYgXgug3QgngvgWgQQgMAFgLATIgTAjQgcAvg0AAQh5AAgkh7IgBgEQgTAGgXAAQhNAAgTg+QgEgNAAg/Qiwg1g2hoQgGgLgehNQgVg2gcgfQgFAAgSAFQgSAEgOAAQg7AAgagvQgPgbAAgdQAAhZAehBQA0hwCAAAQBLAAAhAPQARAIARAMIAcgFIEugDQBLgeAagrQALgTAEgcQglhFgnh0QgghdgchrQgWhXAAgJQgBhlABgZQAAgiAJgWQArhQBEixQAIgWABggIgBg5QABhGAmgkQBdhaAvBLQAOAVAIAbQAeA4ASBnQBTByBOA8QApAgBBAeQAtAWANAYQARAfABBTQAZgDAeAAIAXgZQAcgcAcgPQBZgzBDBUQARAcANA0QAHAdANA9IBAgMQAxgIARAAQAkAAAZAWIAIgMQBEhkA6AAQAcAAApAbQAUAOARAOIB9gvQCFgyA/gNQBGgPAXgwIASgrQANgaAVgSQBihVAxB4QAWgKAdAAQAdAAApAWQBnikB+CUQApAwAuBXIA5BtQCjAaBMDzIALAlIAKgBQAogHANgcQAGgNAFgrQADgkAPgPQAWgXA4AAQA2AAAcAxQASAfAAAZIAAACQBZgRCUgMQBwgTAphNQAnhLAYgWQAUgTAUALQANAGAfAZQAhAWAoAAQARAAAQAKIACgNQAViJBVAAQAiAAAuAfQAQALAzArQAVgpgFg5QgJg9ABgZQAAgqAfgTQAngXBjAAQAlAAA+gtIBnhPQA4glAgAOQApASAPBmQAqAAA0AUQAaAKAUAKQAmAAAVgZQAMgOAQgpQAPgnAQgQQAagZAuAAQAMAAAYAMQAXANAMAAQALAAAogSQAogRASAAQBKAAA4FwQAtEkAFEJIAUG+QAUGdAABMIADCiQACCcgFB2QgPF1hVAAQhkAAgRhhQgFgdABgyQABgvgDgMQgyABghgNQgigNgIAAQgrAAi8BmQi4BogMAEQgGACigARQiUAQgLAPQgWAlgPATQgaAiglAAQhIAAglhYQgmAsgNAYQAAAHAFAiQAEAlgEAbQgNBVhdAAQh2AAgIh+QgDgmAIg4IAIg8QgDAAgIgFIgTgKQgoBig8BnQhTCQgsAAQhUAAgdhSg");
	var mask_graphics_7 = new cjs.Graphics().p("AFQZBQgGgSgJguQgHgigIgKQg3hChkgIQhuADgpgJIAAAdQAAAggEAXQgRBMhTAAQhNAAgzhnQgpA/g4AAQggAAgegaIgSAQQhpBagZAAQgsAAgjgiQgfgeADgVQjQgfhIgxQiGgKiVhwQgxgkgiglQhngIgxjdIghi1QgThjgSghQgjg/hUhHQgYgWgUgTQglDPh9AAQgWAAgogFQgngFgdAAQhaAAgXg+QgGgTgCgcQgCgagCgDIgCgIQgKAFgSAFQgXAFgRAAQgiAAgZgKQgWgJgJgNIgDAfQgEAhgKAaQgeBThOAAQiBAAhiheQgYgXgug4QgngugVgRQgNAGgLASIgTAkQgcAug0AAQh4AAglh7IgBgEQgTAGgXAAQhNAAgTg+QgEgNAAg+Qiwg1g2hoQgGgMgehNQgUg2gdghQgEAAgTAFQgSAFgOAAQg6AAgbgtQgPgcAAgdQAAhYAehBQA0hxCAAAQBLAAAhAPQARAIASAMIAbgFIEugCQBLgfAagrQALgTAEgbQgkhGgohzQgfhdgdhrQgWhXAAgKQgBhkABgaQAAghAKgXQAqhQBEiwQAIgWABghIAAg4QAAhGAmglQBdhaAwBLQANAVAIAcQAeA4ATBnQBSBxBOA9QApAfBBAfQAtAVANAYQARAfABBTQAZgDAeAAIAXgYQAdgcAbgQQBagzBCBUQASAdAMAzQAHAeANA8IBAgMQAygIAQAAQAkAAAaAWIAIgMQBDhkA6AAQAdAAAoAcQAVAOAQANIB9guQCFgyA/gOQBGgOAXgxIASgrQAOgaAUgSQBihUAxB4QAWgLAdAAQAdAAApAXQBpikB+CUQApAwAuBWIA5BtQCjAaBMD0IALAlIAKgCQAogHANgcQAHgMAEgrQADglAPgPQAWgXA2AAQA2AAAcAxQASAfAAAZIAAADQBZgSCVgMQBvgTAphMQAnhLAYgXQAUgTAUALQANAHAfAZQAhAVAoAAQARAAARALIABgNQAViKBVAAQAiAAAuAgQAQALAzAqQAVgogEg5QgKg9ABgZQAAgqAfgTQAngXBjAAQAlAAA+guIBnhOQA5gmAfAPQApASAPBlQArAAA0AUQAaAKATAKQAnAAAUgZQAMgNAQgqQAQgnAQgQQAZgZAuAAQANAAAXANQAXAMANAAQAKAAAogRQAogSASAAQAgAAAcBDQAOgLASgKQA0ggDghZQCxhGBJhZQAbghAUgJQAfgOBKAAQAMAAAoAKQApAKAMAAQBHAAA6ArQAvAjAJAgQAuhkBThKQBghXAyA7QAuAWASAjQAqBLAOASQAZAiA4ApQAiAaAAAnQAAAfhAB+QhACAgCAeQgGBagMBDQgPBVgbBJIAAJ2QgZBOgYBtIgpDBQgFAXguA0Qg1A7gVAxIgLApQgJAhgJAUQgcBAg/AAQgNAAgWgJQgWgJgNAAQgUAAiFAqQiQAuhVAmQgWAJggAhIg8BBQhPBVg8AAQgYAAgzggQgWAbgUARIgDBMQgPF0hVAAQhkAAgRhhQgEgcABgyQABgwgDgMQgyACgigNQgigOgHAAQgsAAi8BmQi4BpgMADQgGACifASQiVAQgLAOQgWAlgOATQgbAiglAAQhIAAgkhXQgnArgNAYQAAAHAFAjQAFAlgFAbQgNBUhdAAQh1AAgJh9QgDgnAIg4IAIg8QgCAAgJgEIgTgLQgoBig8BoQhSCPgtAAQhUAAgdhSg");
	var mask_graphics_8 = new cjs.Graphics().p("EAz6AXnQgDhEAHhpIAJiCQiXAljMBVQhvAvjyBxQjXBlhkAmQiZA7hUAAQhCAAgcgvQgQgaAAgbQAAgVAagcQAZgcA5grQgqgFgMhQQgEgbAAhPIAAgPQgYBCgkAAQhjAAgRhhQgFgcABgyQABgwgDgMQgyACghgNQgigOgIAAQgrAAi9BmQi4BpgMADQgGACifASQiUAQgMAOQgVAlgPATQgaAigmAAQhIAAgkhXQgmArgOAYQAAAHAFAjQAFAlgFAbQgMBUheAAQh1AAgJh9QgDgnAIg4IAJg8QgDAAgIgEIgTgLQgoBig8BoQhTCPgtAAQhUAAgchSQgHgSgJguQgHgigHgKQg3hChjgIQhuADgqgJIAAAdQABAggFAXQgSBMhTAAQhNAAgzhnQgpA/g4AAQgfAAgegaIgTAQQhpBagZAAQgrAAgkgiQgfgeADgVQjQgfhIgxQiGgKiUhwQgxgkgiglQhogIgxjdIghi1QgShjgTghQgjg/hThHQgZgWgUgTQglDPh9AAQgWAAgogFQgngFgdAAQhaAAgWg+QgHgTgCgcQgCgagCgDIgCgIQgKAFgSAFQgXAFgRAAQgiAAgZgKQgVgJgKgNIgDAfQgEAhgKAaQgeBThOAAQiAAAhiheQgYgXgvg4QgngugVgRQgNAGgLASIgTAkQgbAug1AAQh4AAglh7IgBgEQgSAGgYAAQhNAAgTg+QgEgNAAg+Qivg1g2hoQgGgMgehNQgVg2gdgfQgEAAgTAFQgSAFgOAAQg6AAgagvQgQgcAAgdQAAhYAehBQA0hxCAAAQBLAAAhAPQARAIASAMIAbgFIEvgCQBKgfAagrQALgTAEgbQgkhGgohzQgfhdgchrQgXhXAAgKQgBhkABgaQABghAJgXQArhQBDiwQAIgWABghIAAg4QAAhGAnglQBdhaAvBLQANAVAIAcQAeA4ATBnQBSBxBOA9QApAfBBAfQAtAVANAYQASAfAABTQAagDAdAAIAXgYQAdgcAbgQQBagzBCBUQASAdAMAzQAIAeAMA8IBAgMQAygIAQAAQAkAAAaAWIAIgMQBDhkA6AAQAdAAAoAcQAVAOAQANIB9guQCFgyBAgOQBFgOAXgxIATgrQANgaAVgSQBhhUAxB4QAWgLAdAAQAeAAAoAXQBpikB+CUQApAwAvBWIA4BtQCjAaBMD0IALAlIAKgCQApgHANgcQAGgMAEgrQAEglAOgPQAWgXA4AAQA3AAAcAxQARAfAAAZIAAADQBagSCSgMQBwgTAohMQAohLAXgXQAUgTAVALQAMAHAfAZQAhAVAoAAQASAAAQALIABgNQAViKBVAAQAiAAAuAgQARALAzAqQAUgogEg5QgJg9AAgZQAAgqAggTQAngXBiAAQAlAAA+guIBnhOQA5gmAgAPQAoASAPBlQArAAA0AUQAaAKATAKQAnAAAVgZQALgNAQgqQAQgnAQgQQAZgZAuAAQANAAAXANQAXAMANAAQAKAAApgRQAogSARAAQAgAAAdBDQAOgLARgKQA1ggDfhZQCxhGBJhZQAbghAUgJQAfgOBKAAQANAAAoAKQAoAKAMAAQBHAAA7ArQAvAjAIAgQAuhkBThKQBghXAyA7QAuAWATAjQAaAuAPAZIACgQQALhzCCAzQAyAUAyAmQAvAlATAgQAWBcAaDBQgBgNgIgzQgJg8ABglQAFh/ByAAQB0AAADDsQABBIgJB9QgLCCAAAgIAAJYIAAQSQAABrg1CZQgeBXhACqQgLAggQBlQgSBxgNA3Qg0DMhxAAQiCAAgNjZgEAgKANwIgCBMIgDA/IACgIQAOgmAbgdQAJgJApghQATgQAOgSIgEAAQgYAAgzggQgWAbgUARgEA4sgSIIAFAiIACAAQAAgtgCgHg");
	var mask_graphics_9 = new cjs.Graphics().p("EApWAXnQgDhEAHhpIAJiCQiXAljMBVQhvAvjyBxQjXBlhkAmQiZA7hUAAQhCAAgcgvQgQgaAAgbQAAgVAagcQAZgcA5grQgqgFgMhQQgEgbAAhPIgBgPQgXBCgkAAQhjAAgRhhQgFgcABgyQABgwgDgMQgyACghgNQgigOgIAAQgrAAi9BmQi4BpgMADQgGACifASQiUAQgMAOQgVAlgPATQgaAigmAAQhIAAgkhXQgmArgOAYQAAAHAFAjQAFAlgFAbQgMBUhcAAQh1AAgJh9QgDgnAIg4IAJg8QgDAAgIgEIgTgLQgoBig8BoQhTCPgtAAQhUAAgchSQgHgSgJguQgHgigHgKQg3hChlgIQhuADgqgJIAAAdQABAggFAXQgSBMhTAAQhNAAgzhnQgpA/g4AAQgfAAgegaIgTAQQhpBagZAAQgrAAgkgiQgfgeADgVQjQgfhIgxQiGgKiUhwQgxgkgjglQhngIgxjdIghi1QgShjgTghQgjg/hThHQgZgWgUgTQglDPh9AAQgWAAgogFQgngFgdAAQhaAAgWg+QgHgTgCgcQgCgagCgDIgCgIQgKAFgSAFQgXAFgRAAQgiAAgZgKQgVgJgKgNIgDAfQgEAhgKAaQgeBThOAAQiAAAhiheQgYgXgvg4QgngugVgRQgNAGgLASIgTAkQgbAug1AAQh4AAglh7IgBgEQgTAGgXAAQhNAAgTg+QgEgNAAg+Qivg1g2hoQgGgMgehNQgVg2gdgfQgEAAgTAFQgSAFgOAAQg6AAgagvQgQgcAAgdQAAhYAehBQA0hxCAAAQBLAAAhAPQARAIASAMIAbgFIEvgCQBKgfAagrQALgTAEgbQgkhGgohzQgfhdgchrQgXhXAAgKQgBhkABgaQABghAJgXQArhQBDiwQAIgWABghIAAg4QAAhGAnglQBdhaAvBLQANAVAIAcQAeA4ATBnQBSBxBOA9QApAfBBAfQAtAVANAYQARAfABBTQAagDAdAAIAXgYQAdgcAbgQQBagzBCBUQASAdAMAzQAIAeAMA8IBAgMQAygIAQAAQAkAAAaAWIAIgMQBDhkA6AAQAdAAAoAcQAVAOAQANIB9guQCFgyBAgOQBFgOAXgxIATgrQANgaAVgSQBhhUAxB4QAWgLAdAAQAeAAAoAXQBpikB+CUQApAwAvBWIA4BtQCjAaBMD0IALAlIAKgCQApgHANgcQAGgMAEgrQAEglAOgPQAWgXA4AAQA3AAAcAxQARAfAAAZIAAADQBagSCUgMQBwgTAohMQAohLAXgXQAUgTAVALQAMAHAfAZQAhAVAoAAQARAAARALIABgNQAViKBVAAQAgAAAuAgQARALAzAqQAUgogEg5QgJg9AAgZQAAgqAggTQAngXBiAAQAlAAA+guIBnhOQA5gmAgAPQAoASAPBlQArAAA0AUQAaAKATAKQAnAAAVgZQALgNAQgqQAQgnAQgQQAZgZAuAAQANAAAXANQAXAMANAAQAKAAApgRQAogSARAAQAgAAAdBDQAOgLARgKQA1ggDfhZQCxhGBJhZQAbghAUgJQAfgOBKAAQANAAAoAKQAoAKAMAAQBHAAA7ArQAvAjAIAgQAuhkBThKQBghXAyA7QAuAWATAjQAaAuAPAZIACgQQALhzCCAzQAyAUAyAmQAvAlATAgQAWBcAaDBQgBgNgIgzQgJg8ABglQAFh/ByAAQB0AAADDsQABBIgJB9QgIBfgCArQAOhCAwgeQBmhAAvBXQAcA1AABKQACAvARBFQAPA/AVA2IAfgEQAegEAOAAQAoAAAkAPQASAIALAHQCFAACPgFQCEgEAAgDQAAgPgfgDQBnAAAiACQBCADAaANQAWAKAWAhQAqA/ARAVQB1gNAuA1QAZAdAAAkQAAAsgkAgQgnAjhVAYIAAH1QgCAKgGA3QgEAtgJAcQgcBYhgAuQAHAVAEAgQAEAbAAAVQAAA8gMAcQgXA0hBAAQhTAAgth+QgOgmgNg6QgMg5gCgHQhxAlh3AXQgpAIk4A0QgiAngaAYQgwArgyAAQgpAAgdgUIAABEQAABrg1CZQgeBXhACqQgLAggQBlQgSBxgNA3Qg0DMhxAAQiCAAgNjZgAVmNwIgCBMIgEA/IADgIQAOgmAbgdQAJgJApghQATgQANgSIgDAAQgYAAgzggQgWAbgUARgEBATgEIQgEAEAAAbQAAAYAUB9QAWCDANAhIAFAAIAAlmQgvACgJAMgEAxXgJQIAegBQADgqgNhNIgUh6gEAuIgSIIAFAiIACAAQAAgtgCgHg");
	var mask_graphics_10 = new cjs.Graphics().p("EhcLAz4MAAAhnvMC4XAAAMAAABnvg");

	this.timeline.addTween(cjs.Tween.get(mask).to({graphics:mask_graphics_0,x:194.1,y:331.2}).wait(1).to({graphics:mask_graphics_1,x:212,y:338.5}).wait(1).to({graphics:mask_graphics_2,x:242.3,y:351.1}).wait(1).to({graphics:mask_graphics_3,x:283,y:376.6}).wait(1).to({graphics:mask_graphics_4,x:320.5,y:394.6}).wait(1).to({graphics:mask_graphics_5,x:377.1,y:398.1}).wait(1).to({graphics:mask_graphics_6,x:439.8,y:398.1}).wait(1).to({graphics:mask_graphics_7,x:507.1,y:394.7}).wait(1).to({graphics:mask_graphics_8,x:529.5,y:399.2}).wait(1).to({graphics:mask_graphics_9,x:597.1,y:399.2}).wait(1).to({graphics:mask_graphics_10,x:590,y:332}).wait(203));

	// Layer 2
	this.instance = new lib.f11_BANG_ROLL();
	this.instance.setTransform(323.1,350.1,1,1,0,0,0,166,110);

	this.instance.mask = mask;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(74).to({x:283.1,y:1170.1},32,cjs.Ease.get(1)).wait(107));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,1180,664);


(lib.Frame32_OUT = function(mode,startPosition,loop) {
	this.initialize(mode,startPosition,loop,{});

	// Layer 1
	this.instance = new lib.Frame32();
	this.instance.setTransform(416.9,90.9,1,1,0,0,0,590,332);

	this.timeline.addTween(cjs.Tween.get(this.instance).to({y:81.9},12).to({y:90.9},9).wait(1));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(0,0,848,418);


// stage content:



(lib.Video_1184x664 = function(mode,startPosition,loop) {
if (loop == null) { loop = false; }	this.initialize(mode,startPosition,loop,{});

	// timeline functions:
	this.frame_0 = function() {
		playSound("test",99);
	}

	// actions tween:
	this.timeline.addTween(cjs.Tween.get(this).call(this.frame_0).wait(1650));

	// Layer 3
	this.instance = new lib.f1_2_1();
	this.instance.setTransform(587,883,1,1,0,0,0,424.5,152.5);
	this.instance.alpha = 0;
	this.instance._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance).wait(7).to({_off:false},0).to({y:459,alpha:1},11,cjs.Ease.get(1)).to({y:463},2).wait(22).to({y:893,alpha:0},8,cjs.Ease.get(-0.99)).to({_off:true},1).wait(1599));

	// Layer 2
	this.instance_1 = new lib.f1_1_1();
	this.instance_1.setTransform(590,-203,1,1,0,0,0,220.5,142);
	this.instance_1.alpha = 0;

	this.timeline.addTween(cjs.Tween.get(this.instance_1).to({y:211,alpha:1},9,cjs.Ease.get(1)).to({y:207},2).wait(31).to({y:-203,alpha:0},8,cjs.Ease.get(-1)).to({_off:true},1).wait(1599));

	// Layer 5
	this.instance_2 = new lib.rem2();
	this.instance_2.setTransform(590,332,1,1,0,0,180,590,332);
	this.instance_2.alpha = 0;
	this.instance_2._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_2).wait(47).to({_off:false},0).to({x:953.1,alpha:1},4).to({x:1770},18,cjs.Ease.get(0.5)).to({_off:true},1).wait(1580));

	// Layer 4
	this.instance_3 = new lib.rem2();
	this.instance_3.setTransform(590,332,1,1,0,0,0,590,332);
	this.instance_3.alpha = 0;
	this.instance_3._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_3).wait(47).to({_off:false},0).to({x:226.9,alpha:1},4).to({x:-590},18,cjs.Ease.get(0.5)).to({_off:true},1).wait(1580));

	// rem1
	this.instance_4 = new lib.f1_rem1_1();
	this.instance_4.setTransform(590,332,1,1,0,0,0,590,332);

	this.timeline.addTween(cjs.Tween.get(this.instance_4).to({_off:true},51).wait(1599));

	// Layer 30
	this.instance_5 = new lib.f33_1();
	this.instance_5.setTransform(590,333,1,1,0,0,0,296.5,213);
	this.instance_5.alpha = 0;
	this.instance_5._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_5).wait(1594).to({_off:false},0).to({alpha:1},14,cjs.Ease.get(1)).wait(42));

	// Layer 29
	this.instance_6 = new lib.Frame32_OUT();
	this.instance_6.setTransform(597.1,450.1,1,1,0,0,0,424,209);
	this.instance_6._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_6).wait(1515).to({_off:false},0).wait(70).to({y:457.1},2).to({y:-139.9,alpha:0},7,cjs.Ease.get(-1)).wait(56));

	// Layer 26
	this.instance_7 = new lib.f1_rem1_1();
	this.instance_7.setTransform(590.1,332.1,1,1,0,0,0,590,332);
	this.instance_7.alpha = 0;
	this.instance_7._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_7).wait(1505).to({_off:false},0).to({alpha:1},10,cjs.Ease.get(1)).wait(135));

	// Layer 28
	this.instance_8 = new lib.rem2();
	this.instance_8.setTransform(1772.1,332.1,1,1,0,0,180,590,332);
	this.instance_8._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_8).wait(1495).to({_off:false},0).to({x:590.1},14,cjs.Ease.get(1)).to({_off:true},51).wait(90));

	// Layer 27
	this.instance_9 = new lib.rem2();
	this.instance_9.setTransform(-588.9,332.1,1,1,0,0,0,590,332);
	this.instance_9._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_9).wait(1495).to({_off:false},0).to({x:590.1},14,cjs.Ease.get(1)).to({_off:true},51).wait(90));

	// Layer 25
	this.instance_10 = new lib.frame30_Out();
	this.instance_10.setTransform(487.8,221,1,1,0,0,0,-8.3,24);
	this.instance_10._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_10).wait(1422).to({_off:false},0).to({_off:true},138).wait(90));

	// Layer 24
	this.instance_11 = new lib.f30_BG();
	this.instance_11.setTransform(590,332,1.554,1.554,0,0,0,590,332);
	this.instance_11.alpha = 0;
	this.instance_11._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_11).wait(1411).to({_off:false},0).to({scaleX:1,scaleY:1,alpha:1},11,cjs.Ease.get(1)).to({_off:true},138).wait(90));

	// Layer 22
	this.instance_12 = new lib.Fram29();
	this.instance_12.setTransform(482.6,-106.4,1,1,0,0,0,168.5,180.5);
	this.instance_12._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_12).wait(1330).to({_off:false},0).to({y:353.6},24,cjs.Ease.get(1)).to({_off:true},151).wait(145));

	// Layer 17
	this.instance_13 = new lib.frame22_();
	this.instance_13.setTransform(801.2,260.3,1,1,0,0,0,351,219);
	this.instance_13._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_13).wait(1069).to({_off:false},0).wait(55).to({y:-219.7},28,cjs.Ease.get(1)).wait(37).to({alpha:0},9,cjs.Ease.get(1)).to({_off:true},102).wait(350));

	// Layer 18
	this.instance_14 = new lib.f22_trasition();
	this.instance_14.setTransform(589,323,1,1.845,0,0,0,590,191.5);
	this.instance_14._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_14).wait(1189).to({_off:false},0).to({scaleY:1.32,x:589.3,y:445.3},9,cjs.Ease.get(0.2)).to({scaleY:0.21,x:590,y:703.1},23).to({_off:true},79).wait(350));

	// Layer 21
	this.instance_15 = new lib.Frame27();
	this.instance_15.setTransform(590,398,1,1,0,0,0,167.5,221.5);
	this.instance_15._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_15).wait(1191).to({_off:false},0).wait(139).to({y:1026},24,cjs.Ease.get(1)).to({_off:true},58).wait(238));

	// Layer 19
	this.instance_16 = new lib.f18_BG_1();
	this.instance_16.setTransform(590,332,1,1,0,0,0,590,332);
	this.instance_16.alpha = 0;
	this.instance_16._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_16).wait(1189).to({_off:false},0).to({alpha:0.391},2,cjs.Ease.get(1)).to({alpha:1},7).to({_off:true},307).wait(145));

	// Layer 16
	this.instance_17 = new lib.frame22();
	this.instance_17.setTransform(590,253.5,1,1,0,0,0,590,253.5);
	this.instance_17._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_17).wait(1049).to({_off:false},0).to({_off:true},251).wait(350));

	// Layer 14
	this.instance_18 = new lib.Frame17();
	this.instance_18.setTransform(571,458.7,1,1,0,0,0,153.3,163.5);
	this.instance_18._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_18).wait(908).to({_off:false},0).to({_off:true},192).wait(550));

	// Layer 15
	this.instance_19 = new lib.Tween33("synched",0);
	this.instance_19.setTransform(590,332);
	this.instance_19.alpha = 0;
	this.instance_19._off = true;

	this.instance_20 = new lib.Tween34("synched",0);
	this.instance_20.setTransform(590,332);

	this.timeline.addTween(cjs.Tween.get({}).to({state:[]}).to({state:[{t:this.instance_19}]},958).to({state:[{t:this.instance_20}]},7).to({state:[]},135).wait(550));
	this.timeline.addTween(cjs.Tween.get(this.instance_19).wait(958).to({_off:false},0).to({_off:true,alpha:1},7,cjs.Ease.get(1)).wait(685));

	// Layer 11
	this.instance_21 = new lib.frame16();
	this.instance_21.setTransform(598.1,-216.9,1,1,0,0,0,69,95);
	this.instance_21._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_21).wait(819).to({_off:false},0).to({y:314.1},31,cjs.Ease.get(1)).to({x:600.1,y:310.1},11).to({x:594.1,y:324.1},10).to({y:308.1},10).to({x:604.1,y:318.1},10).to({regY:95.2,scaleX:15.45,scaleY:15.45,x:573.9,y:-1539.8},14,cjs.Ease.get(-1)).to({scaleX:16.41,scaleY:16.41,x:574.1,y:-1540.1,alpha:0},3).to({_off:true},85).wait(657));

	// Layer 13
	this.shape = new cjs.Shape();
	this.shape.graphics.f("#FFFFFF").s().p("EhcLAz4MAAAhnvMC4WAAAMAAABnvg");
	this.shape.setTransform(590,332);
	this.shape._off = true;

	this.timeline.addTween(cjs.Tween.get(this.shape).wait(905).to({_off:false},0).to({_off:true},95).wait(650));

	// Layer 1
	this.instance_22 = new lib.Frame11();
	this.instance_22.setTransform(590,332,1,1,0,0,0,590,332);
	this.instance_22.alpha = 0;
	this.instance_22._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_22).wait(743).to({_off:false},0).to({alpha:1},6,cjs.Ease.get(1)).to({x:598,y:324},10).to({y:340},10).to({x:582,y:324},10).to({y:340},10).to({x:598,y:324},10).to({y:340},10).to({x:584,y:338},10).to({x:596,y:326},10).to({x:582,y:324},10).to({x:590,y:332},10).to({alpha:0},10).to({_off:true},1).wait(790));

	// Layer 9
	this.instance_23 = new lib.frame10();
	this.instance_23.setTransform(636.2,346.1,0.412,0.412,0,0,0,51,45.1);
	this.instance_23._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_23).wait(650).to({_off:false},0).to({regY:45,scaleX:1,scaleY:1,x:616.2},9,cjs.Ease.get(1)).to({x:636.2},15).wait(63).to({alpha:0},12,cjs.Ease.get(-1)).to({_off:true},157).wait(744));

	// Layer 12
	this.instance_24 = new lib.frame9();
	this.instance_24.setTransform(454.1,340.1,1,1,0,0,0,87,163.5);
	this.instance_24._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_24).wait(434).to({_off:false},0).to({_off:true},235).wait(981));

	// Layer 10
	this.instance_25 = new lib.frame6();
	this.instance_25.setTransform(390,332,1,1,0,0,0,390,332);
	this.instance_25.alpha = 0;
	this.instance_25._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_25).wait(346).to({_off:false},0).to({alpha:1},8,cjs.Ease.get(1)).to({_off:true},552).wait(744));

	// Layer 7
	this.instance_26 = new lib.f5_BG_1();
	this.instance_26.setTransform(590,-341.9,1,1,0,0,0,590,332);
	this.instance_26._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_26).wait(184).to({_off:false},0).to({y:78.1},22).to({y:332},58).to({_off:true},67).wait(1319));

	// Layer 6
	this.instance_27 = new lib.f2_xxx();
	this.instance_27.setTransform(590,332,1,1,0,0,0,590,332);
	this.instance_27._off = true;

	this.timeline.addTween(cjs.Tween.get(this.instance_27).wait(51).to({_off:false},0).wait(15).to({scaleX:1.09,scaleY:1.09,y:334},14,cjs.Ease.get(1)).wait(266).to({alpha:0},8).to({_off:true},1).wait(1295));

}).prototype = p = new cjs.MovieClip();
p.nominalBounds = new cjs.Rectangle(590,-13,1180,1009);

})(lib = lib||{}, images = images||{}, createjs = createjs||{});
var lib, images, createjs;