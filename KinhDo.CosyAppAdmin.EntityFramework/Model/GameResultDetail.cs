﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("GameResultDetail")]
    public partial class GameResultDetail
    {
        public int Id { get; set; }

        public int PlayerGameResultId { get; set; }

        public string Title { get; set; }

        public int? ItemOrder { get; set; }

        public decimal Score { get; set; }

        public decimal? SubScore1 { get; set; }

        public decimal? SubScore2 { get; set; }

        public decimal? SubScore3 { get; set; }

        // URL to the images display toghether with this result detail

        public string PhotoUrl1 { get; set; }

        public string PhotoUrl2 { get; set; }

        public string PhotoUrl3 { get; set; }

        public string PhotoUrl4 { get; set; }

        public string PhotoUrl5 { get; set; }

        public virtual PlayerGameResult PlayerGameResult { get; set; }
    }
}
