﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("CvApiProvider")]
    public partial class CvApiProvider
    {
        public int Id { get; set; }

        [Required]
        [StringLength(255)]
        public string ApiKey { get; set; }

        [Required]
        [StringLength(255)]
        public string ApiSecret { get; set; }

        public long? RequestCount { get; set; }

        public long? RequestThreshold { get; set; }

        [Required]
        [StringLength(255)]
        public string ApiProvider { get; set; }

        [StringLength(255)]
        public string ApiUserName { get; set; }

        [StringLength(255)]
        public string ApiPassword { get; set; }
    }
}
