﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PlayerFacebookAlbum")]
    public partial class PlayerFacebookAlbum
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PlayerFacebookAlbum()
        {
            PlayerFacebookPhotoes = new HashSet<PlayerFacebookPhoto>();
        }

        [Key]
        public int PlayerAlbumId { get; set; }

        [Required]
        [StringLength(255)]
        public string Id { get; set; }

        public int PlayerId { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        public int? Count { get; set; }

        [StringLength(255)]
        public string Link { get; set; }

        [StringLength(255)]
        public string ImageURL { get; set; }

        [StringLength(255)]
        public string Privacy { get; set; }

        [StringLength(255)]
        public string Type { get; set; }

        [StringLength(255)]
        public string Location { get; set; }

        [StringLength(255)]
        public string Place { get; set; }

        public virtual Player Player { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerFacebookPhoto> PlayerFacebookPhotoes { get; set; }
    }
}