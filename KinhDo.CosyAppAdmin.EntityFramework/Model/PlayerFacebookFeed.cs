﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PlayerFacebookFeed")]
    public partial class PlayerFacebookFeed
    {
        [StringLength(100)]
        public string Id { get; set; }

        [StringLength(255)]
        public string PlayerFbProfileId { get; set; }

        public DateTime? CreatedTime { get; set; }

        [StringLength(20)]
        public string Type { get; set; }

        [StringLength(1000)]
        public string Message { get; set; }

        [StringLength(500)]
        public string Story { get; set; }

        [StringLength(500)]
        public string Link { get; set; }

        [StringLength(255)]
        public string FromFbName { get; set; }

        [StringLength(255)]
        public string FromFbProfileId { get; set; }

        [StringLength(50)]
        public string PlaceId { get; set; }

        [StringLength(255)]
        public string PlaceName { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [StringLength(255)]
        public string Street { get; set; }

        [StringLength(255)]
        public string Zip { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        [StringLength(4000)]
        public string WithTagNames { get; set; }

        [StringLength(4000)]
        public string WithTagIds { get; set; }
    }
}
