﻿using System;
using System.ComponentModel.DataAnnotations;

namespace KinhDo.CosyAppAdmin.Model
{
    public partial class FacebookPhotoImage
    {
        public int Id { get; set; }

        public int PhotoId { get; set; }

        [Required]
        [StringLength(255)]
        public string FacebookPhotoId { get; set; }

        public int Height { get; set; }

        public int Width { get; set; }

        [Required]
        [StringLength(255)]
        public string Source { get; set; }

        public DateTime? UpdatedTime { get; set; }

        public virtual PlayerFacebookPhoto PlayerFacebookPhoto { get; set; }
    }
}
