﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("Setting")]
    public partial class Setting
    {
        public int Id { get; set; }

        public string SettingKey { get; set; }

        public string SettingValueString { get; set; }

        public int? SettingValueNumber { get; set; }

        public string Description { get; set; }

        public string SettingGroup { get; set; }
    }
}