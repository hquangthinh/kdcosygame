﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PlayerGameResult")]
    public partial class PlayerGameResult
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PlayerGameResult()
        {
            GameResultDetails = new HashSet<GameResultDetail>();
        }

        public int Id { get; set; }

        public int PlayerId { get; set; }

        [StringLength(255)]
        public string ClientIpAddress { get; set; }

        [StringLength(255)]
        public string ClientBrowser { get; set; }

        public DateTime ResultDate { get; set; }

        public string Status { get; set; }

        public string ErrorMessage { get; set; }

        public int? PercentComplete { get; set; }

        [StringLength(500)]
        public string AccessToken { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<GameResultDetail> GameResultDetails { get; set; }

        public virtual Player Player { get; set; }
    }
}