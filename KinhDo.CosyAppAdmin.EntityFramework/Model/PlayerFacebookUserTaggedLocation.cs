﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PlayerFacebookUserTaggedLocation")]
    public partial class PlayerFacebookUserTaggedLocation
    {
        [StringLength(50)]
        public string Id { get; set; }

        [Required]
        [StringLength(255)]
        public string PlayerFbProfileId { get; set; }

        public DateTime CreatedTime { get; set; }

        [StringLength(50)]
        public string PlaceId { get; set; }

        [StringLength(255)]
        public string PlaceName { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [StringLength(255)]
        public string Street { get; set; }

        [StringLength(255)]
        public string Zip { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }
    }
}
