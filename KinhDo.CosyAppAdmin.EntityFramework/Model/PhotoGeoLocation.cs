﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PhotoGeoLocation")]
    public partial class PhotoGeoLocation
    {
        public int Id { get; set; }

        public int PhotoId { get; set; }

        public int GeoLocationId { get; set; }

        public virtual GeoLocation GeoLocation { get; set; }

        public virtual PlayerFacebookPhoto PlayerFacebookPhoto { get; set; }
    }
}