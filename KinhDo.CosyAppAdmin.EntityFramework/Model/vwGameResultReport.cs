﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("vwGameResultReport")]
    public partial class VwGameResultReport
    {
        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        [StringLength(255)]
        public string ProfileId { get; set; }

        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int PlayerId { get; set; }

        [Key]
        [Column(Order = 2)]
        public DateTime ResultDate { get; set; }

        [StringLength(255)]
        public string ClientBrowser { get; set; }

        [StringLength(255)]
        public string ClientIpAddress { get; set; }

        [StringLength(100)]
        public string Status { get; set; }

        public int? PercentComplete { get; set; }
    }
}
