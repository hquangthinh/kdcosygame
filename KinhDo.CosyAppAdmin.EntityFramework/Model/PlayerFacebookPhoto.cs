﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("PlayerFacebookPhoto")]
    public partial class PlayerFacebookPhoto
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public PlayerFacebookPhoto()
        {
        }

        [Key]
        public int PlayerPhotoId { get; set; }

        [StringLength(255)]
        public string Id { get; set; }

        [StringLength(255)]
        public string Name { get; set; }

        [StringLength(255)]
        public string Link { get; set; }

        public int? PlaceId { get; set; }

        public string NameTags { get; set; }

        public int? AlbumId { get; set; }

        public DateTime? BackdatedTime { get; set; }

        public DateTime? CreatedTime { get; set; }

        public DateTime? UpdatedTime { get; set; }

        public int PlayerId { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }

        [StringLength(255)]
        public string FacebookPlaceId { get; set; }

        [StringLength(255)]
        public string PlaceName { get; set; }

        [StringLength(255)]
        public string LocationName { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        [StringLength(255)]
        public string Street { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(255)]
        public string State { get; set; }

        [StringLength(255)]
        public string Region { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [StringLength(255)]
        public string Zip { get; set; }

        [StringLength(255)]
        public string Source { get; set; }

        [StringLength(255)]
        public string AlbumName { get; set; }

        // Reaction data

        public int? TotalReactionCount { get; set; }

        public int? TotalReactionLikeCount { get; set; }

        public int? TotalReactionLoveCount { get; set; }

        public int? TotalReactionWowCount { get; set; }

        public int? TotalReactionHahaCount { get; set; }

        public int? TotalReactionSadCount { get; set; }

        public int? TotalReactionAngryCount { get; set; }

        public int? TotalReactionThankfulCount { get; set; }

        public int? TotalReactionNoneCount { get; set; }

        // Location, Tag & Feed related data

        // Number of person tagged in a photo
        public int? TotalProfileTaggedInPhotoCount { get; set; }

        // Count of total places user is tagged
        public int? TotalUserTaggedPlaceCount { get; set; }

        // Count post that the user is being tagged
        public int? TotalPostUserIsTaggedCount { get; set; }

        // Total faces detected by CV api
        public int? TotalFacesCount { get; set; }

        // Emotional scores

        public decimal? Anger { get; set; }

        public decimal? Contempt { get; set; }

        public decimal? Disgust { get; set; }

        public decimal? Fear { get; set; }

        public decimal? Happiness { get; set; }

        public decimal? Neutral { get; set; }

        public decimal? Sadness { get; set; }

        public decimal? Surprise { get; set; }

        public virtual PlayerFacebookAlbum PlayerFacebookAlbum { get; set; }

        public virtual Player Player { get; set; }
    }
}