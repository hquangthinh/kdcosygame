﻿using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("AlbumGeoLocation")]
    public partial class AlbumGeoLocation
    {
        public int Id { get; set; }

        public int? AlbumId { get; set; }

        public int? GeoLocationId { get; set; }

        public virtual GeoLocation GeoLocation { get; set; }

        public virtual PlayerFacebookAlbum PlayerFacebookAlbum { get; set; }
    }
}
