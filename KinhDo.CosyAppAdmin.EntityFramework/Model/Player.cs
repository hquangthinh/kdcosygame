﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace KinhDo.CosyAppAdmin.Model
{
    [Table("Player")]
    public partial class Player
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Player()
        {
            PlayerFacebookAlbums = new HashSet<PlayerFacebookAlbum>();
            PlayerGameResults = new HashSet<PlayerGameResult>();
        }

        public int Id { get; set; }

        [StringLength(255)]
        public string ProfileId { get; set; }

        [Required]
        [StringLength(255)]
        public string UserName { get; set; }

        [StringLength(255)]
        public string FirstName { get; set; }

        [StringLength(255)]
        public string LastName { get; set; }

        [StringLength(255)]
        public string ImageURL { get; set; }

        [StringLength(255)]
        public string LinkURL { get; set; }

        [StringLength(255)]
        public string Locale { get; set; }

        [StringLength(255)]
        public string PhoneNumber { get; set; }

        [StringLength(255)]
        public string Email { get; set; }

        public DateTime? Birthdate { get; set; }

        [StringLength(255)]
        public string Location { get; set; }

        [StringLength(50)]
        public string Gender { get; set; }

        [StringLength(255)]
        public string AgeRange { get; set; }

        [StringLength(255)]
        public string Bio { get; set; }

        [StringLength(255)]
        public string StreetAddress { get; set; }

        [StringLength(255)]
        public string Ward { get; set; }

        [StringLength(255)]
        public string District { get; set; }

        [StringLength(255)]
        public string City { get; set; }

        [StringLength(255)]
        public string Country { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerFacebookAlbum> PlayerFacebookAlbums { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerFacebookPhoto> PlayerFacebookPhotoes { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PlayerGameResult> PlayerGameResults { get; set; }
    }
}