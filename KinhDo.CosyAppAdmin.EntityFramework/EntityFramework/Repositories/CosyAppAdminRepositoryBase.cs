﻿using Abp.Domain.Entities;
using Abp.EntityFramework;
using Abp.EntityFramework.Repositories;

namespace KinhDo.CosyAppAdmin.EntityFramework.Repositories
{
    public abstract class CosyAppAdminRepositoryBase<TEntity, TPrimaryKey> : EfRepositoryBase<CosyAppAdminDbContext, TEntity, TPrimaryKey>
        where TEntity : class, IEntity<TPrimaryKey>
    {
        protected CosyAppAdminRepositoryBase(IDbContextProvider<CosyAppAdminDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //add common methods for all repositories
    }

    public abstract class CosyAppAdminRepositoryBase<TEntity> : CosyAppAdminRepositoryBase<TEntity, int>
        where TEntity : class, IEntity<int>
    {
        protected CosyAppAdminRepositoryBase(IDbContextProvider<CosyAppAdminDbContext> dbContextProvider)
            : base(dbContextProvider)
        {

        }

        //do not add any method here, add to the class above (since this inherits it)
    }
}
