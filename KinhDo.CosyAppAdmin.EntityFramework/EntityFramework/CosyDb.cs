﻿using System.Data.Entity;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyAppAdmin.EntityFramework
{
    public partial class CosyDb : DbContext
    {
        public CosyDb()
            : base("name=Default")
        {
        }

        public virtual DbSet<GameResultDetail> GameResultDetails { get; set; }
        public virtual DbSet<Player> Players { get; set; }
        public virtual DbSet<PlayerFacebookAlbum> PlayerFacebookAlbums { get; set; }
        public virtual DbSet<PlayerFacebookPhoto> PlayerFacebookPhotoes { get; set; }
        public virtual DbSet<PlayerGameResult> PlayerGameResults { get; set; }
        public virtual DbSet<Setting> Settings { get; set; }
        public virtual DbSet<CvApiProvider> CvApiProviders { get; set; }
        public virtual DbSet<PlayerFacebookFeed> PlayerFacebookFeeds { get; set; }
        public virtual DbSet<PlayerFacebookUserTaggedLocation> PlayerFacebookUserTaggedLocations { get; set; }
        public virtual DbSet<VwGameResultReport> VwGameResultReports { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.Score)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore1)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore2)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore3)
                .HasPrecision(10, 2);

            modelBuilder.Entity<PlayerFacebookPhoto>()
                .Property(e => e.Latitude)
                .HasPrecision(25, 15);

            modelBuilder.Entity<PlayerFacebookPhoto>()
                .Property(e => e.Longitude)
                .HasPrecision(25, 15);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerFacebookAlbums)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerFacebookPhotoes)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerGameResults)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlayerFacebookAlbum>()
                .HasMany(e => e.PlayerFacebookPhotoes)
                .WithOptional(e => e.PlayerFacebookAlbum)
                .HasForeignKey(e => e.AlbumId);

            modelBuilder.Entity<PlayerGameResult>()
                .HasMany(e => e.GameResultDetails)
                .WithRequired(e => e.PlayerGameResult)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlayerFacebookFeed>()
                .Property(e => e.Latitude)
                .HasPrecision(25, 15);

            modelBuilder.Entity<PlayerFacebookFeed>()
                .Property(e => e.Longitude)
                .HasPrecision(25, 15);

            modelBuilder.Entity<PlayerFacebookUserTaggedLocation>()
                .Property(e => e.Latitude)
                .HasPrecision(25, 15);

            modelBuilder.Entity<PlayerFacebookUserTaggedLocation>()
                .Property(e => e.Longitude)
                .HasPrecision(25, 15);
        }
    }
}
