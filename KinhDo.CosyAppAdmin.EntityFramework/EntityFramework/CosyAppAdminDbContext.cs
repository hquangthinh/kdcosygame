﻿using System.Data.Common;
using System.Data.Entity;
using Abp.Zero.EntityFramework;
using KinhDo.CosyAppAdmin.Authorization.Roles;
using KinhDo.CosyAppAdmin.MultiTenancy;
using KinhDo.CosyAppAdmin.Users;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyAppAdmin.EntityFramework
{
    public class CosyAppAdminDbContext : AbpZeroDbContext<Tenant, Role, User>
    {
        public virtual IDbSet<FacebookPhotoImage> FacebookPhotoImages { get; set; }
        public virtual IDbSet<GameResultDetail> GameResultDetails { get; set; }
        public virtual IDbSet<GeoLocation> GeoLocations { get; set; }
        public virtual IDbSet<PhotoGeoLocation> PhotoGeoLocations { get; set; }
        public virtual IDbSet<Player> Players { get; set; }
        public virtual IDbSet<PlayerFacebookAlbum> PlayerFacebookAlbums { get; set; }
        public virtual IDbSet<PlayerFacebookPhoto> PlayerFacebookPhotoes { get; set; }
        public virtual IDbSet<PlayerGameResult> PlayerGameResults { get; set; }

        /* NOTE: 
         *   Setting "Default" to base class helps us when working migration commands on Package Manager Console.
         *   But it may cause problems when working Migrate.exe of EF. If you will apply migrations on command line, do not
         *   pass connection string name to base classes. ABP works either way.
         */
        public CosyAppAdminDbContext()
            : base("Default")
        {
        }

        /* NOTE:
         *   This constructor is used by ABP to pass connection string defined in CosyAppAdminDataModule.PreInitialize.
         *   Notice that, actually you will not directly create an instance of CosyAppAdminDbContext since ABP automatically handles it.
         */
        public CosyAppAdminDbContext(string nameOrConnectionString)
            : base(nameOrConnectionString)
        {
        }

        //This constructor is used in tests
        public CosyAppAdminDbContext(DbConnection connection)
            : base(connection, true)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.Score)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore1)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore2)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GameResultDetail>()
                .Property(e => e.SubScore3)
                .HasPrecision(10, 2);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Id)
                .IsUnicode(false);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Street)
                .IsUnicode(false);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.City)
                .IsUnicode(false);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Country)
                .IsUnicode(false);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Latitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<GeoLocation>()
                .Property(e => e.Longitude)
                .HasPrecision(18, 10);

            modelBuilder.Entity<GeoLocation>()
                .HasMany(e => e.PhotoGeoLocations)
                .WithRequired(e => e.GeoLocation)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerFacebookAlbums)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Player>()
                .HasMany(e => e.PlayerGameResults)
                .WithRequired(e => e.Player)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<PlayerFacebookAlbum>()
                .HasMany(e => e.PlayerFacebookPhotoes)
                .WithOptional(e => e.PlayerFacebookAlbum)
                .HasForeignKey(e => e.AlbumId);

            modelBuilder.Entity<PlayerGameResult>()
                .HasMany(e => e.GameResultDetails)
                .WithRequired(e => e.PlayerGameResult)
                .WillCascadeOnDelete(false);
        }
    }
}
