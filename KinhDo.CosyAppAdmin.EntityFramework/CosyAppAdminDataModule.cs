﻿using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using Abp.Zero.EntityFramework;
using KinhDo.CosyAppAdmin.EntityFramework;

namespace KinhDo.CosyAppAdmin
{
    [DependsOn(typeof(AbpZeroEntityFrameworkModule), typeof(CosyAppAdminCoreModule))]
    public class CosyAppAdminDataModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer(new NullDatabaseInitializer<CosyAppAdminDbContext>());

            Configuration.DefaultNameOrConnectionString = "Default";
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}
