﻿if exists (select * from sysobjects where id = OBJECT_ID('dbo.vwGameResultReport') and OBJECTPROPERTY(id, 'IsView') = 1)
    DROP VIEW vwGameResultReport
GO

CREATE  VIEW [dbo].[vwGameResultReport]
AS
SELECT 
	p.FirstName, 
	p.LastName, 
	p.ProfileId, 
    p.Email,
	gr.Id,
	gr.PlayerId,
	gr.ResultDate,
	gr.ClientBrowser,
	gr.ClientIpAddress,
	gr.Status,
	gr.PercentComplete
FROM PlayerGameResult gr INNER JOIN Player p ON gr.PlayerId=p.Id

GO