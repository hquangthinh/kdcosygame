﻿IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.Player') AND name = 'ProfileId')
    ALTER TABLE [dbo].[Player] ADD ProfileId NVARCHAR(255)
GO

/* PlayerFacebookPhoto */

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'PlayerId')
BEGIN	
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD PlayerId Int NULL

    ALTER TABLE [dbo].[PlayerFacebookPhoto]
        WITH CHECK ADD CONSTRAINT [FK_PlayerFacebookPhoto_Player] FOREIGN KEY([PlayerId])
            REFERENCES [dbo].[Player] ([Id])

    ALTER TABLE [dbo].[PlayerFacebookPhoto] 
        CHECK CONSTRAINT [FK_PlayerFacebookPhoto_Player]
END
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'UpdatedTime')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD UpdatedTime Datetime
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.FacebookPhotoImages') AND name = 'UpdatedTime')
    ALTER TABLE [dbo].[FacebookPhotoImages] ADD UpdatedTime Datetime
GO

/* Add additional fields to store photo extra information such as main image url, place information*/
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Height')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Height Int
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Width')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Width Int
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Source')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Source NVARCHAR(512)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'FacebookPlaceId')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD FacebookPlaceId NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'PlaceName')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD PlaceName NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'LocationName')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD LocationName NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Latitude')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Latitude DECIMAL(25,15)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Longitude')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Longitude DECIMAL(25,15)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Street')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Street NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'City')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD City NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'State')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD [State] NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Region')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Region NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Country')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Country NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Zip')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Zip NVARCHAR(20)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionLikeCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionLikeCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionLoveCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionLoveCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionWowCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionWowCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionHahaCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionHahaCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionSadCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionSadCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionAngryCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionAngryCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionThankfulCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionThankfulCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalReactionNoneCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalReactionNoneCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalProfileTaggedInPhotoCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalProfileTaggedInPhotoCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalUserTaggedPlaceCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalUserTaggedPlaceCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalPostUserIsTaggedCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalPostUserIsTaggedCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'TotalFacesCount')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD TotalFacesCount INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Anger')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Anger DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Contempt')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Contempt DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Disgust')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Disgust DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Fear')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Fear DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Happiness')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Happiness DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Neutral')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Neutral DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Sadness')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Sadness DECIMAL(12,8)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Surprise')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD Surprise DECIMAL(12,8)
GO

/* GameResultDetail */
IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'PhotoUrl1')
    ALTER TABLE [dbo].[GameResultDetail] ADD PhotoUrl1 NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'PhotoUrl2')
    ALTER TABLE [dbo].[GameResultDetail] ADD PhotoUrl2 NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'PhotoUrl3')
    ALTER TABLE [dbo].[GameResultDetail] ADD PhotoUrl3 NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'PhotoUrl4')
    ALTER TABLE [dbo].[GameResultDetail] ADD PhotoUrl4 NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'PhotoUrl5')
    ALTER TABLE [dbo].[GameResultDetail] ADD PhotoUrl5 NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'Title')
    ALTER TABLE [dbo].[GameResultDetail] ADD Title NVARCHAR(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.GameResultDetail') AND name = 'ItemOrder')
    ALTER TABLE [dbo].[GameResultDetail] ADD ItemOrder INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'ClientIpAddress')
    ALTER TABLE [dbo].[PlayerGameResult] ADD ClientIpAddress NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'ClientBrowser')
    ALTER TABLE [dbo].[PlayerGameResult] ADD ClientBrowser NVARCHAR(255)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'Status')
    ALTER TABLE [dbo].[PlayerGameResult] ADD Status NVARCHAR(100)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'PercentComplete')
    ALTER TABLE [dbo].[PlayerGameResult] ADD PercentComplete INT
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'ErrorMessage')
    ALTER TABLE [dbo].[PlayerGameResult] ADD ErrorMessage NVARCHAR(255)
GO

/* Setting table */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[Setting]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
BEGIN
    CREATE TABLE [dbo].[Setting](
	    [Id] [int] IDENTITY(1,1) NOT NULL,
	    [SettingKey] [nvarchar](50) NULL,
	    [SettingValueString] [nvarchar](500) NULL,
	    [SettingValueNumber] [int] NULL,
	    [Description] [nvarchar](255) NULL,
    CONSTRAINT [PK_Setting] PRIMARY KEY CLUSTERED 
    (
	    [Id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.Setting') AND name = 'SettingGroup')
    ALTER TABLE [dbo].[Setting] ADD SettingGroup NVARCHAR(50)
GO

UPDATE [Setting] SET SettingGroup = 'Global' WHERE SettingGroup IS NULL OR SettingGroup = ''

IF NOT EXISTS(SELECT 1 FROM [Setting] WHERE SettingKey = 'GetPhotoBackedDateDays')
	INSERT INTO [Setting](SettingKey, SettingValueNumber, Description, SettingGroup)
		VALUES('GetPhotoBackedDateDays', 360, 'Number of days in the past to get the user photo', 'Global')	
GO

IF NOT EXISTS(SELECT 1 FROM [Setting] WHERE SettingKey = 'FbDataCacheDurationInHours')
	INSERT INTO [Setting](SettingKey, SettingValueNumber, Description, SettingGroup)
		VALUES('FbDataCacheDurationInHours', 1, 'Duration in hour to cache user data get from Facebook. Default to 1', 'Global')	
GO

/* CvApiProvider table */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[CvApiProvider]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
BEGIN
    CREATE TABLE [dbo].[CvApiProvider](
	    [Id] [int] IDENTITY(1,1) NOT NULL,
	    [ApiKey] [nvarchar](255) NOT NULL,
	    [ApiSecret] [nvarchar](255) NOT NULL,
	    [RequestCount] [bigint] NULL,
	    [RequestThreshold] [bigint] NULL,
	    [ApiProvider] [nvarchar](255) NOT NULL,
	    [ApiUserName] [nvarchar](255) NULL,
	    [ApiPassword] [nvarchar](255) NULL,
     CONSTRAINT [PK_CvApiProvider] PRIMARY KEY CLUSTERED 
    (
	    [Id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

/* Api keys */
IF NOT EXISTS(SELECT 1 FROM CvApiProvider WHERE ApiKey = '6d3887c8cd70f99166bf47919ac4964f' AND ApiSecret = 'rUVaz8imyHl8GK_KaVTHcKuCBNEXWCWZ')
	BEGIN
		INSERT INTO CvApiProvider(ApiKey, ApiSecret, RequestCount, RequestThreshold, ApiProvider, ApiUserName)
		VALUES
			(
				'6d3887c8cd70f99166bf47919ac4964f',
				'rUVaz8imyHl8GK_KaVTHcKuCBNEXWCWZ',
				0,
				30000,
				'Face++',
				'Cosy Dev 1'
			)
	END
GO

IF NOT EXISTS(SELECT 1 FROM CvApiProvider WHERE ApiKey = 'fd92a2af49a1ad60ef97c58dee716c82' AND ApiSecret = 'KTs6wNCjFX2hSsnvjJNJYxWEdL-wHv8I')
	BEGIN
		INSERT INTO CvApiProvider(ApiKey, ApiSecret, RequestCount, RequestThreshold, ApiProvider, ApiUserName)
		VALUES
			(
				'fd92a2af49a1ad60ef97c58dee716c82',
				'KTs6wNCjFX2hSsnvjJNJYxWEdL-wHv8I',
				0,
				30000,
				'Face++',
				'CosyAppDev'
			)
	END
GO

/* PlayerFacebookFeed */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[PlayerFacebookFeed]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
BEGIN
    CREATE TABLE [dbo].[PlayerFacebookFeed](
	[Id] [nvarchar](100) NOT NULL,
	[PlayerFbProfileId] [nvarchar](255) NULL,
	[CreatedTime] [datetime] NULL,
	[Type] [nvarchar](20) NULL,
	[Message] [nvarchar](1000) NULL,
	[Story] [nvarchar](500) NULL,
	[Link] [nvarchar](500) NULL,
	[FromFbName] [nvarchar](255) NULL,
	[FromFbProfileId] [nvarchar](255) NULL,
	[PlaceId] [nvarchar](50) NULL,
	[PlaceName] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Street] [nvarchar](255) NULL,
	[Zip] [nvarchar](255) NULL,
	[Latitude] [decimal](25, 15) NULL,
	[Longitude] [decimal](25, 15) NULL,
	[WithTagNames] [nvarchar](1000) NULL,
	[WithTagIds] [nvarchar](1000) NULL,
     CONSTRAINT [PK_PlayerFacebookFeed] PRIMARY KEY CLUSTERED 
    (
	    [Id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

/* PlayerFacebookUserTaggedLocation */
IF NOT EXISTS (SELECT * FROM sysobjects WHERE ID = OBJECT_ID('[PlayerFacebookUserTaggedLocation]') AND OBJECTPROPERTY(ID, 'IsTable') = 1)
BEGIN
    CREATE TABLE [dbo].[PlayerFacebookUserTaggedLocation](
	[Id] [nvarchar](50) NOT NULL,
	[PlayerFbProfileId] [nvarchar](255) NOT NULL,
	[CreatedTime] [datetime] NOT NULL,
	[PlaceId] [nvarchar](50) NULL,
	[PlaceName] [nvarchar](255) NULL,
	[City] [nvarchar](255) NULL,
	[Country] [nvarchar](255) NULL,
	[Street] [nvarchar](255) NULL,
	[Zip] [nvarchar](255) NULL,
	[Latitude] [decimal](25, 15) NULL,
	[Longitude] [decimal](25, 15) NULL,
     CONSTRAINT [PK_PlayerFacebookUserTaggedLocation] PRIMARY KEY CLUSTERED 
    (
	    [Id] ASC
    )WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
    ) ON [PRIMARY]
END
GO

IF NOT EXISTS(SELECT 1 FROM [Setting] WHERE SettingKey = 'PlayerGameDataBaseFolder')
	INSERT INTO [Setting](SettingKey, SettingValueString, Description, SettingGroup)
		VALUES('PlayerGameDataBaseFolder', 'D:\proj\kdcosygame\KinhDo.CosyPhotoGame.Web\App_Data', 'Full path to the folder where player data is stored', 'Global')	
GO

-- INSERT ANY CHANGES ABOVE THIS STATEMENT
if not exists (select 1 from DatabaseVersion where Major = 1 and Minor = 1 and Build = 10)
    INSERT INTO [DatabaseVersion] (Major, Minor, Build) VALUES (1, 1, 1)
GO