﻿/*Local Emotion Service using EmguCv*/

IF NOT EXISTS(SELECT 1 FROM CvApiProvider WHERE ApiKey = '981963C8-97EF-45B5-958F-DDD636FA9AB2' AND ApiSecret = '93CFF2B2-E05A-4EBD-83E8-71C536263120')
	BEGIN
		INSERT INTO CvApiProvider(ApiKey, ApiSecret, RequestCount, RequestThreshold, ApiProvider, ApiUserName)
		VALUES
			(
				'981963C8-97EF-45B5-958F-DDD636FA9AB2',
				'93CFF2B2-E05A-4EBD-83E8-71C536263120',
				0,
				9999999,
				'EmguCv',
				'Cosy Dev 1'
			)
	END
GO


IF NOT EXISTS(SELECT 1 FROM [Setting] WHERE SettingKey = 'GameBaseInstallationFolder')
	INSERT INTO [Setting](SettingKey, SettingValueString, Description, SettingGroup)
		VALUES('GameBaseInstallationFolder', 'D:\proj\kdcosygame\KinhDo.CosyPhotoGame.Web', 'Full path to the folder where the game app is installed.', 'Global')	
GO

/* Increase length for fields of table */
IF EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookFeed') AND name = 'WithTagNames')
    ALTER TABLE [dbo].[PlayerFacebookFeed] ALTER COLUMN WithTagNames nvarchar(MAX)
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookFeed') AND name = 'WithTagIds')
    ALTER TABLE [dbo].[PlayerFacebookFeed] ALTER COLUMN WithTagIds nvarchar(MAX)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerGameResult') AND name = 'AccessToken')
    ALTER TABLE [dbo].[PlayerGameResult] ADD AccessToken nvarchar(500)
GO

IF NOT EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'AlbumName')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ADD AlbumName nvarchar(255)
GO

IF EXISTS (SELECT * FROM syscolumns WHERE id = object_id('dbo.PlayerFacebookPhoto') AND name = 'Name')
    ALTER TABLE [dbo].[PlayerFacebookPhoto] ALTER COLUMN Name nvarchar(500)
GO