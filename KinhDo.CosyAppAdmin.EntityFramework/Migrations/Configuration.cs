using System.Data.Entity.Migrations;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using KinhDo.CosyAppAdmin.Migrations.SeedData;
using EntityFramework.DynamicFilters;

namespace KinhDo.CosyAppAdmin.Migrations
{
    public sealed class Configuration : DbMigrationsConfiguration<CosyAppAdmin.EntityFramework.CosyAppAdminDbContext>, IMultiTenantSeed
    {
        public AbpTenantBase Tenant { get; set; }

        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            ContextKey = "CosyAppAdmin";
        }

        protected override void Seed(CosyAppAdmin.EntityFramework.CosyAppAdminDbContext context)
        {
            context.DisableAllFilters();

            if (Tenant == null)
            {
                //Host seed
                new InitialHostDbBuilder(context).Create();

                //Default tenant seed (in host database).
                new DefaultTenantCreator(context).Create();
                new TenantRoleAndUserBuilder(context, 1).Create();
            }
            else
            {
                //You can add seed for tenant databases and use Tenant property...
            }

            context.SaveChanges();
        }
    }
}
