using System.Linq;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyAppAdmin.MultiTenancy;

namespace KinhDo.CosyAppAdmin.Migrations.SeedData
{
    public class DefaultTenantCreator
    {
        private readonly CosyAppAdminDbContext _context;

        public DefaultTenantCreator(CosyAppAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            CreateUserAndRoles();
        }

        private void CreateUserAndRoles()
        {
            //Default tenant

            var defaultTenant = _context.Tenants.FirstOrDefault(t => t.TenancyName == Tenant.DefaultTenantName);
            if (defaultTenant == null)
            {
                _context.Tenants.Add(new Tenant {TenancyName = Tenant.DefaultTenantName, Name = Tenant.DefaultTenantName});
                _context.SaveChanges();
            }
        }
    }
}
