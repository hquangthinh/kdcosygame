﻿using KinhDo.CosyAppAdmin.EntityFramework;
using EntityFramework.DynamicFilters;

namespace KinhDo.CosyAppAdmin.Migrations.SeedData
{
    public class InitialHostDbBuilder
    {
        private readonly CosyAppAdminDbContext _context;

        public InitialHostDbBuilder(CosyAppAdminDbContext context)
        {
            _context = context;
        }

        public void Create()
        {
            _context.DisableAllFilters();

            new DefaultEditionsCreator(_context).Create();
            new DefaultLanguagesCreator(_context).Create();
            new HostRoleAndUserCreator(_context).Create();
            new DefaultSettingsCreator(_context).Create();
        }
    }
}
