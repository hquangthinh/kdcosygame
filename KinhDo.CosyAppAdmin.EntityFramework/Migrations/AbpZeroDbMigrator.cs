﻿using Abp.Dependency;
using Abp.Domain.Uow;
using Abp.MultiTenancy;
using Abp.Zero.EntityFramework;
using KinhDo.CosyAppAdmin.EntityFramework;

namespace KinhDo.CosyAppAdmin.Migrations
{
    public class AbpZeroDbMigrator : AbpZeroDbMigrator<CosyAppAdminDbContext, Configuration>
    {
        public AbpZeroDbMigrator(
            IUnitOfWorkManager unitOfWorkManager,
            IDbPerTenantConnectionStringResolver connectionStringResolver,
            IIocResolver iocResolver)
            : base(
                unitOfWorkManager,
                connectionStringResolver,
                iocResolver)
        {
        }
    }
}
