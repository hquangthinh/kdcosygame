﻿using System;
using System.Configuration;

namespace KinhDo.CosyGame.CleanTempFile
{
    public class FilePathsSection : ConfigurationSection
    {
        [ConfigurationProperty("filePaths", IsDefaultCollection = false)]
        [ConfigurationCollection(typeof(FilePathsCollection),
            AddItemName = "add",
            ClearItemsName = "clear",
            RemoveItemName = "remove")]
        public FilePathsCollection FilePaths
        {
            get
            {
                var filePathsCollection =
                    (FilePathsCollection) base["filePaths"];
                return filePathsCollection;
            }
        }

    }

    public class FilePathsCollection : ConfigurationElementCollection
    {
        public FilePathsCollection()
        {
            var path = (FilePathConfigElement)CreateNewElement();
            Add(path);
        }

        public override ConfigurationElementCollectionType CollectionType => ConfigurationElementCollectionType.AddRemoveClearMap;

        protected override ConfigurationElement CreateNewElement()
        {
            return new FilePathConfigElement();
        }

        protected override Object GetElementKey(ConfigurationElement element)
        {
            return ((FilePathConfigElement)element).Path;
        }

        public FilePathConfigElement this[int index]
        {
            get
            {
                return (FilePathConfigElement)BaseGet(index);
            }
            set
            {
                if (BaseGet(index) != null)
                {
                    BaseRemoveAt(index);
                }
                BaseAdd(index, value);
            }
        }

        new public FilePathConfigElement this[string name] => (FilePathConfigElement)BaseGet(name);

        public int IndexOf(FilePathConfigElement url)
        {
            return BaseIndexOf(url);
        }

        public void Add(FilePathConfigElement url)
        {
            BaseAdd(url);
        }
        protected override void BaseAdd(ConfigurationElement element)
        {
            BaseAdd(element, false);
        }

        public void Remove(FilePathConfigElement url)
        {
            if (BaseIndexOf(url) >= 0)
                BaseRemove(url.Path);
        }

        public void RemoveAt(int index)
        {
            BaseRemoveAt(index);
        }

        public void Remove(string name)
        {
            BaseRemove(name);
        }

        public void Clear()
        {
            BaseClear();
        }
    }

    public class FilePathConfigElement : ConfigurationElement
    {
        public FilePathConfigElement(string path)
        {
            this.Path = path;
        }

        public FilePathConfigElement()
        {
        }

        [ConfigurationProperty("path", IsRequired = true, IsKey = true)]
        public string Path
        {
            get
            {
                return (string)this["path"];
            }
            set
            {
                this["path"] = value;
            }
        }
    }
}