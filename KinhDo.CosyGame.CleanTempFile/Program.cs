﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinhDo.CosyGame.CleanTempFile
{
    class Program
    {
        static void Main(string[] args)
        {
            var tempFileAgingInDaysString = ConfigurationManager.AppSettings["TempFileAgingInDays"];
            var tempFileAgingInDays = 5; // default
            Int32.TryParse(tempFileAgingInDaysString, out tempFileAgingInDays);

            var pathList = ReadPathsFromConfig();
            var fileAgingCutOffDate = DateTime.Now.AddDays(-tempFileAgingInDays);
            foreach (var dirPath in pathList)
            {
                try
                {
                    DeleteAgingFilesFromDir(dirPath, fileAgingCutOffDate);
                }
                catch (Exception)
                {
                    // ignored
                }
            }
        }

        private static void DeleteAgingFilesFromDir(string dirPath, DateTime fileAgingCutOffDate)
        {
            var files = Directory.GetFiles(dirPath);

            foreach (var file in files)
            {
                TryDeleteFile(file, fileAgingCutOffDate);
            }
        }

        private static void TryDeleteFile(string filePath, DateTime fileAgingCutOffDate)
        {
            try
            {
                var fileInfo = new FileInfo(filePath);
                if (fileInfo.CreationTime < fileAgingCutOffDate)
                    File.Delete(filePath);
            }
            catch (Exception)
            {
                // ignored
            }
        }

        static List<string> ReadPathsFromConfig()
        {
            var pathsSection =
                ConfigurationManager.GetSection("FilePaths") as FilePathsSection;

            var result = new List<string>();

            if (pathsSection == null)
                return result;

            for (var i = 0; i < pathsSection.FilePaths.Count; i++)
            {
                result.Add(pathsSection.FilePaths[i].Path);
            }

            return result;
        }
    }
}