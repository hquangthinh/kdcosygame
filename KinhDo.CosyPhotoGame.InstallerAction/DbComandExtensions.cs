﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;

namespace KinhDo.CosyPhotoGame.InstallerAction
{
    public static class DbComandExtensions
    {
        public static DbCommand SetInputParam(this DbCommand cmd, string name, object value = null, DbType type = DbType.String)
        {
            var param = new SqlParameter
            {
                ParameterName = name,
                DbType = type,
                Direction = ParameterDirection.Input,
                Value = value
            };

            cmd.Parameters.Add(param);

            return cmd;
        }

        public static DbParameter SetParam(this DbCommand cmd, string name, DbType type = DbType.String, ParameterDirection direction = ParameterDirection.InputOutput, int size = 0, Object value = null)
        {
            var param = new SqlParameter
            {
                ParameterName = name,
                DbType = type,
                Direction = direction,
                Size = size,
                Value = value
            };

            cmd.Parameters.Add(param);

            return param;
        }
    }

    public static class DbReaderExtensions
    {
        public static T ReadValue<T>(this DbDataReader reader, string fieldName, T defaultValue)
        {
            var ordinal = reader.GetOrdinal(fieldName);
            return reader.IsDBNull(ordinal) ? defaultValue : reader.GetFieldValue<T>(ordinal);
        }
    }

    public static class SqlParameterExtensions
    {
        public static T As<T>(this DbParameter param)
        {
            return As(param, default(T));
        }

        public static T As<T>(this DbParameter param, T defaultValue)
        {
            return param.Value == DBNull.Value ? defaultValue : (T)param.Value;
        }
    }
}
