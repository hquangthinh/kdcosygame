﻿using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using _ = KinhDo.CosyPhotoGame.InstallerAction.Functions;

namespace KinhDo.CosyPhotoGame.InstallerAction
{
    public class DatabaseUpgrade
    {
        private static readonly Regex ScriptExp = new Regex(@"(CosyDb)_(?<major>\d+)\.(?<minor>\d+)\.(?<build>\d+)_(Base|Upgrade|DataMigration)");

        private static Func<string, VersionInfo> RetrieveScriptVersion(Regex regex)
        {
            return fileName =>
            {
                ushort majorVal;
                ushort minorVal;
                ushort buildVal;

                var match = regex.Match(fileName);
                var major = match.Groups["major"].If(g => g.Success)?.Value;
                var minor = match.Groups["minor"].If(g => g.Success)?.Value;
                var build = match.Groups["build"].If(g => g.Success)?.Value;

                if (!ushort.TryParse(major, out majorVal)) majorVal = 0;
                if (!ushort.TryParse(minor, out minorVal)) minorVal = 0;
                if (!ushort.TryParse(build, out buildVal)) buildVal = 0;

                return new VersionInfo(majorVal, minorVal, buildVal);
            };
        }

        private static readonly Func<string, VersionInfo> RetrieveCosyDbScriptVersion = RetrieveScriptVersion(ScriptExp);

        private string CosyDatabase { get; }
        private string CosySchemaScriptDir { get; }
        private string CosyDataMigrationtDir { get; }

        public DatabaseUpgrade(string cosyDb, string scriptFolder)
        {
            CosyDatabase = cosyDb;
            CosySchemaScriptDir = Path.Combine(scriptFolder, "Schema");
            CosyDataMigrationtDir = Path.Combine(scriptFolder, "DataMigration");
        }

        public IOrderedEnumerable<ScriptInfo> LoadUpgradableScripts(VersionInfo appVersion, VersionInfo currentDbVersion)
        {
            var cosySchemaDbScripts =
                ScriptInfo.LoadAllFromDirectory(CosySchemaScriptDir, CosyDatabase, RetrieveCosyDbScriptVersion,
                        ScriptType.SchemaUpgrade)
                    .ToArray();

            var cosyDataMigrationDbScripts =
                ScriptInfo.LoadAllFromDirectory(CosyDataMigrationtDir, CosyDatabase, RetrieveCosyDbScriptVersion,
                        ScriptType.DataMigration)
                    .ToArray();

            var versionMatches = _.Lambda((ScriptInfo script) =>
                script.Version.MajorDotMinor >= currentDbVersion.MajorDotMinor &&
                script.Version.MajorDotMinor <= appVersion.MajorDotMinor);

            var scriptTypeOrder = _.Lambda((ScriptInfo script) => script.ScriptType == ScriptType.DataMigration ? 2 : 1);

            return cosySchemaDbScripts
                    .Union(cosyDataMigrationDbScripts)
                    .Where(versionMatches)
                    .OrderBy(script => script.Version.MajorDotMinor)
                    .ThenBy(scriptTypeOrder)
                    .ThenBy(script => script.Version);
        }

        public string ReadScriptContent(ScriptInfo script)
        {
            return File.ReadAllText(script.FullName);
        }
    }

    public class ScriptInfo
    {
        public string Name { get; }
        public string FullName { get; }
        public VersionInfo Version { get; }
        public string TargetDatabase { get; }
        public ScriptType ScriptType { get; }

        private ScriptInfo(string name, string fullName, VersionInfo version, string targetDb, ScriptType scriptType)
        {
            Name = name;
            FullName = fullName;
            Version = version;
            TargetDatabase = targetDb;
            ScriptType = scriptType;
        }

        public static ScriptInfo[] LoadAllFromDirectory(string path, string targetDatabase, Func<string, VersionInfo> versionSelector, ScriptType scriptType)
        {
            return new DirectoryInfo(path)
                .GetFiles()
                .Select(f => new ScriptInfo(f.Name, f.FullName, versionSelector(f.Name), targetDatabase, scriptType))
                .ToArray();
        }
    }

    public class VersionInfo : IComparable<VersionInfo>
    {
        public ushort Major { get; }
        public ushort Minor { get; }
        public ushort Build { get; }

        public VersionInfo(ushort major, ushort minor = 0, ushort build = 0)
        {
            Major = major;
            Minor = minor;
            Build = build;
        }

        public static VersionInfo FromValue(string versionValue)
        {
            var portions = versionValue.Split('.');
            var major = Convert.ToUInt16(portions.ElementAtOrDefault(0));
            var minor = Convert.ToUInt16(portions.ElementAtOrDefault(1));
            var build = Convert.ToUInt16(portions.ElementAtOrDefault(2));
            return new VersionInfo(major, minor, build);
        }

        public override string ToString()
        {
            return $"{Major}.{Minor}.{Build}";
        }

        private VersionInfo _majorDotMinor;

        /// <summary>
        /// Returns the version with the Build portion omitted,
        /// i.e. returns 10.6 for 10.6.105
        /// </summary>
        public VersionInfo MajorDotMinor => _majorDotMinor ?? (_majorDotMinor = new VersionInfo(Major, Minor));

        public static bool operator <(VersionInfo left, VersionInfo right)
        {
            return left.GetValue() < right.GetValue();
        }

        public static bool operator <=(VersionInfo left, VersionInfo right)
        {
            return left.GetValue() <= right.GetValue();
        }

        public static bool operator >(VersionInfo left, VersionInfo right)
        {
            return left.GetValue() > right.GetValue();
        }

        public static bool operator >=(VersionInfo left, VersionInfo right)
        {
            return left.GetValue() >= right.GetValue();
        }

        /// <summary>
        /// Converts the version into a numeric value which is easier for comparison purpose
        /// </summary>
        /// <returns>  10000100012 if given   1.1.12 or   1.00001.00012</returns>
        /// <returns>1000000100012 if given 100.1.12 or 100.00001.00012</returns>
        private long GetValue()
        {
            // major*1e10 + minor*1e5 + build
            return Major * 10000000000L + Minor * 100000L + Build * 1L;
        }

        public int CompareTo(VersionInfo other)
        {
            return Math.Sign(GetValue() - other.GetValue());
        }
    }

    public enum ScriptType
    {
        SchemaUpgrade,
        DataMigration
    }
}
