﻿using System;

namespace KinhDo.CosyPhotoGame.InstallerAction
{
    public static class MaybeMonad
    {
        public static TResult With<T, TResult>(this T input, Func<T, TResult> evaluator)
            where T : class
            where TResult : class
        {
            return input == null ? null : evaluator(input);
        }

        public static TResult? Return<T, TResult>(this T input, Func<T, TResult> evaluator)
            where T : class
            where TResult : struct
        {
            return input == null ? (TResult?)null : evaluator(input);
        }

        public static TResult Return<T, TResult>(this T input, Func<T, TResult> evaluator, TResult defaultValue)
            where T : class
        {
            return input == null ? defaultValue : evaluator(input);
        }

        public static TResult Return<T, TResult>(this T input, Func<T, TResult> evaluator, Func<TResult> defaultValueFactory)
            where T : class
        {
            return input == null ? defaultValueFactory() : evaluator(input);
        }

        public static T If<T>(this T input, Func<T, bool> predicate) where T : class
        {
            if (input == null) return null;
            return predicate(input) ? input : null;
        }

        public static T? IfN<T>(this T? input, Func<T, bool> predicate) where T : struct
        {
            if (input == null) return null;
            return predicate(input.Value) ? input : null;
        }

        public static T Unless<T>(this T input, Func<T, bool> predicate) where T : class
        {
            if (input == null) return null;
            return predicate(input) ? null : input;
        }

        public static T? UnlessN<T>(this T? input, Func<T, bool> predicate) where T : struct
        {
            if (input == null) return null;
            return predicate(input.Value) ? null : input;
        }

        public static T Do<T>(this T input, Action<T> action) where T : class
        {
            if (input == null) return null;
            action(input);
            return input;
        }

        public static U? Map<T, U>(this T? input, Func<T, U> map) where T : struct where U : struct
        {
            return input.HasValue ? map(input.Value) : (U?)null;
        }

        public static Maybe<U> MapM<T, U>(this T? input, Func<T, U> map) where T : struct where U : class
        {
            return input.HasValue ? map(input.Value) : null;
        }

        public static U? Bind<T, U>(this T? input, Func<T, U?> bind)
            where T : struct where U : struct
        {
            return input.HasValue ? bind(input.Value) : null;
        }

        public static Maybe<U> Bind<T, U>(this T? input, Func<T, Maybe<U>> bind)
            where T : struct where U : class
        {
            return input.HasValue ? bind(input.Value) : null;
        }

        public static T Unwrap<T>(this T? input, T defaultValue = default(T)) where T : struct
        {
            return input ?? defaultValue;
        }

        public static T Unwrap<T>(this T? input, Func<T> defaultValueFactory) where T : struct
        {
            return input ?? defaultValueFactory();
        }

        public static T? OnNoValueRaiseError<T>(this T? input, string message) where T : struct
        {
            if (!input.HasValue) throw new ApplicationException(message);
            return input;
        }

        public static T? OnNoValueRaiseError<T, TException>(this T? input, Func<TException> exceptionFactory)
            where T : struct
            where TException : Exception
        {
            if (!input.HasValue) throw exceptionFactory();
            return input;
        }

        public static T? If<T>(this T? input, Func<T, bool> predicate) where T : struct
        {
            if (!input.HasValue) return null;
            return predicate(input.Value) ? input : null;
        }

        public static T? Unless<T>(this T? input, Func<T, bool> predicate) where T : struct
        {
            if (!input.HasValue) return null;
            return predicate(input.Value) ? null : input;
        }

        public static T? Do<T>(this T? input, Action<T> action) where T : struct
        {
            if (!input.HasValue) return null;
            action(input.Value);
            return input;
        }

        public static Maybe<T> Maybe<T>(this T value) where T : class
        {
            return value;
        }

        public static Maybe<U> Select<T, U>(this Maybe<T> self, Func<T, U> map) where U : class where T : class
        {
            return self.Map(map);
        }

        public static Maybe<V> SelectMany<T, U, V>(this Maybe<T> self, Func<T, Maybe<U>> bind, Func<T, U, V> project)
            where T : class where U : class where V : class
        {
            if (bind == null) throw new ArgumentNullException(nameof(bind));
            if (project == null) throw new ArgumentNullException(nameof(project));

            if (!self.HasValue) return null;

            var maybeU = bind(self.Value);
            return maybeU.HasValue ? project(self.Value, maybeU.Value) : null;
        }
    }

    public struct Maybe<T> : IEquatable<Maybe<T>> where T : class
    {
        private readonly T _value;

        public T Value
        {
            get
            {
                if (!HasValue) throw new InvalidOperationException();
                return _value;
            }
        }

        public bool HasValue => _value != null;

        private Maybe(T value)
        {
            _value = value;
        }

        public static Maybe<T> Of(T value)
        {
            return new Maybe<T>(value);
        }

        public Maybe<TResult> Map<TResult>(Func<T, TResult> map)
           where TResult : class
        {
            return HasValue ? map(Value) : null;
        }

        public TResult? MapN<TResult>(Func<T, TResult> map)
            where TResult : struct
        {
            return HasValue ? map(Value) : (TResult?)null;
        }

        public TResult? Bind<TResult>(Func<T, TResult?> bind)
           where TResult : struct
        {
            return HasValue ? bind(Value) : null;
        }

        public Maybe<TResult> Bind<TResult>(Func<T, Maybe<TResult>> bind)
           where TResult : class
        {
            return HasValue ? bind(Value) : null;
        }

        public Maybe<T> OnNoValueRaiseError(string message)
        {
            if (!HasValue) throw new ApplicationException(message);
            return this;
        }

        public Maybe<T> OnNoValueRaiseError<TException>(Func<TException> exceptionFactory) where TException : Exception
        {
            if (!HasValue) throw exceptionFactory();
            return this;
        }

        public Maybe<T> If(Func<T, bool> predicate)
        {
            if (!HasValue) return null;
            return predicate(Value) ? this : null;
        }

        public Maybe<T> Unless(Func<T, bool> predicate)
        {
            if (!HasValue) return null;
            return predicate(Value) ? null : this;
        }

        public Maybe<T> Do(Action<T> action)
        {
            if (HasValue) action(Value);
            return this;
        }

        public static implicit operator Maybe<T>(T value)
        {
            return new Maybe<T>(value);
        }

        public static bool operator ==(Maybe<T> maybe, T value)
        {
            return maybe.HasValue && maybe.Value.Equals(value);
        }

        public static bool operator !=(Maybe<T> maybe, T value)
        {
            return !(maybe == value);
        }

        public static bool operator ==(Maybe<T> first, Maybe<T> second)
        {
            return first.Equals(second);
        }

        public static bool operator !=(Maybe<T> first, Maybe<T> second)
        {
            return !(first == second);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is Maybe<T>)) return false;

            var other = (Maybe<T>)obj;
            return Equals(other);
        }

        public bool Equals(Maybe<T> other)
        {
            if (!HasValue && !other.HasValue) return true;
            if (!HasValue || !other.HasValue) return false;

            return _value.Equals(other._value);
        }

        public override int GetHashCode()
        {
            return HasValue ? _value.GetHashCode() : 0;
        }

        public override string ToString()
        {
            return HasValue ? Value.ToString() : "No value";
        }

        public T Unwrap(T defaultValue = default(T))
        {
            return HasValue ? Value : defaultValue;
        }

        public T Unwrap(Func<T> defaultValueFactory)
        {
            return HasValue ? Value : defaultValueFactory();
        }
    }
}
