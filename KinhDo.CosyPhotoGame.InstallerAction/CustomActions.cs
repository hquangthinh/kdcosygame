﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.AccessControl;
using System.Security.Principal;
using System.Text;
using Microsoft.Deployment.WindowsInstaller;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using Microsoft.Web.Administration;
using Microsoft.Win32;
using static KinhDo.CosyPhotoGame.InstallerAction.Variables;
using Database = Microsoft.SqlServer.Management.Smo.Database;

namespace KinhDo.CosyPhotoGame.InstallerAction
{
    public static class Variables
    {
        public const string CosyVersion = "COSYGAME_VERSION";

        // IIS setting properties
        public const string IisWebGameHostHeader = "IIS_WEB_GAME_HOST_HEADER";
        public const string IisWebAdminHostHeader = "IIS_WEB_ADMIN_HOST_HEADER";
        public const string IisWebGameName = "IIS_WEB_GAME_NAME";
        public const string IisWebGameWebAppId = "IIS_WEB_GAME_WEB_APP_ID";
        public const string IisWebAdminName = "IIS_WEB_ADMIN_NAME";
        public const string IisWebAdminWebAppId = "IIS_WEB_ADMIN_WEB_APP_ID";
        public const string IisValid = "IIS_VALID";

        public const string CosyWebGameAppPool = "COSY_WEB_GAME_APP_POOL";
        public const string CosyWebAdminAppPool = "COSY_WEB_ADMIN_APP_POOL";

        public const string DbInstallIntegratedSecurity = "DB_INST_INTEGRATED_SECURITY";
        public const string DbInstallSqlUser = "DB_INST_USER";
        public const string DbInstallSqlPassword = "DB_INST_PASSWORD";

        public const string DbInitDefaultAdmin = "DB_INIT_DEFAULT_ADMIN";
        public const string DbVersion = "DB_VERSION";
        public const string DbServer = "DB_SERVER";
        public const string DbExecutionIntegratedSecurity = "DB_INTEGRATED_SECURITY";
        public const string DbDatabase = "DB_DATABASE";
        public const string DbExists = "DB_EXISTS";

        public const string DbIntegratedDomain = "DB_INTEGRATED_DOMAIN";
        public const string DbIntegratedUser = "DB_INTEGRATED_USER";
        public const string DbIntegratedPassword = "DB_INTEGRATED_PASSWORD";
        public const string DbUser = "DB_USER";
        public const string DbPassword = "DB_PASSWORD";
        public const string DbPasswordEncrypted = "DB_PASSWORD_ENCRYPTED";
        public const string DbExecutionValid = "DB_EXECUTION_VALID";

        public const string DbConnectionValid = "DB_CONNECTION_VALID";

        public const string WinServiceUser = "WIN_SERVICE_USER";
        public const string WinServicePassword = "WIN_SERVICE_PASSWORD";
        public const string WinServiceDomain = "WIN_SERVICE_DOMAIN";

        public const string WebGameFolder = "WebGameFolder";
        public const string WebGameContentUserDataFolder = "WebGameContentUserDataFolder";
        public const string WebGameAppDataFolder = "WebGameAppDataFolder";
        public const string WebAdminFolder = "WebAdminFolder";
        public const string DbScriptFolder = "DbScriptFolder";
        public const string AppLogFolder = "AppLogFolder";
        public const string WebGameAppDataTempFolder = "WebGameAppDataTempFolder";
        public const string WebGameGameFolder = "WebGameGameFolder";
    }

    public class CustomActions
    {
        [CustomAction]
        public static ActionResult CheckWebServerConfiguration(Session session)
        {
            WriteLog(session, "Begin CheckWebServerConfiguration");
            
            // All values cannot be blank
            var isValid = !string.IsNullOrWhiteSpace(session[IisWebGameHostHeader]);
            isValid = !string.IsNullOrWhiteSpace(session[IisWebAdminHostHeader]) && isValid;

            isValid = !string.IsNullOrWhiteSpace(session[IisWebGameName]) && isValid;
            isValid = !string.IsNullOrWhiteSpace(session[IisWebAdminName]) && isValid;

            WriteLog(session, $"All values cannot be blank -> {isValid}");

            // Check duplicated
            isValid = !session[IisWebGameHostHeader].Trim().Equals(session[IisWebAdminHostHeader].Trim(), StringComparison.CurrentCultureIgnoreCase) && isValid;
            isValid = !session[IisWebGameName].Trim().Equals(session[IisWebAdminName].Trim(), StringComparison.CurrentCultureIgnoreCase) && isValid;

            WriteLog(session, $"All values must be unique -> {isValid}");

            session[IisValid] = isValid ? "1" : "0";

            WriteLog(session, "CheckWebServerConfiguration Result: " + session[IisValid]);
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult CheckDatabaseConnection(Session session)
        {
            WriteLog(session, "Begin CheckDatabaseConnection");
            NormalizeInputField(session, DbInstallSqlUser);
            try
            {
                var server = new Server(OpenServerConnection(session[DbServer], session[DbInstallIntegratedSecurity] == "true", false, "", session[DbInstallSqlUser], session[DbInstallSqlPassword]));
                var cosyDb = server.Databases[session[DbDatabase]];
                session[DbConnectionValid] = "1";
                session[DbExists] = cosyDb == null ? "no" : "yes";
            }
            catch (Exception ex)
            {
                WriteLog(session, "Could not connect to Database server {0}. Error: {1}", session[DbServer], ex);
                session[DbConnectionValid] = "0";
            }

            WriteLog(session, "Database exists: " + session[DbExists]);

            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult SetDbExecutionAuthentication(Session session)
        {
            NormalizeInputField(session, DbUser);
            NormalizeInputField(session, DbIntegratedDomain);
            NormalizeInputField(session, DbIntegratedUser);
            var credentials = SqlExecutionCredential.FromSession(session);
            try
            {
                var server = OpenServerConnection(session[DbServer], credentials.IntegratedSecurity, credentials.IntegratedSecurity, credentials.Domain, credentials.User, credentials.Password);
                server.Connect();
                if (credentials.IntegratedSecurity)
                {
                    session[DbUser] = "";
                    session[DbPassword] = "";
                }
                session[DbExecutionValid] = "1";
            }
            catch (Exception e)
            {
                WriteLog(session, "Cannot connect to database using the provided execution credentials. {0}", e);
                session[DbExecutionValid] = "0";
            }
            return ActionResult.Success;
        }

        [CustomAction]
        public static ActionResult UpgradeDatabase(Session session)
        {
            if (session[DbConnectionValid] != "1") return ActionResult.Success;
            try
            {
                PrepareDatabases(session);
                var currentDbVersion = GetDatabaseVersion(BuildConnectionString(session));
                var cosyVersion = VersionInfo.FromValue(session[CosyVersion]);
                WriteLog(session, "Start upgrading database: current version: {0} - target version: {1}", currentDbVersion, cosyVersion);

                var dbUpgrade = new DatabaseUpgrade(session[DbDatabase], session[DbScriptFolder]);
                var scripts = dbUpgrade.LoadUpgradableScripts(cosyVersion, currentDbVersion).ToArray();
                string currentScript = null;
                try
                {
                    using (var connection = new SqlConnection(BuildConnectionString(session)))
                    {
                        var server = new Server(new ServerConnection(connection));
                        foreach (var script in scripts)
                        {
                            currentScript = script.Name;
                            WriteLog(session, $"Executing script file {currentScript}");
                            server.ConnectionContext.ExecuteNonQuery(BuildExecutableScript(dbUpgrade, script));
                        }
                    }
                    WriteLog(session, "Database upgrade finished successfully.");
                }
                catch (ExecutionFailureException smoex)
                {
                    WriteLog(session, $"Database upgrade script error in file {currentScript}: {smoex.Message}");
                    var ex = smoex.InnerException;
                    var exception = ex as SqlException;
                    if (exception != null) HandleSqlException(session, exception);
                    else
                    {
                        while (!ReferenceEquals(ex.InnerException, (null)))
                        {
                            WriteLog(session, ex.InnerException.Message);
                            ex = ex.InnerException;
                        }
                    }
                    return ShowDbUpgradeFailure(session);
                }
                catch (SqlException ex)
                {
                    WriteLog(session, $"Database upgrade script error in file {currentScript}: {ex.Message}");
                    HandleSqlException(session, ex);
                    return ShowDbUpgradeFailure(session);
                }
                catch (SmoException smoex)
                {
                    WriteLog(session, $"Database upgrade script error in file {currentScript}: {smoex.Message}");
                    var ex = smoex.InnerException;
                    while (!ReferenceEquals(ex.InnerException, (null)))
                    {
                        WriteLog(session, ex.InnerException.Message);
                        ex = ex.InnerException;
                    }
                    return ShowDbUpgradeFailure(session);
                }
            }
            catch (Exception e)
            {
                WriteLog(session, "Database upgrade failed with error: {0}", e);
                return ShowDbUpgradeFailure(session);
            }

            return ActionResult.Success;
        }

        private static string BuildExecutableScript(DatabaseUpgrade dbUpgrade, ScriptInfo script)
        {
            var scriptToExecute = new StringBuilder()
                .AppendLine($"--- BEGIN {script.Name}")
                .AppendLine($"USE {script.TargetDatabase}")
                .AppendLine("GO")
                .AppendLine(dbUpgrade.ReadScriptContent(script))
                .AppendLine($"--- END {script.Name}");

            return scriptToExecute.ToString();
        }

        private static VersionInfo GetDatabaseVersion(string connectionString)
        {
            var newInstallation = new VersionInfo(0);
            if (!IsDatabaseInitialized(connectionString)) return newInstallation;
            return ExecuteDbQuery(connectionString, command =>
            {
                command.CommandType = CommandType.Text;
                command.CommandText = "select top 1 Major, Minor, Build from DatabaseVersion order by Major desc, Minor desc, Build desc";
                using (var reader = command.ExecuteReader())
                {
                    var versions = new List<VersionInfo> { newInstallation };
                    if (reader.Read())
                    {
                        versions.Add(
                            new VersionInfo(
                                (ushort)reader.ReadValue("Major", 0),
                                (ushort)reader.ReadValue("Minor", 0),
                                (ushort)reader.ReadValue("Build", 0)
                            )
                        );
                    }
                    return versions.Max();
                }
            });
        }

        private static bool IsDatabaseInitialized(string connectionString)
        {
            return ExecuteDbQuery(connectionString, command =>
            {
                command.CommandType = CommandType.Text;
                command.CommandText = @"
                        select top 1 Result
                        from (
	                        SELECT 1 as Result FROM sysobjects WHERE ID = OBJECT_ID('[DatabaseVersion]') AND OBJECTPROPERTY(ID, 'IsTable') = 1
	                        union
	                        select 0
                        ) x
                        order by Result desc";
                return (int)command.ExecuteScalar() == 1;
            });
        }

        [CustomAction]
        public static ActionResult StructureInstallationDirectory(Session session)
        {
            WriteLog(session, "Begin StructureInstallationDirectory");
            foreach (var item in session.CustomActionData)
            {
                WriteLog(session, $"session.CustomActionData -> {item.Key}-{item.Value}");
            }
            var everyone = new SecurityIdentifier(WellKnownSidType.WorldSid, null);
            CreateOrAdjustFolderSecuritySettings(session, everyone, session.CustomActionData[AppLogFolder]);
            CreateOrAdjustFolderSecuritySettings(session, everyone, session.CustomActionData[WebGameGameFolder]);
            CreateOrAdjustFolderSecuritySettings(session, everyone, session.CustomActionData[WebGameAppDataTempFolder]);
            CreateOrAdjustFolderSecuritySettings(session, everyone, session.CustomActionData[WebGameContentUserDataFolder]);
            WriteLog(session, "Complete StructureInstallationDirectory");
            return ActionResult.Success;
        }

        private static void CreateOrAdjustFolderSecuritySettings(Session session, SecurityIdentifier securityIdentifier, string folder)
        {
            try
            {
                WriteLog(session, $"CreateOrAdjustFolderSecuritySettings for {folder}");

                var directoryInfo = new DirectoryInfo(folder);
                if (!directoryInfo.Exists)
                {
                    directoryInfo.Create();
                }
                var sec = directoryInfo.GetAccessControl();
                CanonicalizeDacl(sec);
                sec.AddAccessRule(new FileSystemAccessRule(
                    securityIdentifier,
                    FileSystemRights.Modify | FileSystemRights.Synchronize,
                    InheritanceFlags.ContainerInherit | InheritanceFlags.ObjectInherit,
                    PropagationFlags.None,
                    AccessControlType.Allow));
                directoryInfo.SetAccessControl(sec);
            }
            catch (Exception ex)
            {
                WriteLog(session, $"CreateOrAdjustFolderSecuritySettings error -> {ex}");
            }
            
        }

        static void CanonicalizeDacl(NativeObjectSecurity objectSecurity)
        {
            if (objectSecurity == null) { throw new ArgumentNullException(nameof(objectSecurity)); }
            if (objectSecurity.AreAccessRulesCanonical) { return; }

            // A canonical ACL must have ACES sorted according to the following order:
            //   1. Access-denied on the object
            //   2. Access-denied on a child or property
            //   3. Access-allowed on the object
            //   4. Access-allowed on a child or property
            //   5. All inherited ACEs
            RawSecurityDescriptor descriptor = new RawSecurityDescriptor(objectSecurity.GetSecurityDescriptorSddlForm(AccessControlSections.Access));

            List<CommonAce> implicitDenyDacl = new List<CommonAce>();
            List<CommonAce> implicitDenyObjectDacl = new List<CommonAce>();
            List<CommonAce> inheritedDacl = new List<CommonAce>();
            List<CommonAce> implicitAllowDacl = new List<CommonAce>();
            List<CommonAce> implicitAllowObjectDacl = new List<CommonAce>();

            foreach (CommonAce ace in descriptor.DiscretionaryAcl)
            {
                if ((ace.AceFlags & AceFlags.Inherited) == AceFlags.Inherited) { inheritedDacl.Add(ace); }
                else
                {
                    switch (ace.AceType)
                    {
                        case AceType.AccessAllowed:
                            implicitAllowDacl.Add(ace);
                            break;

                        case AceType.AccessDenied:
                            implicitDenyDacl.Add(ace);
                            break;

                        case AceType.AccessAllowedObject:
                            implicitAllowObjectDacl.Add(ace);
                            break;

                        case AceType.AccessDeniedObject:
                            implicitDenyObjectDacl.Add(ace);
                            break;
                    }
                }
            }

            Int32 aceIndex = 0;
            RawAcl newDacl = new RawAcl(descriptor.DiscretionaryAcl.Revision, descriptor.DiscretionaryAcl.Count);
            implicitDenyDacl.ForEach(x => newDacl.InsertAce(aceIndex++, x));
            implicitDenyObjectDacl.ForEach(x => newDacl.InsertAce(aceIndex++, x));
            implicitAllowDacl.ForEach(x => newDacl.InsertAce(aceIndex++, x));
            implicitAllowObjectDacl.ForEach(x => newDacl.InsertAce(aceIndex++, x));
            inheritedDacl.ForEach(x => newDacl.InsertAce(aceIndex++, x));

            if (aceIndex != descriptor.DiscretionaryAcl.Count)
            {
                System.Diagnostics.Debug.Fail("The DACL cannot be canonicalized since it would potentially result in a loss of information");
                return;
            }

            descriptor.DiscretionaryAcl = newDacl;
            objectSecurity.SetSecurityDescriptorSddlForm(descriptor.GetSddlForm(AccessControlSections.Access), AccessControlSections.Access);
        }

        [CustomAction]
        public static ActionResult SaveApplicationSettings(Session session)
        {
            WriteLog(session, "Begin Saving Application Settings");

            try
            {
                var connString = BuildConnectionString(session);

                ExecuteDbQuery(connString, command =>
                {
                    // PlayerGameDataBaseFolder -> Full path to App_Data of web game
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "UPDATE Setting SET SettingValueString = @SettingValue WHERE SettingKey ='PlayerGameDataBaseFolder' ";
                    command.Parameters.AddWithValue("@SettingValue", session[WebGameAppDataFolder]);
                    command.ExecuteNonQuery();

                    command.Parameters.Clear();

                    // GameBaseInstallationFolder -> Full path to web game
                    command.CommandType = CommandType.Text;
                    command.CommandText =
                        "UPDATE Setting SET SettingValueString = @SettingValue WHERE SettingKey ='GameBaseInstallationFolder' ";
                    command.Parameters.AddWithValue("@SettingValue", session[WebGameFolder]);
                    command.ExecuteNonQuery();

                    return true;
                });

                WriteLog(session, "End Saving Application Settings");
                return ActionResult.Success;
            }
            catch (Exception ex)
            {
                WriteLog(session, $"SaveApplicationSettings error -> {ex}");
                return ActionResult.Failure;
            }
        }

        [CustomAction]
        public static ActionResult CleanupInstallation(Session session)
        {
            WriteLog(session, "Begin CleanupInstallation");
            // Remove all but keep Upload folder
            try
            {
                var regInstallPath = Registry.LocalMachine.OpenSubKey(session["KEYREG_INSTALLPATH"]);
                var appPath = regInstallPath?.GetValue("Path").ToString();
                if (!string.IsNullOrEmpty(appPath))
                {
                    DeleteFolder(session, appPath + @"\WebGame");
                    DeleteFolder(session, appPath + @"\Admin");
                    DeleteFolder(session, appPath + @"\JobServerService");
                    DeleteFolder(session, appPath + @"\Logs");
                    DeleteFolder(session, appPath, false, false);
                }
                // Clean IIS Website
                var iisWebGameSiteName = regInstallPath?.GetValue("WebGameSiteName").ToString();
                var iisWebGameAppPool = regInstallPath?.GetValue("WebGameAppPool").ToString();
                var iisWebAdminSiteName = regInstallPath?.GetValue("WebAdminSiteName").ToString();
                var iisWebAdminAppPool = regInstallPath?.GetValue("WebAdminAppPool").ToString();
                if (!string.IsNullOrEmpty(iisWebGameSiteName))
                {
                    using (var serverManager = new ServerManager())
                    {
                        // Website
                        var webGameSite = serverManager.Sites[iisWebGameSiteName];
                        var webAdminSite = serverManager.Sites[iisWebAdminSiteName];
                        serverManager.Sites.Remove(webGameSite);
                        serverManager.Sites.Remove(webAdminSite);

                        // Apppool
                        var webGameAppPool = serverManager.ApplicationPools[iisWebGameAppPool];
                        var webAdminAppPool = serverManager.ApplicationPools[iisWebAdminAppPool];
                        serverManager.ApplicationPools.Remove(webGameAppPool);
                        serverManager.ApplicationPools.Remove(webAdminAppPool);

                        serverManager.CommitChanges();
                    }
                }

                WriteLog(session, "Complete CleanupInstallation");
                return ActionResult.Success;
            }
            catch (Exception e)
            {
                WriteLog(session, "Cleanup failed. Error {0}", e);
                return ActionResult.Failure;
            }
        }

        private static TResult ExecuteDbQuery<TResult>(string connectionString, Func<SqlCommand, TResult> command)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    return command(cmd);
                }
            }
        }

        private static string BuildConnectionString(Session session)
        {
            return
                $"Data Source={session[DbServer]};Connect Timeout=60;Initial Catalog={session[DbDatabase]};Persist Security Info=True;Integrated Security={session[DbInstallIntegratedSecurity]};User ID={session[DbInstallSqlUser]};Password={session[DbInstallSqlPassword]}";
        }

        private static void PrepareDatabases(Session session)
        {
            var dbServer =
                new Server(OpenServerConnection(session[DbServer], session[DbInstallIntegratedSecurity] == "true", false,
                    "", session[DbInstallSqlUser], session[DbInstallSqlPassword]));
            var cosyDb = dbServer.Databases[session[DbDatabase]];
            
            if (cosyDb == null)
            {
                cosyDb = new Database(dbServer, session[DbDatabase]);
                cosyDb.Create();
            }

            try
            {
                var credentials = SqlExecutionCredential.FromSession(session);
                if (credentials.IntegratedSecurity)
                {
                    WriteLog(session, "Using integrated security");
                    // create login and database mappings
                    var loginName = string.IsNullOrEmpty(credentials.Domain)
                        ? credentials.User
                        : credentials.Domain + "\\" + credentials.User;

                    var login = dbServer.Logins[loginName];
                    if (login == null)
                    {
                        login = new Login(dbServer, loginName) { LoginType = LoginType.WindowsUser };
                        login.Create();
                    }
                    CreateUserMapping(login, loginName, cosyDb);
                }
                else
                {
                    WriteLog(session, "Using SQL Server login");
                    var loginName = credentials.User;
                    var login = dbServer.Logins[loginName];
                    CreateUserMapping(login, loginName, cosyDb);
                }
            }
            catch (Exception e)
            {
                WriteLog(session, "Failed to create login/user mappings. Error {0}", e);
            }
        }

        private static void CreateUserMapping(Login sqlLogin, string loginName, Database db)
        {
            var armDbLogins = db.EnumLoginMappings();
            if (armDbLogins.AsEnumerable().All(r => (string)r["LoginName"] != loginName))
            {
                var user = new User(db, loginName)
                {
                    Login = sqlLogin.Name,
                    UserType = UserType.SqlLogin
                };
                user.Create();
                user.AddToRole("db_owner");
            }
        }

        private static ActionResult ShowDbUpgradeFailure(Session session)
        {
            ShowMessage(session, InstallMessage.FatalExit, "Database Install and/or Upgrade Failure.");
            return ActionResult.Failure;
        }

        private static void ShowMessage(Session session, InstallMessage installMessage, string text)
        {
            session.Message(installMessage, new Record { FormatString = text });
        }

        private static void HandleSqlException(Session session, SqlException ex)
        {
            for (int i = 0; i < ex.Errors.Count; i++)
            {
                WriteLog(session, "  Message: " + ex.Errors[i].Message + Environment.NewLine +
                                  "  LineNumber: " + ex.Errors[i].LineNumber + Environment.NewLine +
                                  "  Source: " + ex.Errors[i].Source + Environment.NewLine +
                                  "  Procedure: " + ex.Errors[i].Procedure + Environment.NewLine);
            }
        }

        private static ServerConnection OpenServerConnection(string server, bool integratedSecrutiy, bool connectAs, string domain, string login, string password)
        {
            var domainIsLocalhost = DomainIsLocalhost(domain, server);
            var result = new ServerConnection
            {
                ServerInstance = server,
                LoginSecure = integratedSecrutiy
            };
            if (integratedSecrutiy)
            {
                if (connectAs)
                {
                    result.ConnectAsUser = true;
                    result.ConnectAsUserName = domainIsLocalhost ? login : login + "@" + domain;
                    result.ConnectAsUserPassword = password;
                }
            }
            else
            {
                result.Login = login;
                result.Password = password;
            }
            return result;
        }

        private static bool DomainIsLocalhost(string domain, string dbServer)
        {
            return string.IsNullOrEmpty(domain)
                   || domain.EqualsIgnoreCase(dbServer)
                   || domain.EqualsIgnoreCase("localhost");
        }

        private static void NormalizeInputField(Session session, string field)
        {
            session[field] = session[field].Trim();
        }

        private static void DeleteFolder(Session session, string folderPath, bool removeItself = true, bool includeSubfolder = true)
        {
            WriteLog(session, "Deleting " + folderPath);
            var folderInfo = new DirectoryInfo(folderPath);
            if (folderInfo.Exists)
            {
                foreach (var file in folderInfo.GetFiles())
                    file.Delete();
                if (includeSubfolder)
                {
                    foreach (var folder in folderInfo.GetDirectories())
                        folder.Delete(true);
                }
                if (removeItself)
                    folderInfo.Delete();
            }
            WriteLog(session, "Finished deleting " + folderPath);
        }

        private static void WriteLog(Session session, string message, params object[] args)
        {
            try
            {
                session.Log(message, args);
                using (var sw = new StreamWriter("C:\\temp\\cosy-installer.log", append: true))
                {
                    sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss - ") + message, args);
                }
            }
            catch { /* do nothing */ }
        }

        class SqlExecutionCredential
        {
            public bool IntegratedSecurity { get; private set; }
            public string Domain { get; private set; }
            public string User { get; private set; }
            public string Password { get; private set; }

            public static SqlExecutionCredential FromSession(Session session)
            {
                var integratedSecurity = session[DbExecutionIntegratedSecurity] == "true";
                var domain = integratedSecurity ? session[DbIntegratedDomain] : "";
                var user = integratedSecurity
                    ? session[DbIntegratedUser]
                    : session[DbUser];
                var password = integratedSecurity
                    ? session[DbIntegratedPassword]
                    : session[DbPassword];

                WriteLog(session, "IntegratedSecurity = {0}", integratedSecurity);
                WriteLog(session, "user = {0}", user);
                WriteLog(session, "password = {0}", password);

                return new SqlExecutionCredential
                {
                    IntegratedSecurity = integratedSecurity,
                    Domain = domain,
                    User = user,
                    Password = password
                };
            }
        }
    }
}
