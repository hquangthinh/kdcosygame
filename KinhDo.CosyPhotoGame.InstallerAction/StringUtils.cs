﻿using System;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace KinhDo.CosyPhotoGame.InstallerAction
{
    public static class StringUtils
    {
        /// <summary>
        /// Check length of input string (and required it)
        /// </summary>
        public static bool InMaxLength(this string source, int length = 256, bool required = false)
        {
            return (required && IsNotNullOrEmpty(source) && source.Trim().Length <= length)
                || (!required && source.Length <= length);
        }

        /// <summary>
        /// Check string include special characters by array of string
        /// </summary>
        public static bool HasCharacters(this string source, string[] chars)
        {
            for (var i = 0; i < chars.Length; i++)
            {
                if (source.IndexOf(chars[i]) != -1)
                    return true;
            }
            return false;
        }

        /// <summary>
        /// the source string is not null or empty
        /// </summary>
        public static bool IsNotNullOrEmpty(this string source)
        {
            return !string.IsNullOrEmpty(source);
        }

        /// <returns>either the passed in string, or if the string is null, the default value</returns>
        public static string DefaultIfNullOrEmpty(this string source, string defaultValue)
        {
            return string.IsNullOrEmpty(source) ? defaultValue : source;
        }

        /// <returns>either the passed in string, or if the string is null, an empty string</returns>
        public static string DefaultString(this string source)
        {
            return DefaultIfNullOrEmpty(source, string.Empty);
        }

        public static string SubstringSafe(this string source, int startIndex, int length)
        {
            return new string(source.DefaultString().Skip(startIndex).Take(length).ToArray());
        }

        public static string SafeReplace(this string source, string oldString, string newString)
        {
            if (string.IsNullOrEmpty(source)) return source;
            return source.Replace(oldString, newString);
        }

        /// <summary>
        /// Make a long string become shorter with max length and the remain characters is replaced with ...
        /// </summary>
        /// <param name="source"></param>
        /// <param name="maxLength"></param>
        /// <returns></returns>
        public static string MakeShortString(this string source, int maxLength)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            if (source.Length > maxLength) return source.SubstringSafe(0, maxLength) + "...";
            return source;
        }

        /// <summary>
        /// Split the string using the separator and remove empty entries
        /// </summary>
        public static string[] SplitBy(this string source, params string[] separator)
        {
            return source.Split(separator, StringSplitOptions.RemoveEmptyEntries);
        }

        /// <summary>
        /// Compares two strings, ignoring their cases
        /// </summary>
        public static bool EqualsIgnoreCase(this string source, string other)
        {
            return string.Equals(source, other, StringComparison.InvariantCultureIgnoreCase);
        }

        public static bool ContainsIgnoreCase(this string source, string other)
        {
            if (string.IsNullOrEmpty(source)) return false;
            return source.IndexOf(other, StringComparison.InvariantCultureIgnoreCase) >= 0;
        }

        public static bool ValidHttpImageUrl(this string source)
        {
            if (string.IsNullOrEmpty(source)) return false;
            var regex = new Regex(@"(http(s?):)|([/|.|\w|\s])*\.(?:jpg|gif|png)");
            return regex.Match(source).Success;
        }

        /// <summary>
        /// Converts the target string into a byte array
        /// </summary>
        public static byte[] GetBytes(this string source)
        {
            if (source == null) return null;
            var bytes = new byte[source.Length * sizeof(char)];
            Buffer.BlockCopy(source.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }

        public static byte[] GetBytesFromHex(this string hex)
        {
            var input = hex
                .Replace("-", "")
                .Replace(" ", "");
            var result = new byte[hex.Length / 2];
            int index = 0;
            for (var i = 0; i < input.Length; i += 2) // every two hex characters are converted into 1 byte
            {
                result[index++] = Convert.ToByte(input.Substring(i, 2), 16);
            }
            return result;
        }

        public static string ToHexString(this byte[] data, bool binaryFormat = false)
        {
            var hex = new StringBuilder(data.Length * 2); // a hex string needs 2 characters to represent a byte
            var separator = binaryFormat ? " " : "";
            foreach (byte b in data)
            {
                hex.Append(Convert.ToString(b, 16).PadLeft(2, '0'));
                hex.Append(separator);
            }
            return hex.ToString().TrimEnd();
        }

        public static string ToCharSequence(this byte[] data)
        {
            var hex = new StringBuilder(data.Length);
            foreach (var b in data)
            {
                hex.Append(Convert.ToChar(b));
            }
            return hex.ToString();
        }

        public static WildcardedString EscapeWildcard(this string source, char wildcard, Func<string, string> action)
        {
            return WildcardedString.From(source, wildcard).Mutate(action);
        }

        public static bool IsBitValue(this string source, out bool boolValue)
        {
            if (!"0".Equals(source) && !"1".Equals(source))
            {
                boolValue = false;
                return false;
            }
            boolValue = "1".Equals(source);
            return true;
        }

        /// <summary>
        /// Extension method for string to check if a string value is equal to Boolean value true or false
        /// </summary>
        /// <param name="source">A case insensive string with value Y/N Yes/No true/false, T/F 0/1</param>
        /// <returns></returns>
        public static bool IsTrueValue(this string source)
        {
            if (string.IsNullOrEmpty(source)) return false;
            var lowerCaseStr = source.ToLower();
            if (lowerCaseStr.Equals("y") || lowerCaseStr.Equals("yes") || lowerCaseStr.Equals("1") ||
                lowerCaseStr.Equals("true") || lowerCaseStr.Equals("t")) return true;
            return false;
        }

        public static bool IsValueInRange(this string source, params string[] valueRanges)
        {
            return valueRanges.ToList().IndexOf(source) > -1;
        }

        /// <summary>
        /// Convert a possible long user name to a short user name (max 20 chars) to be compatible with some name fields in Epicor
        /// </summary>
        /// <param name="target"></param>
        /// <returns></returns>
        public static string ToSafeLengthName(this string target)
        {
            if (string.IsNullOrEmpty(target)) return string.Empty;

            if (target.Length <= 20) return target;

            var pos = target.IndexOf("\\", StringComparison.Ordinal);
            if (pos > -1)
                return target.SubstringSafe(pos + 1, target.Length);

            throw new ArgumentException($"The name {target} exceeds maximum length of 20 characters.");
        }

        /// <summary>
        /// Convert a path with backward slash (\) to a path with forward slash (/)
        /// </summary>
        /// <param name="source"></param>
        /// <returns></returns>
        public static string ToUrlPath(this string source)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            return source.Replace("\\", "/");
        }

        public static string AppendString(this string source, string appendedString)
        {
            return string.IsNullOrEmpty(source) ? string.Empty : $"{source}{appendedString}";
        }

        public class WildcardedString
        {
            private bool _atStart;
            private bool _atEnd;
            private char _wildcard;
            private string _value;

            public string Unescape()
            {
                var result = _value;
                if (_atStart) result = _wildcard + result;
                if (_atEnd) result += _wildcard;
                return result;
            }

            public override string ToString()
            {
                return _value;
            }

            internal static WildcardedString From(string source, char wildcard)
            {
                if (source == null) return Empty();
                var result = new WildcardedString
                {
                    _atStart = source.Length > 0 && source[0] == wildcard,
                    _atEnd = source.Length > 1 && source[source.Length - 1] == wildcard,
                    _wildcard = wildcard,
                    _value = source.Trim(wildcard)
                };
                return result;
            }

            internal WildcardedString Mutate(Func<string, string> action)
            {
                if (_value != null) _value = action(_value);
                return this;
            }

            private static WildcardedString Empty()
            {
                return new WildcardedString { _value = null };
            }
        }
    }
}
