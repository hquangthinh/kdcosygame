﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using Xunit.Abstractions;

namespace KinhDo.CosyAppAdmin.Tests
{
    public class LogOutputTester : IDisposable
    {
        protected readonly IAppenderAttachable Attachable;
        protected readonly TestOutputAppender Appender;
        protected readonly ITestOutputHelper TestOutputHelper;

        protected LogOutputTester(ITestOutputHelper output)
        {
            TestOutputHelper = output;
            log4net.Config.XmlConfigurator.Configure();
            var root = ((log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository()).Root;
            Attachable = root;

            Appender = new TestOutputAppender(output);
            Attachable?.AddAppender(Appender);
        }

        /// <summary>
        /// Performs application-defined tasks associated with freeing, releasing, or resetting unmanaged resources.
        /// </summary>
        public void Dispose()
        {
            Attachable.RemoveAppender(Appender);
        }
    }

    public sealed class TestOutputAppender : AppenderSkeleton
    {
        private readonly ITestOutputHelper _xunitTestOutputHelper;

        public TestOutputAppender(ITestOutputHelper xunitTestOutputHelper)
        {
            _xunitTestOutputHelper = xunitTestOutputHelper;
            Name = "TestOutputAppender";
            Layout = new PatternLayout("%-5p %d %5rms %-22.22c{1} %-18.18M - %m%n");
            Threshold = Level.All;
        }

        protected override void Append(LoggingEvent loggingEvent)
        {
            _xunitTestOutputHelper.WriteLine(RenderLoggingEvent(loggingEvent));
        }
    }
}