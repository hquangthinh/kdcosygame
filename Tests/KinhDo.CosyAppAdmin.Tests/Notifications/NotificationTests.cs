﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using KinhDo.CosyPhotoGame.EmotionApi.Messaging;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using Xunit;
using Shouldly;
using Xunit.Abstractions;
using KinhDo.CosyPhotoGame.Web.Extensions;

namespace KinhDo.CosyAppAdmin.Tests.Notifications
{
    public class NotificationTests
    {
        private readonly ITestOutputHelper _output;

        public NotificationTests(ITestOutputHelper output)
        {
            _output = output;
        }

        [Fact]
        public void TestSendEmail()
        {
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            SendEmailGameResultReady(new GameResultNotificationCommand
            {
                Email = "hquangthinh@gmail.com",
                GameId = 20,
                WebGameUrl = ConfigurationManager.AppSettings["GameSiteUrl"],
                EmailTemplatePath = Path.Combine(gameSettingDto.GameBaseInstallationFolder, @"Content\email_templates"),
                UserFullName = "Thinh Hua Quang",
                FbProfileId = "556899047853771"
            });
        }

        private void SendEmailGameResultReady(GameResultNotificationCommand command)
        {
            var videoResultUrl = $"{command.WebGameUrl.TrimEnd('/')}/{command.GameResultRelativePath}";
            var templateContent = File.ReadAllText(Path.Combine(command.EmailTemplatePath, "game_result_ready.html"));
            var smtpMessageChannel = new SmtpMessageChannel(new WebConfigSmtpSettings());
            var message = new Message
            {
                Recipients = command.Email,
                Subject = "Cosy đếm điều ngọt ngào từ bạn - video của bạn đã sẵn sàng",
                Body = string.Format(templateContent, command.UserFullName, videoResultUrl)
            };
            smtpMessageChannel.Process(message);
        }

        [Fact]
        public async Task TestSendFbNotification()
        {
            var accessToken = "EAAZAkOl8fjysBACRcZCzdNi94DlSZA1W1RCxticT5HqI6byDmSGykgC70GloHT79zyCE15SGQXDm0MEcmQ1GaMKGVe1oh3GS6SDZCNfyGZCcYtn43VeU03ZBXa5ZB536gpB1p1PPUCx4chv3eA6YPIBKZAxx7ZCcqVi22gd1T76zcsAZDZD";
            var fbNotification = new FacebookMessageChannel(accessToken);
            var response = await fbNotification.SendAsync(new FacebookMessageCommand
            {
                FbProfileId = "10209899916435724",
                GameResultRelativePath = "Game/Result?gameTransactionId=18&UserProfileId=10209899916435724",
                Title = "Cosy đếm điều ngọt ngào từ bạn - video của bạn đã sẵn sàng"
            });
            var resString = await response.Content.ReadAsStringAsync();
            _output.WriteLine(resString);
        }

        [Fact]
        public async Task TestSendFbNotificationWithFbClient_10209899916435724()
        {
            await TestSendFbNotificationWithFbClient(18, "10209899916435724");
        }

        [Fact]
        public async Task TestSendFbNotificationWithFbClient_1405877222759786()
        {
            await TestSendFbNotificationWithFbClient(55, "1405877222759786");
        }

        private async Task TestSendFbNotificationWithFbClient(int gameId, string profileId)
        {
            var appId = "1799051727048491";
            var appSecret = "978b818041944af19f410e94bcfc6e56";
            var template = WebUtility.UrlEncode("Cosy đếm điều ngọt ngào từ bạn - video của bạn đã sẵn sàng");
            var href = WebUtility.UrlEncode($"Game/Result?gameTransactionId={gameId}&UserProfileId={profileId}");

            var fbClient = new FacebookClient { Version = "v2.7" };

            dynamic appAccessTokenResult = fbClient.Post("oauth/access_token",
                new
                {
                    client_id = appId,
                    client_secret = appSecret,
                    grant_type = "client_credentials"
                });

            var appAccessToken = appAccessTokenResult.access_token;

            var apiPath = $"{profileId}/notifications?access_token={appAccessToken}&template={template}&href={href}";

            var result = await fbClient.PostTaskAsync(apiPath, null);

            _output.WriteLine(result.ToString());
        }

        [Fact]
        public void TestSendWithGmail()
        {
            var smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = System.Net.Mail.SmtpDeliveryMethod.Network;
            smtp.UseDefaultCredentials = false;
            smtp.Credentials = new NetworkCredential("kinhdocosy2016@gmail.com", "Q@xxw0rd");
            smtp.Timeout = 20000;

            var from = new MailAddress("kinhdocosy2016@gmail.com", "Cosy");

            var to = new MailAddress("hquangthinh@gmail.com", "Thinh Hua");

            var message = new MailMessage(from, to)
            {
                Body = "This is a test e-mail message sent using gmail as a relay server ",
                Subject = "Gmail test email with SSL and Credentials"
            }; 

            smtp.Send(message);
        }

        [Fact]
        public async Task SendGameResultNotificationReadyTest()
        {
            var setting = new GameSettingService().GetGameGlobalSettings();
            var notificationService = new CosyGameNotificationService();
            await notificationService.SendGameResultNotificationReady(new GameResultNotificationCommand
            {
                GameId = 18,
                UserFullName = "Thinh Hua Quang",
                FbProfileId = "10209899916435724",
                Email = "hquangthinh@gmail.com",
                WebGameUrl = ConfigurationManager.AppSettings["GameSiteUrl"],
                EmailTemplatePath = Path.Combine(setting.GameBaseInstallationFolder, @"Content\email_templates")
            });
        }
    }
}
