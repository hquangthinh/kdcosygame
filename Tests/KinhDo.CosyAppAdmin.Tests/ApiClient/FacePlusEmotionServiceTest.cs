﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyPhotoGame.EmotionApi;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.FacePlusPlus;
using Shouldly;
using Xunit;

namespace KinhDo.CosyAppAdmin.Tests.ApiClient
{
    public class FacePlusEmotionServiceTest
    {
        private string apiKey = "fd92a2af49a1ad60ef97c58dee716c82";
        private string apiSecret = "KTs6wNCjFX2hSsnvjJNJYxWEdL-wHv8I";

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_1()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/t31.0-8/12378070_10208731324181648_3238372936679135692_o.jpg";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBe(4);
            result.Happiness.ShouldBeGreaterThan(50);
        }

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_2()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/v/t1.0-9/4557_1168367450529_3974283_n.jpg?oh=c15db75942e9f7969e60265336c3ccb8&oe=584BAF86";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBe(1);
            result.Happiness.ShouldBeGreaterThan(50);
        }

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_3()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/v/t1.0-9/12235077_10207748798139111_6648701346534676547_n.jpg?oh=6a269707eccb2d8b0353ea050625c01b&oe=5875ECE7";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBe(2);
            result.Happiness.ShouldBeGreaterThan(50);
        }

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_4()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/v/t1.0-9/24980_1401811086474_3524162_n.jpg?oh=eac2a756120be202e40914d5f4d2244f&oe=5837B365";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBe(0);
            result.Happiness.ShouldBe(0);
        }

        private async Task<EmotionResult> DetectEmotion(string photoUrl)
        {
            var fpEmotionService = new FacePlusEmotionService(apiKey, apiSecret);
            var result = await fpEmotionService.DetectEmotionAsync(new PhotoEmotionDetectionCommand
            {
                PhotoUrl = photoUrl
            });
            return result;
        }
    }
}