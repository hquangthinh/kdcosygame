﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyPhotoGame.EmotionApi;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.EmguCv;
using Shouldly;
using Xunit;

namespace KinhDo.CosyAppAdmin.Tests.ApiClient
{
    public class EmguCvEmotionServiceTests
    {
        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_1()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/t31.0-8/12378070_10208731324181648_3238372936679135692_o.jpg";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBeInRange(4, 7);
            result.Happiness.ShouldBeGreaterThan(50);
        }

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_2()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/v/t1.0-9/1000885_663679243646258_673412754_n.jpg?oh=8fc0dd8955e2fcbc8e26e68568717d93&oe=58697B94";
            var result = await DetectEmotion(photoUrl);

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBeInRange(1, 3);
            result.Happiness.ShouldBeGreaterThan(50);
        }

        private async Task<EmotionResult> DetectEmotion(string photoUrl)
        {
            var tempPath = @"D:\proj\kdcosygame\KinhDo.CosyPhotoGame.Web\App_Data\Temp";
            Directory.CreateDirectory(tempPath);
            var emotionService = new EmguCvEmotionService();
            var result = await emotionService.DetectEmotionAsync(new PhotoEmotionDetectionCommand
            {
                PhotoUrl = photoUrl,
                PhotoTempFolderPath = tempPath
            });
            return result;
        }

        [Fact]
        public void TestGetApiProvider()
        {
            var cmd = GetNextAvailableProvider();
            cmd.ShouldNotBeNull();
            cmd.ProviderName.ShouldBe("Face++");

            var cmd1 = GetNextAvailableProvider();
            cmd1.ShouldNotBeNull();
            cmd1.ProviderName.ShouldBe("Face++");

            var cmd2 = GetNextAvailableProvider();
            cmd2.ShouldNotBeNull();
            cmd2.ProviderName.ShouldBe("Face++");
        }

        private EmotionDetectorCreationCommand GetNextAvailableProvider()
        {
            using (var db = new CosyDb())
            {
                var reqCount =
                    db.CvApiProviders.Where(item => item.ApiProvider == "Face++").Min(item => item.RequestCount);
                var minReqCount = reqCount.GetValueOrDefault(0);
                var provider = db.CvApiProviders.FirstOrDefault(p => p.RequestCount == minReqCount) ??
                               db.CvApiProviders.FirstOrDefault();

                if (provider == null)
                {
                    return new EmotionDetectorCreationCommand
                    {
                        ProviderName = "Microsoft",
                        ApiKey = "eb581e237aa14740ab111196c1fafbe4"
                    };
                }

                return new EmotionDetectorCreationCommand
                {
                    ProviderName = provider.ApiProvider,
                    ApiKey = provider.ApiKey,
                    ApiSecret = provider.ApiSecret,
                };
            }
        }
    }
}
