﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyPhotoGame.EmotionApi;
using Shouldly;
using Xunit;

namespace KinhDo.CosyAppAdmin.Tests.ApiClient
{
    public class RoundRobinEmotionDetectorManagerTests
    {
        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_01()
        {
            var run1 = Execute_DetectEmotionAsync_Test_Success_Case_01();
            var run2 = Execute_DetectEmotionAsync_Test_Success_Case_01();
            var run3 = Execute_DetectEmotionAsync_Test_Success_Case_01();
            var run4 = Execute_DetectEmotionAsync_Test_Success_Case_01();
            await Task.WhenAll(run1, run2, run3, run4);
        }

        [Fact]
        public async void DetectEmotionAsync_Test_Success_Case_02()
        {
            await Execute_DetectEmotionAsync_Test_Success_Case_01();
        }

        private async Task Execute_DetectEmotionAsync_Test_Success_Case_01()
        {
            var photoUrl = "https://scontent.xx.fbcdn.net/t31.0-8/12378070_10208731324181648_3238372936679135692_o.jpg";
            var detectorManager = new RoundRobinEmotionDetectorManager();
            var result = await detectorManager.DetectEmotionAsync(new PhotoEmotionDetectionCommand
            {
                PhotoUrl = photoUrl
            });

            result.ShouldNotBeNull("Detection result should not be null");
            result.ResponseStatus.ShouldBe(200);
            result.ErrorMessage.ShouldBeNullOrEmpty();
            result.TotalFacesCount.ShouldBe(4);
            result.Happiness.ShouldBeGreaterThan(50);
        }
    }
}
