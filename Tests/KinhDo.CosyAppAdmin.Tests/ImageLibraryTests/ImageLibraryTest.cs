﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaliko.ImageLibrary;
using Kaliko.ImageLibrary.Scaling;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using Shouldly;
using Xunit;

namespace KinhDo.CosyAppAdmin.Tests.ImageLibraryTests
{
    public class ImageLibraryTest
    {
        [Fact]
        public void TestSaveImageFromUrl()
        {
            var imageUrl = @"https://scontent.xx.fbcdn.net/v/t1.0-9/1425542_10208701869245293_8734982554159457720_n.jpg?oh=19f1dd6d2c283745bfb934df075da515&oe=58756C11";
            var image = new KalikoImage(imageUrl);
            image.Resize(360, 480);

            image.SavePng(@"D:\temp\test\image1.png");
        }

        [Fact]
        public void TestGetImageSize()
        {
            var imageUrl = @"http://cache2.baodoi.com/files/4/f/b42bf44e0fc57e7b68eadd21e728774f.jpg?src=http://img.v3.news.zdn.vn/w660/Uploaded/thoitrang/2013_09_06/22.jpg";
            var image = new KalikoImage(imageUrl);
            image.ShouldNotBeNull();
            image.Size.Width.ShouldBe(500);
            image.Size.Height.ShouldBe(750);

            var image2 = new KalikoImage(@"http://assets.pokemon.com/assets/cms2/img/pokedex/full/061.png");
            image2.ShouldNotBeNull();
            image2.Size.Width.ShouldBe(475);
            image2.Size.Height.ShouldBe(475);
        }

        [Fact]
        public void TestBlitImage()
        {
            var sourceImg = new KalikoImage(@"D:\temp\test\src_img.jpg");
            sourceImg.BlitImage(@"D:\temp\test\avt_175.jpg", 213, 82);

            sourceImg.SavePng(@"D:\temp\test\src_img_with_avt_175.png");
        }

        [Fact]
        public void Test_GeneratePngImageWithText_017_f17_NUMBER_223x137()
        {
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 223,
                Height = 137,
                SavedPath = $@"D:\temp\_017_f17_NUMBER_223x137_{Guid.NewGuid()}.png",
                TextOnImage = "17",
                TextColor = Color.Red,
                FontSize = 127,
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Near
            });
        }

        [Fact]
        public void Test_GeneratePngImageWithText_021_f22_NUMBER_220x160()
        {
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 220,
                Height = 160,
                SavedPath = $@"D:\temp\_021_f22_NUMBER_220x160_{Guid.NewGuid()}.png",
                TextOnImage = "36",
                FontFamily = "Tahoma",//MyriadPro-Regular.otf
                TextColor = Color.Red,
                FontSize = 147,
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Near
            });
        }

        [Fact]
        public void Test_Generate_Image_With_Text_026_f28_NUMBER_218x132()
        {
            var score = 677;
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 218,
                Height = 132,
                SavedPath = $@"D:\temp\_026_f28_NUMBER_218x132_near_far_90_{Guid.NewGuid()}.png",
                TextOnImage = score.ToString(),
                FontFamily = "MyriadPro-Regular",
                TextColor = Color.FromArgb(225, 23, 73),
                FontSize = score < 100 ? 137 : 90, // 2 digits - 127
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Far
            });
        }

        [Fact]
        public void Test_GeneratePngImage_013_f11_NUMBER_275x145()
        {
            // _013_f11_NUMBER_275x145.png -> Total reaction count
            var score = 974;
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 275,
                Height = 145,
                SavedPath = Path.Combine($@"D:\temp", "_013_f11_NUMBER_275x145_974.png"),
                TextColor = Color.White,
                TextOnImage = score.ToString(),
                FontSize = score < 100 ? 135 : 100
            });
        }
    }
}
