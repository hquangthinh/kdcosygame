﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kaliko.ImageLibrary;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using Shouldly;
using Xunit;

namespace KinhDo.CosyAppAdmin.Tests.ImageLibraryTests
{
    public class GameResultImageGeneratorTests
    {
        [Fact]
        public void TestWriteTextOnImage()
        {
            var imageGenerator = new GameResultImageGenerator();

            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 329,
                Height = 76,
                TextOnImage = "Tomato Hua",
                TextColor = Color.White,
                SavedPath = @"D:\temp\tomato.png"
            });
        }

        [Fact]
        public void TestResizeImage()
        {
            var image = new KalikoImage("https://scontent.xx.fbcdn.net/v/t1.0-1/p100x100/14191991_1392349867445855_701539302448690869_n.jpg?oh=c9dda5f7b48d67130f5ef24736b79266&oe=5871A70C");
            image.Resize(235,235);
            image.SaveJpg(@"D:\temp\avt.jpg", 100);
        }

        [Fact]
        public void TestGenerateShareThumnail_User_Thinh()
        {
            var userName = "Thinh Hua";
            var image = new KalikoImage(@"D:\proj\kdcosygame\KinhDo.CosyPhotoGame.Web\Content\images\thumbnail.png");
            var textField = new TextField(userName)
            {
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Center,
                Font = new Font("Helvetica", 52, FontStyle.Bold),
                TextColor = Color.Red,
                Point = new Point(860, 115)
            };

            textField.Draw(image);
            image.SavePng($@"D:\temp\thumbnail_{userName}.png");
        }

        [Fact]
        public void TestSaveImageFromUrlWithImageFrameAsJpg_Case1()
        {
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.SaveImageFromUrlWithImageFrameAsJpg(new SaveImageCommand
            {
                ImageSource = "https://scontent.xx.fbcdn.net/v/t1.0-9/13330951_1190154984336473_3447501954204461022_n.jpg?oh=7255e06ba9bafee35795e9b7c7bbdb8c&oe=58A84EDF",
                SavedPath = @"D:\temp\test_img1.jpg",
                Width = 235,
                Height = 235
            });
        }

        [Fact]
        public void TestSaveImage_Resize_Crop()
        {
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.SaveImageFromUrlResizeWithCrop(new SaveImageCommand
            {
                ImageSource = "https://scontent.xx.fbcdn.net/v/t1.0-9/13330951_1190154984336473_3447501954204461022_n.jpg?oh=7255e06ba9bafee35795e9b7c7bbdb8c&oe=58A84EDF",
                SavedPath = $@"D:\temp\test_crop_{Guid.NewGuid()}.jpg",
                Width = 235,
                Height = 235
            });

            imgGenerator.SaveImageFromUrlAsJpg(new SaveImageCommand
            {
                ImageSource = "https://scontent.xx.fbcdn.net/v/t1.0-9/13330951_1190154984336473_3447501954204461022_n.jpg?oh=7255e06ba9bafee35795e9b7c7bbdb8c&oe=58A84EDF",
                SavedPath = $@"D:\temp\test_normal_{Guid.NewGuid()}.jpg",
                Width = 235,
                Height = 235
            });
        }

        [Fact]
        public void TestSaveImage_Resize_Crop_Case01()
        {
            var imgGenerator = new GameResultImageGenerator();
            imgGenerator.SaveImageFromUrlResizeWithCrop(new SaveImageCommand
            {
                ImageSource = "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/10325765_10208731520026544_4392692790172520515_n.jpg?oh=e79cca64be6a4ecd2660704b03faf1b3&oe=589AC214",
                SavedPath = $@"D:\temp\test_crop_{Guid.NewGuid()}.jpg",
                Width = 235,
                Height = 235
            });

            imgGenerator.SaveImageFromUrlAsJpg(new SaveImageCommand
            {
                ImageSource = "https://scontent-hkg3-1.xx.fbcdn.net/v/t1.0-9/10325765_10208731520026544_4392692790172520515_n.jpg?oh=e79cca64be6a4ecd2660704b03faf1b3&oe=589AC214",
                SavedPath = $@"D:\temp\test_normal_{Guid.NewGuid()}.jpg",
                Width = 235,
                Height = 235
            });
        }

        [Fact]
        public void Test_DownloadImages_01()
        {
            var imageSourceList = GetImageSourceList(@"select Source from PlayerFacebookPhoto where PlayerId=35 
                                    and Happiness is not null
                                    and Width>Height");
            foreach (var src in imageSourceList)
            {
                var img = new KalikoImage(src);
                img.SaveJpg($@"D:\temp\DownloadImages_01\{img.Width}x{img.Height}.jpg", 100);
            }
        }

        private List<string> GetImageSourceList(string sql)
        {
            var db = new CosyDb();
            var query = db.Database.SqlQuery<string>(sql);
            return query.ToList();
        }
    }
}
