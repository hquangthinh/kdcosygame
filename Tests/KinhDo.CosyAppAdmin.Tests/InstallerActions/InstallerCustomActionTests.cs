﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyPhotoGame.InstallerAction;
using Xunit;
using Shouldly;

namespace KinhDo.CosyAppAdmin.Tests.InstallerActions
{
    public class InstallerCustomActionTests
    {
        private static VersionInfo GetDatabaseVersion(string connectionString)
        {
            var newInstallation = new VersionInfo(0);
            if (!IsDatabaseInitialized(connectionString)) return newInstallation;
            return ExecuteDbQuery(connectionString, command =>
            {
                command.CommandType = CommandType.Text;
                command.CommandText = "select top 1 Major, Minor, Build from DatabaseVersion order by Major desc, Minor desc, Build desc";
                using (var reader = command.ExecuteReader())
                {
                    var versions = new List<VersionInfo> { newInstallation };
                    if (reader.Read())
                    {
                        versions.Add(
                            new VersionInfo(
                                (ushort)reader.ReadValue("Major", 0),
                                (ushort)reader.ReadValue("Minor", 0),
                                (ushort)reader.ReadValue("Build", 0)
                            )
                        );
                    }
                    return versions.Max();
                }
            });
        }

        private static bool IsDatabaseInitialized(string connectionString)
        {
            return ExecuteDbQuery(connectionString, command =>
            {
                command.CommandType = CommandType.Text;
                command.CommandText = @"
                        select top 1 Result
                        from (
	                        SELECT 1 as Result FROM sysobjects WHERE ID = OBJECT_ID('[DatabaseVersion]') AND OBJECTPROPERTY(ID, 'IsTable') = 1
	                        union
	                        select 0
                        ) x
                        order by Result desc";
                return (int)command.ExecuteScalar() == 1;
            });
        }

        private static TResult ExecuteDbQuery<TResult>(string connectionString, Func<SqlCommand, TResult> command)
        {
            using (var conn = new SqlConnection(connectionString))
            {
                conn.Open();
                using (var cmd = conn.CreateCommand())
                {
                    return command(cmd);
                }
            }
        }

        private static string BuildConnectionString()
        {
            var dbServer = @"USD100\SQLEXPRESS";
            var dbDatabase = "TestForCosyDb";
            return
                $"Data Source={dbServer};Connect Timeout=60;Initial Catalog={dbDatabase};Persist Security Info=True;Integrated Security=true;User ID=sa;Password=Qwerty098765";
        }

        [Fact]
        public void TestLoadUpgradableScripts()
        {
            var currentDbVersion = GetDatabaseVersion(BuildConnectionString());
            var scriptFolderRoot = @"D:\proj\kdcosygame\KinhDo.CosyAppAdmin.EntityFramework\Scripts";
            var cosyVersion = VersionInfo.FromValue("1.01.1.0");
            var dbUpgrade = new DatabaseUpgrade("CosyDb", scriptFolderRoot);
            var scripts = dbUpgrade.LoadUpgradableScripts(cosyVersion, currentDbVersion).ToList();

            scripts.ShouldNotBeNull();
            scripts.Count.ShouldBe(3);

            var script1 = scripts[0];
            var script2 = scripts[1];
            var script3 = scripts[2];

            script1.TargetDatabase.ShouldBe("CosyDb");
            script1.Name.ShouldBe("CosyDb_1.00.01_Base.sql");
            script1.ScriptType.ShouldBe(ScriptType.SchemaUpgrade);

            script2.TargetDatabase.ShouldBe("CosyDb");
            script2.Name.ShouldBe("CosyDb_1.01.01_Upgrade.sql");
            script2.ScriptType.ShouldBe(ScriptType.SchemaUpgrade);

            script3.TargetDatabase.ShouldBe("CosyDb");
            script3.Name.ShouldBe("CosyDb_1.01.01_DataMigration.sql");
            script3.ScriptType.ShouldBe(ScriptType.DataMigration);
        }
    }
}
