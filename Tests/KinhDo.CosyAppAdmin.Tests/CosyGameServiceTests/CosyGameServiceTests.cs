﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyAppAdmin.Model;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.Helper;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using Shouldly;
using Xunit;
using Xunit.Abstractions;

namespace KinhDo.CosyAppAdmin.Tests.CosyGameServiceTests
{
    public class CosyGameServiceTests : LogOutputTester
    {
        public CosyGameServiceTests(ITestOutputHelper output) : base(output)
        {
        }

        [Fact]
        public void TestGenerateGameResult()
        {
            var playerDbId = 1;
            var gameResultId = 8;
            var gameSettingService = new GameSettingService();
            var gameService = new CosyGameService();
            var playerService = new PlayerService();

            var gameSetting = gameSettingService.GetGameGlobalSettings();
            var result = gameService.GenerateGameResult(playerDbId, gameResultId, gameSetting);
            var player = playerService.GetPlayerProfileByDbId(playerDbId);
            
            player.ShouldNotBeNull();
            result.ShouldNotBeNull();
            var xmlResultPath = Path.Combine(gameSetting.PlayerGameDataBaseFolder, player.Id, $"user_{player.Id}.xml");
            var resultAvailable = File.Exists(xmlResultPath);
            resultAvailable.ShouldBeTrue();
        }

        [Fact]
        public void TestWriteLibPropertiesFile()
        {
            var fbId = "1234567890";
            var libProps = UserLibraryPropertyDto.CreateDefault();
            foreach (var manifestItem in libProps.Manifest.Where(item => item.ItemOrder <= 31))
            {
                manifestItem.Source = string.Format(manifestItem.Source, fbId);
            }
            JsonHelper.WriteLibraryProperties(libProps, $@"D:\temp\{fbId}.js");
        }

        [Fact]
        public void TestWriteLibPropertiesFileUsingTemplate()
        {
            var fbId = "8888888899";
            var appBasePath = @"D:\proj\kdcosygame\KinhDo.CosyPhotoGame.Web";
            JsonHelper.WriteLibraryProperties(appBasePath, fbId, @"D:\temp\lib-properties.js");
        }

        [Fact]
        public void TestGenerateImagesForGameResult_346959698973295()
        {
            var playerDbId = 14;
            var gameId = 40;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        [Fact]
        public void TestGenerateImagesForGameResult_1136264593118524()
        {
            var playerDbId = 11;
            var gameId = 227;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        //10154467081992410
        [Fact]
        public void TestGenerateImagesForGameResult_10154467081992410()
        {
            var playerDbId = 9;
            var gameId = 38;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        //10206581334484031
        [Fact]
        public void TestGenerateImagesForGameResult_10206581334484031()
        {
            var playerDbId = 10;
            var gameId = 31;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        // 10210062381376618 - 34 - 13
        [Fact]
        public void TestGenerateImagesForGameResult_10210062381376618()
        {
            var playerDbId = 13;
            var gameId = 24;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        // 1356874520991200 - 33 - 12
        [Fact]
        public void TestGenerateImagesForGameResult_1356874520991200()
        {
            var playerDbId = 12;
            var gameId = 33;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        [Fact]
        public void TestOrderNullData()
        {
            var playerDbId = 21;
            using (var db = new CosyDb())
            {
                var result = db.PlayerFacebookPhotoes
                    .Where(p => p.PlayerId == playerDbId && !p.Happiness.HasValue)
                    .OrderByDescending(p => p.NameTags.Length)
                    .ThenByDescending(p => p.UpdatedTime).ToList();
                result.ShouldNotBeNull();
                result.Count.ShouldBeGreaterThan(0);
            }
        }

        // 10209653603713377 - 71 - 21
        [Fact]
        public void TestGenerateImagesForGameResult_10209653603713377()
        {
            var playerDbId = 21;
            var gameId = 71;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        // 985980591510718 - 78 - 24
        [Fact]
        public void TestGenerateImagesForGameResult_985980591510718()
        {
            var playerDbId = 24;
            var gameId = 78;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        [Fact]
        public async Task Test_DetectionEmotionForPlayerPhotos_1()
        {
            var gameService = new CosyGameService();
            await gameService.DetectionEmotionForPlayerPhotos(new FacebookProfileViewModel
            {
                PlayerId = 1,
                UserName = "ThinhHuaQuang"
            });
        }

        //10154875553048455	35
        [Fact]
        public async Task Test_PrepareGameResult_10154875553048455()
        {
            const int playerDbId = 35;
            const int gameResultId = 225;
            var playerService = new PlayerService();
            var cosyGameService = new CosyGameService();
            var playerProfile = playerService.GetPlayerProfileByDbId(playerDbId);
            var gameTransaction = new GameTransactionDto
            {
                Id = gameResultId,
                PlayerDbId = playerDbId
            };
            await cosyGameService.PrepareForGameResult(playerProfile, gameTransaction);
        }

        [Fact]
        public void Test_GenerateGameResult_10154875553048455()
        {
            var playerDbId = 35;
            var gameId = 225;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        [Fact]
        public void Test_GenerateGameResult_10209899916435724()
        {
            var playerDbId = 1;
            var gameId = 222;
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
            var gameService = new CosyGameService();
            var gameResult = gameService.GenerateGameResult(playerDbId, gameId, gameSettingDto);
            gameResult.ShouldNotBeNull();
        }

        [Fact]
        public void Test_GenerateImageTaggedPostImage_Recursive()
        {
            var parentFolderPath = @"D:\temp\Test_GenerateImageTaggedPostImage_Recursive";
            var imageGenerator = new GameResultImageGenerator();
            var returnCmd = GenerateImageTaggedPostImage(imageGenerator, new List<PlayerFacebookPhoto>(), new List<SaveImageCommand>(),
                0, parentFolderPath, "Test_GenerateImageTaggedPostImage_Recursive");

            var idList = new List<string>();
            idList.Add(returnCmd?.Id);

            Assert.True(idList.Count == 1);
            Assert.Null(idList[0]);
            var containsTest = idList.Contains("123");
            Assert.False(containsTest);
        }

        private SaveImageCommand GenerateImageTaggedPostImage(GameResultImageGenerator imageGenerator,
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<SaveImageCommand> saveImageCommands, int imgIndex,
            string parentFolderPath, string fileName, int newWidth = 0, int newHeight = 0)
        {
            if (saveImageCommands.Count == 0)
                return null;

            try
            {
                if (imgIndex >= 10)
                {
                    return saveImageCommands[0];
                }
                return GenerateImage(imageGenerator, allPlayerPhotosPool, saveImageCommands, imgIndex, parentFolderPath,
                            fileName, newWidth, newHeight);
            }
            catch (Exception ex)
            {
                TestOutputHelper.WriteLine($"GenerateImageTaggedPostImage error - retry at {imgIndex + 1}", ex);
                return GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, saveImageCommands, imgIndex + 1,
                            parentFolderPath, fileName, newWidth, newHeight);
            }
        }

        private SaveImageCommand GenerateImage(GameResultImageGenerator imageGenerator,
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<SaveImageCommand> saveImageCommands, int imgIndex,
            string parentFolderPath, string fileName, int newWidth = 0, int newHeight = 0)
        {
            if (saveImageCommands.Count <= imgIndex)
                return null;

            var cmd = saveImageCommands[imgIndex];
            if (newWidth > 0 & newHeight > 0)
            {
                cmd.Width = newWidth;
                cmd.Height = newHeight;
            }
            cmd.SavedPath = Path.Combine(parentFolderPath, fileName);

            if (saveImageCommands.Count > imgIndex)
            {
                imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
                return cmd;
            }

            // use alternative photo
            var totalPhotoPool = allPlayerPhotosPool.Count;
            if (totalPhotoPool - imgIndex < 0) return null;
            var photo = allPlayerPhotosPool[totalPhotoPool - imgIndex];
            cmd.ImageSource = photo.Source;
            cmd.Id = photo.Id;
            imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
            return cmd;
        }
    }
}