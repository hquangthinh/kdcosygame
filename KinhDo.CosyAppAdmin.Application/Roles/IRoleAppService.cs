﻿using System.Threading.Tasks;
using Abp.Application.Services;
using KinhDo.CosyAppAdmin.Roles.Dto;

namespace KinhDo.CosyAppAdmin.Roles
{
    public interface IRoleAppService : IApplicationService
    {
        Task UpdateRolePermissions(UpdateRolePermissionsInput input);
    }
}
