﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Linq;
using KinhDo.CosyAppAdmin.Dto;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyAppAdmin.Model;
using KinhDo.CosyAppAdmin.Players.Dto;

namespace KinhDo.CosyAppAdmin.Players
{
    public class PlayerAppService : AdminServiceBase, IPlayerAppService
    {
        public PagingResult<PlayerSerchResultDto> SearchPlayers()
        {
            return SearchPlayers(new FindPlayerCommand());
        }

        public PagingResult<PlayerSerchResultDto> SearchPlayers(FindPlayerCommand command)
        {
            var stopWatch = new Stopwatch();

            stopWatch.Start();

            using (var db = DbContext)
            {
                Func<Player, bool> querySelector = item =>
                    (string.IsNullOrEmpty(command.FullName)
                     || (!string.IsNullOrEmpty(command.FullName) && item.FirstName.Contains(command.FullName))
                     || (!string.IsNullOrEmpty(command.FullName) && item.LastName.Contains(command.FullName)))
                    && (string.IsNullOrEmpty(command.Email)
                        || (!string.IsNullOrEmpty(command.Email) && item.Email.Contains(command.Email)));

                var totalCount = db.Players.Count(querySelector);

                var players = db.Players
                    .Where(querySelector)
                    .OrderBy(item => item.FirstName)
                    .ThenBy(item => item.LastName)
                    .Skip((command.PageNumber - 1) * command.PageSize)
                    .Take(command.PageSize)
                    .Select(PlayerSerchResultDto.Mapper).ToList();

                stopWatch.Stop();

                return PagingResult<PlayerSerchResultDto>.FromResult(players, stopWatch.ElapsedMilliseconds, totalCount,
                    command.PageSize,
                    command.PageNumber);
            }
        }

        public PlayerDetailDto GetPlayerDetail(GetPlayerDetailCommand command)
        {
            if(command == null)
                throw new ArgumentNullException(nameof(command));

            using (var db = DbContext)
            {
                var playerEntity = command.Id > 0
                    ? db.Players.FirstOrDefault(p => p.Id == command.Id)
                    : db.Players.FirstOrDefault(p => p.ProfileId == command.ProfileId);

                var playerDto = PlayerDetailDto.Mapper(playerEntity);

                playerDto.PlayerGameResults =
                    db.PlayerGameResults.Where(item => item.PlayerId == playerDto.Id)
                        .OrderByDescending(item => item.ResultDate)
                        .Select(PlayerGameResultDto.Mapper)
                        .ToList();

                if (playerDto.PlayerGameResults.Count > 0)
                {
                    var gameSiteUrl = ConfigurationManager.AppSettings["GameSiteUrl"].TrimEnd('/');
                    var latestGameId = playerDto.PlayerGameResults.FirstOrDefault()?.Id;
                    playerDto.LatestVideoUrl =
                        $"{gameSiteUrl}/Game/Result?gameTransactionId={latestGameId}&UserProfileId={playerDto.ProfileId}";
                }
                

                return playerDto;
            }
        }

        public PlayerDetailDto UpdatePlayerDetail(PlayerDetailDto command)
        {
            throw new NotImplementedException();
        }

        public List<GameResultReportDto> ExportAllGameResults()
        {
            return DbContext.VwGameResultReports
                .OrderBy(item => item.FirstName)
                .ThenBy(item => item.LastName)
                .ThenBy(item => item.ResultDate)
                .Select(GameResultReportDto.Mapper)
                .ToList();
        }
    }
}