﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Abp.Application.Services;
using KinhDo.CosyAppAdmin.Dto;
using KinhDo.CosyAppAdmin.Players.Dto;

namespace KinhDo.CosyAppAdmin.Players
{
    public interface IPlayerAppService : IApplicationService
    {
        PagingResult<PlayerSerchResultDto> SearchPlayers();

        PagingResult<PlayerSerchResultDto> SearchPlayers(FindPlayerCommand command);

        PlayerDetailDto GetPlayerDetail(GetPlayerDetailCommand command);

        PlayerDetailDto UpdatePlayerDetail(PlayerDetailDto command);

        List<GameResultReportDto> ExportAllGameResults();
    }
}
