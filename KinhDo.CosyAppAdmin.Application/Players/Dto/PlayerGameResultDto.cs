﻿using System;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyAppAdmin.Players.Dto
{
    public class PlayerGameResultDto
    {
        public int Id { get; set; }

        public int PlayerId { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientBrowser { get; set; }

        public DateTime ResultDate { get; set; }

        public string Status { get; set; }

        public string StatusDescription
        {
            //New,
            //ResultOk,
            //ResultCancel,
            //ResultNotShare,
            //Error
            get
            {
                if (string.IsNullOrEmpty(Status) || "New".Equals(Status))
                    return "New";

                if ("ResultOk".Equals(Status))
                    return "Ok";

                if ("ResultCancel".Equals(Status))
                    return "Cancel";

                return "ResultNotShare".Equals(Status) ? "Abort" : Status;
            }
        }

        public string StatusLabelClass
        {
            get
            {
                if ("ResultOk".Equals(Status))
                    return "label-success";

                if ("ResultCancel".Equals(Status))
                    return "label-info";

                if ("ResultNotShare".Equals(Status))
                    return "label-warning";

                return "Error".Equals(Status) ? "label-danger" : "label-default";
            }
        }

        public string ErrorMessage { get; set; }

        public int? PercentComplete { get; set; }

        public string AccessToken { get; set; }

        public static Func<PlayerGameResult, PlayerGameResultDto> Mapper = entity =>
        {
            if (entity == null) return new PlayerGameResultDto();

            return new PlayerGameResultDto
            {
                Id = entity.Id,
                PlayerId = entity.PlayerId,
                ResultDate = entity.ResultDate.ToLocalTime(),
                ClientBrowser = entity.ClientBrowser,
                ClientIpAddress = entity.ClientIpAddress,
                Status = entity.Status,
                PercentComplete = entity.PercentComplete,
                ErrorMessage = entity.ErrorMessage,
                AccessToken = entity.AccessToken
            };
        };
    }

    public class GameResultReportDto
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string ProfileId { get; set; }

        public string Email { get; set; }

        public int Id { get; set; }

        public int PlayerId { get; set; }

        public DateTime ResultDate { get; set; }

        public string ClientBrowser { get; set; }

        public string ClientIpAddress { get; set; }

        public string Status { get; set; }

        public int? PercentComplete { get; set; }

        public string CompleteGame => 100 == PercentComplete ? "Yes" : "No";

        public static Func<VwGameResultReport, GameResultReportDto> Mapper = entity =>
        {
            if (entity == null) return new GameResultReportDto();

            return new GameResultReportDto
            {
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email,
                Id = entity.Id,
                ProfileId = entity.ProfileId,
                PlayerId = entity.PlayerId,
                ResultDate = entity.ResultDate,
                ClientBrowser = entity.ClientBrowser,
                ClientIpAddress = entity.ClientIpAddress,
                Status = entity.Status,
                PercentComplete = entity.PercentComplete
            };
        };
    }
}