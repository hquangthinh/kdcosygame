﻿using System;
using System.Collections.Generic;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyAppAdmin.Players.Dto
{
    public class PlayerDetailDto
    {
        public int Id { get; set; }

        public string ProfileId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string ImageURL { get; set; }

        public string LinkURL { get; set; }

        public string Locale { get; set; }

        public string PhoneNumber { get; set; }

        public string Email { get; set; }

        public DateTime? Birthdate { get; set; }

        public string Location { get; set; }

        public string Gender { get; set; }

        public string AgeRange { get; set; }

        public string Bio { get; set; }

        public string StreetAddress { get; set; }

        public string Ward { get; set; }

        public string District { get; set; }

        public string City { get; set; }

        public string Country { get; set; }

        public string LatestVideoUrl { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public ICollection<PlayerGameResultDto> PlayerGameResults { get; set; }

        public static Func<Player, PlayerDetailDto> Mapper = entity =>
        {
            if (entity == null) return new PlayerDetailDto();

            return new PlayerDetailDto
            {
                Id = entity.Id,
                ProfileId = entity.ProfileId,
                UserName = entity.UserName,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Gender = entity.Gender,
                Email = entity.Email,
                PhoneNumber = entity.PhoneNumber,
                Bio = entity.Bio,
                Birthdate = entity.Birthdate,
                Location = entity.Location,
                ImageURL = entity.ImageURL,
                LinkURL = entity.LinkURL,
                Locale = entity.Locale,
                AgeRange = entity.AgeRange,
                StreetAddress = entity.StreetAddress,
                Ward = entity.Ward,
                District = entity.District,
                City = entity.City,
                Country = entity.Country
            };
        };
    }
}