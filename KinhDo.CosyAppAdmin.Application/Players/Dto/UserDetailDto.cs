﻿namespace KinhDo.CosyAppAdmin.Players.Dto
{
    public class UserDetailDto
    {
        public string UserName { get; set; }

        public string Email { get; set; }
    }
}