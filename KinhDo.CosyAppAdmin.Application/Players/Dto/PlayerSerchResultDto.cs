﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyAppAdmin.Players.Dto
{
    public class PlayerSerchResultDto
    {
        public int Id { get; set; }

        public string ProfileId { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string FullName => $"{FirstName} {LastName}";

        public string Email { get; set; }

        public static Func<Player, PlayerSerchResultDto> Mapper = entity =>
        {
            if(entity == null) return new PlayerSerchResultDto();

            return new PlayerSerchResultDto
            {
                Id = entity.Id,
                ProfileId = entity.ProfileId,
                UserName = entity.UserName,
                FirstName = entity.FirstName,
                LastName = entity.LastName,
                Email = entity.Email
            };
        };
    }
}
