﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyAppAdmin.Dto;

namespace KinhDo.CosyAppAdmin.Players.Dto
{
    public class GetPlayerDetailCommand
    {
        public int Id { get; set; }

        public string ProfileId { get; set; }
    }

    public class FindPlayerCommand : Paging
    {
        public string FullName { get; set; }

        public string Email { get; set; }
    }
}