﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;

namespace KinhDo.CosyAppAdmin.Dto
{
    public class Paging
    {
        /// <summary>
        /// Select by page number
        /// </summary>
        [DefaultValue(1)]
        public int PageNumber { get; set; } = 1;

        /// <summary>
        /// Number of record per page
        /// </summary>
        [DefaultValue(10)]
        public int PageSize { get; set; } = 10;

        /// <summary>
        /// Order by field
        /// </summary>
        public String OrderBy { get; set; }

        /// <summary>
        /// Order  ASC=true | DESC=false , default is true
        /// </summary>
        [DefaultValue(true)]
        public bool OrderAsc { get; set; } = true;

        /// <summary>
        /// To control the cache validity of data
        /// </summary>
        public long Timestamp { get; set; }

        public bool FindAll { get; set; }

        protected bool Equals(Paging other)
        {
            return PageNumber == other.PageNumber && PageSize == other.PageSize & Timestamp == other.Timestamp;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((Paging)obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (PageNumber * 397) ^ PageSize;
            }
        }
    }

    /// <summary>
    /// This extend paging base because , previous paging info should be returned to keep track for next action
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    public class PagingResult<TResult> : Paging
    {
        /// <summary>
        /// Number of total pages.
        /// If counting is supported in this function
        /// </summary>
        [DefaultValue(0)]
        public int PageTotal { get; set; }

        /// <summary>
        /// If counting is supported in this function
        /// </summary>
        public long ResultCount { get; set; }

        /// <summary>
        /// Number of milisecond which SQLs is executed. Used to benkmark SQL performance.
        /// </summary>
        public long ExecutionDuration { get; set; }

        /// <summary>
        /// Result set of found items
        /// </summary>
        public IEnumerable<TResult> ResultSet { get; private set; }

        public PagingResult(IEnumerable<TResult> resultSet)
        {
            ResultSet = resultSet;
        }

        public static PagingResult<TResult> Empty()
        {
            return new PagingResult<TResult>(Enumerable.Empty<TResult>());
        }

        public static PagingResult<TResult> FromResult(IEnumerable<TResult> resultSet, long executionTicks, long resultCount,
            int pageSize, int pageNumber)
        {
            var result = new PagingResult<TResult>(resultSet)
            {
                ExecutionDuration = executionTicks,
                ResultCount = resultCount,
                PageSize = pageSize,
                PageNumber = pageNumber,
                PageTotal = (int)(resultCount / pageSize) + (resultCount % pageSize > 0 ? 1 : 0)
            };

            return result;
        }
    }

    /// <summary>
    /// Provides strongly typed OrderBy expression for Paging objects
    /// </summary>
    public static class PagingExt
    {
        /// <summary>
        /// Set the OrderBy using a strong typed expression, thus is refactoring friendly
        /// and error free.
        /// </summary>
        /// <remarks>
        /// Expression creation is costly. Comparing to the direct string assignment
        /// approach this is 100x slower when the expression is built on the fly.
        /// Still it's capable of creating ~200K expressions per second, which should
        /// be enough for the C10k requirements.
        /// If raw performance is a must, use a static expression for each of the command
        /// type or revert to using direct string assignment.
        /// </remarks>
        public static void SetOrderBy<T, T1>(this T model, Expression<Func<T, T1>> member) where T : Paging
        {
            model.OrderBy = ((MemberExpression)member.Body).Member.Name;
        }

        public static IEnumerable<T> Paginate<T>(this IEnumerable<T> target, Paging command)
        {
            return target
                .Skip((command.PageNumber - 1) * command.PageSize)
                .Take(command.PageSize);
        }
    }

    public static class PagingHelper
    {
        public static int GetPageTotal(long resultCount, int pageSize)
        {
            return resultCount / pageSize + (resultCount % pageSize) > 0 ? 1 : 0;
        }
    }
}