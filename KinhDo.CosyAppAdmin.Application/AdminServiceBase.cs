﻿using KinhDo.CosyAppAdmin.EntityFramework;

namespace KinhDo.CosyAppAdmin
{
    public abstract class AdminServiceBase
    {
        private readonly CosyDb _dbContext = new CosyDb();

        protected CosyDb DbContext => _dbContext ?? new CosyDb();
    }
}
