﻿using System.Web.Optimization;

namespace KinhDo.CosyPhotoGame.Web
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery-ui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/Content/cssbootstrapjql").Include(
                        "~/Content/bootstrap.css",
                        "~/Content/themes/base/jquery-ui.min.css"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                        "~/Content/reset.css",
                        "~/Content/bootstrap.css",
                        "~/Content/themes/base/jquery-ui.min.css",
                        "~/Content/style.css"));

            // App specific bundles
            bundles.Add(new StyleBundle("~/Content/csshome").Include(
                        "~/Content/reset.css",
                        "~/Content/style.css"));
        }
    }
}
