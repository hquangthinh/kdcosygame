﻿using System.Web.Mvc;
using KinhDo.CosyPhotoGame.Web.Attributes;

namespace KinhDo.CosyPhotoGame.Web
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new ContentSecurityPolicyFilterAttribute());
            filters.Add(new HandleErrorAttribute());
        }
    }
}
