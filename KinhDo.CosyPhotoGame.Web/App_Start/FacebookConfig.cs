﻿using System.Web.Mvc;
using KinhDo.CosyPhotoGame.Web.Helper;
using Microsoft.AspNet.Facebook;
using Microsoft.AspNet.Facebook.Authorization;

namespace KinhDo.CosyPhotoGame.Web
{
    public static class FacebookConfig
    {
        public static void Register(FacebookConfiguration configuration)
        {
            // Loads the settings from web.config using the following app setting keys:
            var appSetting = AppSettingHelper.LoadAppSetting();
            GlobalFacebookConfiguration.Configuration.AppId = appSetting.FacebookAppId;
            GlobalFacebookConfiguration.Configuration.AppSecret = appSetting.FacebookAppSecret;

            // Adding the authorization filter to check for Facebook signed requests 
            // and permissions
            GlobalFilters.Filters.Add(new FacebookAuthorizeFilter(configuration));
        }
    }
}