﻿using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace KinhDo.CosyPhotoGame.Web.Attributes
{
    public class FacebookAccessTokenAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {

            ApplicationUserManager userManager = filterContext.HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            if (userManager != null)
            {
                var user = filterContext.HttpContext.User.Identity.GetUserId();
                if (user != null)
                {
                    var claimsforUser = userManager.GetClaimsAsync(user);
                    if (claimsforUser != null)
                    {
                        var accessTokenValue = "_DoesNotExists_";
                        var accessToken = claimsforUser.Result.FirstOrDefault(x => x.Type == "FacebookAccessToken");

                        if (accessToken != null)
                        {
                            accessTokenValue = accessToken.Value;
                        }

                        if (filterContext.HttpContext.Items.Contains("access_token"))
                            filterContext.HttpContext.Items["access_token"] = accessTokenValue;
                        else
                            filterContext.HttpContext.Items.Add("access_token", accessTokenValue);
                    }
                }
            }
            base.OnActionExecuting(filterContext);
        }
    }
}