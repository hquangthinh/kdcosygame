﻿using System.Web.Mvc;

namespace KinhDo.CosyPhotoGame.Web.Attributes
{
    public class ContentSecurityPolicyFilterAttribute : ActionFilterAttribute
    {
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            var response = filterContext.HttpContext.Response;

            response.AddHeader("Content-Security-Policy", "child-src *");
            response.AddHeader("X-WebKit-CSP", "child-src *");
            response.AddHeader("X-Content-Security-Policy", "child-src *");

            base.OnActionExecuting(filterContext);
        }
    }
}
