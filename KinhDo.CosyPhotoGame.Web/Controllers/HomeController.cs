﻿using System.Web.Mvc;
using KinhDo.CosyPhotoGame.Web.Helper;

namespace KinhDo.CosyPhotoGame.Web.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            var appSetting = AppSettingHelper.LoadAppSetting();

            ViewBag.FbAppId = appSetting.FacebookAppId;
            ViewBag.FbScope = appSetting.FacebookScope;

            return View("NewIndex");
        }

        //
        // POST: /Home/StartGame
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult StartGame(string provider, string returnUrl)
        {
            ControllerContext.HttpContext.Session?.RemoveAll();

            // Request a redirect to the external login provider
            return new AccountController.ChallengeResult("Facebook", Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = "/Game/Index" }));
        }
    }
}