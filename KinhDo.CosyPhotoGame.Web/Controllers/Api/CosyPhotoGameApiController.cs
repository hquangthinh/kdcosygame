﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using Hangfire;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.Helper;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using KinhDo.CosyPhotoGame.Web.Service;
using Microsoft.AspNet.Identity;

namespace KinhDo.CosyPhotoGame.Web.Controllers.Api
{
    public class FacebookGraphApiCommand
    {
        public string AccessToken { get; set; }

        public int Limit { get; set; }

        public string FbUserProfileId { get; set; }
    }

    public class GetGameResultCommand : FacebookGraphApiCommand
    {
        public int GameTransactionId { get; set; }

        public string UserName { get; set; }

        public string Email { get; set; }
    }

    public class GetFacebookPhotoCommand : FacebookGraphApiCommand
    {
        public string FbPhotoId { get; set; }
    }

    [RoutePrefix("api/cosy-game")]
    public class CosyPhotoGameApiController : BaseApiController
    {
        [HttpPost]
        [Route("prepare-test")]
        [ResponseType(typeof (UserGameResultDto))]
        public IHttpActionResult TestPrepare([FromBody] GetGameResultCommand command)
        {
            return Ok(new UserGameResultDto());
        }

        // POST api/cosy-game/prepare -> Download data from fb, prepare game result
        [HttpPost]
        [Route("prepare")]
        [ResponseType(typeof(UserGameResultDto))]
        public async Task<IHttpActionResult> PreparePlayerGameResult([FromBody]GetGameResultCommand command)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            Log.Debug($"Access token {command.AccessToken}");

            var currentUser = new UserDetailDto();

            var currentAppUser = UserManager.FindById(User.Identity.GetUserId());
            if (currentAppUser != null)
            {
                currentUser.UserName = currentAppUser.UserName;
                currentUser.Email = currentAppUser.Email;
            }
            else
            {
                currentUser.UserName = command.UserName.Replace(" ", string.Empty);
                currentUser.Email = command.Email;
            }

            Log.Info($"Game session start for user {currentUser.UserName}");

            var result = new UserGameResultDto();

            if (string.IsNullOrEmpty(command.AccessToken))
            {
                result.Status = GameResultStatus.Error.ToString();
                result.ErrorMessage = "Invalid access token";
                return Ok(result);
            }

            var fbService = new FacebookClientService(command.AccessToken);

            var playerProfile = await fbService.GetPlayerProfile();

            var playerService = new PlayerService();
            
            var playerProfileModel = playerService.AddOrUpdatePlayerInfo(playerProfile, currentUser);

            // Record game transaction start
            var gameService = new CosyGameService();
            var gameTransactionDto = gameService.StartGameTransaction(new GameTransactionDto
            {
                PlayerDbId = playerProfileModel.PlayerId,
                ClientIpAddress = GetClientIp(),
                ClientBrowser = GetClientBrowserAgentString(),
                ResultDate = DateTime.Now,
                Status = GameResultStatus.New.ToString(),
                AccessToken = command.AccessToken
            });

            stopWatch.Stop();
            Log.Debug($"Get user profile from fb, update db, start game transaction -> {stopWatch.Elapsed.TotalSeconds}");
            stopWatch.Start();

            // Get cached fb data within 1 hour
            var gameSettingService = new GameSettingService();
            var setting = gameSettingService.GetGameGlobalSettings();

            var fbCacheDataEnabled = "true".Equals(ConfigurationManager.AppSettings["EnableFacebookDataCache"]);
            var fbDataCacheDuration = setting?.FbDataCacheDurationInHours ?? 1;
            if (gameService.CanUseCacheDataFromPreviousGameTransaction(fbCacheDataEnabled, playerProfileModel.PlayerId, fbDataCacheDuration))
            {
                Log.Debug($"Use cache data from previous game transaction");
            }
            else
            {
                var playerPhotos = await GetPlayerPhotos(stopWatch, setting, currentUser, fbService, playerService, playerProfileModel);

                // Update game progress to 10%
                UpdateGameProgress(gameService, gameTransactionDto, 10);

                // Photo reactions & name tags are updated toghether with GetPlayerPhotos
                //await GetPlayerPhotoReactions(playerPhotos, fbService, playerService);

                stopWatch.Stop();
                Log.Debug($"Get & update db GetPlayerPhotoReactions -> {stopWatch.Elapsed.TotalSeconds}");
                stopWatch.Start();

                // Update game progress to 20%
                UpdateGameProgress(gameService, gameTransactionDto, 20);

                await GetPlayerTaggedLocations(fbService, playerProfileModel, playerService);

                stopWatch.Stop();
                Log.Debug($"Get & update db GetPlayerTaggedLocations -> {stopWatch.Elapsed.TotalSeconds}");
                stopWatch.Start();

                // Update game progress to 30%
                UpdateGameProgress(gameService, gameTransactionDto, 30);

                await GetPlayerFeed(fbService, playerProfileModel, playerService);

                stopWatch.Stop();
                Log.Debug($"Get & update db GetPlayerFeed -> {stopWatch.Elapsed.TotalSeconds}");
                stopWatch.Start();

                // Update game progress to 50%
                UpdateGameProgress(gameService, gameTransactionDto, 50);
            }

            var jobId = BackgroundJob.Enqueue(() => gameService.PrepareForGameResult(playerProfileModel, gameTransactionDto));
            result.JobId = jobId;
            result.Id = gameTransactionDto.Id;

            Log.Debug($"Enqueue background job -> {jobId} at {DateTime.Now}");

            return Ok(result);
        }

        private static async Task GetPlayerFeed(FacebookClientService fbService, FacebookProfileViewModel playerProfileModel,
            PlayerService playerService)
        {
            // Get user feed
            try
            {
                var userFeed = await fbService.GetUserFeed(playerProfileModel.Id, 100);
                playerService.AddUserFeed(userFeed);
            }
            catch (Exception userFeedException)
            {
                Log.Error($"Error -> GetPlayerFeed {playerProfileModel.Id}", userFeedException);
            }
        }

        private static async Task GetPlayerTaggedLocations(FacebookClientService fbService,
            FacebookProfileViewModel playerProfileModel, PlayerService playerService)
        {
            // Get user tagged locations
            try
            {
                var userTaggedLocations = await fbService.GetUserTaggedPlaces(playerProfileModel.Id);
                playerService.AddUserTaggedLocations(userTaggedLocations);
            }
            catch (Exception userTagException)
            {
                Log.Error($"Error -> GetUserTaggedPlaces {playerProfileModel.Id}", userTagException);
            }
        }

        private static async Task GetPlayerPhotoReactions(List<FacebookPhotoViewModel> playerPhotos, FacebookClientService fbService,
            PlayerService playerService)
        {
            // Get photo reactions
            foreach (var fbPhoto in playerPhotos)
            {
                try
                {
                    var photoReactions = await fbService.GetPhotoReactions(fbPhoto.Id);
                    if (photoReactions?.Data != null)
                    {
                        // get reactions data & update to photo model
                        fbPhoto.TotalReactionCount = photoReactions.Data.Count;
                        fbPhoto.TotalReactionLikeCount = photoReactions.Data.Count(item => item.Type == "LIKE");
                        fbPhoto.TotalReactionLoveCount = photoReactions.Data.Count(item => item.Type == "LOVE");
                        fbPhoto.TotalReactionHahaCount = photoReactions.Data.Count(item => item.Type == "HAHA");
                    }

                    // Get photo tags

                    var photoTags = await fbService.GetTagsFromPhoto(fbPhoto.Id);
                    if (photoTags?.Data != null)
                    {
                        fbPhoto.TotalProfileTaggedInPhotoCount = photoTags.Data.Count;
                        fbPhoto.NameTagsString = string.Join(";", photoTags.Data.Select(item => $"{item.Id}|{item.Name}"));
                    }
                    playerService.UpdatePlayerPhotoByFbPhotoId(fbPhoto);
                }
                catch (Exception photoException)
                {
                    Log.Error($"Error -> GetPhotoReactions & GetTagsFromPhoto {fbPhoto.Id}", photoException);
                }
            }
        }

        private static async Task<List<FacebookPhotoViewModel>> GetPlayerPhotos(Stopwatch stopWatch, GameSettingDto setting, 
            UserDetailDto currentUser, FacebookClientService fbService,
            PlayerService playerService, FacebookProfileViewModel playerProfileModel)
        {
            // Get albumns & photos from facebook
            var numberOfDaysToGetPhotoBackedDate = setting?.GetPhotoBackedDateDays ?? 60;
            Log.Debug(
                $"Get albumns & photos from facebook for {currentUser.UserName} in the past {numberOfDaysToGetPhotoBackedDate} days");

            var playerAlbums = await fbService.GetPlayerAlbums(numberOfDaysToGetPhotoBackedDate);
            playerService.AddOrUpdatePlayerAlbums(playerProfileModel, playerAlbums);

            var playerPhotos = new List<FacebookPhotoViewModel>();
            var photoCreatedAfterDate = DateTime.Now.AddDays(-numberOfDaysToGetPhotoBackedDate*2);

            Log.Debug($"Only get photo after {photoCreatedAfterDate}");

            foreach (var album in playerAlbums)
            {
                try
                {
                    var photoOfAlbums = await fbService.GetPhotosByAlbum(album.Id, album.Name, photoCreatedAfterDate);
                    if (photoOfAlbums.Count > 0)
                        playerPhotos.AddRange(photoOfAlbums);
                }
                catch (Exception abException)
                {
                    Log.Error($"Error -> GetPhotosByAlbum -> AlbumId {album.Id}", abException);
                }
            }

            stopWatch.Stop();
            Log.Debug($"Get user photos from fb -> {stopWatch.Elapsed.TotalSeconds}");
            stopWatch.Start();

            playerService.AddOrUpdatePlayerPhotos(playerProfileModel, playerPhotos);

            stopWatch.Stop();
            Log.Debug($"Update fb photos to db -> {stopWatch.Elapsed.TotalSeconds}");
            stopWatch.Start();

            return playerPhotos;
        }

        private static void UpdateGameProgress(CosyGameService gameService, GameTransactionDto gameTransactionDto, int percentComplete)
        {
            gameTransactionDto.PercentComplete = percentComplete;
            gameService.UpdateGameProgress(gameTransactionDto);
        }

        // POST api/cosy-game/result-result -> get game result status
        [HttpPost]
        [Route("result-status")]
        public IHttpActionResult GetPlayerGameResultStatus([FromBody]GetGameResultCommand command)
        {
            var gameService = new CosyGameService();
            var result = gameService.GetUserGameResultStatus(command.GameTransactionId);
            return Ok(result);
        }

        // POST api/cosy-game/result -> get game final result
        [HttpPost]
        [Route("result")]
        [ResponseType(typeof(UserGameResultDto))]
        public IHttpActionResult GetPlayerGameResult([FromBody]GetGameResultCommand command)
        {
            var gameService = new CosyGameService();
            var result = gameService.GetUserGameResult(command.GameTransactionId);
            return Ok(result);
        }

        [HttpPost]
        [Route("photo-reactions")]
        public async Task<IHttpActionResult> GetPhotoReactions([FromBody] GetFacebookPhotoCommand command)
        {
            if (string.IsNullOrEmpty(command.AccessToken))
                return Ok("Invalid access token.");

            var fbService = new FacebookClientService(command.AccessToken);

            var result = await fbService.GetPhotoReactions(command.FbPhotoId);

            return Ok(result);
        }

        [HttpPost]
        [Route("photo-tags")]
        public async Task<IHttpActionResult> GetPhotoTags([FromBody] GetFacebookPhotoCommand command)
        {
            if (string.IsNullOrEmpty(command.AccessToken))
                return Ok("Invalid access token.");

            var fbService = new FacebookClientService(command.AccessToken);

            var result = await fbService.GetTagsFromPhoto(command.FbPhotoId);

            return Ok(result);
        }

        [HttpPost]
        [Route("tagged-places")]
        public async Task<IHttpActionResult> GetUserTaggedPlaces([FromBody] GetFacebookPhotoCommand command)
        {
            if (string.IsNullOrEmpty(command.AccessToken))
                return Ok("Invalid access token.");

            var fbService = new FacebookClientService(command.AccessToken);

            var result = await fbService.GetUserTaggedPlaces(command.FbUserProfileId);

            return Ok(result);
        }

        [HttpPost]
        [Route("feed")]
        public async Task<IHttpActionResult> GetUserFeed([FromBody] GetFacebookPhotoCommand command)
        {
            if (string.IsNullOrEmpty(command.AccessToken))
                return Ok("Invalid access token.");

            var fbService = new FacebookClientService(command.AccessToken);

            var result = await fbService.GetUserFeed(command.FbUserProfileId, 100);

            return Ok(result);
        }

        [HttpGet]
        [Route("random-job")]
        public IHttpActionResult RunRandomJob()
        {
            var gameService = new CosyGameService();
            var jobId = BackgroundJob.Enqueue(() => gameService.TestJob());
            return Ok(jobId);
        }

        [HttpGet]
        [Route("test-permission")]
        public HttpResponseMessage TestFolderPermission()
        {
            var gameSettingDto = new GameSettingService().GetGameGlobalSettings();

            var pathToGameImagesFolder = Path.Combine(gameSettingDto.GameBaseInstallationFolder, @"Game\perm_test");
            DirectoryHelper.CreateDirectoryForWriting(pathToGameImagesFolder);
            var imageGenerator = new GameResultImageGenerator();
            var filePath = Path.Combine(pathToGameImagesFolder, "_009_f6_Number_430x134.png");
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 430,
                Height = 134,
                SavedPath = filePath,
                TextColor = Color.Red,
                TextOnImage = new Random().Next(100).ToString(),
                FontSize = 124,
                FontBold = true
            });

            var fileBytes = File.ReadAllBytes(filePath);
            var result = Request.CreateResponse(HttpStatusCode.OK);
            result.Content = new ByteArrayContent(fileBytes);
            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            result.Content.Headers.ContentDisposition.FileName = "_009_f6_Number_430x134.png";

            return result;
        }
    }
}