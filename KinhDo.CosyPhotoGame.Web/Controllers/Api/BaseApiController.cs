using System.Net.Http;
using System.Reflection;
using System.Web;
using System.Web.Http;
using log4net;
using Microsoft.AspNet.Identity.Owin;

namespace KinhDo.CosyPhotoGame.Web.Controllers.Api
{
    public class BaseApiController : ApiController
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        private ApplicationUserManager _userManager;

        public BaseApiController()
        {
        }

        public BaseApiController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.Current.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        protected string GetClientIp()
        {
            return HttpContext.Current.GetOwinContext().Request.RemoteIpAddress;
        }

        protected string GetClientBrowserAgentString()
        {
            return Request.Headers.UserAgent.ToString();
        }
    }
}