﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Web;
using System.Web.Hosting;
using System.Web.Http;
using KinhDo.CosyPhotoGame.EmotionApi.Service;

namespace KinhDo.CosyPhotoGame.Web.Controllers.Api
{
    [RoutePrefix("api/cosy-file")]
    public class FileDownloadApiController : ApiController
    {
        private const string VideoFolderPath = "~/App_Data/Video";

        /// <summary>
        /// GET api/cosy-file/File?fileName={fileName}
        /// Download content of {fileName} as a stream 
        /// </summary>
        /// <param name="fileName">Name of the file to be downloaded</param>
        /// <returns></returns>
        [HttpGet]
        [Route("file")]
        public HttpResponseMessage File(string fileName)
        {
            var filePath = HostingEnvironment.MapPath($"{VideoFolderPath}/{fileName}");

            if (!System.IO.File.Exists(filePath))
            {
                return new HttpResponseMessage(HttpStatusCode.NotFound);
            }

            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            var result = new HttpResponseMessage(HttpStatusCode.OK) {Content = new StreamContent(stream)};

            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("application/octet-stream");

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = fileName
            };

            return result;
        }

        [HttpGet]
        [Route("web-game-log")]
        public HttpResponseMessage WebGameLogFile([FromUri] string filePath, [FromUri] string token)
        {
            if (!"978E34E9-8FF9-47A0-9473-D8BEF8EF91D8".Equals(token))
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            if (!string.IsNullOrEmpty(filePath))
            {
                filePath = HttpUtility.UrlDecode(filePath);
            }

            if (string.IsNullOrEmpty(filePath))
            {
                var gameSettingDto = new GameSettingService().GetGameGlobalSettings();
                filePath = Path.Combine(gameSettingDto.PlayerGameDataBaseFolder, @"Logs\Logs.txt");
            }

            return DownloadLogFile(filePath);
        }

        private static HttpResponseMessage DownloadLogFile(string filePath)
        {
            var stream = new FileStream(filePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);

            var result = new HttpResponseMessage(HttpStatusCode.OK) {Content = new StreamContent(stream)};

            result.Content.Headers.ContentType =
                new MediaTypeHeaderValue("text/plain");

            result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment")
            {
                FileName = Path.GetFileName(filePath)
            };

            return result;
        }

        [HttpGet]
        [Route("job-server-log")]
        public HttpResponseMessage JobServerLogFile([FromUri] string filePath, [FromUri] string token)
        {
            if (!"978E34E9-8FF9-47A0-9473-D8BEF8EF91D8".Equals(token))
                return new HttpResponseMessage(HttpStatusCode.NotFound);

            if (!string.IsNullOrEmpty(filePath))
            {
                filePath = HttpUtility.UrlDecode(filePath);
            }

            if (string.IsNullOrEmpty(filePath))
            {
                var gameSettingDto = new GameSettingService().GetGameGlobalSettings();

                var jobSeverLogFolder = new DirectoryInfo(gameSettingDto.GameBaseInstallationFolder).Parent?.FullName;

                filePath = Path.Combine(jobSeverLogFolder, @"JobServerService\App_Data\Logs\Logs.txt");
            }

            return DownloadLogFile(filePath);
        }
    }
}
