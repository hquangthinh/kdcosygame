﻿using System;
using System.Configuration;
using System.IO;
using System.Web;
using System.Web.Mvc;
using KinhDo.CosyPhotoGame.EmotionApi.Service;
using KinhDo.CosyPhotoGame.Web.Helper;
using Microsoft.AspNet.Identity.Owin;

namespace KinhDo.CosyPhotoGame.Web.Controllers
{
    [AllowAnonymous]
    public class GameController : BaseController
    {
        private ApplicationUserManager _userManager;

        public GameController()
        {
        }

        public GameController(ApplicationUserManager userManager)
        {
            _userManager = userManager;
        }

        public ApplicationUserManager UserManager
        {
            get { return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>(); }
            private set { _userManager = value; }
        }

        /// <summary>
        /// GET Game/Index this view is loaded after external authentication is successful
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            ViewBag.AccessToken = GetAccessToken();
            ViewBag.ProfileId = !string.IsNullOrEmpty(Request.QueryString["id"]) ? Request.QueryString["id"] : string.Empty;
            ViewBag.UserName = !string.IsNullOrEmpty(Request.QueryString["n"]) ? Request.QueryString["n"] : string.Empty;
            ViewBag.Email = !string.IsNullOrEmpty(Request.QueryString["em"]) ? Request.QueryString["em"] : string.Empty;

            return View();
        }

        /// <summary>
        /// Same as Game/Index but For testing UI only
        /// </summary>
        /// <returns></returns>
        public ActionResult Loading()
        {
            return View("Loading");
        }

        public ActionResult Result()
        {
            var appSetting = AppSettingHelper.LoadAppSetting();

            ViewBag.FbAppId = appSetting.FacebookAppId;
            ViewBag.UserProfileId = string.IsNullOrEmpty(Request.QueryString["UserProfileId"])
                ? "default_fb_profile_id"
                : Request.QueryString["UserProfileId"];
            var siteUrl = ConfigurationManager.AppSettings["GameSiteUrl"];
            ViewBag.ShareUrl = $"{siteUrl}/Game/VideoSharing?fbid={ViewBag.UserProfileId}";

            return View("MobileResult");
        }

        public ActionResult ResultMobile()
        {
            var appSetting = AppSettingHelper.LoadAppSetting();

            ViewBag.FbAppId = appSetting.FacebookAppId;
            ViewBag.UserProfileId = string.IsNullOrEmpty(Request.QueryString["UserProfileId"])
                ? "default_fb_profile_id"
                : Request.QueryString["UserProfileId"];
            var siteUrl = ConfigurationManager.AppSettings["GameSiteUrl"];
            ViewBag.ShareUrl = $"{siteUrl}/Game/VideoSharing?fbid={ViewBag.UserProfileId}";

            return View("MobileResult");
        }

        /// <summary>
        /// GET Game/DownloadResult
        /// </summary>
        /// <returns></returns>
        public ActionResult DownloadResult()
        {
            var profileId = Request.QueryString["profileId"];
            var gameSettingService = new GameSettingService();
            var gameSetting = gameSettingService.GetGameGlobalSettings();
            var resultFilePath = Path.Combine(gameSetting.PlayerGameDataBaseFolder, profileId, $"user_{profileId}.json");
            if (!System.IO.File.Exists(resultFilePath))
                return HttpNotFound($"File {resultFilePath} not found");

            return File(resultFilePath, "application/json");
        }

        public ActionResult VideoSharing()
        {
            var fbid = Request.QueryString["fbid"];
            var siteUrl = ConfigurationManager.AppSettings["GameSiteUrl"];
            var playerDto = new PlayerService().GetPlayerProfileByFbId(fbid);
            ViewBag.PlayerFullName = playerDto?.Fullname;
            ViewBag.ProfileId = fbid;
            ViewBag.VideoSharingUrl = $"{siteUrl}/Game/VideoSharing?fbid={fbid}";
            ViewBag.OgImageUrl = $"{siteUrl.Replace("https","http")}/Game/{fbid}/thumbnail.png";
            ViewBag.SecureOgImageUrl = $"{siteUrl}/Game/{fbid}/thumbnail.png";
            return View();
        }

        public ActionResult Design()
        {
            return View();
        }

        public ActionResult MvLoading()
        {
            return View();
        }

    }
}