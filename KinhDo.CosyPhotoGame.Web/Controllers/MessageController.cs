﻿using System.Web.Mvc;

namespace KinhDo.CosyPhotoGame.Web.Controllers
{
    [AllowAnonymous]
    public class MessageController : Controller
    {
        public ActionResult Index()
        {
            ViewBag.Message = string.IsNullOrEmpty(Request.QueryString["Message"]) ? "Invalid access token" : Request.QueryString["Message"];
            return View();
        }
    }
}