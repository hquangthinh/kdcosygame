﻿using System;
using System.IO;
using System.Web.Mvc;

namespace KinhDo.CosyPhotoGame.Web.Controllers
{
    public class LogController : Controller
    {
        // GET: GetLog
        public ActionResult GetLog()
        {
            var logFilePath = Path.Combine(Server.MapPath("~/App_Data/Logs"), "Logs.txt");
            if(!System.IO.File.Exists(logFilePath))
                return new HttpNotFoundResult("Log file is not available");
            var fileContent = GetLogFileContentAsBytes(logFilePath);
            return File(fileContent, "text/plain");
        }

        private byte[] GetLogFileContentAsBytes(string fullFilePath)
        {
            using (var fs = new FileStream(fullFilePath, FileMode.Open, FileAccess.Read, FileShare.ReadWrite))
            {
                using (var sr = new StreamReader(fs))
                {
                    return GetBytes(sr.ReadToEnd());
                }
            }
        }

        private static byte[] GetBytes(string source)
        {
            if (string.IsNullOrEmpty(source)) return null;
            var bytes = new byte[source.Length * sizeof(char)];
            Buffer.BlockCopy(source.ToCharArray(), 0, bytes, 0, bytes.Length);
            return bytes;
        }
    }
}