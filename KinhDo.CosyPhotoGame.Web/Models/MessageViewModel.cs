﻿namespace KinhDo.CosyPhotoGame.Web.Models
{
    public class MessageViewModel
    {
        public string Type { get; set; }

        public string Message { get; set; }
    }
}