﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using KinhDo.CosyPhotoGame.Web.Helper;

namespace KinhDo.CosyPhotoGame.Web.Extensions
{
    public static class StringExtension
    {
        public static string GenerateAppSecretProof(this String accessToken)
        {
            //Creates a Facebook appsecret_proof value to be used for each graph api call when appsecret_proof has been enabled for the facebook app
            //Facebook appsecret_proof is SHA256 encrypted string of the current facebook access token using the facebook app secret value as the private key
            var appSetting = AppSettingHelper.LoadAppSetting();
            using (var algorithm = new HMACSHA256(Encoding.ASCII.GetBytes(appSetting.FacebookAppSecret)))
            {
                var hash = algorithm.ComputeHash(Encoding.ASCII.GetBytes(accessToken));
                var builder = new StringBuilder();
                foreach (var hashByte in hash)
                {
                    builder.Append(hashByte.ToString("x2", CultureInfo.InvariantCulture));
                }
                return builder.ToString();
            }
        }

        public static string GraphApiCall(this string baseGraphApiCall, params object[] args)
        {
            //returns a formatted Graph Api Call with a version prefix and appends a query string parameter containing the appsecret_proof value
            if (!string.IsNullOrEmpty(baseGraphApiCall))
            {
                if (args != null && args.Any())
                {
                    //Determine if we need to concatenate appsecret_proof query string parameter or inject it as a single query string paramter

                    var graphApiCall = baseGraphApiCall.Contains("?")
                        ? string.Format(baseGraphApiCall + "&appsecret_proof={" + (args.Count() - 1) + "}", args)
                        : string.Format(baseGraphApiCall + "?appsecret_proof={" + (args.Count() - 1) + "}", args);

                    //prefix with Graph API Version
                    return $"v2.7/{graphApiCall}";
                }

                throw new Exception("GraphApiCall requires at least one string parameter that contains the appsecret_proof value.");
            }

            return string.Empty;
        }
    }
}