﻿using System;
using System.Configuration;
using System.Collections.Generic;

namespace KinhDo.CosyPhotoGame.Web.Helper
{
    public class AppSettingDto
    {
        public string AppMode { get; set; }
        public string FacebookAppId { get; set; }
        public string FacebookAppSecret { get; set; }
        public string AppNamespace { get; set; }
        public string FacebookScope { get; set; }
        public List<string> FacebookScopeList { get; set; }
    }

    public class AppSettingHelper
    {
        public static AppSettingDto LoadAppSetting()
        {
            var appMode = ConfigurationManager.AppSettings["AppMode"];
            var appSetting = new AppSettingDto
            {
                AppMode = appMode,
                FacebookScopeList = new List<string>()
            };
            if (string.Compare("dev", appMode, StringComparison.InvariantCultureIgnoreCase) == 0)
            {
                appSetting.FacebookAppId = ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppId.TestApp"];
                appSetting.FacebookAppSecret =
                    ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppSecret.TestApp"];
                appSetting.AppNamespace =
                    ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppNamespace.TestApp"];
            }
            else
            {
                appSetting.FacebookAppId = ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppId"];
                appSetting.FacebookAppSecret =
                    ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppSecret"];
                appSetting.AppNamespace =
                    ConfigurationManager.AppSettings["ExternalAuth.Facebook.AppNamespace"];
            }

            appSetting.FacebookScope = ConfigurationManager.AppSettings["ExternalAuth.Facebook_Scope"];

            if (!string.IsNullOrEmpty(appSetting.FacebookScope))
            {
                var scopeList = appSetting.FacebookScope.Split(" ".ToCharArray());
                appSetting.FacebookScopeList.AddRange(scopeList);
            }

            return appSetting;
        }
    }
}