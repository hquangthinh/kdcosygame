﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Facebook;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.Web.Extensions;
using log4net;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.Web.Service
{
    public class FacebookClientService
    {
        private static readonly ILog Logger = LogManager.GetLogger("FacebookClientService");

        private readonly string _accessToken;
        private readonly string _appSecretProof;
        private readonly FacebookClient _fbClient;

        protected FacebookClient FbClient => _fbClient ?? new FacebookClient(_accessToken);

        public FacebookClientService(string accessToken)
        {
            _accessToken = accessToken;
            _appSecretProof = _accessToken.GenerateAppSecretProof();
            _fbClient = new FacebookClient(_accessToken);
        }

        public async Task<FacebookProfileViewModel> GetPlayerProfile()
        {
            //Get current user's profile
            dynamic myInfo = await FbClient.GetTaskAsync("me?fields=first_name,last_name,link,locale,email,name,birthday,gender,location,bio,age_range".GraphApiCall(_appSecretProof));

            //get current picture
            dynamic profileImgResult = await FbClient.GetTaskAsync("{0}/picture?width=300&height=300&redirect=false".GraphApiCall((string)myInfo.id, _appSecretProof));

            //Hydrate FacebookProfileViewModel with Graph API results
            var facebookProfile = DynamicExtension.ToStatic<FacebookProfileViewModel>(myInfo);

            facebookProfile.ImageURL = profileImgResult.data.url;

            return facebookProfile;
        }

        public async Task<List<FacebookAlbumViewModel>> GetPlayerAlbums(int daysInThePast)
        {
            dynamic myInfo =
                await
                    FbClient.GetTaskAsync(
                        "me/albums?fields=id,name,count,link,privacy,type,location,created_time,updated_time"
                            .GraphApiCall(_appSecretProof), null);

            //Hydrate FacebookAlbumViewModel with Graph API results
            var albumList = new List<FacebookAlbumViewModel>();
            foreach (dynamic album in myInfo.data)
            {
                albumList.Add(DynamicExtension.ToStatic<FacebookAlbumViewModel>(album));
            }
            
            // Filter latest albums base on updated date time
            return albumList.Where(item => item.UpdatedTime >= DateTime.Now.AddDays(-daysInThePast)).ToList();
        }

        public async Task<List<FacebookPhotoViewModel>> GetPhotosByAlbum(string albumId, string albumName, DateTime createdAfter)
        {
            if (string.IsNullOrEmpty(albumId))
                return new List<FacebookPhotoViewModel>();

            dynamic photoResults = await FbClient.GetTaskAsync(
                                        ($"{albumId}/photos" + "?fields=id,picture,name,link,name_tags,created_time,backdated_time,updated_time,images,place,reactions.limit(50),tags.limit(50)&limit=100")
                                            .GraphApiCall(_appSecretProof));
            var photoList = HydratePhotoList(photoResults, albumName, createdAfter);

            return photoList;
        }

        private List<FacebookPhotoViewModel> HydratePhotoList(dynamic result, string albumName, DateTime createdAfter)
        {
            var photoList = new List<FacebookPhotoViewModel>();
            foreach (dynamic photo in result.data)
            {
                FacebookPhotoViewModel photoDto = DynamicExtension.ToStatic<FacebookPhotoViewModel>(photo);
                photoDto.AlbumName = albumName;
                photoList.Add(photoDto);
            }
            return photoList.Where(item => item.CreatedTime > createdAfter).ToList();
        }

        public async Task<FacebookPhotoReactionViewModel> GetPhotoReactions(string fbPhotoId)
        {
            if(string.IsNullOrEmpty(fbPhotoId))
                return new FacebookPhotoReactionViewModel();

            dynamic photoReactionsResult =
                await
                    FbClient.GetTaskAsync(
                        $"{fbPhotoId}/reactions?summary=total_count,viewer_reaction".GraphApiCall(_appSecretProof));

            if (photoReactionsResult == null)
                return new FacebookPhotoReactionViewModel();

            var result = JsonConvert.DeserializeObject<FacebookPhotoReactionViewModel>(photoReactionsResult.ToString());

            return result;
        }

        public async Task<FacebookPhotoTagViewModel> GetTagsFromPhoto(string fbPhotoId)
        {
            if (string.IsNullOrEmpty(fbPhotoId))
                return new FacebookPhotoTagViewModel();

            dynamic photoTagsResult =
                await
                    FbClient.GetTaskAsync(
                        $"{fbPhotoId}/tags".GraphApiCall(_appSecretProof));

            if (photoTagsResult == null)
                return new FacebookPhotoTagViewModel();

            var result = JsonConvert.DeserializeObject<FacebookPhotoTagViewModel>(photoTagsResult.ToString());

            return result;
        }

        public async Task<FacebookUserTaggedPlaceViewModel> GetUserTaggedPlaces(string fbProfileId)
        {
            dynamic userTaggedPlacesResult =
                await
                    FbClient.GetTaskAsync(
                        "me/tagged_places?limit=100".GraphApiCall(_appSecretProof));

            if (userTaggedPlacesResult == null)
                return new FacebookUserTaggedPlaceViewModel();

            var result = JsonConvert.DeserializeObject<FacebookUserTaggedPlaceViewModel>(userTaggedPlacesResult.ToString());
            if (result != null)
                result.PlayerFbProfileId = fbProfileId;

            return result;
        }

        public async Task<FacebookPostViewModel> GetUserFeed(string fbProfileId, int limit)
        {
            dynamic userPostsResult =
                await
                    FbClient.GetTaskAsync(
                        $"me/feed?fields=id,created_time,type,message,story,link,place,with_tags,from,to&limit={limit}".GraphApiCall(
                            _appSecretProof));

            if (userPostsResult == null)
                return new FacebookPostViewModel();

            var result = JsonConvert.DeserializeObject<FacebookPostViewModel>(userPostsResult.ToString());
            if (result != null)
                result.PlayerFbProfileId = fbProfileId;

            return result;
        } 
    }
}
