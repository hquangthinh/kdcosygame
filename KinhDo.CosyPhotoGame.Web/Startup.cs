﻿using System.Configuration;
using Hangfire;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;

[assembly: OwinStartupAttribute(typeof(KinhDo.CosyPhotoGame.Web.Startup))]
[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]
namespace KinhDo.CosyPhotoGame.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);

            app.UseCors(CorsOptions.AllowAll);

            GlobalConfiguration.Configuration.UseSqlServerStorage("Default");
            if ("true".Equals(ConfigurationManager.AppSettings["HostJobServerInWebProcess"]))
            {
                app.UseHangfireServer();
            }
            app.UseHangfireDashboard();
            System.Web.Helpers.AntiForgeryConfig.SuppressXFrameOptionsHeader = true;
        }
    }
}