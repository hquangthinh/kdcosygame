﻿/*
 * Home view for cosy game
 */
(function ($, document) {

    "use strict";

    var PROGRESS_TIMER_INTERVAL = 1500; // 1.5s
    var PROGRESS_TIMER_INTERVAL_3S = 3000; // 3s
    var PROGRESS_TIMER_INTERVAL_5S = 5000; // 5s
    var GAME_STATUS_POLLING_INTERVAL = 10000;  // 10s

    var progressbar,
        progressTimer,
        gameStatusPollInterval;

    function updateProgress() {

        var val = progressbar.progressbar("value") || 0;

        progressbar.progressbar("value", val + 1);

        if (val < 50) {
            progressTimer = setTimeout(updateProgress, PROGRESS_TIMER_INTERVAL);
        }
        else if (50 <= val && val < 60) {
            progressTimer = setTimeout(updateProgress, PROGRESS_TIMER_INTERVAL_3S);
        }
        else if (60 <= val && val < 90) {
            progressTimer = setTimeout(updateProgress, PROGRESS_TIMER_INTERVAL_5S);
        }
    }

    // set loading % for the pie
    function setLoading(percent) {
        var points = ['0.5 0'];

        if (percent < 13) {
            var point = [
              0.5 + (0.5 / 12) * percent,
              0
            ];
            points.push(point.join(" "));
            points.push('0.5 0.5');
        } else if (percent < 37) {
            var point = [
              1,
              (1 / 24) * (percent - 13)
            ];
            points.push("1 0");
            points.push(point.join(" "));
            points.push('0.5 0.5');

        } else if (percent < 62) {
            var point = [
              1 - (1 / 24) * (percent - 37),
              1
            ];
            points.push("1 0");
            points.push("1 1");
            points.push(point.join(" "));
            points.push('0.5 0.5');
        } else if (percent < 87) {
            var point = [
              0,
              1 - (1 / 24) * (percent - 62)
            ];
            points.push("1 0");
            points.push("1 1");
            points.push("0 1");
            points.push(point.join(" "));
            points.push('0.5 0.5');
        } else if (percent < 100) {
            var point = [
              (1 / 24) * (percent - 87),
              0
            ];
            points.push("1 0");
            points.push("1 1");
            points.push("0 1");
            points.push("0 0");
            points.push(point.join(" "));
            points.push('0.5 0.5');
        }

        var polygonStr = "";
        for (var i = 0; i < points.length; i++) {
            var point = points[i];
            point = point.split(" ");
            if (point[0] == 0) {
                polygonStr += "0";
            }

            polygonStr += parsePoint(point[0]) + " " + parsePoint(point[1]);
            if (i + 1 < points.length) {
                polygonStr += ", ";
            }
        }

        $('#loadingSvg polygon').attr('points', points.join(','));
        $('.loading-img').css({
            '-webkit-clip-path': "polygon(" + polygonStr + ")"
        });

        if (percent === 99) {
            $('.number-load').text("100%");
        } else {
            $('.number-load').text(percent + "%");
        }

        $('.loading-img').show();
    }

    function parsePoint(point) {
        point = parseFloat(point);
        return point == 0 ? "0" : (point * 100) + "%";
    }

    $(document).ready(function () {

        $('#cosy-mv')
                .on('loadstart',
                    function (event) {
                        $(this).addClass('background');
                    });

        $('#cosy-mv')
            .on('canplay',
                function (event) {
                    $(this).removeClass('background');
                });

        var accessToken = $("#hidAccessToken").val();
        var profileId = $("#hidProfileId").val();
        var userName = $("#hidUserName").val();
        var email = $("#hidEmail").val();
        progressbar = $("#progressbar");

        // progress bar setup
        progressbar.progressbar({
            value: false,
            change: function () {
                var progressValInt = progressbar.progressbar("value");
                setLoading(progressValInt);
            },
            complete: function () {
            }
        });

        function pollForGameResultStatus(gameTransactionId) {
            console.log("Polling status for game transaction -> " + gameTransactionId);
            $.post("/api/cosy-game/result-status", { GameTransactionId: gameTransactionId })
                .then(function(gameRes) {
                        console.log(gameRes);
                        if (gameRes && gameRes.Status === "ResultOk" && gameRes.PercentComplete === 100) {

                            clearInterval(gameStatusPollInterval);

                            // update pie loading full
                            try {
                                progressbar.progressbar("value", 100);
                                clearTimeout(progressTimer);
                                setLoading(99);
                            }
                            catch(e) {
                                console.log(e);
                            }

                            setTimeout(function () {
                                // Go to game result page
                                window.location.href = "/Game/Result?gameTransactionId=" + gameTransactionId + "&UserProfileId=" + profileId;
                            }, 2000);

                        } else if (gameRes && gameRes.ErrorMessage) {
                            console.log(gameRes.ErrorMessage);
                            progressbar.progressbar("value", 100);
                            clearTimeout(progressTimer);
                            setLoading(99);
                        }
                    },
                    function(errorRes) {
                        console.log(errorRes);
                    });
        }

        // Start progress
        updateProgress();

        // GET game result
        $.post("/api/cosy-game/prepare", { AccessToken: accessToken, FbUserProfileId: profileId, UserName: userName, Email: email })
            .then(function(res) {
                    console.log(res);
                    initPreloadVideoResources();
                    // polling server for game result
                    gameStatusPollInterval = setInterval(pollForGameResultStatus, GAME_STATUS_POLLING_INTERVAL, res.Id);
                },
                function(errorRes) {
                    console.log(errorRes);
                });
    });

})(jQuery, document);