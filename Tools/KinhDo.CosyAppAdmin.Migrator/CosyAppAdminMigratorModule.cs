using System.Data.Entity;
using System.Reflection;
using Abp.Modules;
using KinhDo.CosyAppAdmin.EntityFramework;

namespace KinhDo.CosyAppAdmin.Migrator
{
    [DependsOn(typeof(CosyAppAdminDataModule))]
    public class CosyAppAdminMigratorModule : AbpModule
    {
        public override void PreInitialize()
        {
            Database.SetInitializer<CosyAppAdminDbContext>(null);

            Configuration.BackgroundJobs.IsJobExecutionEnabled = false;
        }

        public override void Initialize()
        {
            IocManager.RegisterAssemblyByConvention(Assembly.GetExecutingAssembly());
        }
    }
}