﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using Microsoft.ProjectOxford.Emotion;

namespace KinhDo.CosyPhotoGame.EmotionApi.Microsoft
{
    public class MsCognitiveEmotionService : BaseEmotionService, IEmotionDetector
    {
        private readonly EmotionServiceClient _emotionServiceClient;

        public MsCognitiveEmotionService(string apiKey) : base(apiKey)
        {
            _emotionServiceClient = new EmotionServiceClient(apiKey);
        }

        public EmotionResult DetectEmotion(PhotoEmotionDetectionCommand command)
        {
            throw new NotImplementedException("Use Async method instead");
        }

        public async Task<EmotionResult> DetectEmotionAsync(PhotoEmotionDetectionCommand command)
        {
            var result = new EmotionResult
            {
                ResponseStatus = 200
            };

            var emotionResult = await _emotionServiceClient.RecognizeAsync(command.PhotoUrl);
            
            if (emotionResult == null)
            {
                result.ResponseStatus = 1001;
                result.ErrorMessage = "INTERNAL_ERROR";
                return result;
            }

            result.TotalFacesCount = emotionResult.Length;
            foreach (var emotion in emotionResult)
            {
                result.Happiness = Math.Max(result.Happiness, Convert.ToDecimal(emotion.Scores.Happiness));
                result.Anger = Math.Max(result.Anger, Convert.ToDecimal(emotion.Scores.Anger));
                result.Contempt = Math.Max(result.Contempt, Convert.ToDecimal(emotion.Scores.Contempt));
                result.Disgust = Math.Max(result.Disgust, Convert.ToDecimal(emotion.Scores.Disgust));
                result.Fear = Math.Max(result.Fear, Convert.ToDecimal(emotion.Scores.Fear));
                result.Neutral = Math.Max(result.Neutral, Convert.ToDecimal(emotion.Scores.Neutral));
                result.Sadness = Math.Max(result.Sadness, Convert.ToDecimal(emotion.Scores.Sadness));
                result.Surprise = Math.Max(result.Surprise, Convert.ToDecimal(emotion.Scores.Surprise));
            }

            // Convert score to scale 100
            result.Happiness = result.Happiness*100;
            result.Anger = result.Anger*100;
            result.Contempt = result.Contempt*100;
            result.Disgust = result.Disgust*100;
            result.Fear = result.Fear*100;
            result.Neutral = result.Neutral*100;
            result.Sadness = result.Sadness*100;
            result.Surprise = result.Surprise*100;

            return result;
        }
    }
}
