﻿using System.Reflection;
using System.Threading.Tasks;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using log4net;

namespace KinhDo.CosyPhotoGame.EmotionApi
{
    public class EmotionDetectorCreationCommand
    {
        public string ProviderName { get; set; }

        public string ApiKey { get; set; }

        public string ApiSecret { get; set; }
    }

    public class PhotoEmotionDetectionCommand
    {
        public string PhotoUrl { get; set; }

        public string FacebookPhotoId { get; set; }

        public int PhotoId { get; set; }

        public string PhotoTempFolderPath { get; set; }
    }

    public interface IEmotionDetector
    {
        string DetectorProviderName { get; set; }

        EmotionResult DetectEmotion(PhotoEmotionDetectionCommand command);

        Task<EmotionResult> DetectEmotionAsync(PhotoEmotionDetectionCommand command);
    }

    public abstract class BaseEmotionDetector
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public string DetectorProviderName { get; set; }

        protected BaseEmotionDetector()
        {
            DetectorProviderName = "Microsoft";
        }
    }
}