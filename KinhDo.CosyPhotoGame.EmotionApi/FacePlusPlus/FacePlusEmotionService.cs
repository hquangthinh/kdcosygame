﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using FaceppSDK;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.FacePlusPlus
{
    public class FacePlusEmotionService : BaseEmotionService, IEmotionDetector
    {
        private const string BaseUrl = "https://apius.faceplusplus.com";
        private const string DetectionEndPoint = "/v2/detection/detect";
        private readonly FaceService _fppFaceService;

        public FacePlusEmotionService(string apiKey, string apiSecret) : base(apiKey, apiSecret)
        {
            _fppFaceService = new FaceService(apiKey, apiSecret);
        }

        public EmotionResult DetectEmotion(PhotoEmotionDetectionCommand command)
        {
            var result = new EmotionResult
            {
                ResponseStatus = 200
            };
            // strip the query from this photo if there's
            var fppResult = _fppFaceService.Detection_Detect(command.PhotoUrl);
            if (fppResult == null)
            {
                result.ResponseStatus = 1001;
                result.ErrorMessage = "INTERNAL_ERROR";
                return result;
            }

            result.TotalFacesCount = fppResult.face.Count;
            foreach (var face in fppResult.face.Where(f => f.attribute?.smiling != null).ToList())
            {
                var smilingValue = Convert.ToDecimal(face.attribute.smiling.value);
                result.Happiness = Math.Max(result.Happiness, smilingValue);
            }

            return result;
        }

        public async Task<EmotionResult> DetectEmotionAsync(PhotoEmotionDetectionCommand command)
        {
            var result = new EmotionResult
            {
                ResponseStatus = 200
            };

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(BaseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromMinutes(2);
                var url =
                    $"{DetectionEndPoint}?url={WebUtility.UrlEncode(command.PhotoUrl)}&api_key={_apiKey}&api_secret={_apiSecret}&attribute=glass,pose,gender,age,race,smiling";
                var response = await client.GetStringAsync(url);
                if (string.IsNullOrEmpty(response))
                {
                    result.ResponseStatus = 1001;
                    result.ErrorMessage = "INTERNAL_ERROR";
                    return result;
                }

                // parse response to DetectResult object
                var detectResult = JsonConvert.DeserializeObject<DetectResult>(response);
                if (detectResult != null)
                {
                    result.TotalFacesCount = detectResult.face.Count;
                    foreach (var face in detectResult.face.Where(f => f.attribute?.smiling != null).ToList())
                    {
                        var smilingValue = Convert.ToDecimal(face.attribute.smiling.value);
                        result.Happiness = Math.Max(result.Happiness, smilingValue);
                    }
                }
            }

            return result;
        }
    }
}
