﻿using System.Text;
using System.Xml;
using System.Xml.Serialization;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;

namespace KinhDo.CosyPhotoGame.EmotionApi.Helper
{
    public static class XmlHelper
    {
        public static void SerializeObject(UserResultElement userResultElement, string filePath)
        {
            using (var textWriter = new XmlTextWriter(filePath, Encoding.UTF8))
            {
                var xmlSerializer = new XmlSerializer(typeof(UserResultElement));
                xmlSerializer.Serialize(textWriter, userResultElement);
            }
        }
    }
}
