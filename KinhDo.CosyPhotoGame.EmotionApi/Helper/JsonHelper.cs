﻿using System.IO;
using System.Text;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Helper
{
    public static class JsonHelper
    {
        public static void SerializeObject(UserResultElement userResultElement, string filePath)
        {
            var jsonString = JsonConvert.SerializeObject(userResultElement, Formatting.Indented);
            File.WriteAllText(filePath, jsonString);
        }

        public static void WriteLibraryProperties(UserLibraryPropertyDto playerLibProperties, string filePath)
        {
            var libPropsJsonString = JsonConvert.SerializeObject(playerLibProperties, Formatting.Indented);
            var fileContentBuilder = new StringBuilder();
            fileContentBuilder.Append("// library properties for specific user").AppendLine();
            fileContentBuilder.AppendFormat("lib.properties = {0}", libPropsJsonString);
            File.WriteAllText(filePath, fileContentBuilder.ToString());
        }

        public static void WriteLibraryProperties(string appBasePath, string playerFbProfileId, string targetFilePath)
        {
            var templateFilePath = Path.Combine(appBasePath, @"Content\user_data\default_fb_profile_id\lib-properties.js");
            var templateContent = File.ReadAllText(templateFilePath);
            var formattedContent = templateContent.Replace("{profile_id}", playerFbProfileId);
            File.WriteAllText(targetFilePath, formattedContent);
        }
    }
}
