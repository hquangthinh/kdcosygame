﻿using System.Collections.Generic;
using System.IO;
using Kaliko.ImageLibrary;

namespace KinhDo.CosyPhotoGame.EmotionApi.Helper
{
    public static class ImageHelper
    {
        public static string SaveImageAsPngFromUrl(string imageUrl, string directoryPath, string localFileName)
        {
            var filePath = Path.Combine(directoryPath, localFileName);

            if (File.Exists(filePath)) return filePath;

            var image = new KalikoImage(imageUrl);

            image.SavePng(filePath);

            return filePath;
        }

        public static string SaveImageAsPngFromUrl(string imageUrl, string localFilePath, int width, int height)
        {
            var image = new KalikoImage(imageUrl);

            image.Resize(width, height);

            image.SavePng(localFilePath);

            return localFilePath;
        }

        /// <summary>
        /// Save photos to all target folder paths
        /// </summary>
        /// <param name="imageUrl"></param>
        /// <param name="directoryPaths"></param>
        /// <param name="localFileName"></param>
        public static void SaveImageAsPngFromUrl(string imageUrl, List<string> directoryPaths, string localFileName)
        {
            var image = new KalikoImage(imageUrl);
            foreach (var directoryPath in directoryPaths)
            {
                var filePath = Path.Combine(directoryPath, localFileName);
                if (File.Exists(filePath)) continue;
                image.SavePng(filePath);
            }
        }
    }
}
