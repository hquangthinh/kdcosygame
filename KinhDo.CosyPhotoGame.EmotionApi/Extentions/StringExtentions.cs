﻿using System;

namespace KinhDo.CosyPhotoGame.EmotionApi.Extentions
{
    public static class StringExtentions
    {
        /// <summary>
        /// Remove query string from an url
        /// </summary>
        /// <param name="url"></param>
        /// <returns></returns>
        public static string StripQueryString(this string url)
        {
            if(string.IsNullOrEmpty(url)) return string.Empty;
            var uri = new Uri(url);
            return $"{uri.Scheme}://{uri.Host}{uri.AbsolutePath}";
        }

        public static string StripLength(this string source, int maxLength)
        {
            if (string.IsNullOrEmpty(source)) return string.Empty;
            return source.Length <= maxLength ? source : source.Substring(0, maxLength - 1);
        }
    }
}
