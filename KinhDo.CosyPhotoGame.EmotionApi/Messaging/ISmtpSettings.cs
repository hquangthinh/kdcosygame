﻿using System;
using System.Configuration;
using System.Net.Configuration;

namespace KinhDo.CosyPhotoGame.EmotionApi.Messaging
{
    public interface ISmtpSettings
    {
        string FromAddress { get; set; }
        string Host { get; set; }
        int? Port { get; set; }
        bool EnableSsl { get; set; }
        bool RequireCredentials { get; set; }
        string UserName { get; set; }
        string Password { get; set; }
        bool IsValid();
    }

    public abstract class BaseSmtpSettings : ISmtpSettings
    {
        public string FromAddress { get; set; }
        public string Host { get; set; }
        public int? Port { get; set; }
        public bool EnableSsl { get; set; }
        public bool RequireCredentials { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public abstract bool IsValid();
    }

    public class ManualValueSmtpSettings : BaseSmtpSettings
    {
        public override bool IsValid()
        {
            if (string.IsNullOrWhiteSpace(FromAddress))
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Host) && Port == 0)
            {
                return false;
            }

            return true;
        }
    }

    /// <summary>
    /// Email setting based on web.config system.net/mailSettings/smtp section
    /// </summary>
    public class WebConfigSmtpSettings : BaseSmtpSettings
    {
        private readonly SmtpSection _smtpSection;
        public WebConfigSmtpSettings()
        {
            _smtpSection = (SmtpSection)ConfigurationManager.GetSection("system.net/mailSettings/smtp");
            if (_smtpSection == null)
            {
                throw new ArgumentException("Could not find web.config section system.net/mailSettings/smtp");
            }
            FromAddress = _smtpSection.From;
            if (_smtpSection.Network == null)
            {
                return;
            }
            Host = _smtpSection.Network.Host;
            Port = _smtpSection.Network.Port;
            EnableSsl = _smtpSection.Network.EnableSsl;
            RequireCredentials = !_smtpSection.Network.DefaultCredentials;
            UserName = _smtpSection.Network.UserName;
            Password = _smtpSection.Network.Password;
        }

        public override bool IsValid()
        {
            if (!string.IsNullOrWhiteSpace(_smtpSection?.Network.Host))
            {
                return true;
            }

            if (string.IsNullOrWhiteSpace(FromAddress))
            {
                return false;
            }

            if (!string.IsNullOrWhiteSpace(Host) && Port == 0)
            {
                return false;
            }

            return true;
        }
    }
}
