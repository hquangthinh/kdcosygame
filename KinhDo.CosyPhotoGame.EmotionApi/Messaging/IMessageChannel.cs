﻿using System;
using System.Threading.Tasks;
using System.ComponentModel;

namespace KinhDo.CosyPhotoGame.EmotionApi.Messaging
{
    public interface IMessageChannel
    {
        void Process(Message message);

        Task ProcessAsync(Message message);
    }

    public enum ChannelType
    {
        Email,
        Sms,
        Fake, // dummy notification does not really send message
        InApp // built-in notification
    }

    public interface ISmtpChannel : IMessageChannel
    {
        event EventHandler<AsyncCompletedEventArgs> EmailSendCompleted;
    }
}
