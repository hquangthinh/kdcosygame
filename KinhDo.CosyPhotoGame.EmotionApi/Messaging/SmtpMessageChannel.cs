﻿using System;
using System.ComponentModel;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace KinhDo.CosyPhotoGame.EmotionApi.Messaging
{
    public class SmtpMessageChannel : ISmtpChannel, IDisposable
    {
        private readonly ISmtpSettings _smtpSettings;
        private readonly Lazy<SmtpClient> _smtpClientField;
        /// <summary>
        /// Handles the event where an asynchronous email sending
        /// operation is completed.
        /// </summary>
        public event EventHandler<AsyncCompletedEventArgs> EmailSendCompleted;
        public SmtpMessageChannel(ISmtpSettings smtpSettings)
        {
            _smtpSettings = smtpSettings;
            _smtpClientField = new Lazy<SmtpClient>(CreateSmtpClient);
        }

        public void Dispose()
        {
            if (!_smtpClientField.IsValueCreated)
            {
                return;
            }

            _smtpClientField.Value.Dispose();
        }

        /// <summary>
        /// Send mail message synchronously using SmtpClient.Send()
        /// </summary>
        /// <param name="message"></param>
        public void Process(Message message)
        {
            var mailMessage = BuildMailMessage(message);
            if (mailMessage == null)
                return;
            SendMessageInternal(mailMessage);
        }

        private void SendMessageInternal(MailMessage mailMessage)
        {
            try
            {
                _smtpClientField.Value.Send(mailMessage);
            }
            catch (Exception e)
            {
                throw;
            }
            finally
            {
                mailMessage.Dispose();
            }
        }

        /// <summary>
        /// Send mail message assynchronously using SmtpClient.SendMailAsync()
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task ProcessAsync(Message message)
        {
            var mailMessage = BuildMailMessage(message);
            if (mailMessage == null)
                return;
            var client = CreateSmtpClient();
            client.SendCompleted += (sender, e) => SmtpClient_SendCompleted(sender, e, mailMessage);
            await client.SendMailAsync(mailMessage);
        }

        void SmtpClient_SendCompleted(object sender, AsyncCompletedEventArgs e, MailMessage mailMessage)
        {
            mailMessage?.Dispose();

            EmailSendCompleted?.Invoke(this, e);
        }

        private MailMessage BuildMailMessage(Message message)
        {
            if (!_smtpSettings.IsValid())
            {
                throw new ArgumentException("Email cannot be sent. Either E-mail Server or From Address is empty.");
            }

            if (string.IsNullOrEmpty(message.Recipients))
            {
                throw new ArgumentException("Email cannot be sent. Recipients are empty.");
            }

            if (string.IsNullOrEmpty(message.CcList))
            {
                message.CcList = string.Empty;
            }

            if (string.IsNullOrEmpty(message.BccList))
            {
                message.BccList = string.Empty;
            }

            var mailMessage = new MailMessage
            {
                Subject = message.Subject,
                Body = message.Body,
                IsBodyHtml = true
            };

            mailMessage.From = !string.IsNullOrWhiteSpace(message.From) ? new MailAddress(message.From, "Cosy Count Your Positive Vibes") : new MailAddress(_smtpSettings.FromAddress, "Cosy Count Your Positive Vibes");

            foreach (var recipient in message.Recipients.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                mailMessage.To.Add(new MailAddress(recipient));
            }

            foreach (var cc in message.CcList.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                mailMessage.CC.Add(new MailAddress(cc));
            }

            foreach (var cc in message.BccList.Split(new[] { ',', ';' }, StringSplitOptions.RemoveEmptyEntries))
            {
                mailMessage.CC.Add(new MailAddress(cc));
            }

            // add attachments
            if (message.AttachmentFilePaths != null)
            {
                foreach (var attachmentFilePath in message.AttachmentFilePaths)
                {
                    if (!File.Exists(attachmentFilePath))
                    {
                        continue;
                    }
                    mailMessage.Attachments.Add(new Attachment(attachmentFilePath));
                }
            }

            if (message.EmbeddedImages != null)
            {
                foreach (var entry in message.EmbeddedImages)
                {
                    var ms = new MemoryStream(entry.Value);
                    var att = new Attachment(ms, entry.Key)
                    {
                        ContentId = entry.Key,
                        ContentType = new ContentType("image/gif"),
                        Name = entry.Key
                    };
                    att.ContentDisposition.Inline = true;
                    att.ContentDisposition.DispositionType = DispositionTypeNames.Inline;
                    mailMessage.Attachments.Add(att);
                }
            }

            return mailMessage;
        }

        private SmtpClient CreateSmtpClient()
        {
            // if no properties are set in the settings, use the web.config value
            if (string.IsNullOrWhiteSpace(_smtpSettings.Host))
            {
                return new SmtpClient();
            }

            var smtpClient = new SmtpClient();

            if (!string.IsNullOrWhiteSpace(_smtpSettings.UserName) && !string.IsNullOrWhiteSpace(_smtpSettings.Password))
            {
                smtpClient.Credentials = new NetworkCredential(_smtpSettings.UserName, _smtpSettings.Password);
            }

            if (_smtpSettings.Host != null)
            {
                smtpClient.Host = _smtpSettings.Host;
            }
            if (_smtpSettings.Port.HasValue) smtpClient.Port = _smtpSettings.Port.Value;
            smtpClient.EnableSsl = _smtpSettings.EnableSsl;
            smtpClient.DeliveryMethod = SmtpDeliveryMethod.Network;
            return smtpClient;
        }
    }
}