﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Messaging
{
    public class FacebookMessageCommand
    {
        public string FbProfileId { get; set; }

        public string Title { get; set; }

        public string GameResultRelativePath { get; set; }
    }

    public class FbMessageModel
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("href")]
        public string Href { get; set; }

        [JsonProperty("template")]
        public string Template { get; set; }
    }

    /// <summary>
    /// Send notification from app to facebook user
    /// </summary>
    public class FacebookMessageChannel
    {
        private const string FacebookGraphApiBaseUrl = "https://graph.facebook.com";
        private const string FacebookGraphApiVersion = "v2.7";

        private readonly string _appAccessToken;

        public FacebookMessageChannel(string appAccessToken)
        {
            _appAccessToken = appAccessToken;
        }

        public async Task<HttpResponseMessage> SendAsync(FacebookMessageCommand message)
        {
            CheckAccessToken();

            if(message == null)
                throw new ArgumentNullException(nameof(message), "Message must not be null");

            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(FacebookGraphApiBaseUrl);
                client.DefaultRequestHeaders.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                client.Timeout = TimeSpan.FromSeconds(30);

                var href = WebUtility.UrlEncode(message.GameResultRelativePath);

                var url =
                    $"/{FacebookGraphApiVersion}/{message.FbProfileId}/notifications?access_token={_appAccessToken}&template={message.Title}&href={href}";

                var jsonString = JsonConvert.SerializeObject(new FbMessageModel
                {
                    AccessToken = _appAccessToken,
                    Href = message.GameResultRelativePath,
                    Template = message.Title
                });
                return await client.PostAsync(url, new StringContent(jsonString, Encoding.UTF8, "application/json"));
            }
        }

        private void CheckAccessToken()
        {
            if(string.IsNullOrEmpty(_appAccessToken))
                throw new ArgumentNullException(nameof(_appAccessToken), "Access token is required.");
        }
    }
}