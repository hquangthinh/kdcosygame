﻿using System.Collections.Generic;

namespace KinhDo.CosyPhotoGame.EmotionApi.Messaging
{
    public class Message
    {
        /// <summary>
        /// Message channel type could in multi value format separated by ; (i.e Email,InApp,...)
        /// </summary>
        public string MessageChannelType { get; set; }
        public int Priority { get; set; }
        public string From { get; set; }
        public string Recipients { get; set; }
        public string CcList { get; set; }
        public string BccList { get; set; }
        /// <summary>
        /// List of user id for other types of notification
        /// </summary>
        public string ToUserIds { get; set; }
        public string Subject { get; set; }
        public string Type { get; set; }
        public string Body { get; set; }
        public string DetailLink { get; set; }
        public List<string> AttachmentFilePaths { get; set; }
        public IDictionary<string, byte[]> EmbeddedImages { get; set; }

        public bool IsValid()
        {
            if (string.IsNullOrEmpty(From) || string.IsNullOrEmpty(Recipients) || string.IsNullOrEmpty(Subject) ||
                string.IsNullOrEmpty(Body))
            {
                return false;
            }
            return true;
        }
    }
}
