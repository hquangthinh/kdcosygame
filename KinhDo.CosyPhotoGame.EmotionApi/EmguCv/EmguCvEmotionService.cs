﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Threading.Tasks;
using Emgu.CV;
using Emgu.CV.CvEnum;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.Helper;

namespace KinhDo.CosyPhotoGame.EmotionApi.EmguCv
{
    public class EmguCvEmotionService : BaseEmotionDetector, IEmotionDetector
    {
        private string _mTempAppDataPath;

        public EmotionResult DetectEmotion(PhotoEmotionDetectionCommand command)
        {
            if(command == null)
                return new EmotionResult();

            if (string.IsNullOrEmpty(_mTempAppDataPath))
            {
                _mTempAppDataPath = command.PhotoTempFolderPath;
            }

            var localFileName = $"{Guid.NewGuid()}.png";

            var localFilePath = ImageHelper.SaveImageAsPngFromUrl(command.PhotoUrl, _mTempAppDataPath, localFileName);

            var image = new Mat(localFilePath, LoadImageType.Color); //Read the files as an 8-bit Bgr image
            long detectionTime;
            var faces = new List<Rectangle>();
            var eyes = new List<Rectangle>();

            //The cuda cascade classifier doesn't seem to be able to load "haarcascade_frontalface_default.xml" file in this release
            //disabling CUDA module for now
            var tryUseCuda = false;

            DetectFace.Detect(
              image, "haarcascade_frontalface_default.xml", "haarcascade_eye.xml",
              faces, eyes,
              tryUseCuda,
              out detectionTime);

            if(faces.Count == 0)
                return new EmotionResult();

            return new EmotionResult
            {
                ResponseStatus = 200,
                TotalFacesCount = faces.Count,
                Happiness = 80
            };
        }

        public Task<EmotionResult> DetectEmotionAsync(PhotoEmotionDetectionCommand command)
        {
            var result = DetectEmotion(command);
            return Task.FromResult(result);
        }
    }
}
