﻿using System;
using System.ComponentModel.DataAnnotations;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookAlbumViewModel
    {
        [Required]
        [FacebookMapping("id")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "Album Name")]
        [FacebookMapping("name")]
        public string Name { get; set; }

        [Display(Name = "Photo Count")]
        [FacebookMapping("count")]
        public long Count { get; set; }

        [Display(Name = "Link")]
        [FacebookMapping("link")]
        public string Link { get; set; }

        [FacebookMapping("url", parent = "picture")]
        public string ImageURL { get; set; }

        [FacebookMapping("privacy")]
        public string Privacy { get; set; }

        [FacebookMapping("type")]
        public string Type { get; set; }

        [FacebookMapping("location")]
        public string Location { get; set; }

        [FacebookMapping("created_time")]
        public DateTime CreatedTime { get; set; }

        [FacebookMapping("updated_time")]
        public DateTime UpdatedTime { get; set; }
    }
}