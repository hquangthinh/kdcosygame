﻿using System.Collections.Generic;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class Location
    {
        [FacebookMapping("city")]
        [JsonProperty("city")]
        public string City { get; set; }

        [FacebookMapping("country")]
        [JsonProperty("country")]
        public string Country { get; set; }

        [FacebookMapping("latitude")]
        [JsonProperty("latitude")]
        public double Latitude { get; set; }

        [FacebookMapping("longitude")]
        [JsonProperty("longitude")]
        public double Longitude { get; set; }

        [FacebookMapping("street")]
        [JsonProperty("street")]
        public string Street { get; set; }

        [FacebookMapping("zip")]
        [JsonProperty("zip")]
        public string Zip { get; set; }
    }

    public class Place
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("location")]
        [JsonProperty("location")]
        public Location Location { get; set; }

        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class FacebookUserTaggedPlaceItemViewModel
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("created_time")]
        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }

        [FacebookMapping("place")]
        [JsonProperty("place")]
        public Place Place { get; set; }
    }

    public class FacebookUserTaggedPlaceViewModel
    {
        public string PlayerFbProfileId { get; set; }

        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<FacebookUserTaggedPlaceItemViewModel> Data { get; set; }

        [FacebookMapping("paging")]
        [JsonProperty("paging")]
        public FacebookPagingViewModel Paging { get; set; }
    }
}
