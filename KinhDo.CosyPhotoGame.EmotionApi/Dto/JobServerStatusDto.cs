﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class JobServerStatusDto
    {
        public JobServerStatusDto()
        {
            ServerList = new List<JobServerInfoDto>();
        }

        public bool IsActiveServerAlive { get; set; }

        public List<JobServerInfoDto> ServerList { get; set; }
    }

    public class JobServerInfoDto
    {
        public string Name { get; set; }

        public DateTime StartedAt { get; set; }

        public DateTime? Heartbeat { get; set; }
    }
}
