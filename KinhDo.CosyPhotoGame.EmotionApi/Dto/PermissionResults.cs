﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    [DataContract]
    public class PermissionResults
    {
        [DataMember(Name = "data")]
        public List<FacebookPermissionModel> data { get; set; }
    }
}
