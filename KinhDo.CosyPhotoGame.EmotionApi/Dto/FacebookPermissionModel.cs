﻿using System.Runtime.Serialization;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    [DataContract]
    public class FacebookPermissionModel
    {
        [DataMember(Name = "permission")]
        public string permission { get; set; }

        [DataMember(Name = "status")]
        public string status { get; set; }
    }
}