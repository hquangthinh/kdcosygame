﻿using System.Collections.Generic;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    /// <summary>
    /// Represent library properties for user game result
    /// </summary>
    public class UserLibraryPropertyDto
    {
        [JsonProperty("width")]
        public int Width { get; set; }

        [JsonProperty("height")]
        public int Height { get; set; }

        [JsonProperty("fps")]
        public int Fps { get; set; }

        [JsonProperty("color")]
        public string Color { get; set; }

        [JsonProperty("manifest")]
        public List<UserLibraryPropertyManifestDto> Manifest { get; set; }

        public static UserLibraryPropertyDto CreateDefault()
        {
            return new UserLibraryPropertyDto
            {
                Width = 1180,
                Height = 664,
                Fps = 24,
                Color = "#F9F3DE",
                Manifest = new List<UserLibraryPropertyManifestDto>
                {
                    new UserLibraryPropertyManifestDto(1, "{0}/_001_USER_329x76.png", "_001_USER_329x76"),
                    new UserLibraryPropertyManifestDto(2, "{0}/_002_USER_PHOTO_235x235.jpg", "_002_USER_PHOTO_235x235"),
                    new UserLibraryPropertyManifestDto(3, "{0}/_003_USER_NUMBER_274x105.png", "_003_USER_NUMBER_274x105"),
                    new UserLibraryPropertyManifestDto(4, "{0}/_004_f5_hinh1_235x235.jpg", "_004_f5_hinh1_235x235"),
                    new UserLibraryPropertyManifestDto(5, "{0}/_005_f5_hinh2_235x235.jpg", "_005_f5_hinh2_235x235"),
                    new UserLibraryPropertyManifestDto(6, "{0}/_006_f5_hinh3_235x235.jpg", "_006_f5_hinh3_235x235"),
                    new UserLibraryPropertyManifestDto(7, "{0}/_007_f5_hinh4_235x235.jpg", "_007_f5_hinh4_235x235"),
                    new UserLibraryPropertyManifestDto(8, "{0}/_008_f5_hinh5_235x235.jpg", "_008_f5_hinh5_235x235"),
                    new UserLibraryPropertyManifestDto(9, "{0}/_009_f6_Number_430x134.png", "_009_f6_Number_430x134"),
                    new UserLibraryPropertyManifestDto(10, "{0}/_010_f9_hinh1_280x280.jpg", "_010_f9_hinh1_280x280"),

                    new UserLibraryPropertyManifestDto(11, "{0}/_011_f9_hinh2_280x280.jpg", "_011_f9_hinh2_280x280"),
                    new UserLibraryPropertyManifestDto(12, "{0}/_012_f9_hinh3_280x280.jpg", "_012_f9_hinh3_280x280"),
                    new UserLibraryPropertyManifestDto(13, "{0}/_013_f11_NUMBER_275x145.png", "_013_f11_NUMBER_275x145"),
                    new UserLibraryPropertyManifestDto(14, "{0}/_014_f11_KETQUA1_58x33.png", "_014_f11_KETQUA1_58x33"),
                    new UserLibraryPropertyManifestDto(15, "{0}/_015_f11_KETQUA2_58x33.png", "_015_f11_KETQUA2_58x33"),
                    new UserLibraryPropertyManifestDto(16, "{0}/_016_f11_KETQUA3_58x33.png", "_016_f11_KETQUA3_58x33"),
                    new UserLibraryPropertyManifestDto(17, "{0}/_017_f17_NUMBER_223x137.png", "_017_f17_NUMBER_223x137"),
                    new UserLibraryPropertyManifestDto(18, "{0}/_018_f17_hinh1_235x235.jpg", "_018_f17_hinh1_235x235"),
                    new UserLibraryPropertyManifestDto(19, "{0}/_019_f17_hinh2_235x235.jpg", "_019_f17_hinh2_235x235"),
                    new UserLibraryPropertyManifestDto(20, "{0}/_020_f17_hinh3_235x235.jpg", "_020_f17_hinh3_235x235"),

                    new UserLibraryPropertyManifestDto(21, "{0}/_021_f22_NUMBER_220x160.png", "_021_f22_NUMBER_220x160"),
                    new UserLibraryPropertyManifestDto(22, "{0}/_022_f22_hinh1_408x283.jpg", "_022_f22_hinh1_408x283"),
                    new UserLibraryPropertyManifestDto(23, "{0}/_023_f22_hinh2_283x408.jpg", "_023_f22_hinh2_283x408"),
                    new UserLibraryPropertyManifestDto(24, "{0}/_024_f22_hinh3_408x283.jpg", "_024_f22_hinh3_408x283"),
                    new UserLibraryPropertyManifestDto(25, "{0}/_025_f27_USER_288x288.jpg", "_025_f27_USER_288x288"),
                    new UserLibraryPropertyManifestDto(26, "{0}/_026_f28_NUMBER_218x132.png", "_026_f28_NUMBER_218x132"),
                    new UserLibraryPropertyManifestDto(27, "{0}/_027_f29_hinh1_300x300.jpg", "_027_f29_hinh1_300x300"),
                    new UserLibraryPropertyManifestDto(28, "{0}/_028_f29_hinh2_300x300.jpg", "_028_f29_hinh2_300x300"),
                    new UserLibraryPropertyManifestDto(29, "{0}/_029_f29_hinh3_300x300.jpg", "_029_f29_hinh3_300x300"),
                    new UserLibraryPropertyManifestDto(30, "{0}/_030_f29_hinh4_300x300.jpg", "_030_f29_hinh4_300x300"),
                    new UserLibraryPropertyManifestDto(31, "{0}/_031_f29_hinh5_300x300.jpg", "_031_f29_hinh5_300x300"),

                    new UserLibraryPropertyManifestDto(32, "images/f10_1.png", "f10_1"),
                    new UserLibraryPropertyManifestDto(32, "images/f10_2.png", "f10_2"),
                    new UserLibraryPropertyManifestDto(33, "images/f10_heart1.png", "f10_heart1"),
                    new UserLibraryPropertyManifestDto(34, "images/f10_heart2.png", "f10_heart2"),
                    new UserLibraryPropertyManifestDto(35, "images/f10_heart3.png", "f10_heart3"),
                    new UserLibraryPropertyManifestDto(36, "images/f10_temp.jpg", "f10_temp"),
                    new UserLibraryPropertyManifestDto(37, "images/f11_1.png", "f11_1"),
                    new UserLibraryPropertyManifestDto(38, "images/f11_2.png", "f11_2"),
                    new UserLibraryPropertyManifestDto(39, "images/f11_21.png", "f11_21"),
                    new UserLibraryPropertyManifestDto(40, "images/f11_22.png", "f11_22"),

                    new UserLibraryPropertyManifestDto(41, "images/f11_23.png", "f11_23"),
                    new UserLibraryPropertyManifestDto(42, "images/f11_24.png", "f11_24"),
                    new UserLibraryPropertyManifestDto(43, "images/f11_25.png", "f11_25"),
                    new UserLibraryPropertyManifestDto(44, "images/f11_icon1.png", "f11_icon1"),
                    new UserLibraryPropertyManifestDto(45, "images/f11_icon2.png", "f11_icon2"),
                    new UserLibraryPropertyManifestDto(46, "images/f11_icon3.png", "f11_icon3"),
                    new UserLibraryPropertyManifestDto(47, "images/f11_line1.png", "f11_line1"),
                    new UserLibraryPropertyManifestDto(48, "images/f11_line2.png", "f11_line2"),
                    new UserLibraryPropertyManifestDto(49, "images/f11_temp.jpg", "f11_temp"),
                    new UserLibraryPropertyManifestDto(50, "images/f11_text.png", "f11_text"),

                    new UserLibraryPropertyManifestDto(51, "images/f16_1.png", "f16_1"),
                    new UserLibraryPropertyManifestDto(52, "images/f16_2.png", "f16_2"),
                    new UserLibraryPropertyManifestDto(53, "images/f16_3.png", "f16_3"),
                    new UserLibraryPropertyManifestDto(54, "images/f16_4.png", "f16_4"),
                    new UserLibraryPropertyManifestDto(55, "images/f16_5.png", "f16_5"),
                    new UserLibraryPropertyManifestDto(56, "images/f16_text1.png", "f16_text1"),
                    new UserLibraryPropertyManifestDto(57, "images/f17_1.png", "f17_1"),
                    new UserLibraryPropertyManifestDto(58, "images/f17_2.png", "f17_2"),
                    new UserLibraryPropertyManifestDto(59, "images/f17_gra1.png", "f17_gra1"),
                    new UserLibraryPropertyManifestDto(60, "images/f17_gra2.png", "f17_gra2"),

                    new UserLibraryPropertyManifestDto(61, "images/f18_BG.jpg", "f18_BG"),
                    new UserLibraryPropertyManifestDto(62, "images/f1_1.png", "f1_1"),
                    new UserLibraryPropertyManifestDto(63, "images/f1_2.png", "f1_2"),
                    new UserLibraryPropertyManifestDto(64, "images/f1_rem1.jpg", "f1_rem1"),
                    new UserLibraryPropertyManifestDto(65, "images/f1_rem2.png", "f1_rem2"),
                    new UserLibraryPropertyManifestDto(66, "images/f22_1.png", "f22_1"),
                    new UserLibraryPropertyManifestDto(67, "images/f22_2.png", "f22_2"),
                    new UserLibraryPropertyManifestDto(68, "images/f22_22.png", "f22_22"),
                    new UserLibraryPropertyManifestDto(69, "images/f22_3.png", "f22_3"),
                    new UserLibraryPropertyManifestDto(70, "images/f22_4.png", "f22_4"),

                    new UserLibraryPropertyManifestDto(71, "images/f22_5.png", "f22_5"),
                    new UserLibraryPropertyManifestDto(72, "images/f22_bg_text0.png", "f22_bg_text0"),
                    new UserLibraryPropertyManifestDto(73, "images/f22_bgFrame.png", "f22_bgFrame"),
                    new UserLibraryPropertyManifestDto(74, "images/f22_btn.png", "f22_btn"),
                    new UserLibraryPropertyManifestDto(75, "images/f22_text0.png", "f22_text0"),
                    new UserLibraryPropertyManifestDto(76, "images/f22_text1.png", "f22_text1"),
                    new UserLibraryPropertyManifestDto(77, "images/f22_text2.png", "f22_text2"),
                    new UserLibraryPropertyManifestDto(78, "images/f22_Transition.png", "f22_Transition"),
                    new UserLibraryPropertyManifestDto(79, "images/f22_Transition0.png", "f22_Transition0"),
                    new UserLibraryPropertyManifestDto(80, "images/f27_1.png", "f27_1"),

                    new UserLibraryPropertyManifestDto(81, "images/f27_2.png", "f27_2"),
                    new UserLibraryPropertyManifestDto(82, "images/f27_USER.jpg", "f27_USER"),
                    new UserLibraryPropertyManifestDto(83, "images/f28_1.png", "f28_1"),
                    new UserLibraryPropertyManifestDto(84, "images/f28_2.png", "f28_2"),
                    new UserLibraryPropertyManifestDto(85, "images/f28_3.png", "f28_3"),
                    new UserLibraryPropertyManifestDto(86, "images/f28_4.png", "f28_4"),
                    new UserLibraryPropertyManifestDto(87, "images/f28_5.png", "f28_5"),
                    new UserLibraryPropertyManifestDto(88, "images/f28_text.png", "f28_text"),
                    new UserLibraryPropertyManifestDto(89, "images/f29_1.png", "f29_1"),
                    new UserLibraryPropertyManifestDto(90, "images/f29_2.png", "f29_2"),

                    new UserLibraryPropertyManifestDto(91, "images/f29_3.png", "f29_3"),
                    new UserLibraryPropertyManifestDto(92, "images/f29_text.png", "f29_text"),
                    new UserLibraryPropertyManifestDto(93, "images/f2_1.png", "f2_1"),
                    new UserLibraryPropertyManifestDto(94, "images/f2_2.png", "f2_2"),
                    new UserLibraryPropertyManifestDto(95, "images/f2_3.png", "f2_3"),
                    new UserLibraryPropertyManifestDto(96, "images/f2_41.png", "f2_41"),
                    new UserLibraryPropertyManifestDto(97, "images/f2_51.png", "f2_51"),
                    new UserLibraryPropertyManifestDto(98, "images/f30_1.png", "f30_1"),
                    new UserLibraryPropertyManifestDto(99, "images/f30_2.png", "f30_2"),
                    new UserLibraryPropertyManifestDto(100, "images/f30_3.png", "f30_3"),

                    new UserLibraryPropertyManifestDto(101, "images/f30_text1.png", "f30_text1"),
                    new UserLibraryPropertyManifestDto(102, "images/f30_text2.png", "f30_text2"),
                    new UserLibraryPropertyManifestDto(103, "images/f32_1.png", "f32_1"),
                    new UserLibraryPropertyManifestDto(104, "images/f32_2.png", "f32_2"),
                    new UserLibraryPropertyManifestDto(105, "images/f32_3.png", "f32_3"),
                    new UserLibraryPropertyManifestDto(106, "images/f32_4.png", "f32_4"),
                    new UserLibraryPropertyManifestDto(107, "images/f32_5.png", "f32_5"),
                    new UserLibraryPropertyManifestDto(108, "images/f32_6.png", "f32_6"),
                    new UserLibraryPropertyManifestDto(109, "images/f32_temp.jpg", "f32_temp"),
                    new UserLibraryPropertyManifestDto(110, "images/f32_text.png", "f32_text"),

                    new UserLibraryPropertyManifestDto(111, "images/f33.png", "f33"),
                    new UserLibraryPropertyManifestDto(112, "images/f3_face1.png", "f3_face1"),
                    new UserLibraryPropertyManifestDto(113, "images/f3_face2.png", "f3_face2"),
                    new UserLibraryPropertyManifestDto(114, "images/f3_face3.png", "f3_face3"),
                    new UserLibraryPropertyManifestDto(115, "images/f3_face4.png", "f3_face4"),
                    new UserLibraryPropertyManifestDto(116, "images/f3_face5.png", "f3_face5"),
                    new UserLibraryPropertyManifestDto(117, "images/f3_face6.png", "f3_face6"),
                    new UserLibraryPropertyManifestDto(118, "images/f3_face7.png", "f3_face7"),
                    new UserLibraryPropertyManifestDto(119, "images/f3_face8.png", "f3_face8"),
                    new UserLibraryPropertyManifestDto(120, "images/f3_face9.png", "f3_face9"),

                    new UserLibraryPropertyManifestDto(121, "images/f3_tia.png", "f3_tia"),
                    new UserLibraryPropertyManifestDto(122, "images/f4_banh1.png", "f4_banh1"),
                    new UserLibraryPropertyManifestDto(123, "images/f4_banh2.png", "f4_banh2"),
                    new UserLibraryPropertyManifestDto(124, "images/f4_Xanh.png", "f4_Xanh"),
                    new UserLibraryPropertyManifestDto(125, "images/f5_BG.png", "f5_BG"),
                    new UserLibraryPropertyManifestDto(126, "images/f5_item1.png", "f5_item1"),
                    new UserLibraryPropertyManifestDto(127, "images/f5_item2.png", "f5_item2"),
                    new UserLibraryPropertyManifestDto(128, "images/f5_item3.png", "f5_item3"),
                    new UserLibraryPropertyManifestDto(129, "images/f5_item4.png", "f5_item4"),
                    new UserLibraryPropertyManifestDto(130, "images/f5_item5.png", "f5_item5"),

                    new UserLibraryPropertyManifestDto(131, "images/f5_item6.png", "f5_item6"),
                    new UserLibraryPropertyManifestDto(132, "images/f5_Temp.png", "f5_Temp"),
                    new UserLibraryPropertyManifestDto(133, "images/f5_text.png", "f5_text"),
                    new UserLibraryPropertyManifestDto(134, "images/f6_1.png", "f6_1"),
                    new UserLibraryPropertyManifestDto(135, "images/f6_2.png", "f6_2"),
                    new UserLibraryPropertyManifestDto(136, "images/f6_BANH.png", "f6_BANH"),
                    new UserLibraryPropertyManifestDto(137, "images/f6_bg.png", "f6_bg"),
                    new UserLibraryPropertyManifestDto(138, "images/f6_item1.png", "f6_item1"),
                    new UserLibraryPropertyManifestDto(139, "images/f6_temp.jpg", "f6_temp"),
                    new UserLibraryPropertyManifestDto(140, "images/f9_1.png", "f9_1"),

                    new UserLibraryPropertyManifestDto(141, "images/f9_2.png", "f9_2"),
                    new UserLibraryPropertyManifestDto(142, "images/f9_3.png", "f9_3"),
                    new UserLibraryPropertyManifestDto(143, "images/f9_4.png", "f9_4"),
                    new UserLibraryPropertyManifestDto(144, "images/f9_5.png", "f9_5"),
                    new UserLibraryPropertyManifestDto(145, "images/f9_temp.jpg", "f9_temp"),
                    new UserLibraryPropertyManifestDto(146, "images/test.mp3", "test")
                }
            };
        }
    }

    public class UserLibraryPropertyManifestDto
    {
        public UserLibraryPropertyManifestDto()
        {
            
        }

        public UserLibraryPropertyManifestDto(int itemOrder, string source, string id)
        {
            ItemOrder = itemOrder;
            Source = source;
            Id = id;
        }

        [JsonIgnore]
        public int ItemOrder { get; set; }

        [JsonProperty("src")]
        public string Source { get; set; }

        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
