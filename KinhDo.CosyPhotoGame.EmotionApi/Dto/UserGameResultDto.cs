﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public enum GameResultStatus
    {
        New,
        ResultOk,
        ResultCancel,
        ResultNotShare,
        Error
    }

    public class GameResultStatusDto
    {
        public string Status { get; set; }

        public int PercentComplete { get; set; }

        public string ErrorMessage { get; set; }

        public static GameResultStatusDto ErrorWithMessage(string message)
        {
            return new GameResultStatusDto
            {
                Status = GameResultStatus.Error.ToString(),
                ErrorMessage = message
            };
        }
    }

    /// <summary>
    /// Represent overall result for one game play of a user
    /// </summary>
    public class UserGameResultDto
    {
        public UserGameResultDto()
        {
            PlayerInfo = new FacebookProfileViewModel();
            GameResultFrames = new List<GameResultFrameDto>();
            Data = new UserResultElement();
        }

        public int Id { get; set; }

        public int PlayerId { get; set; }

        /// <summary>
        /// Id of long running job to generate game result
        /// </summary>
        public string JobId { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientBrowser { get; set; }

        public DateTime ResultDate { get; set; }

        public string Status { get; set; }

        public string AccessToken { get; set; }

        public string ErrorMessage { get; set; }

        public FacebookProfileViewModel PlayerInfo { get; set; }

        public IList<GameResultFrameDto> GameResultFrames { get; set; }

        public UserResultElement Data { get; set; }

        public int TotalSmileInMonth { get; set; }

        public int TotalGroupSmileInMonth { get; set; }

        public int TotalReaction { get; set; }

        public int TotalLike { get; set; }

        public int TotalLove { get; set; }

        public int TotalHaha { get; set; }

        public int TotalCheckedInLocation { get; set; }

        public int TotalTaggedPosts { get; set; }

        public int TotalGameScore { get; set; }
    }

    [XmlRoot(ElementName = "smiles")]
    public class Smiles
    {
        public Smiles()
        {
            Image = new List<string>();
            Width = 500;
            Height = 300;
        }

        [XmlElement(ElementName = "image")]
        public List<string> Image { get; set; }

        [XmlAttribute(AttributeName = "width")]
        public int Width { get; set; }

        [XmlAttribute(AttributeName = "height")]
        public int Height { get; set; }

        [XmlAttribute(AttributeName = "content")]
        public string Content { get; set; }
    }

    [XmlRoot(ElementName = "groupsmiles")]
    public class Groupsmiles
    {
        public Groupsmiles()
        {
            Image=new List<string>();
            Width = 500;
            Height = 300;
        }

        [XmlElement(ElementName = "image")]
        public List<string> Image { get; set; }

        [XmlAttribute(AttributeName = "width")]
        public int Width { get; set; }

        [XmlAttribute(AttributeName = "height")]
        public int Height { get; set; }

        [XmlAttribute(AttributeName = "content")]
        public string Content { get; set; }
    }

    [XmlRoot(ElementName = "location")]
    public class LocationResultElement
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }

        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }

        [XmlAttribute(AttributeName = "long")]
        public string Long { get; set; }

        [XmlAttribute(AttributeName = "image")]
        public string Image { get; set; }

        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "locations")]
    public class Locations
    {
        public Locations()
        {
            Location = new List<LocationResultElement>();
            Width = 700;
            Height = 500;
        }

        [XmlElement(ElementName = "location")]
        public List<LocationResultElement> Location { get; set; }

        [XmlAttribute(AttributeName = "width")]
        public int Width { get; set; }

        [XmlAttribute(AttributeName = "height")]
        public int Height { get; set; }

        [XmlAttribute(AttributeName = "content")]
        public string Content { get; set; }
    }

    [XmlRoot(ElementName = "taggedPosts")]
    public class TaggedPosts
    {
        public TaggedPosts()
        {
            Image = new List<string>();
        }

        [XmlElement(ElementName = "image")]
        public List<string> Image { get; set; }
    }

    [XmlRoot(ElementName = "user")]
    public class UserResultElement
    {
        public UserResultElement()
        {
            Smiles= new Smiles();
            Groupsmiles= new Groupsmiles();
            Locations= new Locations();
            TaggedPosts= new TaggedPosts();
        }

        [XmlElement(ElementName = "profileId")]
        public string ProfileId { get; set; }

        [XmlElement(ElementName = "fullName")]
        public string FullName { get; set; }

        [XmlElement(ElementName = "totalSmile")]
        public int TotalSmile { get; set; }

        [XmlElement(ElementName = "totalGroupSmile")]
        public int TotalGroupSmile { get; set; }

        [XmlElement(ElementName = "totalLike")]
        public int TotalLike { get; set; }

        [XmlElement(ElementName = "totalLove")]
        public int TotalLove { get; set; }

        [XmlElement(ElementName = "totalHaha")]
        public int TotalHaha { get; set; }

        [XmlElement(ElementName = "totalLocations")]
        public int TotalLocations { get; set; }

        [XmlElement(ElementName = "totalTaggedPosts")]
        public int TotalTaggedPosts { get; set; }

        [XmlElement(ElementName = "totalCount")]
        public int TotalCount { get; set; }

        [XmlElement(ElementName = "smiles")]
        public Smiles Smiles { get; set; }

        [XmlElement(ElementName = "groupsmiles")]
        public Groupsmiles Groupsmiles { get; set; }

        [XmlElement(ElementName = "locations")]
        public Locations Locations { get; set; }

        [XmlElement(ElementName = "taggedPosts")]
        public TaggedPosts TaggedPosts { get; set; }
    }
}