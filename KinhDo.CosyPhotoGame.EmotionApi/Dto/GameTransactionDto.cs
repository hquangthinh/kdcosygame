﻿using System;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class GameTransactionDto
    {
        public int Id { get; set; }

        public int PlayerDbId { get; set; }

        public string ClientIpAddress { get; set; }

        public string ClientBrowser{ get; set; }

        public DateTime? ResultDate { get; set; }

        public int? PercentComplete { get; set; }

        public string Status { get; set; }

        public string AccessToken { get; set; }
    }
}
