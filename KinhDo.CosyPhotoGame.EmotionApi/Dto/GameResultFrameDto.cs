﻿using System;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    /// <summary>
    /// Represent information for one specific game result such as total smiling photos, total likes,...
    /// </summary>
    public class GameResultFrameDto
    {
        public int Id { get; set; }
        public int GameResultId { get; set; }
        public int? ItemOrder { get; set; }
        public string Title { get; set; }
        public decimal Score { get; set; }
        public decimal? SubScore1 { get; set; }
        public decimal? SubScore2 { get; set; }
        public decimal? SubScore3 { get; set; }
        public string PhotoUrl1 { get; set; }
        public string PhotoUrl2 { get; set; }
        public string PhotoUrl3 { get; set; }
        public string PhotoUrl4 { get; set; }
        public string PhotoUrl5 { get; set; }

        public static Func<GameResultDetail, GameResultFrameDto> Mapper = entity =>
        {
            if (entity == null) return new GameResultFrameDto();
            return new GameResultFrameDto
            {
                Id = entity.Id,
                GameResultId = entity.PlayerGameResultId,
                ItemOrder = entity.ItemOrder,
                Score = entity.Score,
                SubScore1 = entity.SubScore1,
                SubScore2 = entity.SubScore2,
                SubScore3 = entity.SubScore3,
                PhotoUrl1 = entity.PhotoUrl1,
                PhotoUrl2 = entity.PhotoUrl2,
                PhotoUrl3 = entity.PhotoUrl3,
                PhotoUrl4 = entity.PhotoUrl4,
                PhotoUrl5 = entity.PhotoUrl5
            };
        };
    }
}