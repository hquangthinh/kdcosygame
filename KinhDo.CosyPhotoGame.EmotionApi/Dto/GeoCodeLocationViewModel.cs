﻿using System.ComponentModel.DataAnnotations;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class GeoCodeLocationViewModel
    {
        [Display(Name = "Center Search on Location")]
        public string Name { get; set; }

        [Display(Name = "Coordinates")]
        public string Coordinates { get; set; }

    }
}
