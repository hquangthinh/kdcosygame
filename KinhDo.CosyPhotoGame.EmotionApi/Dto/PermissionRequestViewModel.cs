﻿using System.Collections.Generic;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class PermissionRequestViewModel
    {
        public PermissionRequestViewModel()
        {
            MissingPermissions = new List<FacebookPermissionRequest>();
        }

        public List<FacebookPermissionRequest> MissingPermissions { get; set; }

        public string redirectURI { get; set; }
    }
}