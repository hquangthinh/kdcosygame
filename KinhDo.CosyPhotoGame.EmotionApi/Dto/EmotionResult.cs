﻿namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class EmotionResult
    {
        public int ResponseStatus { get; set; }

        public string ErrorMessage { get; set; }

        public int TotalFacesCount { get; set; }

        public decimal Anger { get; set; }

        public decimal Contempt { get; set; }

        public decimal Disgust { get; set; }

        public decimal Fear { get; set; }

        public decimal Happiness { get; set; }

        public decimal Neutral { get; set; }

        public decimal Sadness { get; set; }

        public decimal Surprise { get; set; }
    }
}