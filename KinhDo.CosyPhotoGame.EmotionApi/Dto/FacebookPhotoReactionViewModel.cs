﻿using System.Collections.Generic;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookPhotoReactionItemViewModel
    {
        // Fb mapping properties

        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        /// <summary>
        /// Photo reaction enum {NONE, LIKE, LOVE, WOW, HAHA, SAD, ANGRY, THANKFUL}
        /// </summary>
        [FacebookMapping("type")]
        [JsonProperty("type")]
        public string Type { get; set; }
    }

    public class FacebookPhotoReactionSummaryViewModel
    {
        [FacebookMapping("total_count")]
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }

        [FacebookMapping("viewer_reaction")]
        [JsonProperty("viewer_reaction")]
        public string ViewerReaction { get; set; }
    }

    public class FacebookPhotoReactionViewModel
    {
        public string FacebookPhotoId { get; set; }

        public int DbPhotoId { get; set; }

        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<FacebookPhotoReactionItemViewModel> Data { get; set; }

        [FacebookMapping("paging")]
        [JsonProperty("paging")]
        public FacebookPagingViewModel Paging { get; set; }

        [FacebookMapping("summary")]
        [JsonProperty("summary")]
        public FacebookPhotoReactionSummaryViewModel Summary { get; set; }
    }
}