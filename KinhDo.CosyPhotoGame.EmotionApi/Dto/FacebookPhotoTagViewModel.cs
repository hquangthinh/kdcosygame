﻿using System.Collections.Generic;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookPhotoTagItemViewModel
    {
        // Tagged user fb id
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        // Tagged user full name
        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [FacebookMapping("created_time")]
        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }

        [FacebookMapping("x")]
        [JsonProperty("x")]
        public double X { get; set; }

        [FacebookMapping("y")]
        [JsonProperty("y")]
        public double Y { get; set; }
    }

    public class FacebookPhotoTagViewModel
    {
        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<FacebookPhotoTagItemViewModel> Data { get; set; }

        [FacebookMapping("paging")]
        [JsonProperty("paging")]
        public FacebookPagingViewModel Paging { get; set; }
    }
}