﻿using System.IO;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class GameSettingDto
    {
        /// <summary>
        /// Number of days in the past to get Fb photos
        /// </summary>
        public int GetPhotoBackedDateDays { get; set; }

        /// <summary>
        /// Duration in hour to cache user data get from Facebook. Default to 1
        /// </summary>
        public int FbDataCacheDurationInHours { get; set; }

        /// <summary>
        /// Full path to the folder where player data is stored
        /// </summary>
        public string PlayerGameDataBaseFolder { get; set; }

        /// <summary>
        /// Full path to the folder where game app is installed
        /// </summary>
        public string GameBaseInstallationFolder { get; set; }

        /// <summary>
        /// Full path to folder where player library property files are stored
        /// </summary>
        public string PlayerDataPathToLibraryProperty => Path.Combine(GameBaseInstallationFolder, @"Content\user_data");

        /// <summary>
        /// Full path to folder where player game images are stored
        /// </summary>
        public string PlayerDataPathToGameImages => Path.Combine(GameBaseInstallationFolder, "Game");
    }
}
