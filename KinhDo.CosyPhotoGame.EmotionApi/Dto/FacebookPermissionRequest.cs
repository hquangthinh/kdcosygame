namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookPermissionRequest
    {
        public bool requested { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public string permision_scope_value { get; set; }

        public bool granted { get; set; }
    }
}