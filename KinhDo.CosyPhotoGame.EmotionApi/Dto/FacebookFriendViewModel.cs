﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class Summary
    {
        [FacebookMapping("total_count")]
        [JsonProperty("total_count")]
        public int TotalCount { get; set; }
    }

    public class FacebookFriendListViewModel
    {
        public string PlayerFbProfileId { get; set; }

        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<FacebookFriendViewModel> Data { get; set; }

        [FacebookMapping("paging")]
        [JsonProperty("paging")]
        public FacebookPagingViewModel Paging { get; set; }

        [FacebookMapping("summary")]
        [JsonProperty("summary")]
        public Summary Summary { get; set; }
    }

    public class FacebookFriendViewModel
    {
        [Required]
        [FacebookMapping("id")]
        public string TaggingId { get; set; }

        [Required]
        [Display(Name = "Friend's Name")]
        [FacebookMapping("name")]
        public string Name { get; set; }

        [FacebookMapping("url", parent = "picture")]
        public string ImageURL { get; set; }
    }
}