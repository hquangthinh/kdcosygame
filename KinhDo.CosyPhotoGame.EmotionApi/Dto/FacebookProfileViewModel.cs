﻿using System;
using System.ComponentModel.DataAnnotations;
using KinhDo.CosyAppAdmin.Model;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookProfileViewModel
    {
        public int PlayerId { get; set; }

        public string UserName { get; set; }

        [Required]
        [FacebookMapping("id")]
        public string Id { get; set; }

        [Required]
        [Display(Name = "First Name")]
        [FacebookMapping("first_name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name = "Last Name")]
        [FacebookMapping("last_name")]
        public string LastName { get; set; }

        [FacebookMapping("name")]
        public string Fullname { get; set; }

        public string ImageURL { get; set; }

        [FacebookMapping("link")]
        public string LinkURL { get; set; }

        [FacebookMapping("locale")]
        public string Locale { get; set; }

        [FacebookMapping("email")]
        public string Email { get; set; }

        [FacebookMapping("birthday")]
        public DateTime Birthdate { get; set; }

        [FacebookMapping("name", parent = "location")]
        public string Location { get; set; }

        [FacebookMapping("gender")]
        public string Gender { get; set; }

        [FacebookMapping("age_range")]
        public Facebook.JsonObject AgeRange { get; set; }

        [FacebookMapping("bio")]
        public string Bio { get; set; }

        public static Func<Player, FacebookProfileViewModel> Mapper = entity =>
        {
            if(entity == null)
                return new FacebookProfileViewModel();
            return new FacebookProfileViewModel
            {
                UserName = entity.UserName,
                Email = entity.Email,
                Id = entity.ProfileId,
                PlayerId = entity.Id,
                Fullname = $"{entity.FirstName} {entity.LastName}"
            };
        };

    }
}
