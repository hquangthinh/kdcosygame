﻿using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookPagingViewModel
    {
        [FacebookMapping("cursors")]
        [JsonProperty("cursors")]
        public FacebookPagingCursorViewModel Cursors { get; set; }

        [FacebookMapping("next")]
        [JsonProperty("next")]
        public string Next { get; set; }

        [FacebookMapping("previous")]
        [JsonProperty("previous")]
        public string Previous { get; set; }
    }

    public class FacebookPagingCursorViewModel
    {
        [FacebookMapping("before")]
        [JsonProperty("before")]
        public string Before { get; set; }

        [FacebookMapping("after")]
        [JsonProperty("after")]
        public string After { get; set; }
    }
}