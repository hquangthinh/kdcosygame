﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookUserPostViewModel
    {
        [Required]
        [FacebookMapping("id")]
        public string Id { get; set; }

        [FacebookMapping("created_time")]
        public DateTime Created_Time { get; set; }

        [FacebookMapping("id", parent = "from")]
        public string From_Id { get; set; }

        [FacebookMapping("name", parent = "from")]
        public string From_Name { get; set; }

        [FacebookMapping("url", parent = "from")]
        public string From_Picture_Url { get; set; }

        [FacebookMapping("story")]
        public string Story { get; set; }

        [FacebookMapping("message")]
        public string Message { get; set; }

        [FacebookMapping("picture")]
        public string Picture_Url { get; set; }

        [FacebookMapping("link")]
        public string Link { get; set; }

        [FacebookMapping("description")]
        public string Description { get; set; }

        [FacebookMapping("caption")]
        public string Caption { get; set; }

        [FacebookMapping("type")]
        public string Type { get; set; }

        [FacebookMapping("likes")]
        public dynamic Likes { get; set; }

        [FacebookMapping("comments")]
        public dynamic Comments { get; set; }
    }

    public class ItemViewModel
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }
    }

    public class WithTagItemViewModel : ItemViewModel
    {
    }

    public class FacebookPostWithTagViewModel
    {
        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<WithTagItemViewModel> Data { get; set; }
    }

    public class FacebookPostFromViewModel : ItemViewModel
    {
    }

    public class FacebookPostItemViewModel
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("created_time")]
        [JsonProperty("created_time")]
        public string CreatedTime { get; set; }

        [FacebookMapping("type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [FacebookMapping("message")]
        [JsonProperty("message")]
        public string Message { get; set; }

        [FacebookMapping("story")]
        [JsonProperty("story")]
        public string Story { get; set; }

        [FacebookMapping("link")]
        [JsonProperty("link")]
        public string Link { get; set; }

        [FacebookMapping("with_tags")]
        [JsonProperty("with_tags")]
        public FacebookPostWithTagViewModel WithTags { get; set; }

        [FacebookMapping("from")]
        [JsonProperty("from")]
        public FacebookPostFromViewModel From { get; set; }

        [FacebookMapping("place")]
        [JsonProperty("place")]
        public Place Place { get; set; }
    }

    public class FacebookPostViewModel
    {
        public string PlayerFbProfileId { get; set; }

        [FacebookMapping("data")]
        [JsonProperty("data")]
        public List<FacebookPostItemViewModel> Data { get; set; }

        [FacebookMapping("paging")]
        [JsonProperty("paging")]
        public FacebookPagingViewModel Paging { get; set; }
    }
}