﻿using System.ComponentModel.DataAnnotations;
using System;
using KinhDo.CosyPhotoGame.EmotionApi.Attributes;
using Newtonsoft.Json;

namespace KinhDo.CosyPhotoGame.EmotionApi.Dto
{
    public class FacebookPhotoViewModel
    {
        // Db mapping properties

        public int PlayerPhotoId { get; set; }

        public int? PlaceId { get; set; }

        public int? AlbumId { get; set; }

        public int? Height { get; set; }

        public int? Width { get; set; }

        public string FacebookPlaceId { get; set; }

        public string PlaceName { get; set; }

        public string LocationName { get; set; }

        public decimal? Latitude { get; set; }

        public decimal? Longitude { get; set; }

        public string Street { get; set; }

        public string City { get; set; }

        public string State { get; set; }

        public string Region { get; set; }

        public string Country { get; set; }

        public string Zip { get; set; }

        public string Source { get; set; }

        public string AlbumName { get; set; }

        // Reaction data

        public int? TotalReactionCount { get; set; }

        public int? TotalReactionLikeCount { get; set; }

        public int? TotalReactionLoveCount { get; set; }

        public int? TotalReactionWowCount { get; set; }

        public int? TotalReactionHahaCount { get; set; }

        public int? TotalReactionSadCount { get; set; }

        public int? TotalReactionAngryCount { get; set; }

        public int? TotalReactionThankfulCount { get; set; }

        public int? TotalReactionNoneCount { get; set; }

        // Location, Tag & Feed related data

        // Number of person tagged in a photo
        public int? TotalProfileTaggedInPhotoCount { get; set; }

        // Count of total places user is tagged
        public int? TotalUserTaggedPlaceCount { get; set; }

        // Count post that the user is being tagged
        public int? TotalPostUserIsTaggedCount { get; set; }

        // Total faces detected by CV api
        public int? TotalFacesCount { get; set; }

        // Emotional scores

        public decimal? Anger { get; set; }

        public decimal? Contempt { get; set; }

        public decimal? Disgust { get; set; }

        public decimal? Fear { get; set; }

        public decimal? Happiness { get; set; }

        public decimal? Neutral { get; set; }

        public decimal? Sadness { get; set; }

        public decimal? Surprise { get; set; }

        // Fb mapping properties

        [Required]
        [FacebookMapping("id")]
        public string Id { get; set; }

        [Display(Name = "Name")]
        [FacebookMapping("name")]
        public string Name { get; set; }

        public string ShortName => !string.IsNullOrEmpty(Name) && Name.Length > 255 ? Name.Substring(0, 255) : Name; 

        [Display(Name = "Link")]
        [FacebookMapping("link")]
        public string Link { get; set; }

        [Display(Name = "Name Tags")]
        [FacebookMapping("name_tags")]
        public Facebook.JsonArray NameTags { get; set; }

        // Same as NameTags but is in csv string format
        public string NameTagsString { get; set; }

        [FacebookMapping("picture")]
        public string SmallPicture { get; set; }

        [FacebookMapping("created_time")]
        public DateTime CreatedTime { get; set; }

        [FacebookMapping("backdated_time")]
        public DateTime BackdatedTime { get; set; }

        [FacebookMapping("updated_time")]
        public DateTime UpdatedTime { get; set; }

        [FacebookMapping("images")]
        public Facebook.JsonArray Images { get; set; }

        [FacebookMapping("place")]
        public Facebook.JsonObject Place { get; set; }

        [FacebookMapping("reactions")]
        public Facebook.JsonObject Reactions { get; set; }

        [FacebookMapping("tags")]
        public Facebook.JsonObject Tags { get; set; }
    }

    public class FacebookPhotoImageViewModel
    {
        // Db mapping properties

        public int Id { get; set; }

        public int PhotoId { get; set; }

        public string FacebookPhotoId { get; set; }

        // Fb mapping properties

        [FacebookMapping("height")]
        [JsonProperty("height")]
        public int Height { get; set; }

        [FacebookMapping("width")]
        [JsonProperty("width")]
        public int Width { get; set; }

        [FacebookMapping("source")]
        [JsonProperty("source")]
        public string Source { get; set; }
    }

    public class FacebookPlaceViewModel
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [FacebookMapping("location")]
        [JsonProperty("location")]
        public FacebookLocationViewModel Location { get; set; } 
    }

    public class FacebookLocationViewModel
    {
        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [FacebookMapping("street")]
        [JsonProperty("street")]
        public string Street { get; set; }

        [FacebookMapping("city")]
        [JsonProperty("city")]
        public string City { get; set; }

        [FacebookMapping("state")]
        [JsonProperty("state")]
        public string State { get; set; }

        [FacebookMapping("region")]
        [JsonProperty("region")]
        public string Region { get; set; }

        [FacebookMapping("country")]
        [JsonProperty("country")]
        public string Country { get; set; }

        [FacebookMapping("zip")]
        [JsonProperty("zip")]
        public string Zip { get; set; }

        [FacebookMapping("latitude")]
        [JsonProperty("latitude")]
        public float Latitude { get; set; }

        [FacebookMapping("longitude")]
        [JsonProperty("longitude")]
        public float Longitude { get; set; }
    }

    public class NameTagViewModel
    {
        [FacebookMapping("id")]
        [JsonProperty("id")]
        public string Id { get; set; }

        [FacebookMapping("length")]
        [JsonProperty("length")]
        public int Length { get; set; }

        [FacebookMapping("name")]
        [JsonProperty("name")]
        public string Name { get; set; }

        [FacebookMapping("type")]
        [JsonProperty("type")]
        public string Type { get; set; }

        [FacebookMapping("offset")]
        [JsonProperty("offset")]
        public int Offset { get; set; }
    }
}