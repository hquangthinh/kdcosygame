﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using Facebook;
using KinhDo.CosyPhotoGame.EmotionApi.Messaging;
using log4net;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public class GameResultNotificationCommand
    {
        public int GameId { get; set; }

        public string FbProfileId { get; set; }

        public string UserFullName { get; set; }

        public string Email { get; set; }

        public string AccessToken { get; set; }

        public string PhoneNumber { get; set; }

        public string WebGameUrl { get; set; }

        public string EmailTemplatePath { get; set; }

        public string GameResultRelativePath => $"Game/Result?gameTransactionId={GameId}&UserProfileId={FbProfileId}";
    }

    /// <summary>
    /// Send notification to users when game result is ready
    /// </summary>
    public class CosyGameNotificationService
    {
        private static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);

        public async Task SendGameResultNotificationReady(GameResultNotificationCommand command)
        {
            //SendEmailGameResultReady(command);
            await SendFacebookNotificationGameResultReady(command);
        }

        private void SendEmailGameResultReady(GameResultNotificationCommand command)
        {
            try
            {
                var videoResultUrl = $"{command.WebGameUrl.TrimEnd('/')}/{command.GameResultRelativePath}";
                var templateContent = File.ReadAllText(Path.Combine(command.EmailTemplatePath, "game_result_ready.html"));
                var smtpMessageChannel = new SmtpMessageChannel(new WebConfigSmtpSettings()); 
                var message = new Message
                {
                    Recipients = command.Email,
                    Subject = "Cosy đếm điều ngọt ngào từ bạn - video của bạn đã sẵn sàng",
                    Body = string.Format(templateContent, command.UserFullName, videoResultUrl)
                };
                smtpMessageChannel.Process(message);
            }
            catch (Exception ex)
            {
                Log.Error("Error SendEmailGameResultReady -> ", ex);
            }
        }

        private async Task SendFacebookNotificationGameResultReady(GameResultNotificationCommand command)
        {
            try
            {
                var appId = "1799051727048491";
                var appSecret = "978b818041944af19f410e94bcfc6e56";
                var template = WebUtility.UrlEncode("Cosy đếm điều ngọt ngào từ bạn - video của bạn đã sẵn sàng");
                var href = WebUtility.UrlEncode($"Game/Result?gameTransactionId={command.GameId}&UserProfileId={command.FbProfileId}");

                var fbClient = new FacebookClient { Version = "v2.7" };

                dynamic appAccessTokenResult = fbClient.Post("oauth/access_token",
                    new
                    {
                        client_id = appId,
                        client_secret = appSecret,
                        grant_type = "client_credentials"
                    });

                var appAccessToken = appAccessTokenResult.access_token;

                var apiPath = $"{command.FbProfileId}/notifications?access_token={appAccessToken}&template={template}&href={href}";

                await fbClient.PostTaskAsync(apiPath, null);
            }
            catch (Exception ex)
            {
                Log.Error("Error SendFacebookNotificationGameResultReady -> ", ex);
            }
        }
    }
}