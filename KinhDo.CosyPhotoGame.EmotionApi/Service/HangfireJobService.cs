﻿using System;
using System.Linq;
using Hangfire;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public class HangfireJobService
    {
        public JobServerStatusDto GetJobServers(int from, int perPage)
        {
            var serverStatus = new JobServerStatusDto();
            var monitor = JobStorage.Current.GetMonitoringApi();
            var servers = monitor.Servers();
            if (servers == null || servers.Count == 0)
                return serverStatus;

            serverStatus.ServerList.AddRange(servers.Select(server => new JobServerInfoDto
            {
                Name = server.Name,
                StartedAt = server.StartedAt.ToLocalTime(),
                Heartbeat = server.Heartbeat?.ToLocalTime() ?? null
            }));

            serverStatus.IsActiveServerAlive =
                    serverStatus.ServerList.All(s => s.Heartbeat.HasValue && s.Heartbeat.Value.AddMinutes(1) >= DateTime.Now);

            return serverStatus;
        }
    }
}
