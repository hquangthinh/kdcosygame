﻿using System.Linq;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public class GameSettingService : ServiceBase
    {
        public GameSettingDto GetGameGlobalSettings()
        {
            var settingEntities = DbContext.Settings.Where(s => s.SettingGroup == "Global").ToList();
            var result = new GameSettingDto
            {
                GetPhotoBackedDateDays = settingEntities.First(s=>s.SettingKey == "GetPhotoBackedDateDays").SettingValueNumber ?? 60,
                FbDataCacheDurationInHours = settingEntities.First(s => s.SettingKey == "FbDataCacheDurationInHours").SettingValueNumber ?? 1,
                PlayerGameDataBaseFolder = settingEntities.First(s => s.SettingKey == "PlayerGameDataBaseFolder").SettingValueString,
                GameBaseInstallationFolder = settingEntities.First(s => s.SettingKey == "GameBaseInstallationFolder").SettingValueString
            };
            return result;
        }
    }
}