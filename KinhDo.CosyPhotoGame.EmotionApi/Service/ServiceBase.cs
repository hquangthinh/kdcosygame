﻿using System;
using System.Data.Entity.Core;
using System.Data.Entity.Validation;
using System.Reflection;
using KinhDo.CosyAppAdmin.EntityFramework;
using log4net;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public abstract class ServiceBaseWithLog
    {
        protected static readonly ILog Log = LogManager.GetLogger(MethodBase.GetCurrentMethod().DeclaringType);
    }

    public abstract class ServiceBase : ServiceBaseWithLog
    {
        private readonly CosyDb _dbContext = new CosyDb();

        protected CosyDb DbContext => _dbContext ?? new CosyDb();

        protected void SaveChanges(string objectName = "N/A", string objectKey = "N/A")
        {
            var msg = $"Updated {objectName} {objectKey}";
            Log.Info(msg);

            try
            {
                DbContext.SaveChanges();
            }
            catch (DbEntityValidationException dbe)
            {
                foreach (var eve in dbe.EntityValidationErrors)
                {
                    Log.ErrorFormat(
                        "Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                        eve.Entry.Entity.GetType().Name, eve.Entry.State
                    );
                    foreach (var ve in eve.ValidationErrors)
                        Log.ErrorFormat("- Property: \"{0}\", Error: \"{1}\"", ve.PropertyName, ve.ErrorMessage);
                }

                throw new ApplicationException(msg, dbe);
            }
            catch (EntityCommandExecutionException ecx)
            {
                var message = ecx.InnerException?.Message;
                Log.Error($"{msg} failed.\n {message}", ecx);

                throw new ApplicationException(message, ecx);
            }
            catch (Exception ex)
            {
                Log.Error($"Save changes for {objectName} errors -> ", ex);
                throw new ApplicationException(ex.Message, ex);
            }
        }
    }
}