﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Facebook;
using KinhDo.CosyAppAdmin.Model;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.Extentions;
using Newtonsoft.Json;


namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public interface IPlayerService
    {
        int GetSettingForGetPhotoBackedDateDays();
        FacebookProfileViewModel AddOrUpdatePlayerInfo(FacebookProfileViewModel playerProfile, UserDetailDto currentUser);
        void AddOrUpdatePlayerAlbums(FacebookProfileViewModel playerProfile, List<FacebookAlbumViewModel> playerAlbums);
        void AddOrUpdatePlayerPhotos(FacebookProfileViewModel playerProfile, List<FacebookPhotoViewModel> playerPhotos);
        void UpdatePlayerPhotoByFbPhotoId(FacebookPhotoViewModel photoViewModel);
        void AddUserTaggedLocations(FacebookUserTaggedPlaceViewModel userTaggedLocationViewModel);
        void AddUserFeed(FacebookPostViewModel postViewModel);
        FacebookProfileViewModel GetPlayerProfileByDbId(int playerDbId);
        FacebookProfileViewModel GetPlayerProfileByFbId(string fbId);
    }

    public class PlayerService : ServiceBase, IPlayerService
    {
        public int GetSettingForGetPhotoBackedDateDays()
        {
            var setting = DbContext.Settings.FirstOrDefault(s => s.SettingKey == "GetPhotoBackedDateDays");
            return setting?.SettingValueNumber ?? 60;
        }

        public FacebookProfileViewModel AddOrUpdatePlayerInfo(FacebookProfileViewModel playerProfile, UserDetailDto currentUser)
        {
            Log.Debug($"AddOrUpdatePlayerInfo");

            var player = DbContext.Players.FirstOrDefault(p => p.UserName == currentUser.UserName);

            if (player == null)
            {
                player = new Player
                {
                    UserName = currentUser.UserName,
                    Email = string.IsNullOrEmpty(playerProfile.Email) ? currentUser.Email : playerProfile.Email,
                    ProfileId = playerProfile.Id,
                    FirstName = playerProfile.FirstName,
                    LastName = playerProfile.LastName,
                    ImageURL = playerProfile.ImageURL,
                    LinkURL = playerProfile.LinkURL,
                    Locale = playerProfile.Locale,
                    Location = playerProfile.Location,
                    Gender = playerProfile.Gender,
                    Bio = playerProfile.Bio
                };

                DbContext.Players.Add(player);
            }
            else
            {
                player.Email = string.IsNullOrEmpty(playerProfile.Email) ? currentUser.Email : playerProfile.Email;
                player.ProfileId = playerProfile.Id;
                player.ImageURL = playerProfile.ImageURL;
                player.LinkURL = playerProfile.LinkURL;
            }
            
            SaveChanges("Players");

            playerProfile.PlayerId = player.Id;
            playerProfile.UserName = currentUser.UserName;
            playerProfile.Email = player.Email;

            return playerProfile;
        }

        public void AddOrUpdatePlayerAlbums(FacebookProfileViewModel playerProfile, List<FacebookAlbumViewModel> playerAlbums)
        {
            if (playerProfile == null || playerAlbums == null || playerAlbums.Count == 0)
                return;

            foreach (var album in playerAlbums)
            {
                var existingAlbum = DbContext.PlayerFacebookAlbums
                    .FirstOrDefault(item => item.PlayerId == playerProfile.PlayerId && item.Id == album.Id);

                if(existingAlbum != null)
                {
                    existingAlbum.Name = album.Name;
                    existingAlbum.Count = (int)album.Count;
                    existingAlbum.Link = album.Link;
                    existingAlbum.ImageURL = album.ImageURL;
                    existingAlbum.Privacy = album.Privacy;
                    existingAlbum.Type = album.Type;
                    existingAlbum.Location = album.Location;
                }
                else
                {
                    var playerAlbum = new PlayerFacebookAlbum
                    {
                        Id = album.Id,
                        PlayerId = playerProfile.PlayerId,
                        Name = album.Name,
                        Count = (int)album.Count,
                        Link = album.Link,
                        ImageURL = album.ImageURL,
                        Privacy = album.Privacy,
                        Type = album.Type,
                        Location = album.Location
                    };
                    DbContext.PlayerFacebookAlbums.Add(playerAlbum);
                }
            }

            SaveChanges("PlayerFacebookAlbums");
        }

        public void AddOrUpdatePlayerPhotos(FacebookProfileViewModel playerProfile, List<FacebookPhotoViewModel> playerPhotos)
        {
            if (playerProfile == null || playerPhotos == null || playerPhotos.Count == 0)
                return;

            foreach(var photo in playerPhotos)
            {
                var playerPhoto = DbContext.PlayerFacebookPhotoes
                    .FirstOrDefault(item => item.PlayerId == playerProfile.PlayerId && item.Id == photo.Id);
                var isNew = false;

                if (playerPhoto != null)
                {
                    playerPhoto.Name = photo.Name.StripLength(255);
                    playerPhoto.Link = photo.Link;
                    playerPhoto.UpdatedTime = photo.UpdatedTime;
                    playerPhoto.AlbumName = photo.AlbumName.StripLength(255);
                    //playerPhoto.NameTags = ConvertNameTagsToCsvString(photo.NameTags);
                }
                else
                {
                    playerPhoto = new PlayerFacebookPhoto
                    {
                        Id = photo.Id,
                        PlayerId = playerProfile.PlayerId,
                        Name = photo.Name.StripLength(255),
                        Link = photo.Link,
                        AlbumName = photo.AlbumName.StripLength(255),
                        AlbumId = photo.AlbumId,
                        CreatedTime = photo.CreatedTime,
                        UpdatedTime = photo.UpdatedTime
                    };
                    isNew = true;
                }

                // Get image detail
                if (photo.Images != null && photo.Images.Count > 0)
                {
                    // Get the first & largest size of image only
                    if (photo.Images[0] == null) continue;
                    var img = photo.Images[0].ToString();
                    var imgViewModel = JsonConvert.DeserializeObject<FacebookPhotoImageViewModel>(img);

                    if(imgViewModel != null)
                    {
                        playerPhoto.Height = imgViewModel.Height;
                        playerPhoto.Width = imgViewModel.Width;
                        playerPhoto.Source = imgViewModel.Source;
                    }
                }

                // Get place & location detail
                if(photo.Place != null)
                {
                    var placeViewModel = JsonConvert.DeserializeObject<FacebookPlaceViewModel>(photo.Place.ToString());
                    if(placeViewModel != null)
                    {
                        playerPhoto.FacebookPlaceId = placeViewModel.Id;
                        playerPhoto.PlaceName = placeViewModel.Name;
                        if(placeViewModel.Location != null)
                        {
                            playerPhoto.LocationName = placeViewModel.Location.Name;
                            playerPhoto.Street = placeViewModel.Location.Street;
                            playerPhoto.City = placeViewModel.Location.City;
                            playerPhoto.State = placeViewModel.Location.State;
                            playerPhoto.Region = placeViewModel.Location.Region;
                            playerPhoto.Country = placeViewModel.Location.Country;
                            playerPhoto.Zip = placeViewModel.Location.Zip;
                            playerPhoto.Latitude = Convert.ToDecimal(placeViewModel.Location.Latitude);
                            playerPhoto.Longitude = Convert.ToDecimal(placeViewModel.Location.Longitude);
                        }
                    }
                }
                
                // Get reactions detail
                if (photo.Reactions != null)
                {
                    var reactionViewModel =
                        JsonConvert.DeserializeObject<FacebookPhotoReactionViewModel>(photo.Reactions.ToString());
                    if (reactionViewModel?.Data != null)
                    {
                        playerPhoto.TotalReactionCount = reactionViewModel.Data.Count;
                        playerPhoto.TotalReactionLikeCount = reactionViewModel.Data.Count(item => item.Type == "LIKE");
                        playerPhoto.TotalReactionLoveCount = reactionViewModel.Data.Count(item => item.Type == "LOVE");
                        playerPhoto.TotalReactionHahaCount = reactionViewModel.Data.Count(item => item.Type == "HAHA");
                    }
                    else
                    {
                        playerPhoto.TotalReactionCount = 0;
                        playerPhoto.TotalReactionLikeCount = 0;
                        playerPhoto.TotalReactionLoveCount = 0;
                        playerPhoto.TotalReactionHahaCount = 0;
                    }
                }
                
                // Get tags detail
                if (photo.Tags != null)
                {
                    var tagViewModel = JsonConvert.DeserializeObject<FacebookPhotoTagViewModel>(photo.Tags.ToString());
                    if (tagViewModel?.Data != null)
                    {
                        playerPhoto.TotalProfileTaggedInPhotoCount = tagViewModel.Data.Count;
                        playerPhoto.NameTags = string.Join(";",
                            tagViewModel.Data.Select(item => $"{item.Id}|{item.Name}"));
                    }
                    else
                    {
                        playerPhoto.TotalProfileTaggedInPhotoCount = 0;
                        playerPhoto.NameTags = string.Empty;
                    }
                }
                if (string.IsNullOrEmpty(playerPhoto.NameTags))
                    playerPhoto.NameTags = string.Empty;

                if (isNew)
                    DbContext.PlayerFacebookPhotoes.Add(playerPhoto);
            }

            SaveChanges("PlayerFacebookPhotoes");
        }

        private string ConvertNameTagsToCsvString(JsonArray jsonArray)
        {
            if(jsonArray == null)
                return string.Empty;

            var nameTagsBuilder = new StringBuilder();

            foreach (var item in jsonArray)
            {
                var nameTag = JsonConvert.DeserializeObject<NameTagViewModel>(item.ToString());
                if (nameTag == null) continue;
                nameTagsBuilder.Append($"{nameTag.Id}|{nameTag.Name};");
            }

            return nameTagsBuilder.ToString().TrimEnd(';');
        }

        /// <summary>
        /// Update player photo identify the photo by fb photo id
        /// </summary>
        /// <param name="photoViewModel"></param>
        public void UpdatePlayerPhotoByFbPhotoId(FacebookPhotoViewModel photoViewModel)
        {
            var playerPhotoEntity = DbContext.PlayerFacebookPhotoes.FirstOrDefault(item => item.Id == photoViewModel.Id);

            if (playerPhotoEntity == null) return;

            playerPhotoEntity.TotalReactionCount = photoViewModel.TotalReactionCount;
            playerPhotoEntity.TotalReactionLikeCount = photoViewModel.TotalReactionLikeCount;
            playerPhotoEntity.TotalReactionLoveCount = photoViewModel.TotalReactionLoveCount;
            playerPhotoEntity.TotalReactionWowCount = photoViewModel.TotalReactionWowCount;
            playerPhotoEntity.TotalReactionHahaCount = photoViewModel.TotalReactionHahaCount;
            playerPhotoEntity.TotalReactionSadCount = photoViewModel.TotalReactionSadCount;
            playerPhotoEntity.TotalReactionAngryCount = photoViewModel.TotalReactionAngryCount;
            playerPhotoEntity.TotalReactionThankfulCount = photoViewModel.TotalReactionThankfulCount;
            playerPhotoEntity.TotalReactionNoneCount = photoViewModel.TotalReactionNoneCount;
            playerPhotoEntity.TotalProfileTaggedInPhotoCount = photoViewModel.TotalProfileTaggedInPhotoCount;
            playerPhotoEntity.NameTags = photoViewModel.NameTagsString;

            SaveChanges("PlayerFacebookPhoto");
        }

        public void AddUserTaggedLocations(FacebookUserTaggedPlaceViewModel userTaggedLocationViewModel)
        {
            if (string.IsNullOrEmpty(userTaggedLocationViewModel?.PlayerFbProfileId))
                return;

            var existingUserTaggedLocations =
                DbContext.PlayerFacebookUserTaggedLocations.Where(
                    item => item.PlayerFbProfileId == userTaggedLocationViewModel.PlayerFbProfileId);

            DbContext.PlayerFacebookUserTaggedLocations.RemoveRange(existingUserTaggedLocations);

            foreach (var userTagged in userTaggedLocationViewModel.Data)
            {
                var playerFbUserTaggedEntity = new PlayerFacebookUserTaggedLocation
                {
                    Id = userTagged.Id,
                    PlayerFbProfileId = userTaggedLocationViewModel.PlayerFbProfileId,
                    CreatedTime =
                        DateTime.Parse(userTagged.CreatedTime, null, System.Globalization.DateTimeStyles.RoundtripKind),
                    PlaceId = userTagged.Place?.Id,
                    PlaceName = userTagged.Place?.Name,
                    City = userTagged.Place?.Location?.City,
                    Country = userTagged.Place?.Location?.Country,
                    Street = userTagged.Place?.Location?.Street,
                    Zip = userTagged.Place?.Location?.Zip,
                    Latitude = Convert.ToDecimal(userTagged.Place?.Location?.Latitude),
                    Longitude = Convert.ToDecimal(userTagged.Place?.Location?.Longitude)
                };
                DbContext.PlayerFacebookUserTaggedLocations.Add(playerFbUserTaggedEntity);
            }
            SaveChanges("PlayerFacebookUserTaggedLocation");
        }

        public void AddUserFeed(FacebookPostViewModel postViewModel)
        {
            if(string.IsNullOrEmpty(postViewModel?.PlayerFbProfileId))
                return;

            var existingUserFeed =
                DbContext.PlayerFacebookFeeds.Where(item => item.PlayerFbProfileId == postViewModel.PlayerFbProfileId);

            DbContext.PlayerFacebookFeeds.RemoveRange(existingUserFeed);

            var tagVm = new WithTagItemViewModel
            {
                Id = postViewModel.PlayerFbProfileId,
                Name = ""
            };

            var postFromFriendsOfUser =
                postViewModel.Data.Where(
                    item =>
                        item.From.Id != postViewModel.PlayerFbProfileId && item.WithTags?.Data != null &&
                        item.WithTags.Data.Contains(tagVm,
                            new UserTagItemComparer()));

            foreach (var userFeed in postFromFriendsOfUser)
            {
                var userFeedEntity = new PlayerFacebookFeed
                {
                    PlayerFbProfileId = postViewModel.PlayerFbProfileId,
                    Id = userFeed.Id,
                    CreatedTime = DateTime.Parse(userFeed.CreatedTime, null, System.Globalization.DateTimeStyles.RoundtripKind),
                    Type = userFeed.Type,
                    Message = userFeed.Message.StripLength(1000),
                    Story = userFeed.Story.StripLength(1000),
                    Link = userFeed.Link,
                    FromFbName = userFeed.From?.Name,
                    FromFbProfileId = userFeed.From?.Id,
                    PlaceId = userFeed.Place?.Id,
                    PlaceName = userFeed.Place?.Name,
                    City = userFeed.Place?.Location?.City,
                    Country = userFeed.Place?.Location?.Country,
                    Street = userFeed.Place?.Location?.Street,
                    Zip = userFeed.Place?.Location?.Zip,
                    Latitude = Convert.ToDecimal(userFeed.Place?.Location?.Latitude),
                    Longitude = Convert.ToDecimal(userFeed.Place?.Location?.Longitude)
                };

                if (userFeed.WithTags?.Data != null)
                {
                    userFeedEntity.WithTagNames = string.Join(";", userFeed.WithTags.Data.Select(item => item.Name)).StripLength(4000);
                    userFeedEntity.WithTagIds = string.Join(";", userFeed.WithTags.Data.Select(item => item.Id)).StripLength(4000);
                }
                DbContext.PlayerFacebookFeeds.Add(userFeedEntity);
            }
            SaveChanges("PlayerFacebookFeed");
        }

        public FacebookProfileViewModel GetPlayerProfileByDbId(int playerDbId)
        {
            var entity = DbContext.Players.FirstOrDefault(p => p.Id == playerDbId);
            return FacebookProfileViewModel.Mapper(entity);
        }

        public FacebookProfileViewModel GetPlayerProfileByFbId(string fbId)
        {
            var entity = DbContext.Players.FirstOrDefault(p => p.ProfileId == fbId);
            return FacebookProfileViewModel.Mapper(entity);
        }
    }

    internal class UserTagItemComparer : IEqualityComparer<WithTagItemViewModel>
    {
        public bool Equals(WithTagItemViewModel x, WithTagItemViewModel y)
        {
            return x != null && y != null && x.Id.Equals(y.Id);
        }

        public int GetHashCode(WithTagItemViewModel obj)
        {
            return obj.Id.GetHashCode();
        }
    }
}