﻿using System;
using System.Collections.Generic;
using System.Drawing;
using Kaliko.ImageLibrary;
using Kaliko.ImageLibrary.Scaling;
using KinhDo.CosyAppAdmin.Model;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public class ImageGeneratorCommand
    {
        /// <summary>
        /// Path where to save the generated image
        /// </summary>
        public string SavedPath { get; set; }

        public string TextOnImage { get; set; }

        public int Width { get; set; }

        public int Height { get; set; }

        public Color TextColor { get; set; }

        public float? FontSize { get; set; }

        public string FontFamily { get; set; }

        public bool FontBold { get; set; }

        public Point? Point { get; set; }

        public StringAlignment Alignment { get; set; }

        public StringAlignment VerticalAlignment { get; set; }
    }

    public class FbSharingImageCommand : ImageGeneratorCommand
    {
        /// <summary>
        /// Full path to the template image
        /// </summary>
        public string PathToTemplateImage { get; set; }
    }

    public class SaveImageCommand  : ImageGeneratorCommand
    {
        public string Id { get; set; }

        public int SourceWidth { get; set; }

        public int SourceHeight { get; set; }

        public bool MaintainAspectRatio => false;

        //public bool CropImageToMakeSquare => true;

        public bool AddFrameAroundImage => false; // change value for different behaviour

        public int ScaleRatioByWidth => Width/SourceWidth;

        public int ScaleRatioByHeight => Height/SourceHeight;

        public string ImageSource { get; set; }

        public static Func<PlayerFacebookPhoto, int?, int?, SaveImageCommand> Mapper = (photo, width, height) => new SaveImageCommand
        {
            ImageSource = photo.Source,
            Width = width.GetValueOrDefault(photo.Width.GetValueOrDefault(235)), // desired width
            Height = height.GetValueOrDefault(photo.Height.GetValueOrDefault(235)), // desired height
            SourceWidth = photo.Width.GetValueOrDefault(0), // source width of the photo
            SourceHeight = photo.Height.GetValueOrDefault(0), // source height of the photo
            Id = photo.Id
        };

        public static SaveImageCommand Copy(SaveImageCommand cmd)
        {
            return new SaveImageCommand
            {
                ImageSource = cmd.ImageSource,
                Width = cmd.Width,
                Height = cmd.Height,
                SourceHeight = cmd.SourceHeight,
                SourceWidth = cmd.SourceHeight,
                Id = cmd.Id,
                SavedPath = cmd.SavedPath,
                TextColor = cmd.TextColor,
                TextOnImage = cmd.TextOnImage,
                FontSize = cmd.FontSize
            };
        }
    }

    public class SaveImageCommandComparer : IEqualityComparer<SaveImageCommand>
    {
        public bool Equals(SaveImageCommand x, SaveImageCommand y)
        {
            return x != null && y != null &&
                   string.Equals(x.ImageSource, y.ImageSource, StringComparison.CurrentCultureIgnoreCase);
        }

        public int GetHashCode(SaveImageCommand obj)
        {
            return obj.ImageSource.GetHashCode() + obj.Width.GetHashCode() + obj.Height.GetHashCode();
        }
    }

    /// <summary>
    /// Responsible for generating game result images to be used by video
    /// </summary>
    public class GameResultImageGenerator : ServiceBaseWithLog
    {
        public string GeneratePngImageWithText(ImageGeneratorCommand command)
        {
            try
            {
                var image = new KalikoImage(command.Width, command.Height, Color.Transparent);
                var fontFamily = string.IsNullOrEmpty(command.FontFamily) ? "Tahoma" : command.FontFamily;

                var textField = new TextField(command.TextOnImage)
                {
                    Alignment = command.Alignment,
                    VerticalAlignment = command.VerticalAlignment,
                    Font =
                        new Font(fontFamily, command.FontSize.GetValueOrDefault(32),
                            command.FontBold ? FontStyle.Bold : FontStyle.Regular, GraphicsUnit.Pixel),
                    TextColor = command.TextColor
                };

                if (command.Point.HasValue)
                {
                    textField.Point = command.Point.Value;
                }

                textField.Draw(image);

                image.SavePng(command.SavedPath);
            }
            catch (Exception ex)
            {
                Log.Error("Error GeneratePngImageWithText", ex);
            }

            return command.SavedPath;
        }

        public string GeneratePngImage(ImageGeneratorCommand command)
        {
            try
            {
                var image = new KalikoImage(command.Width, command.Height, Color.Transparent);

                var textField = new TextField(command.TextOnImage)
                {
                    Alignment = StringAlignment.Center,
                    VerticalAlignment = StringAlignment.Center,
                    Font = new Font("Tahoma", command.FontSize.GetValueOrDefault(20), command.FontBold ? FontStyle.Bold : FontStyle.Regular),
                    TextColor = command.TextColor
                };

                textField.Draw(image);

                image.SavePng(command.SavedPath);
            }
            catch (Exception ex)
            {
                Log.Error("Error GeneratePngImage", ex);
            }

            return command.SavedPath;
        }

        /// <summary>
        /// Download image from an url, save as jpg, resize do not keep aspect ratio
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SaveImageFromUrlAsJpg(SaveImageCommand command)
        {
            try
            {
                var image = new KalikoImage(command.ImageSource);
                image.Resize(command.Width, command.Height);
                image.SaveJpg(command.SavedPath, 100);
            }
            catch (Exception ex)
            {
                Log.Error($"Error save image for {command.ImageSource}", ex);
            }

            return command.SavedPath;
        }

        /// <summary>
        /// Download image from an url, save as png, resize do not keep aspect ratio
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SaveImageFromUrlAsPng(SaveImageCommand command)
        {
            try
            {
                var image = new KalikoImage(command.ImageSource);
                image.Resize(command.Width, command.Height);
                image.SavePng(command.SavedPath);
            }
            catch (Exception ex)
            {
                Log.Error($"Error save image for {command.ImageSource}", ex);
            }

            return command.SavedPath;
        }

        /// <summary>
        /// Download image from an url, save as jpg, resize keep aspect ratio and add frame border around the image
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SaveImageFromUrlWithImageFrameAsJpg(SaveImageCommand command)
        {
            try
            {
                var image = new KalikoImage(command.ImageSource);
                var targetW = command.Width;
                var targetH = command.Height;
                var scaleRatio = image.Width > image.Height ? (float)targetW / image.Width : (float)targetH / image.Height;

                targetW = Convert.ToInt32(scaleRatio * image.Width);
                targetH = Convert.ToInt32(scaleRatio * image.Height);

                const int frameBorderWidth = 10;

                var frameImage = new KalikoImage(targetW + 20, targetH + 40, Color.White);
                var scaleImage = image.Scale(new FitScaling(targetW, targetH));
                frameImage.BlitImage(scaleImage, frameBorderWidth, frameBorderWidth);

                frameImage.SaveJpg(command.SavedPath, 100);
            }
            catch (Exception ex)
            {
                Log.Error($"Error save image for {command.ImageSource}", ex);
            }

            return command.SavedPath;
        }

        public string SaveImageFromUrlResizeWithCrop(SaveImageCommand command)
        {
            try
            {
                var image = new KalikoImage(command.ImageSource);
                var targetW = command.Width;
                var targetH = command.Height;
                var scaleRatio = image.Width <= image.Height ? (float)targetW / image.Width : (float)targetH / image.Height;

                targetW = Convert.ToInt32(scaleRatio * image.Width);
                targetH = Convert.ToInt32(scaleRatio * image.Height);

                image.Resize(targetW, targetH);
                var newSize = Math.Min(targetW, targetH);
                image.Crop(0, 0, newSize, newSize);

                image.SaveJpg(command.SavedPath, 100);
            }
            catch (Exception ex)
            {
                Log.Error($"Error save image for {command.ImageSource}", ex);
            }

            return command.SavedPath;
        }

        /// <summary>
        /// Download image from an url, save as jpg, resize keep aspect ratio, do not add frame around image
        /// </summary>
        /// <param name="command"></param>
        /// <returns></returns>
        public string SaveImageFromUrlWithImageScaleAsJpg(SaveImageCommand command)
        {
            try
            {
                var image = new KalikoImage(command.ImageSource);
                var targetW = command.Width;
                var targetH = command.Height;
                var scaleRatio = image.Width > image.Height ? (float)targetW / image.Width : (float)targetH / image.Height;

                targetW = Convert.ToInt32(scaleRatio * image.Width);
                targetH = Convert.ToInt32(scaleRatio * image.Height);

                var scaleImage = image.Scale(new FitScaling(targetW, targetH));

                scaleImage.SaveJpg(command.SavedPath, 100);
            }
            catch (Exception ex)
            {
                Log.Error($"Error save image for {command.ImageSource}", ex);
                throw;
            }
            return command.SavedPath;
        }

        public string GenerateThumbnailImageForFbSharing(FbSharingImageCommand command)
        {
            if (command == null) return string.Empty;

            var image = new KalikoImage(command.PathToTemplateImage);

            var textField = new TextField(command.TextOnImage)
            {
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Center,
                Font = new Font("Helvetica", 52, FontStyle.Bold),
                TextColor = Color.Red,
                Point = new Point(860, 115)
            };

            textField.Draw(image);

            image.SavePng(command.SavedPath);

            return command.SavedPath;
        }
    }
}