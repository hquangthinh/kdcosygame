﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity.Core;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Hangfire;
using KinhDo.CosyAppAdmin.Model;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;
using KinhDo.CosyPhotoGame.EmotionApi.Extentions;
using KinhDo.CosyPhotoGame.EmotionApi.Helper;

namespace KinhDo.CosyPhotoGame.EmotionApi.Service
{
    public class CosyGameService : ServiceBase
    {
        private const int MinPhotoCountForRunningDetection = 30;
        private const int MaxPhotoCountForRunningDetection = 100;
        private const int PhotoBackedDateDays = 360;
        private const int ExtendedPhotoBackedDateDays = 540;
        private const int HappinessScore = 0;
        private const string ProfilePicturesAlbumName = "Profile Pictures";
        private const string MobileUploadsAlbumName = "Mobile Uploads";

        public GameTransactionDto StartGameTransaction(GameTransactionDto gameTransaction)
        {
            var gameResultEntity = new PlayerGameResult
            {
                PlayerId = gameTransaction.PlayerDbId,
                ClientIpAddress = gameTransaction.ClientIpAddress.StripLength(255),
                ClientBrowser = gameTransaction.ClientBrowser.StripLength(255),
                ResultDate = gameTransaction.ResultDate.GetValueOrDefault(DateTime.Now),
                PercentComplete = 0,
                Status = gameTransaction.Status,
                AccessToken = gameTransaction.AccessToken.StripLength(500)
            };
            DbContext.PlayerGameResults.Add(gameResultEntity);
            SaveChanges("PlayerGameResult");
            gameTransaction.Id = gameResultEntity.Id;
            return gameTransaction;
        }

        /// <summary>
        /// Update the percent complete while the game processing is in progress
        /// </summary>
        /// <param name="gameTransaction"></param>
        /// <returns></returns>
        public GameTransactionDto UpdateGameProgress(GameTransactionDto gameTransaction)
        {
            if(gameTransaction == null)
                throw new ArgumentNullException(nameof(gameTransaction));

            var gameResultEntity = DbContext.PlayerGameResults.FirstOrDefault(item => item.Id == gameTransaction.Id);

            if (gameResultEntity == null)
                throw new ObjectNotFoundException($"Object not found for PlayerGameResult -> {gameTransaction.Id}");

            gameResultEntity.PercentComplete = gameTransaction.PercentComplete;

            SaveChanges("PlayerGameResult");

            return gameTransaction;
        }

        public bool CanUseCacheDataFromPreviousGameTransaction(bool fbCacheDataEnabled, int playerDbId, int fbDataCacheDuration)
        {
            return fbCacheDataEnabled && DbContext.PlayerGameResults.Where(item => item.PlayerId == playerDbId).ToList().Any(item =>
                    item.ResultDate.AddHours(fbDataCacheDuration) >= DateTime.Now && item.PercentComplete >= 50);
        }

        /// <summary>
        /// Detection emotion for player photos, store result to table PlayerFacebookPhoto
        /// </summary>
        /// <param name="playerProfile"></param>
        public async Task DetectionEmotionForPlayerPhotos(FacebookProfileViewModel playerProfile)
        {
            var playerDbId = playerProfile.PlayerId;
            Log.Info($"Start DetectionEmotionForPlayerPhotos -> playerDbId {playerDbId} -> {playerProfile.Id} -> {playerProfile.UserName}");

            var gameSettingService = new GameSettingService();
            var gameSetting = gameSettingService.GetGameGlobalSettings();
            var photoTempFolderPath = Path.Combine(gameSetting.PlayerGameDataBaseFolder, "Temp");
            DirectoryHelper.CreateDirectoryForWriting(photoTempFolderPath);

            //if (DbContext.PlayerFacebookPhotoes.Count(p => p.PlayerId == playerDbId && p.Happiness.HasValue) >=
            //    MaxPhotoCountForRunningDetection)
            //{
            //    Log.Info($"Enough emotion data for user {playerProfile.UserName} skip emotion detection.");
            //    return;
            //}

            var photoCreatedAfterDate = GetPhotoCreatedDateFilter();
            var profilePicturesCreatedAfterDate = photoCreatedAfterDate.AddDays(-PhotoBackedDateDays);
            var emotionDetector = new RoundRobinEmotionDetectorManager();
            
            // all player photos before the cut off date and do not have emotion data set
            var allRecentPhotos =
                DbContext.PlayerFacebookPhotoes.Where(
                    p => p.PlayerId == playerDbId && p.Happiness == null && p.CreatedTime.HasValue &&
                         p.CreatedTime.Value > photoCreatedAfterDate).ToList();

            var allRecentProfilePictures =
                DbContext.PlayerFacebookPhotoes.Where(
                    item => item.PlayerId == playerDbId && item.Happiness == null && 
                    item.CreatedTime.HasValue && item.CreatedTime.Value > profilePicturesCreatedAfterDate && 
                    item.AlbumName == ProfilePicturesAlbumName)
                    .OrderByDescending(item => item.CreatedTime).ToList();

            // limit to photo have tags and face count
            var playerPhotosWhichHaveTags =
                allRecentProfilePictures.Union(
                    allRecentPhotos.Where(p => p.TotalProfileTaggedInPhotoCount > 0 && p.NameTags != null 
                        && p.AlbumName != ProfilePicturesAlbumName)
                            .OrderByDescending(p => p.UpdatedTime))
                                .Take(MaxPhotoCountForRunningDetection)
                                    .ToList();

            // fall back to player photo and does not have emotion score yet
            if (playerPhotosWhichHaveTags.Count < MinPhotoCountForRunningDetection)
            {
                playerPhotosWhichHaveTags =
                    playerPhotosWhichHaveTags.Union(
                        allRecentPhotos.OrderByDescending(p => p.NameTags.Length)
                            .ThenByDescending(p => p.CreatedTime)
                                .Take(MaxPhotoCountForRunningDetection))
                                    .ToList();
            }

            Log.Debug($"Total player photos candidate for detection is {allRecentPhotos.Count} photos");

            Log.Debug($"Run detection for {playerPhotosWhichHaveTags.Count} photos");

            var idTrackingList = new List<string>();

            foreach (var photo in playerPhotosWhichHaveTags)
            {
                try
                {
                    if (idTrackingList.Contains(photo.Id) || photo.Happiness.HasValue)
                    {
                        Log.Info($"Photo {photo.Id} already has emotion analysis, skip.");
                        continue;
                    }

                    Log.Info($"Start DetectEmotionAsync -> PhotoUrl -> {photo.Source}");

                    var photoEmoResult = await emotionDetector.DetectEmotionAsync(new PhotoEmotionDetectionCommand
                    {
                        PhotoUrl = photo.Source,
                        PhotoTempFolderPath = photoTempFolderPath
                    });
                    photo.TotalFacesCount = photoEmoResult.TotalFacesCount;
                    photo.Anger = photoEmoResult.Anger;
                    photo.Contempt = photoEmoResult.Contempt;
                    photo.Disgust = photoEmoResult.Disgust;
                    photo.Fear = photoEmoResult.Fear;
                    photo.Happiness = photoEmoResult.Happiness;
                    photo.Neutral = photoEmoResult.Neutral;
                    photo.Sadness = photoEmoResult.Sadness;
                    photo.Surprise = photoEmoResult.Surprise;

                    SaveChanges("PlayerFacebookPhoto");
                    idTrackingList.Add(photo.Id);
                }
                catch (Exception ex)
                {
                    Log.Error("Error -> DetectionEmotionForPlayerPhotos -> ", ex);
                }
            }
        }

        private DateTime GetPhotoCreatedDateFilter()
            => DateTime.Now.AddDays(-PhotoBackedDateDays);

        private DateTime GetPhotoExtendedCreatedDateFilter()
            => DateTime.Now.AddDays(-ExtendedPhotoBackedDateDays);

        /// <summary>
        /// Aggregate game result and store result detail to GameResultDetail
        /// </summary>
        /// <param name="playerDbId"></param>
        /// <param name="gameResultId"></param>
        /// <param name="gameSettingDto"></param>
        public UserGameResultDto GenerateGameResult(int playerDbId, int gameResultId, GameSettingDto gameSettingDto)
        {
            Log.Info($"Start GenerateGameResult -> playerDbId -> {playerDbId}");
            var result = new UserGameResultDto();

            var playerEntity = DbContext.Players.FirstOrDefault(p => p.Id == playerDbId);
            if (playerEntity == null)
                return result;

            // Game result header
            var gameResultEntity = DbContext.PlayerGameResults.FirstOrDefault(item => item.Id == gameResultId);

            if (gameResultEntity == null)
            {
                result.Status = GameResultStatus.Error.ToString();
                result.ErrorMessage = $"Game result transaction is not available for player -> {playerDbId}";
                return result;
            }

            // db result
            result.Id = gameResultEntity.Id;
            result.PlayerId = gameResultEntity.PlayerId;
            result.ResultDate = gameResultEntity.ResultDate;
            result.ClientIpAddress = gameResultEntity.ClientIpAddress;
            result.ClientBrowser = gameResultEntity.ClientBrowser;
            result.Status = gameResultEntity.Status;
            result.AccessToken = gameResultEntity.AccessToken;
            result.PlayerInfo = FacebookProfileViewModel.Mapper(playerEntity);

            // Download, generate images for videos and build library properties file
            var photoCreatedAfterDate = GetPhotoCreatedDateFilter();
            var allPlayerPhotosPool =
                DbContext.PlayerFacebookPhotoes
                    .Where(item => item.PlayerId == playerDbId && item.CreatedTime.HasValue &&
                                   item.CreatedTime.Value > photoCreatedAfterDate).ToList();

            // personal smile photos
            var allPlayerPhotosWithSmile = allPlayerPhotosPool
                .Where(item => item.Happiness > HappinessScore && item.AlbumName == ProfilePicturesAlbumName)
                    .OrderByDescending(item => item.CreatedTime)
                        .Union(
                            allPlayerPhotosPool.Where(item => item.AlbumName == ProfilePicturesAlbumName 
                                && (item.Happiness == 0 || item.Happiness == null))
                                    .OrderByDescending(item => item.CreatedTime), PlayerFacebookPhotoComparer.DefaultComparer)
                                        .ToList();

            if (allPlayerPhotosWithSmile.Count < 5)
            {
                var tempIdList = allPlayerPhotosWithSmile.Select(item => item.Id);
                var extendedPhotoCreatedAfterDate = photoCreatedAfterDate.AddDays(-PhotoBackedDateDays);
                var extendedProfilePictures =
                    DbContext.PlayerFacebookPhotoes
                        .Where(item => item.PlayerId == playerDbId && 
                                       item.CreatedTime.HasValue && item.CreatedTime.Value > extendedPhotoCreatedAfterDate &&
                                       item.AlbumName == ProfilePicturesAlbumName &&
                                       !tempIdList.Contains(item.Id))
                        .Take(10)
                        .ToList();

                allPlayerPhotosWithSmile = allPlayerPhotosWithSmile.Union(extendedProfilePictures).ToList();

                if (allPlayerPhotosWithSmile.Count < 5)
                {
                    tempIdList = allPlayerPhotosWithSmile.Select(item => item.Id);

                    allPlayerPhotosWithSmile =
                        allPlayerPhotosWithSmile.Union(allPlayerPhotosPool.Where(item => item.AlbumName != ProfilePicturesAlbumName
                            && item.TotalFacesCount > 0 && !tempIdList.Contains(item.Id))
                                .OrderBy(item => item.TotalFacesCount)
                                    .ThenByDescending(item => item.CreatedTime), PlayerFacebookPhotoComparer.DefaultComparer).ToList();

                    if (allPlayerPhotosWithSmile.Count < 5)
                    {
                        tempIdList = allPlayerPhotosWithSmile.Select(item => item.Id);

                        allPlayerPhotosWithSmile = allPlayerPhotosWithSmile.Union(
                                allPlayerPhotosPool.Where(item => !tempIdList.Contains(item.Id))
                                    .OrderByDescending(item => item.TotalFacesCount)
                                        .ThenByDescending(item => item.CreatedTime), PlayerFacebookPhotoComparer.DefaultComparer)
                                            .ToList();
                    }
                }
            }

            allPlayerPhotosPool =
                allPlayerPhotosPool.OrderByDescending(item => item.CreatedTime).ToList();

            var playerTaggedLocations =DbContext.PlayerFacebookUserTaggedLocations
                    .Where(item => item.PlayerFbProfileId == playerEntity.ProfileId)
                    .OrderByDescending(item => item.CreatedTime)
                    .ToList();

            var playerTaggedPost =
                DbContext.PlayerFacebookFeeds
                    .Where(item => item.PlayerFbProfileId == playerEntity.ProfileId)
                    .OrderByDescending(item => item.CreatedTime)
                    .ToList();

            var allPlayerPhotosWithSmileForStatisticCount =
                allPlayerPhotosPool.Where(item => item.Happiness >= 0 ||
                                                  item.TotalProfileTaggedInPhotoCount >= 0).ToList();

            CalculateGameResultScore(allPlayerPhotosWithSmileForStatisticCount, playerTaggedLocations, playerTaggedPost, result);
            DownloadAndGeneratePlayerPhotos(gameSettingDto, playerEntity, result, allPlayerPhotosPool, allPlayerPhotosWithSmile,
                playerTaggedLocations, playerTaggedPost);
            // Export library properties file to user_data
            ExportLibraryPropertiesFile(gameSettingDto, playerEntity.ProfileId);

            // set status to complete for game result
            gameResultEntity.PercentComplete = 100;
            gameResultEntity.Status = GameResultStatus.ResultOk.ToString();
            SaveChanges("GameResultDetails");

            return result;
        }

        private void CalculateGameResultScore(List<PlayerFacebookPhoto> allPlayerPhotosWithSmile,
            List<PlayerFacebookUserTaggedLocation> playerTaggedLocations,
            List<PlayerFacebookFeed> playerTaggedPost, UserGameResultDto result)
        {
            result.TotalSmileInMonth = allPlayerPhotosWithSmile.Count > 0 ? allPlayerPhotosWithSmile.Count : new Random(10).Next(10);
            result.TotalCheckedInLocation = playerTaggedLocations.Count > 0 ? playerTaggedLocations.Count : new Random(20).Next(20);
            result.TotalTaggedPosts = playerTaggedPost.Count > 0 ? playerTaggedPost.Count : new Random(20).Next(20);

            foreach (var photo in allPlayerPhotosWithSmile)
            {
                result.TotalLike += photo.TotalReactionLikeCount.GetValueOrDefault(0);
                result.TotalLove += photo.TotalReactionLoveCount.GetValueOrDefault(0);
                result.TotalHaha += photo.TotalReactionHahaCount.GetValueOrDefault(0);
            }

            result.TotalGroupSmileInMonth = allPlayerPhotosWithSmile.Count(item => item.TotalFacesCount >= 4);

            if (result.TotalGroupSmileInMonth < 10)
                result.TotalGroupSmileInMonth = allPlayerPhotosWithSmile.Count(item => item.TotalFacesCount >= 3);

            if (result.TotalGroupSmileInMonth < 10)
                result.TotalGroupSmileInMonth = allPlayerPhotosWithSmile.Count(item => item.TotalFacesCount >= 2);

            if (result.TotalGroupSmileInMonth == 0)
                result.TotalGroupSmileInMonth = new Random(10).Next(10);

            result.TotalReaction = result.TotalLike + result.TotalLove + result.TotalHaha;
            result.TotalGameScore = result.TotalSmileInMonth + result.TotalGroupSmileInMonth +
                                    result.TotalCheckedInLocation + result.TotalTaggedPosts + result.TotalReaction;
            //result.TotalGameScore = result.TotalSmileInMonth + result.TotalGroupSmileInMonth +
            //                        result.TotalCheckedInLocation + result.TotalTaggedPosts;
            if (result.TotalGameScore > 999)
                result.TotalGameScore -= result.TotalReaction;
        }

        private void ExportLibraryPropertiesFile(GameSettingDto gameSettingDto, string playerFbProfileId)
        {
            var pathToUserDataLibProperties = Path.Combine(gameSettingDto.GameBaseInstallationFolder, $@"Content\user_data\{playerFbProfileId}");
            DirectoryHelper.CreateDirectoryForWriting(pathToUserDataLibProperties); // path will be created if does not exist
            var filePath = Path.Combine(pathToUserDataLibProperties, "lib-properties.js");
            JsonHelper.WriteLibraryProperties(gameSettingDto.GameBaseInstallationFolder, playerFbProfileId, filePath);
        }

        private void DownloadAndGeneratePlayerPhotos(GameSettingDto gameSettingDto, Player playerEntity,
            UserGameResultDto gameResultDto,
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<PlayerFacebookPhoto> allPlayerPhotosWithSmile,
            List<PlayerFacebookUserTaggedLocation> playerTaggedLocations,
            List<PlayerFacebookFeed> playerTaggedPost)
        {
            // create folder to store user images
            var pathToPlayerGameImages = Path.Combine(gameSettingDto.GameBaseInstallationFolder, $@"Game\{playerEntity.ProfileId}");
            DirectoryHelper.CreateDirectoryForWriting(pathToPlayerGameImages);

            var imageGenerator = new GameResultImageGenerator();
            //var playerSummaryPhotoSaveCommands = new List<SaveImageCommand>();
            var playerName = $"{playerEntity.FirstName} {playerEntity.LastName}";
            var photoIdTrackingList = new List<string>(); // to track id of photos have been used

            // thumbnail image for fb sharing
            imageGenerator.GenerateThumbnailImageForFbSharing(new FbSharingImageCommand
            {
                TextOnImage = playerEntity.FirstName,
                PathToTemplateImage = Path.Combine(gameSettingDto.GameBaseInstallationFolder, @"Content\images\thumbnail.png"),
                SavedPath = Path.Combine(pathToPlayerGameImages, "thumbnail.png")
            });

            // image _001_USER_329x76.png - user name
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 329,
                Height = 76,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_001_USER_329x76.png"),
                TextColor = Color.White,
                TextOnImage = playerName,
                FontSize = 32
            });

            // _002_USER_PHOTO_235x235.jpg -> user avatar
            imageGenerator.SaveImageFromUrlAsJpg(new SaveImageCommand
            {
                Width = 235,
                Height = 235,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_002_USER_PHOTO_235x235.jpg"),
                ImageSource = playerEntity.ImageURL
            });

            // _003_USER_NUMBER_274x105.png -> Total smile count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 274,
                Height = 105,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_003_USER_NUMBER_274x105.png"),
                TextColor = Color.White,
                TextOnImage = gameResultDto.TotalSmileInMonth.ToString(),
                FontSize = 95
            });

            // _004_f5_hinh1_235x235.jpg -> Smile photo 1
            GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, 0, pathToPlayerGameImages,
                "_004_f5_hinh1_235x235.jpg", photoIdTrackingList);

            // _005_f5_hinh2_235x235.jpg -> Smile photo 2
            GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, 1, pathToPlayerGameImages,
                "_005_f5_hinh2_235x235.jpg", photoIdTrackingList);

            // _006_f5_hinh3_235x235.jpg -> Smile photo 3
            GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, 2, pathToPlayerGameImages,
                "_006_f5_hinh3_235x235.jpg", photoIdTrackingList);

            // _007_f5_hinh4_235x235.jpg -> Smile photo 4
            GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, 3, pathToPlayerGameImages,
                "_007_f5_hinh4_235x235.jpg", photoIdTrackingList);

            // _008_f5_hinh5_235x235.jpg -> Smile photo 5
            GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, 4, pathToPlayerGameImages,
                "_008_f5_hinh5_235x235.jpg", photoIdTrackingList);

            // _009_f6_Number_430x134.png -> Total group image count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 430,
                Height = 134,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_009_f6_Number_430x134.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                TextOnImage = gameResultDto.TotalGroupSmileInMonth.ToString(),
                FontSize = 124,
                FontBold = true
            });

            // query to find smile group images
            var playerTaggedGroupSmilePhotos =
                allPlayerPhotosPool.Where(
                    item => item.NameTags != null && item.NameTags.Contains(playerEntity.ProfileId)
                            && !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                    .OrderByDescending(item => item.TotalFacesCount)
                    .ThenByDescending(item => item.NameTags.Length)
                    .ThenByDescending(item => item.Happiness)
                    .Select(item => SaveImageCommand.Mapper(item, 280, 280))
                    .Take(6).ToList();

            var groupSmileImages = new List<SaveImageCommand>();

            if (playerTaggedGroupSmilePhotos.Count < 3)
            {
                //photoIdTrackingList.AddRange(playerTaggedGroupSmilePhotos.Select(item => item.Id).ToList());

                groupSmileImages =
                    playerTaggedGroupSmilePhotos.Union(allPlayerPhotosPool
                        .Where(item => item.TotalFacesCount > 0 || item.TotalProfileTaggedInPhotoCount > 0 
                            && !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                        .OrderByDescending(item => item.TotalFacesCount)
                        .ThenByDescending(item => item.NameTags.Length)
                        .ThenByDescending(item => item.Happiness)
                        .Select(item => SaveImageCommand.Mapper(item, 280, 280)).Take(6)).ToList();

                if (groupSmileImages.Count < 3)
                {
                    //photoIdTrackingList.AddRange(groupSmileImages.Select(item => item.Id).ToList());

                    groupSmileImages = groupSmileImages.Union(
                        allPlayerPhotosPool.Where(item => !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                            .OrderByDescending(item => item.TotalFacesCount)
                            .ThenByDescending(item => item.TotalProfileTaggedInPhotoCount)
                            .ThenByDescending(item => item.CreatedTime)
                            .ThenByDescending(item => item.Happiness)
                            .Select(item => SaveImageCommand.Mapper(item, 280, 280)).Take(6)).ToList();

                    if (groupSmileImages.Count < 3)
                    {
                        //photoIdTrackingList.AddRange(groupSmileImages.Select(item => item.Id).ToList());

                        groupSmileImages = groupSmileImages.Union(
                            allPlayerPhotosPool.Where(item => !photoIdTrackingList.Contains(item.Id))
                                .OrderByDescending(item => item.TotalFacesCount)
                                .ThenByDescending(item => item.TotalProfileTaggedInPhotoCount)
                                .ThenByDescending(item => item.CreatedTime)
                                .ThenByDescending(item => item.Happiness)
                                .Select(item => SaveImageCommand.Mapper(item, 280, 280)).Take(6)).ToList();
                    }
                }
            }
            else
            {
                groupSmileImages.AddRange(playerTaggedGroupSmilePhotos);
            }

            // _010_f9_hinh1_280x280.jpg -> group smile image 1
            var groupSmileImg1 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, groupSmileImages, 0, pathToPlayerGameImages,
                "_010_f9_hinh1_280x280.jpg");
            photoIdTrackingList.Add(groupSmileImg1?.Id);
            //CollectSummaryPhoto(playerSummaryPhotoSaveCommands, groupSmileImages, 0, 300, 300);

            // _011_f9_hinh2_280x280.jpg -> group smile image 2
            var groupSmileImg2 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, groupSmileImages, 1, pathToPlayerGameImages,
                "_011_f9_hinh2_280x280.jpg");
            photoIdTrackingList.Add(groupSmileImg2?.Id);
            //CollectSummaryPhoto(playerSummaryPhotoSaveCommands, groupSmileImages, 1, 300, 300);

            // _012_f9_hinh3_280x280.jpg -> group smile image 3
            var groupSmileImg3 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, groupSmileImages, 2, pathToPlayerGameImages,
                "_012_f9_hinh3_280x280.jpg");
            photoIdTrackingList.Add(groupSmileImg3?.Id);

            // _013_f11_NUMBER_275x145.png -> Total reaction count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 275,
                Height = 145,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_013_f11_NUMBER_275x145.png"),
                TextColor = Color.White,
                TextOnImage = gameResultDto.TotalReaction.ToString(),
                FontSize = gameResultDto.TotalReaction < 100 ? 135 : gameResultDto.TotalReaction < 1000 ? 100 : 80
            });

            // _014_f11_KETQUA1_58x33.png -> like count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 58,
                Height = 33,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_014_f11_KETQUA1_58x33.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                TextOnImage = gameResultDto.TotalLike.ToString(),
                FontSize = 28
            });

            // _015_f11_KETQUA2_58x33.png -> love count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 58,
                Height = 33,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_015_f11_KETQUA2_58x33.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                TextOnImage = gameResultDto.TotalLove.ToString(),
                FontSize = 28
            });

            // _016_f11_KETQUA3_58x33.png -> haha count
            imageGenerator.GeneratePngImage(new ImageGeneratorCommand
            {
                Width = 58,
                Height = 33,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_016_f11_KETQUA3_58x33.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                TextOnImage = gameResultDto.TotalHaha.ToString(),
                FontSize = 28
            });


            // _017_f17_NUMBER_223x137.png -> total location count
            imageGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 223,
                Height = 137,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_017_f17_NUMBER_223x137.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                FontFamily = "MyriadPro-Regular",
                TextOnImage = gameResultDto.TotalCheckedInLocation.ToString(),
                FontSize = 127,
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Near
            });

            var placeIdList = playerTaggedLocations.Select(item => item.PlaceId).ToList();
            var playerCheckedInPhotoSaveCommands =
                allPlayerPhotosPool.Where(item => placeIdList.Contains(item.FacebookPlaceId) &&
                                                       !photoIdTrackingList.Contains(item.Id))
                    .OrderByDescending(item => item.TotalFacesCount)
                    .ThenByDescending(item => item.CreatedTime)
                    .ThenByDescending(item => item.Happiness)
                    .Select(item => SaveImageCommand.Mapper(item, 235, 235)).Take(6).ToList();

            if (playerCheckedInPhotoSaveCommands.Count < 3)
            {
                playerCheckedInPhotoSaveCommands = playerCheckedInPhotoSaveCommands.Union(
                    allPlayerPhotosPool.Where(item => !photoIdTrackingList.Contains(item.Id))
                        .OrderByDescending(item => item.TotalFacesCount)
                        .ThenByDescending(item => item.CreatedTime)
                        .ThenByDescending(item => item.Happiness)
                        .Select(item => SaveImageCommand.Mapper(item, 235, 235)).Take(6)).ToList();
            }

            // _018_f17_hinh1_235x235.jpg -> Location image 1
            var userCheckinPhoto1 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerCheckedInPhotoSaveCommands, 0, pathToPlayerGameImages,
                "_018_f17_hinh1_235x235.jpg");
            photoIdTrackingList.Add(userCheckinPhoto1?.Id);
            //CollectSummaryPhoto(playerSummaryPhotoSaveCommands, playerCheckedInPhotoSaveCommands, 0, 300, 300);

            // _019_f17_hinh2_235x235.jpg -> Location image 2
            var userCheckinPhoto2 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerCheckedInPhotoSaveCommands, 1, pathToPlayerGameImages,
                "_019_f17_hinh2_235x235.jpg");
            photoIdTrackingList.Add(userCheckinPhoto2?.Id);

            // _020_f17_hinh3_235x235.jpg -> Location image 3
            var userCheckinPhoto3 = GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerCheckedInPhotoSaveCommands, 2, pathToPlayerGameImages,
                "_020_f17_hinh3_235x235.jpg");
            photoIdTrackingList.Add(userCheckinPhoto3?.Id);

            // _021_f22_NUMBER_220x160.png -> Total tagged posts
            imageGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 220,
                Height = 160,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_021_f22_NUMBER_220x160.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                FontFamily = "MyriadPro-Regular",
                TextOnImage = gameResultDto.TotalTaggedPosts.ToString(),
                FontSize = 147,
                Alignment = StringAlignment.Near,
                VerticalAlignment = StringAlignment.Near
            });

            // list of place id where user is tagged & posted by friends
            var playerTaggedPostsPlaceIdList =
                playerTaggedPost.Where(item => item.FromFbProfileId != playerEntity.ProfileId)
                    .Select(item => item.PlaceId)
                    .ToList();

            // Extended photos pool for fall back query
            var photoExtendedCreatedDateBefore = GetPhotoExtendedCreatedDateFilter();
            var extendedPhotoPool =
                DbContext.PlayerFacebookPhotoes
                    .Where(item => item.PlayerId == playerEntity.Id && item.CreatedTime > photoExtendedCreatedDateBefore)
                    .OrderByDescending(item => item.CreatedTime)
                    .ToList();

            // all player tagged post photos
            var playerTaggedPostPhotoSaveCommands = allPlayerPhotosPool.Where(
                    item => playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) /*&& item.Width > item.Height*/
                            && !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                    .OrderByDescending(item => item.TotalFacesCount)
                    .ThenByDescending(item => item.NameTags.Length)
                    .ThenByDescending(item => item.CreatedTime)
                    .Select(item => SaveImageCommand.Mapper(item, 408, 283))
                    .Take(6)
                    .ToList();

            if (playerTaggedPostPhotoSaveCommands.Count < 3)
            {
                // extend the place list id including user's posts and friends' posts
                playerTaggedPostsPlaceIdList = playerTaggedPost.Select(item => item.PlaceId).ToList();
                var tempLandscapePhotosIList = playerTaggedPostPhotoSaveCommands.Select(item => item.Id).ToList();

                playerTaggedPostPhotoSaveCommands =
                    playerTaggedPostPhotoSaveCommands.Union(extendedPhotoPool.Where(
                        item =>
                            playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) /*&& item.Width > item.Height*/ &&
                            !photoIdTrackingList.Contains(item.Id) && !tempLandscapePhotosIList.Contains(item.Id) 
                            && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                        .OrderByDescending(item => item.TotalFacesCount)
                        .ThenByDescending(item => item.CreatedTime)
                        .ThenByDescending(item => item.NameTags.Length)
                        .Select(item => SaveImageCommand.Mapper(item, 408, 283))
                        .Take(6)).ToList();

                if (playerTaggedPostPhotoSaveCommands.Count < 3)
                {
                    tempLandscapePhotosIList.AddRange(playerTaggedPostPhotoSaveCommands.Select(item => item.Id).ToList());

                    playerTaggedPostPhotoSaveCommands =
                        playerTaggedPostPhotoSaveCommands.Union(extendedPhotoPool.Where(
                            item => /*item.Width > item.Height &&*/ !photoIdTrackingList.Contains(item.Id) && !tempLandscapePhotosIList.Contains(item.Id)
                            && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
                            .OrderByDescending(item => item.TotalFacesCount)
                            .ThenByDescending(item => item.CreatedTime)
                            .ThenByDescending(item => item.NameTags.Length)
                            .Select(item => SaveImageCommand.Mapper(item, 408, 283))
                            .Take(6)).ToList();

                    if (playerTaggedPostPhotoSaveCommands.Count < 3)
                    {
                        tempLandscapePhotosIList.AddRange(playerTaggedPostPhotoSaveCommands.Select(item => item.Id).ToList());

                        playerTaggedPostPhotoSaveCommands =
                            playerTaggedPostPhotoSaveCommands.Union(extendedPhotoPool.Where(
                                item => !photoIdTrackingList.Contains(item.Id) && !tempLandscapePhotosIList.Contains(item.Id) /*&& item.AlbumName != ProfilePicturesAlbumName*/)
                                .OrderByDescending(item => item.TotalFacesCount)
                                .ThenByDescending(item => item.CreatedTime)
                                .ThenByDescending(item => item.NameTags.Length)
                                .Select(item => SaveImageCommand.Mapper(item, 408, 283))
                                .Take(6)).ToList();
                    }
                }
            }


            // landscape photos for user tagged posts photos
            //var playerTaggedPostLandscapePhotoSaveCommands =
            //    allPlayerPhotosPool.Where(
            //        item => playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) /*&& item.Width > item.Height*/
            //                && !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //        .OrderByDescending(item => item.TotalFacesCount)
            //        .ThenByDescending(item => item.CreatedTime)
            //        .ThenByDescending(item => item.NameTags.Length)
            //        .Select(item => SaveImageCommand.Mapper(item, 408, 283))
            //        .Take(4)
            //        .ToList();

            //var playerTaggedPostPortraitPhotoSaveCommands =
            //    allPlayerPhotosPool.Where(
            //        item => playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) /*&& item.Width < item.Height*/
            //                && !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //        .OrderByDescending(item => item.TotalFacesCount)
            //        .ThenByDescending(item => item.CreatedTime)
            //        .ThenByDescending(item => item.NameTags.Length)
            //        .Select(item => SaveImageCommand.Mapper(item, 283, 408))
            //        .Take(4)
            //        .ToList();

            //if (playerTaggedPostLandscapePhotoSaveCommands.Count < 2)
            //{
            //    // extend the place list id including user's posts and friends' posts
            //    playerTaggedPostsPlaceIdList = playerTaggedPost.Select(item => item.PlaceId).ToList();
            //    photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //    playerTaggedPostLandscapePhotoSaveCommands =
            //        playerTaggedPostLandscapePhotoSaveCommands.Union(extendedPhotoPool.Where(
            //            item =>
            //                playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) /*&& item.Width > item.Height*/ &&
            //                !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //            .OrderByDescending(item => item.TotalFacesCount)
            //            .ThenByDescending(item => item.CreatedTime)
            //            .ThenByDescending(item => item.NameTags.Length)
            //            .Select(item => SaveImageCommand.Mapper(item, 408, 283))
            //            .Take(4)).ToList();

            //    if (playerTaggedPostLandscapePhotoSaveCommands.Count < 2)
            //    {
            //        photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //        playerTaggedPostLandscapePhotoSaveCommands =
            //            playerTaggedPostLandscapePhotoSaveCommands.Union(extendedPhotoPool.Where(
            //                item => /*item.Width > item.Height &&*/ !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //                .OrderByDescending(item => item.TotalFacesCount)
            //                .ThenByDescending(item => item.CreatedTime)
            //                .ThenByDescending(item => item.NameTags.Length)
            //                .Select(item => SaveImageCommand.Mapper(item, 408, 283))
            //                .Take(4)).ToList();

            //        if (playerTaggedPostLandscapePhotoSaveCommands.Count < 2)
            //        {
            //            photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //            playerTaggedPostLandscapePhotoSaveCommands =
            //                playerTaggedPostLandscapePhotoSaveCommands.Union(extendedPhotoPool.Where(
            //                    item => !photoIdTrackingList.Contains(item.Id) /*&& item.AlbumName != ProfilePicturesAlbumName*/)
            //                    .OrderByDescending(item => item.TotalFacesCount)
            //                    .ThenByDescending(item => item.CreatedTime)
            //                    .ThenByDescending(item => item.NameTags.Length)
            //                    .Select(item => SaveImageCommand.Mapper(item, 408, 283))
            //                    .Take(4)).ToList();
            //        }
            //    }
            //}

            // temp id list of tagged post photos landscape
            //var tempLandscapePhotosIList = playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList();

            // portrait photos
            //if (playerTaggedPostPortraitPhotoSaveCommands.Count < 1)
            //{
            //    playerTaggedPostsPlaceIdList = playerTaggedPost.Select(item => item.PlaceId).ToList();
            //    photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //    playerTaggedPostPortraitPhotoSaveCommands =
            //        playerTaggedPostPortraitPhotoSaveCommands.Union(extendedPhotoPool.Where(
            //            item =>
            //                playerTaggedPostsPlaceIdList.Contains(item.FacebookPlaceId) && /*item.Width < item.Height &&*/
            //                !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //            .OrderByDescending(item => item.TotalFacesCount)
            //            .ThenByDescending(item => item.CreatedTime)
            //            .ThenByDescending(item => item.NameTags.Length)
            //            .Select(item => SaveImageCommand.Mapper(item, 283, 408))
            //            .Take(4)).ToList();

            //    if (playerTaggedPostPortraitPhotoSaveCommands.Count < 1)
            //    {
            //        photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //        playerTaggedPostPortraitPhotoSaveCommands =
            //            playerTaggedPostPortraitPhotoSaveCommands.Union(extendedPhotoPool.Where(
            //                item => /*item.Width < item.Height &&*/ !photoIdTrackingList.Contains(item.Id) && (string.IsNullOrEmpty(item.AlbumName) || item.AlbumName != ProfilePicturesAlbumName))
            //                .OrderByDescending(item => item.TotalFacesCount)
            //                .ThenByDescending(item => item.CreatedTime)
            //                .ThenByDescending(item => item.NameTags.Length)
            //                .Select(item => SaveImageCommand.Mapper(item, 283, 408))
            //                .Take(4)).ToList();

            //        if (playerTaggedPostPortraitPhotoSaveCommands.Count < 1)
            //        {
            //            photoIdTrackingList.AddRange(playerTaggedPostLandscapePhotoSaveCommands.Select(item => item.Id).ToList());

            //            playerTaggedPostPortraitPhotoSaveCommands =
            //                playerTaggedPostPortraitPhotoSaveCommands.Union(extendedPhotoPool.Where(
            //                    item => !photoIdTrackingList.Contains(item.Id) /*&& item.AlbumName != ProfilePicturesAlbumName*/)
            //                    .OrderByDescending(item => item.TotalFacesCount)
            //                    .ThenByDescending(item => item.CreatedTime)
            //                    .ThenByDescending(item => item.NameTags.Length)
            //                    .Select(item => SaveImageCommand.Mapper(item, 283, 408))
            //                    .Take(4)).ToList();
            //        }
            //    }
            //}

            // _022_f22_hinh1_408x283.jpg -> Tagged post image 1
            //var taggedPostImage1 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, playerTaggedPostLandscapePhotoSaveCommands, 0, pathToPlayerGameImages,
            //    "_022_f22_hinh1_408x283.jpg");
            var taggedPostImage1 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool,
                playerTaggedPostPhotoSaveCommands, 0, pathToPlayerGameImages,
                "_022_f22_hinh1_408x283.jpg", 408, 283);
            photoIdTrackingList.Add(taggedPostImage1?.Id);
            //CollectSummaryPhoto(playerSummaryPhotoSaveCommands, playerTaggedPostLandscapePhotoSaveCommands, 0, 300, 300);

            // _023_f22_hinh2_283x408.jpg -> Tagged post image 2
            //var taggedPostImage2 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, playerTaggedPostPortraitPhotoSaveCommands, 0,
            //    pathToPlayerGameImages,
            //    "_023_f22_hinh2_283x408.jpg", 283, 408);
            var taggedPostImage2 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, playerTaggedPostPhotoSaveCommands, 1,
                pathToPlayerGameImages,
                "_023_f22_hinh2_283x408.jpg", 283, 408);
            photoIdTrackingList.Add(taggedPostImage2?.Id);

            // _024_f22_hinh3_408x283.jpg -> Tagged post image 3
            //var taggedPostImage3 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, playerTaggedPostLandscapePhotoSaveCommands, 1, pathToPlayerGameImages,
            //    "_024_f22_hinh3_408x283.jpg");
            var taggedPostImage3 = GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool,
                playerTaggedPostPhotoSaveCommands, 2, pathToPlayerGameImages,
                "_024_f22_hinh3_408x283.jpg", 408, 283);
            photoIdTrackingList.Add(taggedPostImage3?.Id);

            // _025_f27_USER_288x288.jpg -> User avatar
            imageGenerator.SaveImageFromUrlAsJpg(new SaveImageCommand
            {
                Width = 288,
                Height = 288,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_025_f27_USER_288x288.jpg"),
                ImageSource = playerEntity.ImageURL
            });

            // _026_f28_NUMBER_218x132.png -> 
            imageGenerator.GeneratePngImageWithText(new ImageGeneratorCommand
            {
                Width = 218,
                Height = 132,
                SavedPath = Path.Combine(pathToPlayerGameImages, "_026_f28_NUMBER_218x132.png"),
                TextColor = Color.FromArgb(225, 23, 73),
                FontFamily = "MyriadPro-Regular",
                TextOnImage = gameResultDto.TotalGameScore.ToString(),
                FontSize = gameResultDto.TotalGameScore < 100 ? 137 : 90,
                Alignment = StringAlignment.Near,
                VerticalAlignment = gameResultDto.TotalGameScore < 100 ? StringAlignment.Near : StringAlignment.Far
            });

            // Collect 5 photos from above and resize - 5

            var playerSummaryPhotoSaveCommands = CollectSummaryPhotosFromPhotoPool(allPlayerPhotosPool, extendedPhotoPool, photoIdTrackingList);

            // _027_f29_hinh1_300x300.jpg ->
            GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerSummaryPhotoSaveCommands, 0,
                pathToPlayerGameImages, "_027_f29_hinh1_300x300.jpg");

            // _028_f29_hinh2_300x300.jpg ->
            GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerSummaryPhotoSaveCommands, 1,
                pathToPlayerGameImages, "_028_f29_hinh2_300x300.jpg");

            // _029_f29_hinh3_300x300.jpg ->
            GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerSummaryPhotoSaveCommands, 2,
                pathToPlayerGameImages, "_029_f29_hinh3_300x300.jpg");

            // _030_f29_hinh4_300x300.jpg ->
            GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerSummaryPhotoSaveCommands, 3,
                pathToPlayerGameImages, "_030_f29_hinh4_300x300.jpg");

            // _031_f29_hinh5_300x300.jpg ->
            GenerateSquareImage(imageGenerator, allPlayerPhotosPool, playerSummaryPhotoSaveCommands, 4,
                pathToPlayerGameImages, "_031_f29_hinh5_300x300.jpg");

        }

        private List<SaveImageCommand> CollectSummaryPhotosFromPhotoPool(List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<PlayerFacebookPhoto> extendedPhotoPool,
            List<string> photoIdTrackingList)
        {
            // Filter level 1
            Func<PlayerFacebookPhoto, bool> summaryImageFilterLevel1 =
                photo => !photoIdTrackingList.Contains(photo.Id) && photo.TotalFacesCount > 2;

            var result = allPlayerPhotosPool.Where(summaryImageFilterLevel1)
                .Select(item => SaveImageCommand.Mapper(item, 300, 300)).Take(10).ToList();

            if (result.Count >= 5) return result;

            photoIdTrackingList.AddRange(result.Select(item => item.Id));

            result = result
                .Union(extendedPhotoPool
                    .Where(summaryImageFilterLevel1)
                    .Select(item => SaveImageCommand.Mapper(item, 300, 300)).Take(10)).ToList();

            // Filter level 2

            Func<PlayerFacebookPhoto, bool> summaryImageFilterLevel2 =
                photo => !photoIdTrackingList.Contains(photo.Id) && photo.TotalProfileTaggedInPhotoCount > 2;

            result = result.Union(extendedPhotoPool.Where(summaryImageFilterLevel2)
                .Select(item => SaveImageCommand.Mapper(item, 300, 300)).Take(10)).ToList();

            if (result.Count >= 5) return result;

            photoIdTrackingList.AddRange(result.Select(item => item.Id));

            Func<PlayerFacebookPhoto, bool> summaryImageFilterL3 = photo => !photoIdTrackingList.Contains(photo.Id)
                && !string.IsNullOrEmpty(photo.NameTags);

            result = result
                .Union(extendedPhotoPool
                    .Where(summaryImageFilterL3)
                    .Select(item => SaveImageCommand.Mapper(item, 300, 300)).Take(10)).ToList();

            if (result.Count >= 5) return result;

            photoIdTrackingList.AddRange(result.Select(item => item.Id));

            Func<PlayerFacebookPhoto, bool> summaryImageFilterL4 = photo => !photoIdTrackingList.Contains(photo.Id);

            result = result
                .Union(extendedPhotoPool
                    .Where(summaryImageFilterL4)
                    .Select(item => SaveImageCommand.Mapper(item, 300, 300)).Take(10)).ToList();

            return result;
        }

        private SaveImageCommand GenerateSquareImage(GameResultImageGenerator imageGenerator, 
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<SaveImageCommand> saveImageCommands, int imgIndex, 
            string parentFolderPath, string fileName, int newWidth = 0 , int newHeight = 0)
        {
            if (saveImageCommands.Count == 0 || imgIndex >= saveImageCommands.Count)
                return null;

            try
            {
                if (imgIndex >= 6 || saveImageCommands.Count <= imgIndex)
                    return saveImageCommands[0];

                var cmd = saveImageCommands[imgIndex];
                if (newWidth > 0 & newHeight > 0)
                {
                    cmd.Width = newWidth;
                    cmd.Height = newHeight;
                }
                cmd.SavedPath = Path.Combine(parentFolderPath, fileName);

                if (saveImageCommands.Count > imgIndex)
                {
                    imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
                    return cmd;
                }

                // use alternative photo
                var totalPhotoPool = allPlayerPhotosPool.Count;
                if (totalPhotoPool - imgIndex < 0) return null;
                var photo = allPlayerPhotosPool[totalPhotoPool - imgIndex];
                cmd.ImageSource = photo.Source;
                cmd.Id = photo.Id;
                imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
                return cmd;
            }
            catch (Exception ex)
            {
                Log.Error($"GenerateSquareImage error, retry at {imgIndex + 1}", ex);
                return GenerateSquareImage(imageGenerator, allPlayerPhotosPool, saveImageCommands, imgIndex + 1,
                            parentFolderPath, fileName, newWidth, newHeight);
            }
            
        }

        private SaveImageCommand GenerateImageTaggedPostImage(GameResultImageGenerator imageGenerator,
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<SaveImageCommand> saveImageCommands, int imgIndex,
            string parentFolderPath, string fileName, int newWidth = 0, int newHeight = 0)
        {
            if (saveImageCommands == null || saveImageCommands.Count == 0)
                return null;

            try
            {
                if (imgIndex >= 10)
                {
                    return saveImageCommands[0];
                }
                return GenerateImage(imageGenerator, allPlayerPhotosPool, saveImageCommands, imgIndex, parentFolderPath,
                            fileName, newWidth, newHeight);
            }
            catch (Exception ex)
            {
                Log.Error($"GenerateImageTaggedPostImage error - retry at {imgIndex + 1}", ex);
                return GenerateImageTaggedPostImage(imageGenerator, allPlayerPhotosPool, saveImageCommands, imgIndex + 1,
                            parentFolderPath, fileName, newWidth, newHeight);
            }
        }

        private SaveImageCommand GenerateImage(GameResultImageGenerator imageGenerator,
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<SaveImageCommand> saveImageCommands, int imgIndex,
            string parentFolderPath, string fileName, int newWidth = 0, int newHeight = 0)
        {
            if (saveImageCommands.Count <= imgIndex)
                return null;

            var cmd = saveImageCommands[imgIndex];
            if (newWidth > 0 & newHeight > 0)
            {
                cmd.Width = newWidth;
                cmd.Height = newHeight;
            }
            cmd.SavedPath = Path.Combine(parentFolderPath, fileName);

            if (saveImageCommands.Count > imgIndex)
            {
                imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
                return cmd;
            }

            // use alternative photo
            var totalPhotoPool = allPlayerPhotosPool.Count;
            if (totalPhotoPool - imgIndex < 0) return null;
            var photo = allPlayerPhotosPool[totalPhotoPool - imgIndex];
            cmd.ImageSource = photo.Source;
            cmd.Id = photo.Id;
            imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(cmd);
            return cmd;
        }

        private static readonly Func<List<PlayerFacebookPhoto>, string, PlayerFacebookPhoto> PhotoRemoveFilter
            = (allPlayerPhotosPool, photoId) =>
                allPlayerPhotosPool.FirstOrDefault(item => item.Id == photoId);

        private void GeneratePlayerSmileImage(GameResultImageGenerator imageGenerator, 
            List<PlayerFacebookPhoto> allPlayerPhotosPool,
            List<PlayerFacebookPhoto> allPlayerPhotosWithSmile, 
            int imgIndex, string parentFolderPath, string fileName, List<string> photoIdTrackingList)
        {
            try
            {
                if (imgIndex >= 8 || allPlayerPhotosWithSmile.Count <= imgIndex)
                    return;

                if (allPlayerPhotosWithSmile.Count > imgIndex)
                {
                    var photoToAdd = allPlayerPhotosWithSmile[imgIndex];
                    var imageSource = photoToAdd.Source;
                    imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(new SaveImageCommand
                    {
                        Width = 235,
                        Height = 235,
                        SavedPath = Path.Combine(parentFolderPath, fileName),
                        ImageSource = imageSource
                    });
                    photoIdTrackingList.Add(photoToAdd.Id);
                    allPlayerPhotosPool.Remove(PhotoRemoveFilter(allPlayerPhotosPool, photoToAdd.Id));
                    return;
                }

                // fallback to use alternative photo - reverse photo order from the photo pool
                var totalPhotoPool = allPlayerPhotosPool.Count;
                if (totalPhotoPool - imgIndex < 0) return;
                var photo = allPlayerPhotosPool[totalPhotoPool - imgIndex];
                imageGenerator.SaveImageFromUrlWithImageScaleAsJpg(new SaveImageCommand
                {
                    Width = 235,
                    Height = 235,
                    SavedPath = Path.Combine(parentFolderPath, fileName),
                    ImageSource = photo.Source
                });
                photoIdTrackingList.Add(photo.Id);
            }
            catch (Exception ex)
            {
                Log.Error($"GeneratePlayerSmileImage error, retry at {imgIndex + 1}", ex);
                GeneratePlayerSmileImage(imageGenerator, allPlayerPhotosPool, allPlayerPhotosWithSmile, imgIndex + 1,
                            parentFolderPath, fileName, photoIdTrackingList);
            }
        }

        [AutomaticRetry(Attempts = 0)]
        public async Task PrepareForGameResult(FacebookProfileViewModel playerProfile, GameTransactionDto gameTransactionDto)
        {
            var stopWatch = new Stopwatch();
            stopWatch.Start();

            var gameSettingService = new GameSettingService();
            var setting = gameSettingService.GetGameGlobalSettings();
            await DetectionEmotionForPlayerPhotos(playerProfile);

            stopWatch.Stop();
            Log.Debug($"DetectionEmotionForPlayerPhotos runs duration -> {stopWatch.Elapsed.TotalSeconds}");
            stopWatch.Start();

            var gameResult = GenerateGameResult(playerProfile.PlayerId, gameTransactionDto.Id, setting);

            stopWatch.Stop();
            Log.Debug($"GenerateGameResult runs duration -> {stopWatch.Elapsed.TotalSeconds}");
            stopWatch.Start();

            try
            {
                var notificationService = new CosyGameNotificationService();
                await notificationService.SendGameResultNotificationReady(new GameResultNotificationCommand
                {
                    GameId = gameResult.Id,
                    UserFullName = gameResult.PlayerInfo.Fullname,
                    FbProfileId = gameResult.PlayerInfo.Id,
                    Email = gameResult.PlayerInfo.Email,
                    AccessToken = gameResult.AccessToken,
                    WebGameUrl = ConfigurationManager.AppSettings["GameSiteUrl"],
                    EmailTemplatePath = Path.Combine(setting.GameBaseInstallationFolder, @"Content\email_templates")
                });
            }
            catch (Exception ex)
            {
                Log.Error("Error -> send notification game result ready -> ", ex);
            }

            stopWatch.Stop();
            Log.Debug($"SendGameResultNotificationReady runs duration -> {stopWatch.Elapsed.TotalSeconds}");
        }

        public UserGameResultDto GetUserGameResult(int gameTransactionId)
        {
            var result = new UserGameResultDto();

            var gameResultEntity = DbContext.PlayerGameResults.FirstOrDefault(item => item.Id == gameTransactionId);
            if (gameResultEntity == null)
                return result;

            result.Id = gameResultEntity.Id;
            result.PlayerId = gameResultEntity.PlayerId;
            result.ResultDate = gameResultEntity.ResultDate;
            result.ClientIpAddress = gameResultEntity.ClientIpAddress;
            result.ClientBrowser = gameResultEntity.ClientBrowser;
            result.Status = gameResultEntity.Status;

            // Player Info
            var playerEntity = DbContext.Players.FirstOrDefault(p => p.Id == gameResultEntity.PlayerId);
            result.PlayerInfo = FacebookProfileViewModel.Mapper(playerEntity);

            return result;
        }

        [AutomaticRetry(Attempts = 0)]
        public void TestJob()
        {
            Log.Debug($"Job run at {DateTime.Now.ToLongDateString()}");
        }

        public GameResultStatusDto GetUserGameResultStatus(int gameTransactionId)
        {
            var gameResultEntity = DbContext.PlayerGameResults.FirstOrDefault(item => item.Id == gameTransactionId);
            if (gameResultEntity == null)
            {
                var message = $"Result for transaction {gameTransactionId} is not available. Game result transaction is not found.";
                Log.Error(message);
                return GameResultStatusDto.ErrorWithMessage(message);
            }

            // check if job server is running and if this job is in waiting state for too long
            if (DateTime.Now.Subtract(gameResultEntity.ResultDate).TotalMinutes > 4)
            {
                var jobServerService = new HangfireJobService();
                var jobServer = jobServerService.GetJobServers(0, 10);
                if (!jobServer.IsActiveServerAlive)
                {
                    var message = $"Result for transaction {gameTransactionId} is not available. Job server is not running.";
                    Log.Error(message);
                    return GameResultStatusDto.ErrorWithMessage(message);
                }
            }

            return new GameResultStatusDto
            {
                Status = gameResultEntity.Status,
                PercentComplete = gameResultEntity.PercentComplete.GetValueOrDefault(0),
                ErrorMessage = gameResultEntity.ErrorMessage
            };
        }
    }

    internal class PlayerFacebookPhotoComparer : IEqualityComparer<PlayerFacebookPhoto>
    {
        public bool Equals(PlayerFacebookPhoto x, PlayerFacebookPhoto y)
        {
            return x != null & y != null && x.Id.Equals(y.Id);
        }

        public int GetHashCode(PlayerFacebookPhoto obj)
        {
            return obj.Id.GetHashCode();
        }

        public static PlayerFacebookPhotoComparer DefaultComparer => new PlayerFacebookPhotoComparer();
    }
}