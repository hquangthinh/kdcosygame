﻿using System;
using System.Linq;
using System.Threading.Tasks;
using KinhDo.CosyAppAdmin.EntityFramework;
using KinhDo.CosyPhotoGame.EmotionApi.Dto;

namespace KinhDo.CosyPhotoGame.EmotionApi
{
    public class RoundRobinEmotionDetectorManager : BaseEmotionDetector, IEmotionDetector
    {
        private EmotionDetectorCreationCommand GetNextAvailableProvider()
        {
            using (var db = new CosyDb())
            {
                var reqCount =
                    db.CvApiProviders.Where(item => item.ApiProvider == "Face++").Min(item => item.RequestCount);
                var minReqCount = reqCount.GetValueOrDefault(0);
                var provider = db.CvApiProviders.FirstOrDefault(p => p.RequestCount == minReqCount) ??
                               db.CvApiProviders.FirstOrDefault();

                if (provider == null)
                {
                    return new EmotionDetectorCreationCommand
                    {
                        ProviderName = "Microsoft",
                        ApiKey = "eb581e237aa14740ab111196c1fafbe4"
                    };
                }

                DetectorProviderName = provider.ApiProvider;

                return new EmotionDetectorCreationCommand
                {
                    ProviderName = provider.ApiProvider,
                    ApiKey = provider.ApiKey,
                    ApiSecret = provider.ApiSecret,
                };
            }
        }

        private IEmotionDetector GetAuthorizedEmotionDetector(EmotionDetectorCreationCommand command)
        {
            var detector = EmotionDetectorFactory.Current.CreateDetector(command);
            return detector;
        }

        public EmotionResult DetectEmotion(PhotoEmotionDetectionCommand command)
        {
            var nextAvailableProvider = GetNextAvailableProvider();
            // Handle round robin for emotion detector
            var detector = GetAuthorizedEmotionDetector(nextAvailableProvider);
            var emotionResult = detector.DetectEmotion(command);
            UpdateRequestCountForProvider(nextAvailableProvider);
            return emotionResult;
        }

        public async Task<EmotionResult> DetectEmotionAsync(PhotoEmotionDetectionCommand command)
        {
            var apiProviderName = string.Empty;
            var emotionResult = new EmotionResult();
            try
            {
                var nextAvailableProvider = GetNextAvailableProvider();
                apiProviderName = nextAvailableProvider.ProviderName;
                var detector = GetAuthorizedEmotionDetector(nextAvailableProvider);
                Log.Debug($"Start DetectEmotionAsync with Provider -> {DetectorProviderName}");

                emotionResult = await detector.DetectEmotionAsync(command);

                Log.Debug($"Emotion result FaceCount,Happiness -> {emotionResult.TotalFacesCount},{emotionResult.Happiness}");
                UpdateRequestCountForProvider(nextAvailableProvider);
            }
            catch (Exception ex)
            {
                Log.Error($"Error -> DetectEmotionAsync -> {apiProviderName}", ex);
                Log.Info("Use fall back detection with emgucv");
                if (apiProviderName.Equals("EmguCv"))
                {
                    return emotionResult;
                }
                emotionResult = EmotionDetectorFactory.Current.CreateEmguCvDetector().DetectEmotion(command);
            }
            return emotionResult;
        }

        private void UpdateRequestCountForProvider(EmotionDetectorCreationCommand command)
        {
            using (var db = new CosyDb())
            {
                var provider = db.CvApiProviders.FirstOrDefault(p => p.ApiProvider == command.ProviderName
                                                                     && p.ApiKey == command.ApiKey &&
                                                                     p.ApiSecret == command.ApiSecret);
                if(provider == null) return;

                provider.RequestCount++;
                db.SaveChanges();
            }
        }
    }
}