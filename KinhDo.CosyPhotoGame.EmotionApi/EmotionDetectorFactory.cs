﻿using KinhDo.CosyPhotoGame.EmotionApi.EmguCv;
using KinhDo.CosyPhotoGame.EmotionApi.FacePlusPlus;
using KinhDo.CosyPhotoGame.EmotionApi.Google;
using KinhDo.CosyPhotoGame.EmotionApi.Microsoft;

namespace KinhDo.CosyPhotoGame.EmotionApi
{
    public class EmotionDetectorFactory
    {
        public static EmotionDetectorFactory Current = new EmotionDetectorFactory();

        public IEmotionDetector CreateDetector(EmotionDetectorCreationCommand command)
        {
            if ("Microsoft".Equals(command.ProviderName))
                return new MsCognitiveEmotionService(command.ApiKey);

            if ("Google".Equals(command.ProviderName))
                return new GoogleVisionService();

            if ("Face++".Equals(command.ProviderName))
                return new FacePlusEmotionService(command.ApiKey, command.ApiSecret);

            if ("EmguCv".Equals(command.ProviderName))
                return new EmguCvEmotionService();

            return new MsCognitiveEmotionService(command.ApiKey);
        }

        public IEmotionDetector CreateEmguCvDetector()
        {
            return new EmguCvEmotionService();
        }
    }
}
