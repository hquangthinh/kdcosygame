﻿namespace KinhDo.CosyPhotoGame.EmotionApi
{
    public abstract class BaseEmotionService : BaseEmotionDetector
    {
        protected string _apiKey;
        protected string _apiSecret;

        protected BaseEmotionService(string apiKey, string apiSecret)
        {
            _apiKey = apiKey;
            _apiSecret = apiSecret;
        }

        protected BaseEmotionService(string apiKey) : this(apiKey, string.Empty)
        {
        }
    }
}
