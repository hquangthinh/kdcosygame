﻿<?xml version="1.0" encoding="utf-8"?>
<Project ToolsVersion="3.5" DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003">
    <PropertyGroup>
        <CosyGameVersion>1.01.1.0</CosyGameVersion>
        <WixPath>C:\Program Files (x86)\WiX Toolset v3.10\bin\</WixPath>
        <DBScriptSource>..\KinhDo.CosyAppAdmin.EntityFramework\Scripts\</DBScriptSource>
        <WebGameSource>..\KinhDo.CosyPhotoGame.Web\</WebGameSource>
        <WebGameEmotionApiSource>..\KinhDo.CosyPhotoGame.EmotionApi\</WebGameEmotionApiSource>
        <WebAdminSource>..\KinhDo.CosyAppAdmin.Web\</WebAdminSource>
        <JobServerServiceSource>..\KinhDo.CosyPhotoGame.JobServer\</JobServerServiceSource>
        <SetupF>..\Setup\</SetupF>
        <PublishF>publish\</PublishF>
        <Publish>$(SetupF)$(PublishF)</Publish>
        <PublishAdmin>$(SetupF)admin\</PublishAdmin>
        <PublishDbScripts>$(PublishAdmin)DbScripts\</PublishDbScripts>
        <PublishJobServerService>$(SetupF)jobserverservice\</PublishJobServerService>
        <WebGameContentCode>WebGameContent.wxs</WebGameContentCode>
        <WebGameContentObject>WebGameContent.wixobj</WebGameContentObject>
        <WebAdminCode>WebAdmin.wxs</WebAdminCode>
        <WebAdminObject>WebAdmin.wixobj</WebAdminObject>
        <JobServerServiceCode>JobServerService.wxs</JobServerServiceCode>
        <JobServerServiceObject>JobServerService.wixobj</JobServerServiceObject>
        <MsiLanguage>Languages\en-us.wxl</MsiLanguage>
        <MsiOut>bin\Release\CosyGameSetup.msi</MsiOut>
    </PropertyGroup>

    <!-- Defining group of temporary files which is the content of the web site. -->
    <ItemGroup>
        <WebGameContent Include="$(WebGameContentCode)" />
        <DBScriptFiles Include="$(DbScriptSource)**\*.sql" />
        <WebAdmin Include="$(WebAdminCode)" />
        <JobServerService Include="$(JobServerServiceCode)" />
        <JobServerServiceFiles Include="$(JobServerServiceSource)bin\Release\*.*" />
        <JobServerServiceEmguCvReferenceFiles Include="$(WebGameEmotionApiSource)lib\**\*.*" />
        <!-- css, images, fonts, ... -->
        <WebGameContentFiles Include="$(WebGameSource)Content\**\*.*" />
        <WebAdminContentFiles Include="$(WebAdminSource)Content\**\*.*" />
        <!-- view files cshtml, html ... -->
        <WebGameViewFiles Include="$(WebGameSource)Views\**\*.*" />
        <WebAdminViewFiles Include="$(WebAdminSource)Views\**\*.*" />
    </ItemGroup>

    <!-- The list of WIX input files -->
    <ItemGroup>
      <WixCode Include="Product.wxs" />
      <WixCode Include="$(WebGameContentCode)" />
      <WixCode Include="$(WebAdminCode)" />
      <WixCode Include="$(JobServerServiceCode)" />
      <WixCode Include="UIMain.wxs" />
      <WixCode Include="UIDialogConfiguration.wxs" />
      <WixCode Include="UIDialogConfigurationError.wxs" />
      <WixCode Include="UIDialogDBInstallation.wxs" />
      <WixCode Include="UIDialogDBExecution.wxs" />
      <WixCode Include="UIDialogDBExecutionError.wxs" />
      <WixCode Include="UIDialogWindowsService.wxs" />
      <WixCode Include="Configuration.wxs" />
    </ItemGroup>

    <!-- The list of WIX after candle files -->
    <ItemGroup>
      <WixObject Include="Product.wixobj" />
      <WixObject Include="$(WebGameContentObject)" />
      <WixObject Include="$(WebAdminObject)" />
      <WixObject Include="$(JobServerServiceObject)" />
      <WixObject Include="UIMain.wixobj" />
      <WixObject Include="UIDialogConfiguration.wixobj" />
      <WixObject Include="UIDialogConfigurationError.wixobj" />
      <WixObject Include="UIDialogDBInstallation.wixobj" />
      <WixObject Include="UIDialogDBExecution.wixobj" />
      <WixObject Include="UIDialogDBExecutionError.wixobj" />
      <WixObject Include="UIDialogWindowsService.wixobj" />
      <WixObject Include="Configuration.wixobj" />
    </ItemGroup>

    <Target Name="Build">
        <!-- Compile whole solution in release mode -->
        <MSBuild
            Projects="..\KinhDo.CosyAppAdmin.sln"
            Targets="Clean;ReBuild"
            Properties="Configuration=Release" />
    </Target>

    <Target Name="PublishWebsite">
        <!-- Remove complete publish folder in order to be sure that evrything will be newly compiled -->
        <Message Text="Removing publish directory: $(SetupF)"/>
        <RemoveDir Directories="$(SetupF)" ContinueOnError="false" />
        <Message Text="Start to publish website" Importance="high" />
        <!-- DB Scripts-->
        <Copy SourceFiles="@(DbScriptFiles)" DestinationFolder="$(PublishDbScripts)%(RecursiveDir)" />
        <!-- Web Game Content -->
        <MSBuild
            Projects="$(WebGameSource)KinhDo.CosyPhotoGame.Web.csproj"
            Targets="ResolveReferences;_CopyWebApplication"
            Properties="OutDir=$(Publish)bin\;WebProjectOutputDir=$(Publish);Configuration=Release" />
        <!-- Web Admin -->
        <MSBuild
            Projects="$(WebAdminSource)KinhDo.CosyAppAdmin.Web.csproj"
            Targets="ResolveReferences;_CopyWebApplication"
            Properties="OutDir=$(PublishAdmin)bin\;WebProjectOutputDir=$(PublishAdmin);Configuration=Release" />

        <!-- JobserverService -->
        <MSBuild
            Projects="$(JobServerServiceSource)KinhDo.CosyPhotoGame.JobServer.csproj"
            Targets="ResolveReferences"
            Properties="Configuration=Release" />
        <Message Text="Copy job server service files" Importance="high" />
        <Copy
            SourceFiles="@(JobServerServiceFiles)"
            DestinationFolder="$(PublishJobServerService)%(RecursiveDir)" />
        <Copy
            SourceFiles="@(JobServerServiceEmguCvReferenceFiles)"
            DestinationFolder="$(PublishJobServerService)%(RecursiveDir)" />
      
        <Message Text="Copy web game opencv files" Importance="high" />
        <Copy
            SourceFiles="@(JobServerServiceEmguCvReferenceFiles)"
            DestinationFolder="$(Publish)bin\%(RecursiveDir)" />

        <Message Text="Copy web game content files" Importance="high" />
        <Copy SourceFiles="@(WebGameContentFiles)" DestinationFolder="$(Publish)Content\%(RecursiveDir)" />

        <Message Text="Copy web game view files" Importance="high" />
        <Copy SourceFiles="@(WebGameViewFiles)" DestinationFolder="$(Publish)Views\%(RecursiveDir)" />

        <Message Text="Copy web admin content files" Importance="high" />
        <Copy SourceFiles="@(WebAdminContentFiles)" DestinationFolder="$(PublishAdmin)Content\%(RecursiveDir)" />

        <Message Text="Copy web admin view files" Importance="high" />
        <Copy SourceFiles="@(WebAdminViewFiles)" DestinationFolder="$(PublishAdmin)Views\%(RecursiveDir)" />
      
    </Target>

    <Target Name="Harvest">
        <!-- Harvest all content of published result -->
        <Exec
                Command='"$(WixPath)heat" dir $(Publish) -dr WebGameFolder -ke -srd -cg WebGameComponents -var var.publishDir -gg -out $(WebGameContentCode)'
                ContinueOnError="false"
                WorkingDirectory="." />
        <Exec
            Command='"$(WixPath)heat" dir $(PublishAdmin) -dr WebAdminFolder -ke -srd -cg WebAdminComponents -var var.publishAdminDir -gg -out $(WebAdminCode)'
            ContinueOnError="false"
            WorkingDirectory="." />
        <Exec
            Command='"$(WixPath)heat" dir $(PublishJobServerService) -dr JobServerServiceFolder -ke -srd -cg JobServerServiceComponents -var var.publishJobServerServiceDir -gg -out $(JobServerServiceCode)'
            ContinueOnError="false"
            WorkingDirectory="." />
    </Target>

    <Target Name="WIX">
        <!-- At last create an installer -->
        <Message Text="TEST: @(WixCode)"/>
        <Exec
              Command='"$(WixPath)candle" -ext WixIISExtension -ext WixUtilExtension -ext WixSqlExtension -dCosyGameVersion=$(CosyGameVersion) -dpublishDir=$(Publish) -dpublishAdminDir=$(PublishAdmin) -dpublishJobServerServiceDir=$(PublishJobServerService) -dMyWebResourceDir=. @(WixCode, &apos; &apos;)'
              ContinueOnError="false"
              WorkingDirectory="." />
        <Exec
            Command='"$(WixPath)light" -ext WixIISExtension -ext WixUIExtension -ext WixNetFxExtension -ext WixUtilExtension -ext WixSqlExtension -cultures:en-us -loc $(MsiLanguage) -out $(MsiOut) @(WixObject, &apos; &apos;)'
            ContinueOnError="false"
            WorkingDirectory="." />

        <!-- A message at the end -->
        <Message Text="Install package has been created." />
    </Target>

    <!-- Optional target for deleting temporary files. Usually after build -->
    <Target Name="DeleteTmpFiles">
        <RemoveDir Directories="$(PublishJobServerService)" ContinueOnError="false" />
        <RemoveDir Directories="$(PublishAdmin)" ContinueOnError="false" />
        <RemoveDir Directories="$(Publish)" ContinueOnError="false" />
        <RemoveDir Directories="$(SetupF)" ContinueOnError="false" />
        <Delete Files="@(WixObject);@(WebGameContent);@(WebAdmin);@(JobServerService)" />
    </Target>
</Project>